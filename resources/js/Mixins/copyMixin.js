export default {
    methods: {
        copy(text, showNotification = false) {
            window.copyText(text)
            if (showNotification) {
                alert('Copied!')
            }

        }
    },
    mounted() {
        window.copyText = function (value) {
            var s = document.createElement('input');
            s.value = value;
            document.body.appendChild(s);

            if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
                s.contentEditable = true;
                s.readOnly = false;
                var range = document.createRange();
                range.selectNodeContents(s);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
                s.setSelectionRange(0, 999999);
            } else {
                s.select();
            }
            try {
                document.execCommand('copy');

            } catch (err) {

            }
            s.remove();
        };
    }
}
