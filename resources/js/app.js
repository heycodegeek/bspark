require('./bootstrap')

// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import helpers from "./helpers";
import inputErrorDirective from "./Directives/inputErrorDirective";

const el = document.getElementById('app');

const app = createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: name => import(`./Pages/${name}`).then(module => module.default),
        }),
})
app.mixin({methods: {route}})
app.use(InertiaPlugin)
app.directive('input-error', inputErrorDirective)
app.config.globalProperties.$helpers = helpers
app.mount(el);

InertiaProgress.init({ color: '#4B5563' });
