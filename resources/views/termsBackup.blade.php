@extends('layouts.front')

@section('content')
    <!-- Banner Area Starts -->
    <section class="banner-area">
        <div class="banner-overlay">
            <div class="banner-text text-center">
                <div class="container">
                    <!-- Section Title Starts -->
                    <div class="row text-center">
                        <div class="col-xs-12">
                            <!-- Title Starts -->
                            <h2 class="title-head">TERMS & <span>CONDITIONS</span></h2>
                            <!-- Title Ends -->
                            <hr>
                            <!-- Breadcrumb Starts -->
                            <ul class="breadcrumb">
                                <li><a href="/"> home</a></li>
                                <li>terms & Conditions</li>
                            </ul>
                            <!-- Breadcrumb Ends -->
                        </div>
                    </div>
                    <!-- Section Title Ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area Ends -->
    <!-- Section Terms of Services Starts -->
    <section class="terms-of-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <h4>Risk Notice</h4>
                    <p>SureTraders Metavers Coin is a not backed or value guaranteed by any financial institution; when purchasing SureTraders Metavers Coins
                    the customer assumes all risk the SureTraders Metavers Coins may become worthless in value. Customers should research
                    and consider the risks before purchasing any SureTraders Metavers Coins. The company makes absolutely no guarantee
                    about the future value of the SureTraders Metavers Coins purchased.</p>

                    <h4>Severability</h4>
                    <p>In the event any court shall declare any section or sections of this Agreement invalid or void, such
                    declaration shall not invalidate the entire Agreement and all other paragraphs of the Agreement
                    shall remain in full force and effect.</p>

                    <h4>Customer input errors</h4>
                    <p>It is the sole responsibility of the customer to check the accuracy of information entered and saved
                    on the website. Account details displayed on the order summary webpage will be the final transfer
                    destination. In the case that this information is incorrect, and funds are transferred to an
                    unintended destination, the company shall not reimburse the customer and shall not transfer
                    additional funds. As such customers must ensure the SureTraders Metavers Coin address and bank information they enter
                    is completely correct.</p>

                    <h4> Binding Agreement</h4>
                    <p>The terms and provisions of this Agreement are binding upon Your heirs, successors, assigns, and
                    other representatives. This Agreement may be executed in counterparts, each of which shall be
                    considered to be an original, but both of which constitute the same Agreement.</p>

                    <h4> Expired orders</h4>
                    <p>If the company receives payment for an order that has already expired, the company reserves the
                    right to recalculate the SureTraders Metavers Coin to Thai Baht exchange rate at the time of processing the transfer
                    to the customer. This may result in the customer receiving less SureTraders Metavers Coins or Thai Baht than the
                    original ordered amount.</p>

                    <h4> Choice of Law</h4>
                    <p>This Agreement, and its application and interpretation, shall be governed exclusively by the laws of
                    the State of Georgia, without regard to its conflict of law rules. You consent to the exclusive
                    jurisdiction of the federal and state courts located in or near Reykjavik, Iceland for any dispute
                    arising under this Agreement.</p>

                    <h4> Security</h4>
                    <p>We have implemented security measures designed to secure your information from accidental loss and
                    from unauthorized access, use, alteration or disclosure. However, we cannot guarantee that
                    unauthorized persons will never gain access to your information, and you acknowledge that you
                        provide your information at your own risk, except as otherwise provided by applicable law.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Terms of Services Ends -->
@endsection
