<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Withdrawal History</title>
    <style>
        table, th, td {
            text-align: center;
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px;
        }

        tr:nth-child(even) {
            background-color: rgba(150, 212, 212, 0.4);
        }

        th:nth-child(even),td:nth-child(even) {
            background-color: rgba(150, 212, 212, 0.4);
        }
    </style>
</head>

<body>
<div class="container">
    <h2>Withdrawal Histories</h2>
{{--    <div class="table-responsive-lg table-responsive-md table-responsive-sm ">--}}
        <table class="table" style="width: 600px">
            <thead>
            <tr>
                <th style="width: 5%">Txn Raw Id</th>
                <th style="width: 5%">User</th>
                <th style="width: 10%">Txn Details</th>
                <th style="width: 5%">Fee</th>
                <th style="width: 5%">Amount</th>
                <th style="width: 5%">Status</th>
                <th style="width: 5%">Transaction Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $dt)
            <tr>

                <td>
                    {{$dt->id }}
                </td>
                <td>
                    <b>{!!  nl2br($dt->user->ref_code)  !!}</b>
                    <br>
                    <b>{{$dt->user->name }}</b>
                    <br>
                    <small>{{$dt->user->email }}</small>
                </td>

                <td>
                    {{ \Illuminate\Support\Str::limit($dt->txn_id, 10, $end='...')  }}
                    <br>
                    <b>{{$dt->withdraw_coin_id}}</b>
                    <br>
                    <small>
                        {{ \Illuminate\Support\Str::limit($dt->address, 10, $end='...')  }}
                    </small>
                </td>
                <td class="contact-num"><i class="las la-phone mr-2"></i>{{$dt->fees }}</td>
                <td class="text-center">
                    <small>{{$dt->amount }}</small>
                </td>
                <td class="text-center">
                    <small>{{$dt->status }}</small>
                </td>
                <td>
                    {{ $dt->created_at->format('Y-m-d h:i:s') }}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
{{--    </div>--}}
</div>

</body>

</html>
