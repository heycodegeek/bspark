@extends('layouts.front')

@section('content')
    <!-- Banner Area Starts -->
    <section class="banner-area">
        <div class="banner-overlay">
            <div class="banner-text text-center">
                <div class="container">
                    <!-- Section Title Starts -->
                    <div class="row text-center">
                        <div class="col-xs-12">
                            <!-- Title Starts -->
                            <h2 class="title-head">our <span>services</span></h2>
                            <!-- Title Ends -->
                            <hr>
                            <!-- Breadcrumb Starts -->
                            <ul class="breadcrumb">
                                <li><a href="/"> home</a></li>
                                <li>services</li>
                            </ul>
                            <!-- Breadcrumb Ends -->
                        </div>
                    </div>
                    <!-- Section Title Ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area Ends -->
    <!-- Section Services Starts -->
    <section class="services">
        <div class="container">
            <div class="row">
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/download-bitcoin.png" alt="download bitcoin">
                        <div class="service-box-content">
                            <h3>Bot Trading</h3>
                            <p>Cryptocurrency trading bots are automated trading systems that make crypto-trading easier
                                by simplifying the process of investing.<br> Traders can leverage the pre-defined set of
                                rules into a robot to execute the trade in the most efficient way possible.</p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/add-bitcoins.png" alt="add bitcoins">
                        <div class="service-box-content">
                            <h3>Staking</h3>
                            <p>Staking is a way to put your crypto to work and earn rewards on it.Staking
                                cryptocurrencies is a process that involves committing your crypto assets to support a
                                blockchain network and confirm transactions. It's available with cryptocurrencies that
                                use the proof-of-stake model to process payments</p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/buy-sell-bitcoins.png" alt="buy and sell bitcoins">
                        <div class="service-box-content">
                            <h3>Subscription</h3>
                            <p>Subscription refers to the process of investors signing up and committing to invest in a
                                financial instrument, before the actual closing of the purchase. Here we provide a
                                product Staking: Staking Pool minimum deposit is $10 to unlimited. Staking Pool Holder
                                get an assured bonus of 0.3% per day till the initial deposited fund value reaches
                                250%.</p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/strong-security.png" alt="strong security"/>
                        <div class="service-box-content">
                            <h3>Consultancy</h3>
                            <p><br>A Crypto consultant researches, analyzes, develops, and tests blockchain
                                technologies.<br><br> The goal of crypto consulting is to translate business goals into
                                technology roadmaps that clients can follow to achieve target outcomes.<br><br><br><br>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/world-coverage.png" alt="world coverage"/>
                        <div class="service-box-content">
                            <h3>Crypto Exchange</h3>
                            <p>Cryptocurrency exchanges are platforms that facilitate the trading of cryptocurrencies
                                for other assets, including digital and fiat currencies. In effect, cryptocurrency
                                exchanges act as an intermediary between a buyer and a seller and make money through
                                commissions and transaction fees.</p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->
                <!-- Service Box Starts -->
                <div class="col-md-6 service-box">
                    <div>
                        <img src="front_assets/images/icons/orange/payment-options.png" alt="payment options"/>
                        <div class="service-box-content">
                            <h3>Crypto Investment</h3>
                            <p><br>Customers around the world discover and begin their journeys with crypto
                                investment.<br> <br>Out of all, we are the one trading for our Subscribers and making
                                profits through crypto investments.</p>
                        </div>
                    </div>
                </div>
                <!-- Service Box Ends -->

            </div>
        </div>
    </section>
    <!-- Section Services Ends -->
    <!-- Call To Action Section Starts -->
    <section class="call-action-all">
        <div class="call-action-all-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Call To Action Text Starts -->
                        <div class="action-text">
                            <h2>Get Started Today With Crypto</h2>
                            <p class="lead">Open account for free and start trading Crypto!</p>
                        </div>
                        <!-- Call To Action Text Ends -->
                        <!-- Call To Action Button Starts -->
                        <p class="action-btn"><a class="btn btn-primary" href="/register">Register Now</a></p>
                        <!-- Call To Action Button Ends -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Section Ends -->
    <!-- Footer Starts -->
@endsection
