<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>SureTraders Free Airdrop</title>
    <meta name="description" content="Eventup - Responsive Marketing Landing Pages">
    <meta name="author" content="BigThemez">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- favicon -->
    <link rel="shortcut icon" href="/landing/images/favicon.png'" type="image/x-icon">
    <link rel="icon" href="/landing/images/favicon.png" type="image/x-icon">
    <!--Bootstrap-->
    <link rel="stylesheet" href="/landing/css/bootstrap.css">
    <!--owl carousel-->
    <link rel="stylesheet" href="/landing/css/owl.carousel.css">
    <!--animation-->
    <link rel="stylesheet" href="/landing/css/animate.css">
    <!--headline-->
    <link rel="stylesheet" href="/landing/css/headline.css">
    <!--Sweet Alert-->
    <link rel="stylesheet" href="/landing/css/sweetalert.css">
    <!--custom-->
    <link rel="stylesheet" href="/landing/css/style.css">
    <!--Icons-->
    <link rel="stylesheet" href="/landing/css/icon_fonts.css">
    <!-- Google font -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CRaleway:300,400,500,600,700,800"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
          rel="stylesheet">
    <style>


    </style>

</head>
<body>
<!--Pre-loader-->
<div id="loader-wrapper">
    <div class="circle">
        <h6>Loading...</h6>
    </div>
</div>
<!--Pre-loader ends!-->

<!--Header area section-->
<header class="header-area">
    <div class="container-fluid">
        <!-- Nav -->
        <nav class="navbar navbar-expand-lg">
            <!-- Logo -->
            <div id="logo_home">
                <a href="{{route('home')}}">SureTraders</a>
            </div>
            <!-- Logo ends! -->
            <!--menu toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu"
                    aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon">
               <span class="ti-menu"></span>
               </span>
            </button>
            <!--menu toggler ends! -->
            <!--menu -->
            <div class="collapse navbar-collapse" id="menu">
                <ul id="menu_scroll" class="navbar-nav menu">
                    <li><a href="#home" class="scroll active">Home</a></li>
                    <li><a href="#about" class="scroll">About Us</a></li>
                    <li><a href="#speakers" class="scroll">Business Plan</a></li>
                </ul>
            </div>
            <!--menu ends!-->

            <ul class="social-icons">
                <li><a href="#0" target="_blank" title="Facebook"> <i class="icon-facebook"></i> </a></li>
                <li><a href="#0" target="_blank" title="linkedin"> <i class="icon-linkedin-1"></i> </a></li>
                <li><a href="#0" target="_blank" title="Twitter"> <i class="icon-twitter"></i> </a></li>
            </ul>
        </nav>
        <!-- Nav ends!-->


    </div>
</header>
<!--Header area section ends!-->


<!--Home section-->
<div class="single-section home-area" id="home">
    <div class="bg"></div>
    <div class="bg bg2"></div>
    <div class="bg bg3"></div>
    <!--home text-->
    <div class="container h-100">
        <!--        <div class="row h-100 align-items-center">-->
        </div>
</div>
</div>
<!--home text ends!-->
</div>
<!--Home section ends!-->

<!--Countdown-->
<div class="container">
{{--    <div class="row">--}}
{{--        <div class="col-12">--}}
{{--            <div class="home-bottom-area">--}}


{{--                <div class="row">--}}
{{--                    <div class="venue col-lg-5 col-sm-12">--}}
{{--                        <div class="date"><span class="icon icon-calendar"></span>30 June 2021</div>--}}
{{--                        <div class="location"><span class="icon icon-alert"></span> SureTraders Token Airdrop</div>--}}
{{--                    </div>--}}


{{--                    <div class="col-lg-7 col-sm-12">--}}
{{--                        <div id="demo" class="row">--}}
{{--                            <div class="counter col-md-3"><span id="days">00</span><span> Days</span></div>--}}
{{--                            <div class="counter col-md-3"><span id="hours">00</span><span>Hours</span></div>--}}
{{--                            <div class="counter col-md-3"><span id="minutes">00</span><span>Minutes</span></div>--}}
{{--                            <div class="counter col-md-3"><span id="seconds">00</span><span>Seconds</span></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
</div>

<!--Countdown ends! -->

<!--About section-->
<div class="single-section about-area silver-bg" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-12">
                <img src="/landing/images/about.png" style="width:100%" class="img-item" alt="SureTraders">
            </div>
            <div class="col-lg-7 col-sm-12">
                <!--section heading-->
                <div class="section-heading">
                    <h2 class="section-title">Welcome to SureTraders</h2>

                    <p class="section-description">We build next-generation data infrastructure for blockchain
                        application.</p>

                    <p>SureTraders Limited.is poised to become the go-to decentralized finance (DeFi) solution for auto-mated
                        smart-escrows, smart-swaps, SureTraders token. SureTraders Limited Provide a unique technology to
                        its users which will allow people and generations to easily execute smart blockchain
                        based protocol at a fraction of the cost of similar services provided by law
                        firms and banks. SureTraders Limited Launched the platform called SureTraders
                        which will help to grow online business & future of Decentralized
                        finance.</p>
                    <p>The Dukupayecosystem is powered by its own native token,DUKU, which is built on its own
                        blockchain. The token will be used for discounts on services, dividends from
                        staking, and as a governance token allowing the community to vote on how
                        foundation funds are spent. Staking the tokens at SureTraders platform.</p>

                    <a href="{{route('register',[$ref_code,'pos'=>request()->get('pos')])}}" class="about-more">Register Now <i
                            class="icon-right-open"></i></a>
                </div>
                <!--section heading ends!-->
            </div>

        </div>


        <!--Statistic-->
    {{--        <div class="row statistic-area">--}}
    {{--            <div class="col-md-12">--}}
    {{--                <div class="row">--}}
    {{--                    <!-- participants -->--}}
    {{--                    <div class="statistic-item col-lg-3 col-sm-6">--}}
    {{--                        <h3><span class="statistic-number">20</span>k</h3>--}}

    {{--                        <div class="statistic-title">Worldwide<span> Participants</span></div>--}}
    {{--                    </div>--}}
    {{--                    <!-- participants ends! -->--}}
    {{--                    <!-- topics -->--}}
    {{--                    <div class="statistic-item col-lg-3 col-sm-6">--}}
    {{--                        <h3><span class="statistic-number">50</span>k</h3>--}}

    {{--                        <div class="statistic-title">Worldwide <span>Active Associate</span></div>--}}
    {{--                    </div>--}}
    {{--                    <!--topics ends!-->--}}
    {{--                    <!--sessions-->--}}
    {{--                    <div class="statistic-item col-lg-3 col-sm-6">--}}
    {{--                        <h3><span class="statistic-number">550</span>K</h3>--}}

    {{--                        <div class="statistic-title">Worldwide <span> Users</span></div>--}}
    {{--                    </div>--}}
    {{--                    <!--sessions ends!-->--}}
    {{--                    <!--speakers-->--}}
    {{--                    <div class="statistic-item col-lg-3 col-sm-6">--}}
    {{--                        <h3><span class="statistic-number">24</span>Hr</h3>--}}

    {{--                        <div class="statistic-title">Worldwide <span> Support</span></div>--}}
    {{--                    </div>--}}
    {{--                    <!--speakers ends!-->--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    <!--Statistic ends!-->


    </div>
</div>
<!--About section ends!-->


<!--Speaker section-->
<div class="single-section speaker-area" id="speakers">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--section heading-->
                <div class="section-heading">
                    <h2 class="section-title">Do You Want To Know More About Us?</h2>

                    <p class="section-description col-lg-8 col-md-10 col-sm-12">SureTraders Limitd has made happy clients in
                        last 4 years all over the world and we
                        are proud to share some of our business assocaites testimonials here.
                    </p>

                </div>
                <!--section heading ends!-->
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <!--Speaker item-->
            <div class="speaker-item col-lg-4 col-md-6 col-sm-12">
                <div class="img-item">
                    <iframe width="100%" src="https://www.youtube.com/embed/Lw8H7DgZajs">
                    </iframe>
                </div>
                <div class="speaker-detail">
                    <div class="speaker-caption">
                        <h4 class="name">Sandra</h4>
                        <h5 class="job-title">SureTraders Member</h5>
                    </div>
                </div>
            </div>
            <!--Speaker item ends!-->
            <!--Speaker item-->
            <div class="speaker-item col-lg-4 col-md-6 col-sm-12">
                <div class="img-item">
                    <iframe width="100%" src="https://www.youtube.com/embed/VWRWp72RjVI">
                    </iframe>
                </div>
                <div class="speaker-detail">
                    <div class="speaker-caption">
                        <h4 class="name">Lenka</h4>
                        <h5 class="job-title">SureTraders Member</h5>
                    </div>
                </div>
            </div>
            <!--Speaker item ends!-->
            <!--Speaker item-->
            <div class="speaker-item col-lg-4 col-md-6 col-sm-12">
                <div class="img-item">

                    <!--                    <img src="/landing/images/speaker-3.jpg" alt=""/>-->
                    <iframe width="100%" src="https://www.youtube.com/embed/EWGMtCylFUg">
                    </iframe>
                </div>
                <div class="speaker-detail">
                    <div class="speaker-caption">
                        <h4 class="name">Daniel</h4>
                        <h5 class="job-title">SureTraders Member</h5>
                    </div>
                </div>
            </div>
            <!--Speaker item ends!-->

        </div>
    </div>


</div>

<div class="mobile-bottom">

    <a class="mobile-button btn" href="{{route('register',[$ref_code,'pos'=>request()->get('pos')])}}">Register Here</a>
    <a class="mobile-button btn" href="{{route('login')}}">Login Here</a>

</div>

<!--Footer section-->
<footer class="footer-area">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12">

            </div>

            <div class="col-md-6 col-lg-6 col-sm-12">
                <div class="subscribe-form-area">

                    <img src="/landing/images/curve-left.svg" class="curve-left" alt="curve-left">

                    <form action="#" method="post">
                        <input type="email" name="email" value="Your Email Address"
                               onfocus="if(this.value==this.defaultValue)this.value='';"
                               onblur="if(this.value=='')this.value=this.defaultValue;">
                        <button type="submit">notify</button>
                    </form>
                    <img src="/landing/images/curve-right.svg" class="curve-right" alt="curve-right"></div>
            </div>
        </div>

        <div class="row">
            <!-- <div class="col-lg-4 col-md-6 col-sm-12 text-sm-left text-center text-md-left">
                <img src="/landing/images/logo-white.png" alt="Footer logo" class="foot-logo-item">

                <p>SureTraders Limited Launched the platform called DukuPay which will help to grow online business & future of Decentralized finance. Now We are launching SureTraders Token</p>
                <ul class="footer-social">
                    <li><a href="#0" target="_blank" title="Facebook"> <i class="icon-facebook"></i> </a></li>
                    <li><a href="#0" target="_blank" title="Twitter"> <i class="icon-twitter"></i> </a></li>
                    <li><a href="#0" target="_blank" title="Linkedin"> <i class="icon-linkedin-1"></i> </a></li>
                    <li><a href="#0" target="_blank" title="Youtube"> <i class="icon-youtube-play"></i> </a></li>
                    <li><a href="#0" target="_blank" title="Instagram"> <i class="icon-instagram-3"></i></a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 text-sm-left text-center text-md-left">
                <h3>Contact Info</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <ul>
                    <li><a href="mailto:hello@example.com" target="_blank"><span class="icon-email"></span>
                        hello@example.com</a></li>
                    <li><a href="#" target="_blank"> <span class="icon-call"></span> 1-800-222-4545 </a></li>
                    <li><a href="#" target="_blank"> <span class="icon-location-2"></span> Location </a></li>
                </ul>
            </div>



         <div class="col-lg-5 col-md-4 col-sm-12 text-sm-left text-center text-md-left instagram">
                    <h3>Instagram</h3>
                    <ul>
                    <li> <a href="#"> <img src="/landing/images/instagram-1.jpg" alt="image"> </a> </li>
                     <li> <a href="#"> <img src="/landing/images/instagram-2.jpg" alt="image"> </a> </li>
                      <li> <a href="#"> <img src="/landing/images/instagram-3.jpg" alt="image"> </a> </li>
                       <li> <a href="#"> <img src="/landing/images/instagram-4.jpg" alt="image"> </a> </li>
                        <li> <a href="#"> <img src="/landing/images/instagram-5.jpg" alt="image"> </a> </li>
                        <li> <a href="#"> <img src="/landing/images/instagram-6.jpg" alt="image"> </a> </li>
                        </ul>
                    </div>



                </div>


                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li><a href="#0">F.A.Q.</a></li>
                                <li><a href="#0">Terms of Use</a></li>
                                <li><a href="#0">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            Copyright © Eventup
                        </div>
                    </div>
                </div>

            </div> -->

</footer>
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("Jun 30, 2021 23:59:59").getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("days").innerHTML = days;
        document.getElementById("hours").innerHTML = hours;
        document.getElementById("minutes").innerHTML = minutes;
        document.getElementById("seconds").innerHTML = seconds;

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/landing/js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap JS -->
<script src="/landing/js/bootstrap.js"></script>
<!-- Carousel JS -->
<script src="/landing/js/owl.carousel.js"></script>
<!-- Headline JS -->
<script src="/landing/js/headline.js"></script>
<!-- smoothscroll -->
<script src="/landing/js/smoothscroll.js"></script>
<!-- Sweet Alert -->
<script src="/landing/js/sweetalert.js"></script>
<!-- Statistic -->
<script src="/landing/js/jquery.countimator.js"></script>
<!-- Ajax form -->
<script src="/landing/js/jquery.validate.js"></script>
<script src="/landing/js/jquery.form.js"></script>
<!-- countdown -->
<script src="/landing/js/jquery.countdown.js"></script>
<!-- core custom js -->
<script src="/landing/js/custom.js"></script>
</body>
</html>
