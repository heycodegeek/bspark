<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>SureTraders The Innovative Future Blockchain Company.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="SureTraders holding a blockchain technology basket & representing the top DeFi assets in a fully decentralized space.">
    <meta name="keywords" content="">
    <meta name="author" content="SureTraders">


    <!--[if lt IE 9]>
    <script src="/front/js/html5shiv.js"></script>
    <![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="/front/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="/front/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="/front/css/animate.css" type="text/css">
    <link rel="stylesheet" href="/front/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="/front/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="/front/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="/front/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="/front/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="/front/css/jquery.countdown.css" type="text/css">
    <link rel="stylesheet" href="/front/css/style.css" type="text/css">
    <link rel="stylesheet" href="/front/css/twentytwenty.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="/front/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="/front/css/colors/magenta.css" type="text/css" id="colors">
    <link rel="stylesheet" href="/front/css/color.css" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="/front/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="/front/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="/front/fonts/et-line-font/style.css" type="text/css">

    <!-- custom font -->
    <link rel="stylesheet" href="/front/css/font-style.css" type="text/css">
</head>
<body id="homepage">

<div id="wrapper">

    <!-- header begin -->
    <header class="transparent">
        <div class="info">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- social icons -->
                        <div class="column social">
                            {{--                            <a href="#"><i class="fa fa-facebook"></i></a>--}}
                            {{--                            <a href="#"><i class="fa fa-twitter"></i></a>--}}
                            {{--                            <a href="#"><i class="fa fa-rss"></i></a>--}}
                            {{--                            <a href="#"><i class="fa fa-google-plus"></i></a>--}}
                            {{--                            <a href="#"><i class="fa fa-envelope-o"></i></a>--}}
                        </div>
                        <!-- social icons close -->
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- logo begin -->
                    <div id="logo">
                        <a href="{{route('home')}}">
                            <img class="logo" src="/front/images-event/logo.png" alt="SureTraders">
                        </a>
                    </div>
                    <!-- logo close -->

                    <!-- small button begin -->
                    <span id="menu-btn"></span>
                    <!-- small button close -->

                    <div class="header-extra">
                        <div class="v-center">
                                    <span id="b-menu">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </span>
                        </div>
                    </div>

                    <!-- mainmenu begin -->
                    <ul id="mainmenu" class="ms-2">
                        <li><a href="#section-hero">Home<span></span></a></li>
                        <li><a href="#section-about">SureTraders<span></span></a></li>
                        <li><a href="#section-speakers">Our Team<span></span></a></li>
                        <li><a href="#section-schedule">Roadmap<span></span></a></li>
                        <li><a href="{{route('login')}}">Login<span></span></a></li>
                        <li><a href="{{route('register')}}">Sign Up<span></span></a></li>
                    </ul>
                    <!-- mainmenu close -->


                </div>


            </div>
        </div>
    </header>
    <!-- header close -->


    <!-- content begin -->
    <div id="content" class="no-bottom no-top">

        <!-- parallax section -->
        <section id="section-hero" class="full-height text-light"
                 data-bgimage="url(/front/images-event/bg/7.jpg) fixed top center" data-stellar-background-ratio=".2">
            <div class="center-y text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="spacer-single"></div>
                            <h1 class="title-3">
                                The Innovative Future.<br> Platform <span class="id-color">2021</span>
                            </h1>
                        </div>

                        <div class="col-md-6 offset-md-3">
                            <div class="spacer-single"></div>
                            <div class="d-lg-flex justify-content-center">
                                <div>The development ideology of SureTraders implies the creation of its own unified
                                    ecosystem based on innovative technologies.
                                </div>
                            </div>
                            <div class="spacer-single"></div>

                            <a href="#section-ticket" class="btn-custom text-white scroll-to">Join Now</a>
                            <div class="spacer-double"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->

        <section id="section-about" data-bgimage="url(/front/images-event/bg/1.png) fixed no-repeat">
            <div class="wm wm-border dark wow fadeInDown">About Us</div>
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-lg-6 wow fadeInLeft" data-wow-delay="0s">
                        <h2>Welcome to SureTraders Inovative <br>Decentralised Space World.</h2>
                        <p>
                            SureTraders is poised to become the go-to decentralized finance (DeFi) solution for auto-mated
                            smart-escrows, smart-swaps, smart economy token. SureTraders unique technology will allow people
                            and or generations to easily execute smart blockchain based protocol at a fraction of the
                            cost of similar services provided by law firms and banks.

                            With a focus on building three primary functions as part of its phase one deployment.
                        </p>

                        <div class="spacer10"></div>

                        <a href="#section-speakers" class="btn-custom font-weight-bold text-white sm-mb-30 scroll-to">Know
                            More</a>
                    </div>

                    <div class="col-lg-6 mb-sm-30 text-center wow fadeInRight">
                        <div class="de-images">
                            <img class="di-small wow fadeIn" src="/front/images-event/misc/2.jpg" alt=""/>
                            <img class="di-small-2" src="/front/images-event/misc/3.jpg" alt=""/>
                            <img class="img-fluid wow fadeInRight" data-wow-delay=".25s"
                                 src="/front/images-event/misc/1.jpg" alt=""/>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- section begin -->
        <section id="section-features">
            <div class="wm wm-border dark wow fadeInDown ">Features</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1>Why you should Join SureTraders?</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>


                    <div class="col-lg-4 wow fadeIn" data-wow-delay="0s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-microphone text-light"></i>
                            <div class="text">
                                <h3><span>Cross-chain Technology</span></h3>
                                <p>We seeks to solve all these issues, by enabling interoperability between blockchains
                                    thus making it easy for them to communicate with another information.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 wow fadeIn" data-wow-delay=".25s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-lightbulb-o text-light"></i>
                            <div class="text">
                                <h3><span>Automated Rebalance</span></h3>
                                <p>We are maintaining your crypto holdings. which helps to Market fluctuations may cause
                                    some of the securities in your portfolio to appreciate or depreciate in value.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 wow fadeIn" data-wow-delay=".5s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-sitemap text-light"></i>
                            <div class="text">
                                <h3><span>100% Decentralized Governance</span></h3>
                                <p>We represented by rules encoded as a computer program that is transparent, controlled
                                    by the organization members and not influenced by a central government.</p>
                            </div>
                        </div>
                    </div>

                    <div class="spacer-single sm-hide"></div>

                    <div class="col-lg-4 wow fadeIn" data-wow-delay=".75s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-building text-light"></i>
                            <div class="text">
                                <h3><span>Worldwide Community</span></h3>
                                <p>We created the membership organization that builds and supports a global community of
                                    loyal business technology customers.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 wow fadeIn" data-wow-delay="1s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2  fa fa-users text-light"></i>
                            <div class="text">
                                <h3><span>100% Safe & Secure</span></h3>
                                <p>We Keeping people safe & secure around the world. Using real-time intelligence from
                                    over millions of active users.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 wow fadeIn" data-wow-delay="1.25s">
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-mortar-board text-light"></i>
                            <div class="text">
                                <h3><span>100% Legit Company</span></h3>
                                <p>We are a 100% legit and worldwide business presence company dealing with Crypto
                                    trading & Forex trading market..</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-speakers" class="text-light"
                 data-bgimage="url(/front/images-event/bg/1.jpg) fixed top center" data-stellar-background-ratio=".2">
            <div class="wm wm-border dark wow fadeInDown">Our Team</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1>Meet Our Team</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 mb30 wow fadeInUp">
                        <!-- team member -->
                        <div class="de-team-list">
                            <div class="team-pic">
                                <img src="/front/images-event/team/1.png" class="img-responsive" alt=""/>
                            </div>
                            <div class="team-desc">
                                <h3>David Piper</h3>
                                <p class="lead">Director of the Advisory Board</p>
                                <div class="small-border"></div>
                                <p>17 years of experience in strategy, partnerships and innovation in Financial
                                    Services.</p>
                            </div>
                        </div>
                        <!-- team close -->
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 mb30 wow fadeInUp">
                        <!-- team member -->
                        <div class="de-team-list">
                            <div class="team-pic">
                                <img src="/front/images-event/team/2.png" class="img-responsive" alt=""/>
                            </div>
                            <div class="team-desc">
                                <h3>Abdennacer Guesmi</h3>
                                <p class="lead">Head of Corporate Development</p>
                                <div class="small-border"></div>
                                <p>Extensive experience in investment banking, corporate development, investor
                                    relations.</p>
                            </div>
                        </div>
                        <!-- team close -->
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 mb30 wow fadeInUp">
                        <!-- team member -->
                        <div class="de-team-list">
                            <div class="team-pic">
                                <img src="/front/images-event/team/3.png" class="img-responsive" alt=""/>
                            </div>
                            <div class="team-desc">
                                <h3>Lucy Kovalova</h3>
                                <p class="lead">Chief Business Officer</p>
                                <div class="small-border"></div>
                                <p>12 years of experience in international payments, including MD of Alliance Payment
                                    Solutions.</p>
                            </div>
                        </div>
                        <!-- team close -->
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 mb30 wow fadeInUp">
                        <!-- team member -->
                        <div class="de-team-list">
                            <div class="team-pic">
                                <img src="/front/images-event/team/4.png" class="img-responsive" alt=""/>
                            </div>
                            <div class="team-desc">
                                <h3>Harold Enrique Sanchez</h3>
                                <p class="lead">SVP, Risk & Operations</p>
                                <div class="small-border"></div>
                                <p>With more than 10 years of experience in risk management in the online payments
                                    ecosystem.</p>

                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- team close -->
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </section>
        <!-- section close -->


        <!-- section begin -->
        <section id="section-sponsors" data-bgimage="url(/front/images-event/bg/3.png) fixed center no-repeat">
            <div class="wm wm-border dark wow fadeInDown ">sponsors</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1>Official Sponsors</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="col-md-12 text-center wow fadeInUp">
                        <h3>Gold Sponsors</h3>
                        <div class="spacer-single"></div>
                        <img src="/front/images-event/logo/1.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/2b.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/3b.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/4b.png" alt="" class="grey-hover">
                        <div class="spacer-double"></div>

                        <h3>Silver Sponsors</h3>
                        <div class="spacer-single"></div>
                        <img src="/front/images-event/logo/5.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/6.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/7.png" alt="" class="grey-hover">
                        <img src="/front/images-event/logo/8b.png" alt="" class="grey-hover">
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->

    </div>
    <!-- section close -->

    <footer class="style-2">
        <div class="container">
            <div class="row align-items-middle">
                <div class="col-md-3">
                    <img src="/front/images-event/logo.png" class="logo-small" alt="SureTraders"><br>
                </div>

                <div class="col-md-6">
                    &copy; Copyright 2021 - Duku Limited. - www.SureTraders.com
                </div>

                <div class="col-md-3 text-right">
                    <div class="social-icons">
                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                        <a href="#"><i class="fa fa-rss fa-lg"></i></a>
                        <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                        <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                        <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <a href="#" id="back-to-top" class="custom-1"></a>
    </footer>
</div>


<!-- Javascript Files ================================================== -->
<script src="/front/js/jquery.min.js"></script>
<script src="/front/js/jpreLoader.js"></script>
<script src="/front/js/bootstrap.min.js"></script>
<script src="/front/js/jquery.isotope.min.js"></script>
<script src="/front/js/easing.js"></script>
<script src="/front/js/jquery.flexslider-min.js"></script>
<script src="/front/js/jquery.scrollto.js"></script>
<script src="/front/js/owl.carousel.js"></script>
<script src="/front/js/jquery.countTo.js"></script>
<script src="/front/js/video.resize.js"></script>
<script src="/front/js/validation.js"></script>
<script src="/front/js/wow.min.js"></script>
<script src="/front/js/jquery.magnific-popup.min.js"></script>
<script src="/front/js/jquery.stellar.min.js"></script>
<script src="/front/js/enquire.min.js"></script>
<script src="/front/js/designesia.js"></script>
<script src="/front/js/jquery.event.move.js"></script>
<script src="/front/js/jquery.plugin.js"></script>
<script src="/front/js/jquery.countdown.js"></script>
<script src="/front/js/countdown-custom.js"></script>
<script src="/front/js/jquery.twentytwenty.js"></script>


<script>
    $(window).on("load", function () {
        $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.7});
        $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
            default_offset_pct: 0.3,
            orientation: 'vertical'
        });
    });
</script>


</body>
</html>
