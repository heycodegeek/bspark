<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="A commercial website that lists trade signals, exchanges & others crypto related info.">
        <meta name="keywords" content="best crypto signals website,free crypto signals telegram 2022, daily cryptocurrency trading signals, best crypto signals telegram, best free crypto signals, free crypto signals app, best paid crypto signals">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>SureTraders | best crypto signals website</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="main-body dark-theme">
        @inertia
    </body>
</html>
