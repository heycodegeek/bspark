<!DOCTYPE html>
<html>

<body
    style="background-color: #222533; padding: 20px; font-size: 14px; line-height: 1.43; font-family: Helvetica, Arial, sans-serif;">
<div style="max-width: 600px; margin: 10px auto 20px; font-size: 12px; color: #A5A5A5; text-align: center;">If you are
    unable to see this message, <a href="#" style="color: #A5A5A5; text-decoration: underline;">click here to view in
        browser</a></div>
<div style="max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);">
    <table style="width: 100%;">
        <tr class="background-color: #222633;">
            <td>
                <img alt="" src="{{asset('/img/brand/logo.png')}}" style="width: 100px; padding: 10px">
            </td>
            <td style="padding-left: 50px; text-align: right; padding-right: 20px;">
                <a href="{{route('login')}}"
                   style="color: #fff; text-decoration: underline; font-size: 14px; letter-spacing: 1px;">Sign
                    In</a>
            </td>
        </tr>
    </table>
    <div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);">
        <div style="color: #636363; font-size: 18px; padding: 10px">
            <h1>Name : {{$name}}</h1>
            <h1>Email : {{$email}}</h1>
            <h1>Message : {{ $comment}}</h1>
        </div>
    </div>
    <div style="background-color: #F5F5F5; padding: 40px; text-align: center;">
        <div style="color: #A5A5A5; font-size: 12px; margin-bottom: 20px; padding: 0px 50px;">You are receiving this
            email because you signed up for {{env('APP_NAME')}}
        </div>
        <div style="margin-bottom: 20px;"><a href="#" style="display: inline-block; margin: 0px 10px;">
                <img alt="" src="img/market-google-play.png" style="height: 33px;"></a>
            <a href="#" style="display: inline-block; margin: 0px 10px;"><img alt="" src="img/market-ios.png"
                                                                              style="height: 33px;"></a>
        </div>
        <div style="margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);">
            {{--            <div style="color: #A5A5A5; font-size: 10px; margin-bottom: 5px;">1073 Madison Ave, suite 649, New York, NY--}}
            {{--                10001--}}
            {{--            </div>--}}
            <div style="color: #A5A5A5; font-size: 10px;">Copyright {{ date("Y") }} {{config('app.name')}}. All rights
                reserved.
            </div>
        </div>
    </div>
</div>
</body>
</html>
