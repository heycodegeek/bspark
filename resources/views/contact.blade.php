@extends('layouts.front')

@section('content')
    <div class="container">
        <section class="about-us">
            <div class="container">
                <!-- Section Title Starts -->
                <div class="row text-center">
                    <h2 class="title-head">Contact <span>Us</span></h2>
                    <div class="title-head-subtitle">
                        <p></p>
                    </div>
                </div>
                <!-- Section Title Ends -->
                <!-- Section Content Starts -->
                <div class="row about-content">
                    <!-- Image Starts -->
                    <div class="col-sm-12 col-md-5 col-lg-6 text-center">
                        <img class="img-responsive img-about-us" src="front_assets/images/about-us.png" alt="about us">
                    </div>
                    <!-- Image Ends -->
                    <!-- Content Starts -->
                    <div class="col-sm-12 col-md-7 col-lg-6">
                        <h3 class="title-about mb-5 pb-5">Get in touch with us</h3>
                        <h1  class="tt mt-5 pt-5 text-primary">Email: <a href="mailto:info@suretraders.io"> info@suretraders.io</a></h1>
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div>

                            <div class="row">
                                <div class="col-12 p-5">
                                    <p></p>
                                </div>
                            </div>
                            {{--                            <form method="post" action="{{ route('contact.post') }}">--}}
                            {{--                                @csrf--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label class="form-label">Full Name</label>--}}
                            {{--                                    <input class="form-control" name="name" placeholder="Please enter your name">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label class="form-label">Email</label>--}}
                            {{--                                    <input class="form-control" name="email" placeholder="Please enter your email">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label class="form-label">Message</label>--}}
                            {{--                                    <textarea class="form-control" name="comment"></textarea>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <button class="btn btn-info btn-block">Submit</button>--}}
                            {{--                                </div>--}}
                            {{--                            </form>--}}
                        </div>

                        <!-- Content Ends -->
                    </div>
                    <!-- Section Content Ends -->
                </div>
            </div>
        </section>
    </div>
@endsection
