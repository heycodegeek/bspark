@extends('layouts.front')

@section('content')
    <div id="main-contant" class="main-contant">
        <!-- About Us Section Start -->
        <section class="border-bottom">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>

                            We at Initiative SureTraders, Limited (DBA SureTraders, "SureTraders" "us", "we", or "our") recognize and
                            respect the importance of maintaining the privacy of our customers. If not otherwise defined
                            herein, capitalized terms have the meaning given to them in the Terms of Service, available
                            at https://SureTraders.io/terms (the "Terms "). This Privacy Notice describes the types of data
                            we collect from you when you visit our website (the "Site"). This Privacy Notice also
                            explains how we may process, transfer, store and disclose the data collected, as well as
                            your ability to control certain uses of the collected data.
                        </p>
                        <p>
                            SureTraders, Limited is the data controller in respect of the processing activities outlined in
                            this Privacy Notice.
                        </p>
                        <p>
                            "Personal Data" means any data that refers, is related to, or is associated with an
                            identified or identifiable individual or as otherwise may be defined by applicable law.
                        </p>
                        <p>
                            Privacy Notice Key Points
                        </p>
                        <p>
                            The key points listed below are presented in further detail throughout this Privacy Notice.
                            These key points do not substitute the full Privacy Notice.
                        </p>
                        <p>
                            Personal Data We Collect. When you register, we collect Personal Data provided by you, such
                            as your name and email address. We collect Personal Data regarding who invited whom to
                            register for our Site. We also (automatically) collect data about your use of the Site. If
                            you contact us for questions or complaints, we will collect the data related to your
                            inquiry.
                            Basis for Processing Your Personal Data. Processing your Personal Data is necessary for the
                            performance of the Terms and the provision of the Services to you. Processing for the
                            purposes of analytics and usage analysis and for our recordkeeping and protection of our
                            legal rights – are all necessary for the purposes of legitimate interests that we pursue.
                            How We Use the Data We Collect. We use the data (including Personal Data) we collect mainly
                            to administer and provide the Site, contact you with administrative information, including
                            in connection with our future payment system for which you can sign up, as well as to
                            improve the Site.
                        </p>
                        <p>
                            Sharing the Personal Data We Collect. We may share the Personal Data we collect with our
                            service providers (e.g. cloud computing service providers like Amazon Web Services) and
                            subcontractors who assist us in the operation of the Site and process the data on our behalf
                            and under our instructions. We ensure in our agreements with such service providers and
                            subcontractors that they do not make independent use of the Personal Data, such as your name
                            or email address. For example, we run our service on Amazon’s servers. While this means that
                            our users’ Personal Data is sent to Amazon, it does not, however, mean that Amazon can use
                            this Personal Data to advertise its products to our users. Any Personal Data transferred at
                            liquidation or if we sell our business, assets or shares will continue to be subject to the
                            provisions of this Privacy Notice.
                        </p>
                        <p>
                            International Transfer. We may use service
                        </p>
                        <p>
                            providers (e.g. cloud computing service providers like Amazon Web Services) and/or
                            subcontractors and/or cooperate with or have business partners and affiliates located in
                            countries other than your own, and send them your Personal Data. We will ensure to have
                            agreements in place with such parties ensuring the same level of privacy and data protection
                            as set forth in this Privacy Notice. You hereby consent to such international transfer.
                            <br>
                            Your Rights. Subject to applicable law and additional rights as set forth below, you may
                            have a right to access, update and/or delete your Personal Data and obtain a copy of the
                            Personal Data we have collected about you.
                            <br>
                            Use of Cookies and Similar Technologies. We use cookies and similar technologies to help
                            personalize your experience. You can adjust your settings to determine which cookies you do
                            or do not allow. Changing your settings and/or deleting existing cookies may affect the
                            Services.
                            Retention. We retain data for as long as necessary for the purposes set forth in this
                            Privacy Notice.
                            <br>
                            Security. We implement industry standard measures aimed at reducing the risks of damage and
                            unauthorized access or use of Personal Data, but they do not provide absolute data security.
                            Changes to the Privacy Notice. We may change this Privacy Notice from time and encourage you
                            to review it periodically.

                        </p>
                        <p>
                            Data We Collect
                        </p>
                        <p>
                            We collect data from you when you choose to use our Site. In order to use our Site and/or
                            receive related services, you will be required to register and provide us with certain
                            Personal Data. We also collect Personal Data when you make use of the Site, request
                            information from us, sign up for newsletters or our email lists, complete online forms, or
                            contact us for any other reason.
                        </p>
                        <p>
                            Examples of the Personal Data that we collect from you may include your name and e-mail
                            address. Such Personal Data may be collected by us through the Site.
                        </p>
                        <p>
                            We also collect your browsing history on the Site.
                        </p>
                        <p>
                            In addition, when you use the Site, certain data may be automatically gathered about your
                            computer or mobile device, such as operating system, IP address, and subject to your consent
                            as may be required under applicable law, (geo) location, as well as your browsing history
                            and any data regarding your viewing on our Site. It is your voluntary decision whether to
                            provide us with any such Personal Data, but if you refuse to provide such data we may not be
                            able to register you to the Site and/or provide you with our services.
                        </p>
                        <p>
                            How We Use Your Personal Data
                        </p>
                        <p>
                            We collect your data and use it for purposes of the Site, as well as for purposes of the
                            future payment system that we intend to set up for such users who sign up.
                        </p>
                        <p>
                            General
                        </p>
                        <p>
                            We and any of our trusted third-party subcontractors and service providers may use the
                            Personal Data we collect from and about you for any of the following purposes: (1) to
                            provide you with the Site; (2) to respond to your inquiries or requests, contact and
                            communicate with you; (3) to contact you with informational newsletters and updates relating
                            to our Site and services; (4) to review the usage and operations of our Site and services;
                            (5) to use your data in an aggregated, non-specific format for analytical purposes (as
                            detailed below); (6) to prevent fraud, protect the security of our Site and services, and
                            address any problems with the our Site and services and (7) to provide customer support.
                        </p>
                        <p>
                            Statistical Data
                        </p>
                        <p>
                            By analyzing all data we receive, including all data concerning users, we may compile
                            statistical data across a variety of platforms and users ("Statistical Data"). Statistical
                            Data helps understand trends and customer-needs so that new products and services can be
                            considered and so existing products and services can be tailored to customer desires.
                            Statistical Data is anonymous and aggregated and we will not link Statistical Data to any
                            Personal Data. We may share such Statistical Data with our partners, without restriction, on
                            commercial terms that we can determine in our sole discretion.
                        </p>
                        <p>
                            Analytics
                        </p>
                        <p>
                            We, or our service providers or subcontractors, may use analytics tools ("Tools"), including
                            "Google Analytics" to collect data about the use of the Site and/or services. Such Tools
                            collect data such as how often users visit the Site, what pages they visit when they do so,
                            and what other sites and mobile applications they used prior to visiting the Site. The Tools
                            may collect certain Personal Data, and may link such Personal Data to specific data stored
                            in our customer database. We use the data we get from the Tools to improve our Site and
                            services. Google's ability to use and share data collected by Google Analytics about your
                            visits to this site is restricted by the Google Analytics Terms of Use located at
                            http://www.google.com/analytics/terms/us.html and the Google Privacy Policy located at
                            http://www.google.com/policies/privacy/.
                        </p>
                        <p>Legal uses</p>

                        <p>We may use your Personal Data as required or permitted by any applicable law.</p>

                        <p>Disclosure of Data</p>

                        <p>We may share your data, including Personal Data, as follows:</p>

                        <p>If you are invited by a friend to join the Site, we may let that friend know how many people
                            you have invited to join the Site as well. We will not disclose the names of those
                            people.</p>

                        <p>Business Partners, Service Providers, Affiliates, and Subcontractors
                            <br>
                            We may disclose data, including Personal Data we collect from and/or about you, to our
                            trusted service providers (e.g. cloud computing service providers like Amazon Web Services),
                            business partners, affiliates, subcontractors, who may use such data to: (1) help us provide
                            you with the Site and/or services; and (2) aid in their understanding of how users are using
                            our Site and/or services. We ensure in our agreements with our service providers and
                            subcontractors that they do not make independent use of the Personal Data, such as your name
                            or email address.
                        </p>


                        <p>International Transfer</p>

                        <p>We may use subcontractors and service providers and have business partners (e.g. cloud
                            computing service providers like Amazon Web Services) and affiliates who are located in
                            countries other than your own and send them data we receive (including Personal Data). We
                            will ensure that these third parties will be subject to written agreements ensuring the same
                            level of privacy and data protection as set forth in this Privacy Notice. You hereby consent
                            to such international transfer.</p>

                        <p>Business Transfers</p>

                        <p>We may transfer our databases containing your Personal Data if we sell our business or part
                            of it, including in cases of liquidation. Data about our users, including Personal Data, may
                            be disclosed as part of, or during negotiations of, any merger, sale of company assets or
                            acquisition and shall continue being subject to the provisions of this Privacy Notice.</p>

                        <p>Law Enforcement Related Disclosure</p>

                        <p>We will fully cooperate with any law enforcement authorities or court order requesting or
                            directing us to disclose the identity, behavior or (digital) content and data of or related
                            to an individual, including in the event of any user suspected to have engaged in illegal or
                            infringing behavior.</p>

                        <p>We may also share your Personal Data with third parties:</p>

                        <p>(I) if we believe in good faith that disclosure is appropriate to protect our rights,
                            property or safety (including the enforcement of the Terms and this Privacy Notice);<br>
                            (II) to protect the rights, property or safety of third parties;<br>
                            (III) when required by law, regulation subpoena, court order or other law enforcement
                            related issues; or<br>
                            (IV) as is necessary to comply with any legal and/or regulatory obligation. You can request
                            such Personal Data as specified herein by emailing us at info@SureTraders.io.</p>

                        <p>How We Protect Data</p>

                        <p>We make efforts to follow generally accepted industry standards to protect the Personal Data
                            submitted to us, both during transmission and once we receive it. However, no method of
                            transmission over the Internet, or method of electronic storage is 100% secure. Therefore,
                            while
                            we strive to use commercially acceptable means to protect your Personal Data, we cannot
                            guarantee its absolute security.</p>

                        <p>How to Access and Limit Our Use of Certain Data
                            You have certain rights in relation to the Personal Data that we hold about you, as detailed
                            below. We reserve the right to ask for reasonable evidence to verify your identity before we
                            provide you with any information and/or comply with any of your requests, as detailed
                            below:</p>

                        <p>Right of Access and Data Portability. You have a right to know what Personal Data we collect
                            about you and, in some cases, to have the data communicated to you. Subject to the
                            limitations
                            in applicable law, you may be entitled to obtain from us a copy of the Personal Data you
                            provided to us (excluding data that we obtained from other sources) in a structured,
                            commonly-used, and machine-readable format, and you may have the right to (request that we)
                            transmit such Personal Data to another party. If you wish to exercise this right please
                            contact
                            us letting us know what data in particular you would like to receive and/or transmit.
                            Subject to
                            applicable law, we may charge you with a fee. Please note that we may not be able to provide
                            you
                            with all the data you request, for instance, if the data includes Personal Data about
                            another
                            person. Where we are not able to provide you with data that you have asked for, we will
                            endeavor
                            to explain to you why. We will try to respond to any request for a right of access as soon
                            as
                            possible.<br>
                            Right to Correct Personal Data. Subject to the limitations in applicable law, you may
                            request
                            that we update, correct or delete inaccurate or outdated Personal Data and/or that we
                            suspend
                            the use of Personal Data, the accuracy of which you may contest, while we verify the status
                            of
                            that Personal Data. We will correct your Personal Data within a reasonable time from the
                            receipt
                            of your written request thereof.<br>
                            Deletion of Personal Data. In certain circumstances you have a right to have Personal Data
                            that
                            we hold about you deleted. Should you wish to have any Personal Data about you deleted,
                            please
                            contact us, using the contact information specified in this Privacy Notice. Subject to
                            applicable law, we will delete Personal Data provided to us by a user within a reasonable
                            time
                            from the receipt of a written (including via email) request by such user to delete such
                            collected Personal Data. We cannot restore data once it has been deleted. Please note that
                            to
                            ensure that we do not collect any further Personal Data, you should terminate your account
                            with
                            us by pressing the “Delete Account” button on the Site and clear our cookies from any device
                            where you have accessed our Site. We may retain certain Personal Data (including following
                            your
                            request to delete) for audit and record-keeping purposes, as well as other purposes, all as
                            permissible and/or required under applicable law. We may also retain your data in an
                            anonymized
                            form.<br>
                            Account Deactivation. You can delete your account with us by pressing the “Delete Account”
                            button on the Site.<br>
                            Right to Object. Subject to applicable law, you may have the right to object to processing
                            of
                            your Personal Information for certain purposes.<br>
                            Supervisory Authority. If you are a European Citizen, you may have the right to submit a
                            complaint to the relevant supervisory data protection authority.</p>

                        <p>Data Retention</p>

                        <p>Subject to applicable law, we retain data as necessary for the purposes set forth above. We
                            may
                            delete data from our systems, without notice to you, once we deem it is no longer necessary
                            for
                            the purposes set forth in this Privacy Notice.</p>

                        <p>Cookies and Similar Technologies</p>
                        <p>SureTraders uses cookies and similar technologies to help personalize your experience. A
                            "persistent" cookie may be used to help save your settings and customizations across visits.
                            Third parties through which we provide the Services and/or our business partners may be
                            placing
                            and reading cookies on your browsers, or using web beacons to collect information in the
                            course
                            of advertising being served on different websites.</p>

                        <p>How to Adjust Your Preferences</p>

                        <p>Most Web browsers are initially configured to accept cookies, but you can change this setting
                            so
                            your browser either refuses all cookies or informs you when a cookie is being sent. In
                            addition,
                            you are free to delete any existing cookies at any time. Please note that some features of
                            the
                            Services may function improperly when cookies are disabled or removed.</p>

                        Third-Party Applications and Services
                        All use of third-party applications or services is at your own risk and subject to such third
                        party's privacy policies.

                        Communications

                        Please note that we reserve the right to send you service-related communications, including
                        service announcements and administrative messages relating to your account, without offering you
                        the opportunity to opt out of receiving them. Should you not wish to receive such communications
                        you may cancel your account.

                        Children

                        We do not knowingly collect personally-identifiable data from children under the age of eighteen
                        (18). In the event that you become aware that an individual under the age of eighteen (18) has
                        enrolled without parental permission, please advise us immediately.

                        Changes

                        When visiting this Site, you shall be asked to accept the terms of this Privacy Notice. If you
                        do not agree with the terms hereof, please do not use the Site. We may update this Privacy
                        Notice from time to time – we encourage you to review it periodically. By continuing to access
                        or use the Site and/or services after those changes become effective, you agree to be bound by
                        the revised privacy notices. We will post the updated Privacy Notice on this page. Please come
                        back to this page every now and then to make sure you are familiar with the latest version. Any
                        new Privacy Notice will be effective from the date it is accepted by you.

                        Comments and Questions

                        If you have any comments or questions about our privacy policy, or if you wish for us to amend
                        or delete your Personal Data, please contact us at info@SureTraders.io.

                        Last updated: June 2021.
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
