@extends('layouts.front')

@section('content')
    <!-- Banner Area Starts -->
    <section class="banner-area">
        <div class="banner-overlay">
            <div class="banner-text text-center">
                <div class="container">
                    <!-- Section Title Starts -->
                    <div class="row text-center">
                        <div class="col-xs-12">
                            <!-- Title Starts -->
                            <h2 class="title-head">About <span>Us</span></h2>
                            <!-- Title Ends -->
                            <hr>
                            <!-- Breadcrumb Starts -->
                            <ul class="breadcrumb">
                                <li><a href="/"> home</a></li>
                                <li>About</li>
                            </ul>
                            <!-- Breadcrumb Ends -->
                        </div>
                    </div>
                    <!-- Section Title Ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area Starts -->
    <!-- About Section Starts -->
    <section class="about-page">
        <div class="container">
            <!-- Section Content Starts -->
            <div class="row about-content">
                <!-- Image Starts -->

                <!-- Image Ends -->
                <!-- Content Starts -->
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="feature-about">
                        <h3 class="title-about">WE ARE SURE TRADERS</h3>
                        <style type="text/css">


                            span.s1 {
                                color: #0bc634
                            }
                        </style>
                        <p class="p1">Despite the struggling performance of the Cryptocurrency in 2019, Crypto traders
                            were optimistic about the performance. And surely their optimism bore sweet fruits. We were
                            in late November of 2020, and the price of Bitcoin had already crossed $18K.</p>
                        <p class="p2">The past few years of the Crypto trade industry were less fruitful. There was a
                            point when people had accepted the fact that soon the crypto bubble will burst. The threat
                            increased with the onset of the COVID 19 pandemic when the price of the Bitcoin dropped
                            below $3000.</p>
                        <p class="p2">However, to everyone&apos;s surprise, Bitcoin started a mad bull run without
                            seeing back. This positive run of the bitcoin price has filled the crypto traders with the
                            hope to see its price soaring above $40K.</p>
                        <p class="p2">The Cryptocurrency trade was indeed on the back foot for a couple of months.
                            However, the Crypto exchanges were optimistic about its performance.</p>
                        <p class="p3">Hence, Looking forward to see the rise in the Crypto world, John Paul (born
                            November 21, 1981) is an American business executive, <span class="s1">entrepreneur</span>,
                            and <span class="s1">high-frequency trading</span> expert, who is the current owner of
                            SureTrader pro, linked with Suretraders.io from London, U.K and is best known for founding
                            two high-frequency trading firms started the revolution in the Digital Currency.</p>
                        <p class="p4">In 2006 Paul founded <em>Fast trader</em>, currently owned by <em>Stock USA
                                Investments, Inc</em>, (formerly <em>Fast Trader</em>, Inc). Then in 2014 Paul founded
                            <em>SureTraders</em>, a division of <em>London &amp; Capital dealing in Virtual
                                Currency.</em> In less than a year in operation, SureTraders reported more than 10,000
                            blockchain transactions daily. Its success prompted a major expansion to meet scaling
                            demands, and the firm set out to increase staff by two hundred percent during March 2016. By
                            2018 both SureTraders and Fast Trader were ranked among top online trading firms on Digital
                            Currency.</p>
                        <p class="p4">In Oct 2019, as Suretraders reported averaging 30,000 transactions daily, Paul
                            then broke ground on the Blockchain,&nbsp;1.2 million-dollar experience in just few hours.
                            He received more than 500 advance applications for its new account openings that night.</p>
                        <p class="p5"><br></p>
                        <p class="p6">In addition to the Blockchain Technology, Smart contract Technology, Paul upgraded
                            himself with the latest and upcoming next gen technology called Metaverse. The metaverse is
                            essentially a merging of virtual, augmented, and physical reality, and blurs the line
                            between your interactions online and in real life. And as more people begin to place their
                            bets on a future embedded in the metaverse, businesses have already begun to start new
                            ventures in this digital space. In 2021 Facebook was renamed &quot;Meta Platforms&quot; and
                            its chairman Mark Zuckerberg declared a company commitment to developing a metaverse. Many
                            of the virtual reality technologies advertised by Meta Platforms remains to be
                            developed.</p>
                        <p class="p4">This is how Suretraders have now started its function almost in 20 countries
                            around the world.<span class="Apple-converted-space">&nbsp;</span></p>
                    </div>
                    <!-- Content Ends -->

                </div>
                <!-- Section Content Ends -->
            </div><!--/ Content row end -->
    </section>
    <!-- About Section Ends -->



    <!-- Call To Action Section Starts -->
    <section class="call-action-all">
        <div class="call-action-all-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Call To Action Text Starts -->
                        <div class="action-text">
                            <h2>Get Started Today With Crypto</h2>
                            <p class="lead">Open account for free and start trading Crypto!</p>
                        </div>
                        <!-- Call To Action Text Ends -->
                        <!-- Call To Action Button Starts -->
                        <p class="action-btn"><a class="btn btn-primary" href="/register">Register Now</a></p>
                        <!-- Call To Action Button Ends -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Section Ends -->
@endsection
