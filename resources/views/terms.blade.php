@extends('layouts.front')

@section('content')
    <!-- Banner Area Starts -->
    <section class="banner-area">
        <div class="banner-overlay">
            <div class="banner-text text-center">
                <div class="container">
                    <!-- Section Title Starts -->
                    <div class="row text-center">
                        <div class="col-xs-12">
                            <!-- Title Starts -->
                            <h2 class="title-head">TERMS & <span>CONDITIONS</span></h2>
                            <!-- Title Ends -->
                            <hr>
                            <!-- Breadcrumb Starts -->
                            <ul class="breadcrumb">
                                <li><a href="/"> home</a></li>
                                <li>terms & Conditions</li>
                            </ul>
                            <!-- Breadcrumb Ends -->
                        </div>
                    </div>
                    <!-- Section Title Ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area Ends -->
    <!-- Section Terms of Services Starts -->
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            text-indent: 0;
        }

        h2 {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 16pt;
        }

        .s1 {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
            vertical-align: 3pt;
        }

        .s2 {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 13pt;
        }

        .p,
        p {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 14pt;
            margin: 0pt;
        }

        .a {
            color: #00F;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: underline;
            font-size: 14pt;
        }

        .s5 {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 7pt;
            vertical-align: 3pt;
        }

        .s6 {
            color: white;
            font-family: "Times New Roman", serif;
            font-style: italic;
            font-weight: normal;
            text-decoration: none;
            font-size: 10pt;
        }

        li {
            display: block;
        }

        #l1 {
            padding-left: 0pt;
            counter-reset: c1 1;
        }

        #l1>li>*:first-child:before {
            counter-increment: c1;
            content: counter(c1, decimal)". ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 16pt;
        }

        #l1>li:first-child>*:first-child:before {
            counter-increment: c1 0;
        }

        #l2 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l2>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
        }

        #l2>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        #l3 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l3>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 12pt;
        }

        #l3>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        #l4 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l4>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
        }

        #l4>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        #l5 {
            padding-left: 0pt;
            counter-reset: c3 1;
        }

        #l5>li>*:first-child:before {
            counter-increment: c3;
            content: "(" counter(c3, lower-latin)") ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 10pt;
        }

        #l5>li:first-child>*:first-child:before {
            counter-increment: c3 0;
        }

        #l6 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l6>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
        }

        #l6>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        #l7 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l7>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
        }

        #l7>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        #l8 {
            padding-left: 0pt;
            counter-reset: d1 1;
        }

        #l8>li>*:first-child:before {
            counter-increment: d1;
            content: counter(d1, decimal)". ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 11pt;
        }

        #l8>li:first-child>*:first-child:before {
            counter-increment: d1 0;
        }

        #l9 {
            padding-left: 0pt;
            counter-reset: c2 1;
        }

        #l9>li>*:first-child:before {
            counter-increment: c2;
            content: counter(c1, decimal)"." counter(c2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 10pt;
        }

        #l9>li:first-child>*:first-child:before {
            counter-increment: c2 0;
        }

        li {
            display: block;
        }

        #l10 {
            padding-left: 0pt;
            counter-reset: e1 1;
        }

        #l10>li>*:first-child:before {
            counter-increment: e1;
            content: "(" counter(e1, lower-latin)") ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
            font-size: 10pt;
        }

        #l10>li:first-child>*:first-child:before {
            counter-increment: e1 0;
        }

        li {
            display: block;
        }

        #l11 {
            padding-left: 0pt;
            counter-reset: f1 11;
        }

        #l11>li>*:first-child:before {
            counter-increment: f1;
            content: counter(f1, decimal)" ";
            color: white;
            font-style: normal;
            font-weight: normal;
            text-decoration: none;
        }

        #l11>li:first-child>*:first-child:before {
            counter-increment: f1 0;
        }

        #l12 {
            padding-left: 0pt;
            counter-reset: f2 2;
        }

        #l12>li>*:first-child:before {
            counter-increment: f2;
            content: counter(f1, decimal)"." counter(f2, decimal)" ";
            color: white;
            font-family: "Times New Roman", serif;
            font-style: normal;
            font-weight: bold;
            text-decoration: none;
            font-size: 12pt;
        }

        #l12>li:first-child>*:first-child:before {
            counter-increment: f2 0;
        }
    </style>
    <section class="terms-of-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 style="padding-top: 3pt;text-indent: 0pt;text-align: center;"><a name="bookmark0">USER
                            AGREEMENT VERSION 1.03</a></h2>
                    <h2 style="text-indent: 0pt;line-height: 11pt;text-align: center;">LAST REVISED ON: 1<span
                            class="s1">st</span> Sep, 2022</h2>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p class="s2" style="padding-left: 5pt;text-indent: 0pt;text-align: left;">This user agreement (this
                        “<b>Agreement</b>”) sets forth the legally binding terms and conditions your access to and use of any websites,
                        mobile sites, mobile applications, desktop applications, products or services (the “<b>Services</b>”) offered
                        bySuretraders and the business known as Blockchain Education (further described on www.suretraders.io).</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">Any reference to “<b>Suretraders</b>”,
                        “<b>us</b>”, “<b>our</b>”, and “<b>we</b>” in this Agreement shall be construed to be a reference to Suretraders
                        depending on the relevant part of the Service in question.</p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">The “<b>User</b>”, “<b>you</b>”, “<b>your</b>”
                        shall refer to any natural person or entity and its authorized users that subscribes or uses the Services.
                        Certain features of the Site may be subject to additional guidelines, terms, or rules, which will be posted on
                        the Site in connection with such features. All such additional terms, guidelines, and rules are incorporated by
                        reference into this Agreement.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">BY ACCESSING OR USING THE SERVICES, YOU ARE
                        ACCEPTING THIS AGREEMENT (ON BEHALF OF YOURSELF OR THE ENTITY THAT YOU REPRESENT), AND YOU REPRESENT AND WARRANT
                        THAT YOU HAVE THE RIGHT, AUTHORITY, AND CAPACITY TO ENTER INTO THIS AGREEMENT (ON BEHALF OF YOURSELF OR THE
                        ENTITY THAT YOU REPRESENT). YOU MAY NOT ACCESS OR USE THE SERVICES OR ACCEPT THIS AGREEMENT IF YOU ARE NOT AT
                        LEAST 18 YEARS OLD. IF YOU DO NOT AGREE WITH ALL OF THE PROVISIONS OF THIS AGREEMENT, DO NOT ACCESS AND/OR USE
                        THE SITE. THESE TERMS REQUIRE THE USE OF ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES AND ALSO LIMIT
                        THE REMEDIES AVAILABLE TO YOU IN THE EVENT OF A DISPUTE.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">We may amend this Agreement related to the
                        Services from time to time. Amendments will be effective upon our posting of such updated Agreement at this
                        location or the amended policies or supplemental terms on the applicable Service. Your continued access or use
                        of the Services after such posting constitutes your consent to be bound by the Agreement, as amended.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">YOU ACKNOWLEDGE THAT TRADING IN CRYPTOCURRENCIES
                        (ALSO REFERRED TO AS VIRTUAL DIGITAL ASSETS, CRYPTO-ASSETS, ETC.) INVOLVES A HIGH DEGREE OF RISK.
                        CRYPTOCURRENCIES ARE SUBJECT TO CONSTANT AND FREQUENT FLUCTUATIONS IN VALUE AND EXCHANGE RATES, AND THE VALUE OF
                        YOUR CRYPTOCURRENCY ASSETS MAY INCREASE OR DECREASE AT ANY TIME. ACCORDINGLY, YOU MAY SUFFER A COMPLETE LOSS OF
                        THE FUNDS HELD IN YOUR ACCOUNT. YOU ACKNOWLEDGE THAT YOU ARE SOLELY RESPONSIBLE FOR DETERMINING THE NATURE,
                        SUITABILITY, AND APPROPRIATENESS OF THESE TRADING RISKS FOR YOU. YOU ACKNOWLEDGE AND AGREE THAT SURETRADERS
                        BEARS NO RESPONSIBILITY OR LIABILITY TO YOU OR ANY OTHER PERSON WHATSOEVER FOR ANY LOSSES OR GAINS INCURRED IN
                        CONNECTION WITH YOUR USE OF THE SERVICES. YOU ALSO ACKNOWLEDGE AND AGREE THAT SURETRADERS DOES NOT GIVE ADVICE
                        OR RECOMMENDATIONS REGARDING THE TRADING OF CRYPTOCURRENCIES, INCLUDING THE SUITABILITY AND APPROPRIATENESS OF,
                        AND INVESTMENT STRATEGIES FOR, CRYPTOCURRENCIES. THE REFERRALS MADE BY YOU TO US ARE IN YOUR INDIVIDUALCAPACITY.
                        IN THE EVENT YOU ARE UNDERTAKING SUCH REFERRALS IN FURTHERANCE TO A BUSINESS OR PROFESSION, YOU ARE UNDER THE
                        OBLIGATION TO BRING IT TO OUR NOTICE. IN SUCH A CASE, WE WILL BE ENTITLED TO DEDUCT / COLLECT ADDITIONAL
                        WITHHOLDING TAXES AS MAY BE REQUIRED BY APPLICABLE LAW.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">YOU ALSO ACKNOWLEDGE AND AGREE THAT SURETRADERS
                        AND YOUR ABILITY TO USE THE SERVICES MAY BE DETRIMENTALLY IMPACTED BY REGULATORY ACTION OR CHANGES IN
                        REGULATIONS</p>
                    <p style="padding-top: 3pt;padding-left: 11pt;text-indent: 0pt;text-align: justify;">APPLICABLE TO CRYPTOCURRENCY.
                        YOU AGREE THAT WE MAY DISCLOSE YOUR PERSONAL AND ACCOUNT INFORMATION IF WE BELIEVE THAT IT IS REASONABLY
                        NECESSARY TO COMPLY WITH A LAW, REGULATION, LEGAL PROCESS, OR GOVERNMENTAL REQUEST.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">YOU HEREBY ACKNOWLEDGE AND AGREE THAT
                        SURETRADERS IS NOT A FINANCIAL INSTITUTION, BANK, CREDIT UNION, TRUST, HEDGE FUND, BROKER OR INVESTMENT OR
                        FINANCIAL ADVISOR, AND IS NOT SUBJECT TO THE SAME LAWS, REGULATIONS, DIRECTIVES OR REQUIREMENTS APPLICABLE TO
                        SUCH PERSONS. YOU ACKNOWLEDGE AND AGREE THAT NO ORAL OR WRITTEN INFORMATION OR ADVICE PROVIDED BY SURETRADERS,
                        ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS, NOR ANY INFORMATION OBTAINED THROUGH THE SERVICES, WILL OR SHALL
                        CONSTITUTE INVESTMENT, OR FINANCIAL ADVICE. YOU ARE SOLELY AND EXCLUSIVELY RESPONSIBLE FOR ALL TRADING DECISIONS
                        MADE BY YOU WHILE USING THE SERVICES.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 11pt;text-indent: 0pt;text-align: justify;">SURETRADERS IS MERELY A TECHNOLOGY PLATFORM AND
                        YOU ARE SOLELY AND ENTIRELY RESPONSIBLE FOR THE LEGAL, REGULATORY AND TAX COMPLIANCE OF ALL TRANSACTIONS CARRIED
                        OUT BY YOU USING THE SERVICES, SUBJECT TO THE TERMS OF THIS AGREEMENT.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <ol id="l1">
                        <li data-list-text="1.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark1">ACCOUNTS</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l2">
                                <li data-list-text="1.1">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Account Creation. <span
                                            class="p">In order to use certain features of the Site, you must register for an account
                            (“</span>Account<span class="p">”) and provide certain information about yourself as
                            prompted by the account registration form. You represent and warrant that: (a) all required
                            registration information you submit is truthful and accurate; (b) you will maintain the
                            accuracy of such information; (c) your use of the Services is and shall be in accordance
                            with applicable law. You may temporarily suspend your Account at any time, for any reason,
                            by following the instructions on the Site. Suretraders may suspend or terminate your Account
                            in accordance with the ZTP.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="1.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Identity Verification. <span
                                            class="p">You agree to provide us with the information we request for the purposes of
                            identity verification and the detection of money laundering, terrorist financing, fraud, or
                            any other financial crime and permit us to keep a record of such information. You will need
                            to complete certain verification procedures before you are permitted to use the Services and
                            your access to the Services may be altered as a result of information collected on an
                            ongoing basis. The information we request may include certain personal information,
                            including, but not limited to, your name, address, telephone number, e-mail address, date of
                            birth, taxpayer identification number, government identification number, and information
                            regarding your bank account (such as the name of the bank, the account type, routing number,
                            and account number). In providing us with this or any other information that may be
                            required, you confirm that the information is accurate and authentic. You agree to update
                            this information and keep it current at all times. You authorize us to make the inquiries,
                            whether directly or through third parties, that we consider appropriate, in our sole
                            discretion, to verify your identity or protect you and/or us against fraud or other
                            financial crimes, and to take action we reasonably deem necessary based on the results of
                            such inquiries.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="1.3">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Certain Restrictions. <span
                                            class="p">By using the Services, you represent and warrant that: (i) neither you nor any of
                            your directors, officers, employees, agents, affiliates or representatives is an individual
                            or an entity that is, or is owned or controlled by an individual or entity that is (a)
                            currently the subject of any Sanctions, or (b) located, organized or resident in a
                            Designated Jurisdiction; (ii) you have complied in all material respects with all applicable
                            laws relating to Sanctions, anti-terrorism, anti-corruption and anti-money laundering; and
                            (iii) you have instituted and maintained policies and procedures designed to promote and
                            achieve compliance with such laws. For the purpose of this Agreement, “</span>Designated
                                        Jurisdiction<span class="p">” means any country or territory to the extent that such country or
                            territory itself is the subject of any Sanction.</span></h2>
                                </li>
                                <li data-list-text="1.4">
                                    <h2 style="padding-top: 3pt;padding-left: 68pt;text-indent: -28pt;text-align: justify;">Account
                                        Responsibilities. <span class="p">You are responsible for maintaining the confidentiality of
                            your Account login information and are fully responsible for all activities that occur under
                            your Account including all consequences under applicable laws. You agree to immediately
                            notify Suretraders of any unauthorized use,or suspected unauthorized use of your Account or
                            any other breach of security. Suretraders cannot and will not be liable for any loss or
                            damage arising from your failure to comply with the above requirements. Suretraders will not
                            be liable or responsible for compromise, unauthorized use or suspected unauthorized use of
                            your Account or any claims or actions in any manner related thereto.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="1.5">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Compliance. <span
                                            class="p">You are solely responsible for ensuring that your use of the Services is in
                            compliance with all laws, rules and regulations applicable to you and the right to access
                            the Services is automatically revoked where use of the Services is prohibited or to the
                            extent that the offering, sale or provision of the Services conflicts with any law, rule or
                            regulation applicable to you.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="1.6">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Available Only Where
                                        Permitted by Law<span class="p">. The Services are only available in jurisdictions where they
                            may be legally offered for sale</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="2.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark2">ACCESS TO THE
                                    SERVICES</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l3">
                                <li data-list-text="2.1">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">License. <span
                                            class="p">Subject to this Agreement, Suretraders grants you a non-transferable,
                            non-exclusive, revocable, limited license to use and access the Services solely for your own
                            personal or internal business purposes,in accordance with the Account type you are currently
                            subscribing to.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="2.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Modification. <span
                                            class="p">Suretraders reserves the right, at any time, to modify, suspend, or discontinue
                            the Services (in whole or in part) with or without notice to you. You agree that Suretraders
                            will not be liable to you or toany third party for any modification, suspension, or
                            discontinuation of the Services or any part thereof.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="2.3">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">No Support or Maintenance.
                                        <span class="p">You acknowledge and agree that Suretraders will have no obligation to provide
                            you with any support or maintenance in connection with the Services.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="2.4">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Ownership. <span
                                            class="p">You acknowledge that all the intellectual property rights, including copyrights,
                            patents, trademarks, and trade secrets, in the Services and its content are owned by
                            Suretraders or its suppliers. Neither this Agreement (nor your access to the Services)
                            transfers to you or any third party any rights, title or interest in or to such intellectual
                            property rights.</span></h2>
                                </li>
                                <li data-list-text="2.5">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Feedback. <span class="p">If
                            you provide Suretraders with any feedback or suggestions regarding the Site
                            (“</span>Feedback<span class="p">”), you hereby </span>assign <span class="p">to Suretraders
                            all rights in such Feedback and agree that Suretraders shall have the right to use and fully
                            exploit such Feedback and related information in any manner it deems appropriate.
                            Suretraderswill treat any Feedback you provide to Suretraders as non-confidential and
                            non-proprietary. You agree that you will not submit to Suretraders any information or ideas
                            that you consider to be confidential or proprietary.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="3.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark3">PROHIBITED
                                    ACTIVITIES</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l4">
                                <li data-list-text="3.1">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: left;">Prohibited Activities. <span
                                            class="p">In connection with your use of Services, you hereby agree that you will
                            not:</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                    <ol id="l5">
                                        <li data-list-text="(a)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: left;">violate (or assist any
                                                other party in violating) any applicable law, statute, ordinance, or regulation;</p>
                                        </li>
                                        <li data-list-text="(b)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: left;">intentionally try to
                                                defraud (or assist in the defrauding of) Suretraders or any other User;</p>
                                        </li>
                                        <li data-list-text="(c)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: left;">provide false,
                                                inaccurate, or misleading information;</p>
                                        </li>
                                        <li data-list-text="(d)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: left;">trade taking advantage of
                                                any technical glitch, malfunction, failure, delay, default, or security breach;</p>
                                        </li>
                                        <li data-list-text="(e)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: left;">take any action that
                                                interferes with, intercepts, or expropriates any system, data, or information;</p>
                                        </li>
                                        <li data-list-text="(f)">
                                            <p style="padding-top: 3pt;padding-left: 96pt;text-indent: -28pt;text-align: justify;">
                                                partake in any transaction involving the proceeds of illegal activity;</p>
                                        </li>
                                        <li data-list-text="(g)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">attempt to gain
                                                unauthorized access to other Suretraders Accounts, the Services, or any related networks
                                                or systems;</p>
                                        </li>
                                        <li data-list-text="(h)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">use the Services on
                                                behalf of any third party or otherwise act as an intermediary between Suretradersand any
                                                third parties, unless Suretraders and the User have specifically entered into a written
                                                agreement enabling such use or action;</p>
                                        </li>
                                        <li data-list-text="(i)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">use the Services to
                                                engage in conduct that is detrimental to Suretraders or to any other User or any other
                                                third party;</p>
                                        </li>
                                        <li data-list-text="(j)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">collect any user
                                                information from other Users, including, without limitation, email addresses;</p>
                                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                        </li>
                                        <li data-list-text="(k)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">defame, harass, or
                                                violate the privacy or intellectual property rights of Suretraders or any other User;or
                                            </p>
                                        </li>
                                        <li data-list-text="(l)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">upload, display or
                                                transmit any messages, photos, videos or other media that contain illegal goods,
                                                pornographic, violent, obscene or copyrighted images or materials for use as an avatar,
                                                in connection with a payment or payment request, or otherwise.</p>
                                        </li>
                                        <li data-list-text="(m)">
                                            <p style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">In addition, you agree
                                                not to: (i) upload, transmit, or distribute to or through the Services any computer
                                                viruses, worms, or any software intended to damage or alter a computer system or data;
                                            </p>
                                            <p style="padding-left: 96pt;text-indent: 0pt;text-align: justify;">(ii) interfere with,
                                                disrupt, or create an undue burden on servers or networks connected to the Services, or
                                                violate the regulations, policies or procedures of such networks; (iii) attempt to
                                                reverse engineer, de-compile, disable, interfere with, disassemble, copy, or disrupt the
                                                integrity or the performance of the Services, any third-party use of the Services, or
                                                any third-party data contained therein (except to the extent such restrictions are
                                                prohibited by applicable law; or (iv) access the Services in order to build a
                                                competitive product or service or copy any ideas, features, functions, or graphics of
                                                the Services.</p>
                                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                        </li>
                                    </ol>
                                </li>
                                <li data-list-text="3.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Enforcement. <span
                                            class="p">We reserve the right (but have no obligation) to investigate and/or take
                            appropriate action against you in our sole discretion if you engage in Prohibited Activities
                            or violate any other provision of this Agreement or otherwise create liability for us or any
                            other person. Such action may include, in our sole and absolute discretion, terminating your
                            Account in accordance with Section 9, reporting you to law enforcement authorities without
                            providing any notice of you about any such report and confiscating any balance remaining in
                            an Account which has been terminated.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="4.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark4">THIRD-PARTY PRODUCTS
                                    AND SERVICES; OTHER USERS</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l6">
                                <li data-list-text="4.1">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Third-Party Services. <span
                                            class="p">You acknowledge that the Service will enable or assist you to access, interact
                            with, and/or purchase services from several supported platforms and other third parties via
                            third-party websites or applications (collectively, “</span>Third-Party Services<span
                                            class="p">”). Such Third-Party Services are not under the control of Suretraders,
                            Suretraders does not review, approve, monitor, endorse, warrant, or make anyrepresentations
                            with respect to Third-Party Services and is not responsible for any Third-Party Services.You
                            use all Third-Party Services at your own risk, and should apply a suitable level of caution
                            and discretion in doing so. Any use of Third-Party Services is governed solely by the terms
                            and conditions of such Third-Party Services and any contract entered into, or any
                            transaction completed via any Third- Party Services, is between you and the relevant third
                            party, and not with Suretraders. You shall comply in all respects with all applicable terms
                            of the Third-Party Services that you access or subscribe to in connection with the Services.
                            If at any time any Third-Party Services cease to make their programs available to us on
                            reasonable terms, we may cease to provide such features to you without entitling youto
                            refund, credit, or other compensation.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="4.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Other Users. <span
                                            class="p">We do not guarantee the identity of any User or other party or ensure that a buyer
                            or seller is authorized to complete a transaction or will complete a transaction. Your
                            interactions with other Users are solely between you and such Users. You agree that
                            Suretraders will not be responsible for any loss or damage incurred as the result of any
                            such interactions. If there is a dispute between you and any User, we are under no
                            obligation to become involved.</span></h2>
                                </li>
                                <li data-list-text="4.3">
                                    <h2 style="padding-top: 3pt;padding-left: 68pt;text-indent: -28pt;text-align: justify;">Release.
                                        <span class="p">You hereby release and forever discharge Suretraders (and our officers,
                            employees, agents, successors, and assigns) from, and hereby waive and relinquish, each and
                            every past, present and future dispute, claim, controversy, demand, right, obligation,
                            liability, action and cause of action of every kind and nature (including personal injuries,
                            death, and property damage), that has arisen or arises directly or indirectly out of, or
                            that relates directly or indirectly to, the Services (including any interactions with, or
                            act or omission of, other Users or any Third-Party Products and Services).</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="5.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark5">FEES AND TAXES.</a>
                            </h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l7">
                                <li data-list-text="5.1">
                                    <p style="padding-left: 68pt;text-indent: -28pt;text-align: justify;"><b>General</b>. You agree to
                                        pay Suretraders the fees set forth at <a href="http://www.suretraders.iowhich/" class="a"
                                                                                 target="_blank">https://www.suretraders.io </a>which may be updated from time to time in our
                                        sole discretion. Any such updated fees will apply prospectively to any trades or other
                                        transactions that take place following the effective date of such updated fees. You authorize
                                        Suretraders to remove any amounts from your Account for any applicable fees owed by you under
                                        this Agreement</p>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="5.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Withdrawal / Send fees. <span
                                            class="p">You may be charged a fee to send or withdraw a Supported Cryptocurrency from your
                            Suretraders wallet.</span></h2>
                                </li>
                                <li data-list-text="5.3">
                                    <h2 style="padding-top: 4pt;padding-left: 68pt;text-indent: -28pt;text-align: justify;">Service
                                        fees. <span class="p">Suretraders applies a Maker / Taker fee structure for customers who trade
                            Supported Cryptocurrency using the Services. Suretraders will, at the time of any
                            transaction, notify you of any fees that will apply to the transaction. By proceeding with
                            any transaction, you accept and agree to the applicable fees. Such fees will also be
                            displayed in your transaction history upon completion of the transaction.</span></h2>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="6.">
                            <h2 style="padding-top: 9pt;padding-left: 34pt;text-indent: -28pt;text-align: left;">CONTESTS. <span
                                    class="p">We may from time to time offer you the ability to participate in contests of skill
                    (“</span>Contest<span class="p">” or “</span>Contests<span class="p">”). If you choose to
                    participate in any Contests, you accept and agree to be bound and abide by the applicable Contest
                    terms and conditions which will be published on the Suretraders website from time to time and herein
                    by reference (the “</span>Contest Terms<span class="p">”). Suretraders reserves the right to modify
                    the Contest Terms or discontinue any contests at any time for any reason, including but not limited
                    to product availability,pricing issues, reduced demand. Prizes, discounts and special pricing may be
                    changed or substituted at any time without prior notice. Taxes on any prize or award are the sole
                    responsibility of the recipient. We have theright to withhold tax as may be applicable at applicable
                    rates on the winnings in Contest. In such a case, we will provide you with evidence of such
                    withholding as per prescribed timelines.</span></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="7.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;">OTHER REWARDS. <span class="p">You may
                    receive or be entitled to receive crypto-currency as airdrops or on redemption of coupons or
                    campaign rewards or referral rewards. You hereby declare that you have not paid any consideration,
                    either in kind or otherwise, for receipt of these rewards. You hereby also agree that you are solely
                    responsible for paying any taxes and / or complying with tax obligations which may be applicable
                    pursuant to receipt of such rewards by you.</span></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="8.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;line-height: 11pt;text-align: left;">IMPORTANT NOTES.</h2>
                            <ol id="l8">
                                <li data-list-text="1.">
                                    <p class="s2" style="padding-left: 41pt;text-indent: -18pt;line-height: 13pt;text-align: left;">A
                                        Subscriber in Suretraders gets Basic Blockchain Education for free.</p>
                                </li>
                                <li data-list-text="2.">
                                    <p class="s2" style="padding-top: 1pt;padding-left: 41pt;text-indent: -18pt;text-align: left;">User
                                        account in Buddy Bot gets 0.3% profit sharing daily up to 250%.</p>
                                </li>
                                <li data-list-text="3.">
                                    <p class="s2" style="padding-left: 41pt;text-indent: -18pt;text-align: left;">There is NO REFUND
                                        POLICY for Buddy Bot, Delegators and Validators.</p>
                                </li>
                                <li data-list-text="4.">
                                    <p class="s2"
                                       style="padding-top: 1pt;padding-left: 41pt;text-indent: -18pt;line-height: 108%;text-align: left;">
                                        Delegators and Validators gets profit sharing of 2% every 10<span class="s5">th</span> day from
                                        the date of subscription till we introduce the Blockchain System.</p>
                                </li>
                                <li data-list-text="5.">
                                    <p class="s2" style="padding-left: 41pt;text-indent: -18pt;line-height: 108%;text-align: left;">
                                        After the Blockchain is introduced, the profit of 2% which is distributed every 10<span
                                            class="s5">th</span> Day, will stop, and the amount of profit-sharing ratio of the
                                        Blockchain, which company has decided will be given to all Delegators and Validators henceforth.
                                    </p>
                                </li>
                                <li data-list-text="6.">
                                    <p class="s2" style="padding-left: 41pt;text-indent: -18pt;text-align: left;">Without verifying the
                                        mail, no profit will be given to the Subscribers, Delegators or Validators.</p>
                                </li>
                                <li data-list-text="7.">
                                    <p class="s2" style="padding-top: 1pt;padding-left: 41pt;text-indent: -18pt;text-align: left;">No
                                        User can claim any other profit not mentioned by the company at present or in future.</p>
                                </li>
                            </ol>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="9.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark6">SUSPENSION,
                                    TERMINATION, AND CANCELLATION.</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <ol id="l9">
                                <li data-list-text="9.1">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">General<span class="p">. This
                            Agreement will continue to apply to you until terminated after each year. Any provision of
                            this Agreement which, either by its terms or to give effect to its meaning, must survive,
                            andsuch other</span></h2>
                                    <p style="padding-top: 3pt;padding-left: 68pt;text-indent: 0pt;text-align: justify;">provisions
                                        which expressly, or by their nature, are intended to survive termination shall survive the
                                        expiration or termination of this Agreement.</p>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                                <li data-list-text="9.2">
                                    <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Suspension, Termination<span
                                            class="p">. We may terminate your Account or suspend your access to the Services at any time
                            and with immediate effect for any reason or no reason, in our sole and absolute discretion.
                            We may decline to process any deposit or withdrawal without prior notice and may limit or
                            suspend your use of one or more Services at any time, in our sole discretion. For example,
                            we may, in our sole discretion, not process, withhold, suspend, pause, or hold the deposits
                            or withdrawals if we believe the transaction is suspicious, may involve fraud or misconduct,
                            violates applicable laws, or violates the terms of this Agreement. If you have a balance
                            remaining in an Account which has been suspended, we may freeze such balance for so long as
                            the Account is suspended. If the Account is terminated due to fraud,</span></h2>
                                    <p style="padding-top: 3pt;padding-left: 68pt;text-indent: 0pt;text-align: justify;">Violation of
                                        law, or violation of this Agreement, Suretraders may, in its discretion, confiscate any balance
                                        remaining in the Account and deliver it to any applicable government, law enforcement, or other
                                        authorities where circumstances warrant.</p>
                                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                                </li>
                            </ol>
                        </li>
                        <li data-list-text="10.">
                            <h2 style="padding-top: 9pt;padding-left: 34pt;text-indent: -28pt;text-align: left;"><a
                                    name="bookmark7">DISCLAIMERS</a></h2>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <p style="padding-left: 39pt;text-indent: 0pt;text-align: justify;">THE SERVICES ARE PROVIDED ON AN “AS-IS”
                                AND “AS AVAILABLE” BASIS, AND SURETRADERS EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES AND CONDITIONS OF
                                ANY KIND, WHETHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING ALL WARRANTIES OR CONDITIONS OF
                                MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, QUIET ENJOYMENT, ACCURACY, OR
                                NON-INFRINGEMENT. WE (AND OUR SUPPLIERS) MAKE NO WARRANTY THAT THE SERVICES WILL MEET YOUR REQUIREMENTS,
                                WILL BE AVAILABLE ON AN UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE BASIS, OR WILL BE ACCURATE,
                                RELIABLE, FREE OF VIRUSES OR OTHER HARMFUL CODE, COMPLETE, LEGAL, OR SAFE. IF APPLICABLE LAW REQUIRES
                                ANY WARRANTIES WITH RESPECT TO THE SITE, ALL SUCH WARRANTIES ARE LIMITED IN DURATION TO NINETY (90) DAYS
                                FROM THE DATE OF FIRST USE. SURETRADERS DISCLAIMS ANY AND ALL RESPONSIBILITY OR LIABILITY IN RELATION TO
                                THECONTENT MADE AVAILABLE THROUGH THE SERVICES, INCLUDING THE CUSTOMER CONTENT, OR ANY CONTENT OR
                                SERVICES PROVIDED BY THIRD PARTIES. WAZIRX DOES NOT CONTROL OR VET CUSTOMER CONTENT AND IS NOT
                                RESPONSIBLE FOR WHAT USERS POST, TRANSMIT, OR SHARE ON OR THROUGH THE SERVICES. SURETRADERS IS NOT
                                RESPONSIBLE OR LIABLE IN ANY MANNER FOR ANY THIRD-PARTY SERVICES</p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <p style="padding-left: 39pt;text-indent: 0pt;text-align: justify;">ASSOCIATED WITH OR UTILIZED IN
                                CONNECTION WITH THE SERVICES, INCLUDING THE FAILURE OF ANY SUCH THIRD-PARTY SERVICES OR SUPPORTED
                                PLATFORMS.</p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            <p style="padding-left: 39pt;text-indent: 0pt;text-align: justify;">SOME JURISDICTIONS DO NOT ALLOW THE
                                EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSION MAY NOT APPLY TO YOU. SOME JURISDICTIONS DO NOT
                                ALLOW LIMITATIONS ON HOW LONG AN IMPLIED WARRANTY LASTS, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.
                            </p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="11.">
                            <h2 style="padding-left: 34pt;text-indent: -28pt;text-align: left;"><a name="bookmark8">LIMITATION ON
                                    LIABILITY</a></h2>
                        </li>
                    </ol>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 39pt;text-indent: 0pt;text-align: justify;">TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE
                        LAW, IN NO EVENT WILL SURETRADERS, ITS AFFILIATES AND THEIR RESPECTIVE SHAREHOLDERS, MEMBERS, DIRECTORS,
                        OFFICERS, EMPLOYEES, ATTORNEYS, AGENTS, REPRESENTATIVES, SUPPLIERS OR CONTRACTORS BE LIABLE FOR ANY INCIDENTAL,
                        INDIRECT, SPECIAL, PUNITIVE, CONSEQUENTIAL OR SIMILAR DAMAGES OR LIABILITIES WHATSOEVER (INCLUDING, WITHOUT
                        LIMITATION, DAMAGES FOR LOSS OF DATA, INFORMATION, REVENUE, PROFITS OR OTHER BUSINESS OR FINANCIAL BENEFIT)
                        ARISING OUT OF OR IN CONNECTION WITH THE SERVICES, ANY PERFORMANCE OR NON-PERFORMANCE OF THE SERVICES, OR ANY
                        OTHER PRODUCT, SERVICE OR OTHER ITEM</p>
                    <p style="padding-top: 3pt;padding-left: 39pt;text-indent: 0pt;text-align: left;">PROVIDED BY OR ON BEHALF OF
                        SURETRADERS AND ITS AFFILIATES, WHETHER UNDER CONTRACT, STATUTE, STRICT LIABILITY OR OTHER THEORY EVEN IF
                        SURETRADERS HAS BEEN ADVISED OF THEPOSSIBILITY OF SUCH DAMAGES EXCEPT TO THE EXTENT OF A FINAL JUDICIAL
                        DETERMINATION THAT SUCH DAMAGES WERE A RESULT OF SURETRADERS’S GROSS NEGLIGENCE,FRAUD, WILLFUL MISCONDUCT OR
                        INTENTIONAL VIOLATION OF LAW. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR
                        CONSEQUENTIALDAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU. NOTWITHSTANDING THE FOREGOING, IN NO EVENT
                        WILL THE LIABILITY OF SURETRADERS, ITS AFFILIATES AND THEIR RESPECTIVE SHAREHOLDERS, MEMBERS, DIRECTORS,
                        OFFICERS, EMPLOYEES, ATTORNEYS, AGENTS, REPRESENTATIVES, SUPPLIERS OR CONTRACTORS ARISING OUT OF OR IN
                        CONNECTION THE SERVICES, ANY PERFORMANCE OR NON-PERFORMANCE OF THE SERVICES, OR ANY OTHER PRODUCT, SERVICE OR
                        OTHER ITEM PROVIDED BY OR ON BEHALF OF SURETRADERS OR ITS AFFILIATES WHETHER UNDER CONTRACT, STATUTE, STRICT
                        LIABILITY OR OTHER THEORY, EXCEED THE AMOUNT OF THE FEES PAID BY YOU TO SURETRADERS UNDER THIS AGREEMENT IN THE
                        TWELVE-MONTH PERIOD IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO THE CLAIM FOR LIABILITY.</p>
                    <p style="padding-top: 3pt;padding-left: 125pt;text-indent: 0pt;text-align: left;">representations made by us)
                        through final and binding arbitration. You agree to first give us an opportunity to resolve any claims by
                        contacting us on our website / mobile / desktop applications. If we are not able to resolve your claims within
                        60 days of receiving the notice, you may seek relief through arbitration as set forth below.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <p style="padding-left: 125pt;text-indent: 0pt;text-align: justify;">Either you or Zanmai may submit a dispute
                        (after having made good faith efforts to resolve such dispute) for final and binding resolution by arbitration
                        under the arbitration rules of the Singapore International Arbitration Centre (“SIAC”), which are deemed to be
                        incorporated into these Terms by reference, read with the Indian Arbitration and Conciliation Act, 1996. The
                        arbitration tribunal shall consist of a sole arbitrator to be appointed by the President of SIAC. The language
                        of the arbitration hearings shall be English and the seat of arbitration shall be Singapore.</p>
                    <p style="text-indent: 0pt;text-align: left;"><br /></p>
                    <ol id="l10">
                        <li data-list-text="(a)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Waiver of Jury Trial. <span
                                    class="p">THE PARTIES HEREBY WAIVE THEIR RIGHTS TO GO TO COURT</span></p>
                            <p style="padding-left: 96pt;text-indent: 0pt;text-align: left;">AND HAVE A TRIAL IN FRONT OF A JUDGE OR A
                                JURY, as applicable, instead electing that all claims and disputes shall be resolved by arbitration
                                under this Arbitration Agreement. Arbitration procedures are typically more limited, more efficient and
                                less costly than rules applicable in a court and are subject to very limited review by a court. In the
                                event any litigation should arise between you and WazirX in any state or federal court in a suit to
                                vacate or enforce an arbitration award or otherwise, YOU AND WAZIRX WAIVE ALL RIGHTS TO A JURY TRIAL,
                                instead electing that the dispute be resolved by a judge.</p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(b)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Waiver of Class or
                                Consolidated Actions<span class="p">. ALL CLAIMS AND DISPUTES WITHIN THE SCOPE OF THIS ARBITRATION
                    AGREEMENT MUST BE ARBITRATED OR LITIGATED ON AN INDIVIDUAL BASIS AND NOT ON A CLASS BASIS, AND
                    CLAIMS OF MORE THAN ONE CUSTOMER OR USER CANNOT BE ARBITRATED OR LITIGATED JOINTLY OR CONSOLIDATED
                    WITH THOSE OF ANY OTHER CUSTOMER OR USER.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(c)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Confidentiality. <span
                                    class="p">All aspects of the arbitration proceeding, including but not limited to the award of the
                    arbitrator and compliance therewith, shall be strictly confidential. The parties agree to maintain
                    confidentiality unless otherwise required by law. This paragraph shall not prevent a party from
                    submitting to a court of law any information necessary to enforce this Agreement, to enforce an
                    arbitration award, or to seek injunctive or equitable relief.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(d)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: justify;">Severability. <span
                                    class="p">If any part or parts of this Arbitration Agreement are found under the law to be invalid
                    or unenforceable by a court of competent jurisdiction, then such specific part or parts shall be of
                    no force and effect and shall be severed and the remainder of the Agreement shall continue in full
                    force and effect.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(e)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Right to Waive. <span
                                    class="p">Any or all of the rights and limitations set forth in this Arbitration Agreement may be
                    waived by the party against whom the claim is asserted. Such waiver shall not waive or affect any
                    other portion of this Arbitration Agreement.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(f)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Survival of Agreement. <span
                                    class="p">This Arbitration Agreement will survive the termination of your relationship with
                    WazirX.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(g)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Emergency Equitable
                                Relief<span class="p">. Notwithstanding the foregoing, either party may seek emergency equitable relief
                    before a court of competent jurisdiction in order to maintain the status quo pending arbitration. A
                    request for interim measures shall not be deemed a waiver of any other rights or obligations under
                    this Arbitration Agreement.</span></p>
                            <p style="text-indent: 0pt;text-align: left;"><br /></p>
                        </li>
                        <li data-list-text="(h)">
                            <p class="s6" style="padding-left: 96pt;text-indent: -28pt;text-align: left;">Claims Not Subject to
                                Arbitration. <span class="p">Notwithstanding the foregoing, claims of defamation and infringement or
                    misappropriation of the other party’s patent, copyright, trademark or trade secrets shall not be
                    subject to this Arbitration Agreement.</span></p>
                        </li>
                    </ol>
                    <ol id="l11">
                        <ol id="l12">
                            <li data-list-text="11.2">
                                <h2 style="padding-top: 3pt;padding-left: 68pt;text-indent: -28pt;text-align: justify;">Applicable Law.
                                    <span class="p">The laws of India excluding its choice of law provisions, will govern these Terms
                        and any dispute that arises between you and Zanmai.</span></h2>
                                <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            </li>
                            <li data-list-text="11.3">
                                <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Force Majeure. <span
                                        class="p">WazirX is not responsible for damages caused by delay or failure to perform
                        undertakings under this Agreement when the delay or failure is due to fires; strikes; floods;
                        power outages or failures; acts of God or the state’s enemies; lawful acts of public
                        authorities; any and all market movements, shifts, or volatility; computer, server, or Internet
                        malfunctions; security breaches or cyberattacks; criminal acts; delays or defaults caused by
                        common carriers; acts or omissions of third parties; or, any other delays, defaults, failures or
                        interruptions that cannot reasonably be foreseen or provided against. In the event of force
                        majeure, WazirX is excused from any and all performance obligations and this Agreement shall be
                        fully and conclusively at an end.</span></h2>
                                <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            </li>
                            <li data-list-text="11.4">
                                <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Electronic Communications. <span
                                        class="p">The communications between you and WazirX use electronic means, whether you use the
                        Site or send us emails, or whether WazirX posts notices on the Site or communicates with you via
                        email. For contractual purposes, you (a) consent to receive communications from WazirX in an
                        electronic form; and (b) agree that all terms and conditions, agreements, notices, disclosures,
                        and other communications that WazirX provides to you electronically satisfy any legal
                        requirement that such communications would satisfy if it were be in a hardcopy writing. The
                        foregoing does not affect your non-waivable rights.</span></h2>
                                <p style="text-indent: 0pt;text-align: left;"><br /></p>
                            </li>
                            <li data-list-text="11.5">
                                <h2 style="padding-left: 68pt;text-indent: -28pt;text-align: justify;">Entire Agreement. <span
                                        class="p">This Agreement, together with WazirX’s Privacy Policy, constitute the entire agreement
                        between you and us regarding the use of the Services. Our failure to exercise or enforce any
                        right or provision of this Agreement shall not operate as a waiver of such right or provision.
                        The section titles in this Agreement are for convenience only and have no legal or contractual
                        effect. The word “including” means “including without limitation”. If any provision of this
                        Agreement is, for any reason, held to be invalid or unenforceable, the other provisions of this
                        Agreement will be unimpaired and the invalid or unenforceable provision will be deemed modified
                        so that it is valid and enforceable to the maximum extent permitted by law. Your relationship to
                        WazirX is that of an independent contractor, and neither party is an agent or partner of the
                        other. This Agreement, and your rights and obligations herein, may not be assigned,
                        subcontracted, delegated, or otherwise transferred by you without WazirX’s prior written
                        consent, and any attempted assignment, subcontract, delegation, or transfer in violation of the
                        foregoing will be null and void. WazirX may freely assign this Agreement. The terms and
                        conditions set forth in this Agreement shall be binding upon assignees.</span></h2>
                            </li>
                        </ol>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Terms of Services Ends -->
@endsection
