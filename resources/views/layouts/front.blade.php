<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="robots" content="index, follow">
    <meta name="description" content="A commercial website that lists trade signals, exchanges & others crypto related info.">
    <meta name="keywords" content="best crypto signals website,free crypto signals telegram 2022, daily cryptocurrency trading signals, best crypto signals telegram, best free crypto signals, free crypto signals app, best paid crypto signals">
    <meta name="theme-color" content="#4b6fff">
    <title>SureTraders | best crypto signals website</title>
    <!-- Bootstrap -->
    <link href="{{asset('/front_assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="{{asset('/front_new/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Flat Icon CSS -->
    <link href="{{asset('/front_assets/css/magnific-popup.css')}}" rel="stylesheet">
    <!-- Responsive Menu css -->
    <link rel="stylesheet" href="{{asset('/front_assets/css/select2.min.css')}}">
    <!-- Slick Slider CSS -->
    <link href="{{asset('/front_assets/css/style.css')}}" rel="stylesheet">
    <!-- Magnific Popup CSS -->
    <link href="{{asset('/front_assets/css/skins/orange.css')}}" rel="stylesheet">

    <script src="{{{asset('/front_assets/js/modernizr.js')}}}"></script>
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="/front_assets/images/logo.png">
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }
    </style>

    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

</head>

<body>
<!-- SVG Preloader Starts -->
<div id="preloader">
    <div id="preloader-content">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="150px" height="150px"
             viewBox="100 100 400 400" xml:space="preserve">
                <filter id="dropshadow" height="130%">
                    <feGaussianBlur in="SourceAlpha" stdDeviation="5"/>
                    <feOffset dx="0" dy="0" result="offsetblur"/>
                    <feFlood flood-color="red"/>
                    <feComposite in2="offsetblur" operator="in"/>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            <path class="path" fill="#000000" d="M446.089,261.45c6.135-41.001-25.084-63.033-67.769-77.735l13.844-55.532l-33.801-8.424l-13.48,54.068
                    c-8.896-2.217-18.015-4.304-27.091-6.371l13.568-54.429l-33.776-8.424l-13.861,55.521c-7.354-1.676-14.575-3.328-21.587-5.073
                    l0.034-0.171l-46.617-11.64l-8.993,36.102c0,0,25.08,5.746,24.549,6.105c13.689,3.42,16.159,12.478,15.75,19.658L208.93,357.23
                    c-1.675,4.158-5.925,10.401-15.494,8.031c0.338,0.485-24.579-6.134-24.579-6.134l-9.631,40.468l36.843,9.188
                    c8.178,2.051,16.209,4.19,24.098,6.217l-13.978,56.17l33.764,8.424l13.852-55.571c9.235,2.499,18.186,4.813,26.948,6.995
                    l-13.802,55.309l33.801,8.424l13.994-56.061c57.648,10.902,100.998,6.502,119.237-45.627c14.705-41.979-0.731-66.193-31.06-81.984
                    C425.008,305.984,441.655,291.455,446.089,261.45z M368.859,369.754c-10.455,41.983-81.128,19.285-104.052,13.589l18.562-74.404
                    C306.28,314.65,379.774,325.975,368.859,369.754z M379.302,260.846c-9.527,38.187-68.358,18.781-87.442,14.023l16.828-67.489
                    C327.767,212.14,389.234,221.02,379.302,260.846z"/>
            </svg>
    </div>
</div>
<!-- SVG Preloader Ends -->
<!-- Wrapper Starts -->
<div class="wrapper">
    <!-- Header Starts -->
    <header class="header">
        <div class="container">
            <div class="row">
                <!-- Logo Starts -->
                <div class="main-logo col-xs-12 col-md-3 col-md-2 col-lg-2 hidden-xs">
                    <a href="/">
                        <img class="img-responsive" src="front_assets/images/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Logo Ends -->
                <!-- Statistics Starts -->
                <div class="col-md-7 col-lg-7">
                    <ul class="unstyled bitcoin-stats text-center">
                        <li>
                            <div class="btcwdgt-price" data-bw-theme="light" data-bw-cur="usd"></div>
                            <span>Live Bitcoin price</span>
                        </li>
                    </ul>
                </div>
                <!-- Statistics Ends -->
                <!-- User Sign In/Sign Up Starts -->
                <div class="col-md-3 col-lg-3">
                    <ul class="unstyled user">
                        <li class="sign-in"><a href="/login" class="btn btn-primary"><i class="fa fa-user"></i> sign in</a>
                        </li>
                        <li class="sign-up"><a href="/register" class="btn btn-primary"><i class="fa fa-user-plus"></i>
                                register</a></li>
                    </ul>
                </div>
                <!-- User Sign In/Sign Up Ends -->
            </div>
        </div>
        <!-- Navigation Menu Starts -->
        <nav class="site-navigation navigation" id="site-navigation">
            <div class="container">
                <div class="site-nav-inner">
                    <!-- Logo For ONLY Mobile display Starts -->
                    <a class="logo-mobile" href="/">
                        <img class="img-responsive" src="front_assets/images/logo.png" alt="">
                    </a>
                    <!-- Logo For ONLY Mobile display Ends -->
                    <!-- Toggle Icon for Mobile Starts -->
                    <button type="button" class="navbar-toggle hide" data-toggle="collapse"
                            data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Toggle Icon for Mobile Ends -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Main Menu Starts -->
                    {{--                        <ul class="nav navbar-nav">--}}
                    {{--                            <li class="active"><a href="index.html">Home</a></li>--}}
                    {{--                            <li><a href="about.html">About Us</a></li>--}}
                    {{--                            <li><a href="services.html">Services</a></li>--}}


                    {{--                            <li><a href="contact.html">Contact</a></li>--}}
                    {{--                            <!-- Cart Icon Starts -->--}}
                    {{--                            <li class="cart"><a href="shopping-cart.html"><i class="fa fa-shopping-cart"></i></a></li>--}}
                    {{--                            <!-- Cart Icon Starts -->--}}
                    {{--                            <!-- Search Icon Starts -->--}}
                    {{--                            <li class="search"><button class="fa fa-search"></button></li>--}}
                    {{--                            <!-- Search Icon Ends -->--}}
                    {{--                        </ul>--}}
                    <!-- Main Menu Ends -->
                    </div>
                </div>
            </div>
            <!-- Search Input Starts -->
            <div class="site-search">
                <div class="container">
                    <input type="text" placeholder="type your keyword and hit enter ...">
                    <span class="close">×</span>
                </div>
            </div>
            <!-- Search Input Ends -->
        </nav>
        <!-- Navigation Menu Ends -->
    </header>
    <!-- Header Ends -->
@yield('content')
<!-- Footer Starts -->
    <footer class="footer">
        <!-- Footer Top Area Starts -->
        <div class="top-footer">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Starts -->
                    <div class="col-sm-4 col-md-4">
                        <h4>Our Company</h4>
                        <div class="menu">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/about">About</a></li>
                                <li><a href="/services">Services</a></li>

                            </ul>
                        </div>
                    </div>
                    <!-- Footer Widget Ends -->
                    <!-- Footer Widget Starts -->
                    <div class="col-sm-4 col-md-4">
                        <h4>Help & Support</h4>
                        <div class="menu">
                            <ul>
                                <li><a href="/terms">Terms & Conditions</a></li>
                                <li><a href="/register">Register</a></li>
                                <li><a href="/login">Login</a></li>
                                <li><a href="/contact">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Footer Widget Ends -->
                    <!-- Footer Widget Starts -->
                    <div class="col-sm-4 col-md-4">
                        <h4>Contact Us </h4>
                        <div class="contacts">
                            <div>
                                <span>info@suretraders.io</span>
                            </div>
                            <div>

                            </div>
                            <div>
                                <span>mon-sat 08am &#x21FE; 05pm</span>
                            </div>
                        </div>
                        <!-- Social Media Profiles Starts -->
                        <div class="social-footer">
                            <ul>
                                <li><a href="https://www.facebook.com/Sure-Traders-103239202262309" target="_blank"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/Suretraders3" target="_blank"><i
                                            class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/suretradersofficial" target="_blank"><i
                                            class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <!-- Social Media Profiles Ends -->
                        <hr>
                        <!-- Supported Payment Cards Logo Starts -->
                        <div class="payment-logos">
                            <h4 class="payment-title">supported payment method</h4>
                            <p>Crypto</p>
                        </div>
                    </div>
                    <!-- Footer Widget Ends -->

                </div>
            </div>
        </div>
        <!-- Footer Top Area Ends -->
        <!-- Footer Bottom Area Starts -->
        <div class="bottom-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- Copyright Text Starts -->
                        <p class="text-center">Copyright © {{ date("Y") }} Sure Traders All Rights Reserved </p>
                        <!-- Copyright Text Ends -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Bottom Area Ends -->
    </footer>
    <!-- Footer Ends -->
    <!-- Back To Top Starts  -->
    <a href="#" id="back-to-top" class="back-to-top fa fa-arrow-up"></a>
    <!-- Back To Top Ends  -->
    <!-- jQuery -->
    <script src="{{asset('/front_assets/js/jquery-2.2.4.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('/front_assets/js/bootstrap.min.js')}}"></script>
    <!-- Slick Slider -->
    <script src="{{asset('/front_assets/js/select2.min.js')}}"></script>
    <!-- Smooth Scroll -->
    <script src="{{asset('/front_assets/js/jquery.magnific-popup.min.js')}}"></script>
    <!--Responsive Menu js -->
    <script src="{{{asset('/front_assets/js/custom.js')}}}"></script>

</body>
</html>
