const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .copyDirectory('resources/assets/img', 'public/img')
    .copyDirectory('resources/front', 'public/front')
    .copyDirectory('resources/front_new', 'public/front_new')
    .copyDirectory('resources/front_assets', 'public/front_assets')
    .copyDirectory('resources/landing', 'public/landing')
    .webpackConfig(require('./webpack.config'));

if (mix.inProduction()) {
    mix.version();
}
