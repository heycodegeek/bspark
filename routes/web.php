<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\Admin\AdminMetaverseController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AdminUserWalletController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\Auth\AdminAuthenticatedSessionController;
use App\Http\Controllers\BonusController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DepositController;
use App\Http\Controllers\GatewayApiController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MetaTokenController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\StakePurchaseController;
use App\Http\Controllers\StakingController;
use App\Http\Controllers\TncVerificationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserMetaverseWalletController;
use App\Http\Controllers\UserSmartMetaController;
use App\Http\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'showHome'])->name('home');
Route::get('about', [HomeController::class, 'showAbout'])->name('about');
Route::get('services', [HomeController::class, 'showServices'])->name('services');
Route::get('contact', [HomeController::class, 'contact'])->name('contact');
Route::post('contact-submit', [HomeController::class, 'contactPost'])->name('contact.post');
Route::get('system-under-updation', [HomeController::class, 'systemUpdation'])->name('updation');
Route::get('terms', [HomeController::class, 'showTnC'])->name('tnc');
Route::get('privacy-policy', [HomeController::class, 'showPrivacyPolicy'])->name('privacy');
Route::post('validate/user', [UserController::class, 'isUserExistByRefCode'])->name('validate.user');
Route::get('get-countries', [CountryController::class, 'getCountries'])->name('countries.get');
Route::get('refer/{ref_code?}', [HomeController::class, 'showReferPage'])->name('refer.page');


Route::get('buddy/{user_id?}', [HomeController::class, 'buddyBotBusiness']);
Route::get('level/{user_id?}', [AdminUserController::class, 'check']);

Route::get('gateway/callback', [GatewayApiController::class, 'processRequest'])->name('gateway.callback');

require __DIR__ . '/auth.php';


Route::prefix('admin')->name('admin.')->group(function () {

    Route::get('login', [AdminAuthenticatedSessionController::class, 'create'])->name('login.create');
    Route::post('login', [AdminAuthenticatedSessionController::class, 'store'])->name('login.store');

    Route::middleware(['auth:admin'])->group(function () {
        Route::get('logout', [AdminAuthenticatedSessionController::class, 'destroy'])->name('logout');
        Route::get('dashboard', [AdminDashboardController::class, 'showDashboard'])->name('dashboard');
        Route::get('subscription-pool-distribute', [AdminDashboardController::class, 'distributeSubscriptionPool'])->name('pool.distribute');

        Route::prefix('users')->name('users.')->group(function () {
            Route::get('/', [AdminUserController::class, 'showAllUsersPage'])->name('index');
            Route::post('get-users', [AdminUserController::class, 'getUsers'])->name('get_users');
            Route::post('filter-users', [AdminUserController::class, 'filterUsers'])->name('filter_users');
        });

        Route::prefix('staking')->name('staking.')->group(function () {
            Route::get('list', [StakingController::class, 'adminStakingList'])->name('list');
            Route::get('list-get', [StakingController::class, 'adminGetStakingList'])->name('list.get');

            Route::get('bonus', [StakingController::class, 'adminStakingBonus'])->name('bonus');
            Route::get('bonus-get', [StakingController::class, 'adminGetStakingBonus'])->name('bonus.get');
            Route::post('bonus-distribute', [StakingController::class, 'adminBonusDistribute'])->name('bonus.save');
            Route::post('add-bonus-in-pool', [StakingController::class, 'adminBonusPoolAdd'])->name('bonus.pool');

        });
        Route::prefix('withdrawal')->name('withdrawal.')->group(function () {
            Route::get('/', [AdminUserController::class, 'showWithdrawalHistory'])->name('index');
//            Route::get('/pdf', [AdminUserController::class, 'exportPdf'])->name('export_pdf');
            Route::post('withdrawal-history', [AdminUserController::class, 'withdrawalHistory'])->name('withdrawal_history');
            Route::post('filter-withdrawal-history', [AdminUserController::class, 'filterWithdrawalHistory'])->name('filter_withdrawal_history');
            Route::post('status-filter-withdrawal-history', [AdminUserController::class, 'statusFilterWithdrawalHistory'])->name('status_filter_withdrawal_history');
//            Route::post('/process', [AdminUserController::class, 'processWithdrawal'] )->name('process');
        });

        Route::prefix('metaverse')->name('metaverse.')->group(function () {
            Route::get('holders', [AdminMetaverseController::class, 'Index'])->name('holders');
            Route::get('get-holders', [AdminMetaverseController::class, 'getMetaUser'])->name('holders.get');

            Route::get('buddy-bot', [AdminMetaverseController::class, 'metaPool'])->name('buddy.bot');
            Route::post('update-meta-price', [AdminMetaverseController::class, 'addUsdInMetaPool'])->name('update.meta.price');

            Route::get('eligibility', [AdminMetaverseController::class, 'viewMetaEligibility'])->name('eligibility');
//            Route::post('eligibility-check', [AdminMetaverseController::class, 'checkMetaEligibility'])->name('eligibility.check');

            Route::post('release-meta-token-to-user', [AdminMetaverseController::class, 'releaseMetaTokenToUser'])->name('token.release');

        });

        Route::prefix('user')->name('user.')->group(function () {
            Route::get('/{user}', [AdminUserController::class, 'showUser'])->name('create');
            Route::post('update-profile', [AdminUserController::class, 'updateProfile'])->name('store');
            Route::post('add-fund', [AdminUserWalletController::class, 'addFund'])->name('add_fund');

            Route::get('user-panel/{user}', [AdminUserController::class, 'loginByUserId'])->name('access.panel');
        });

    });
});
Route::middleware(['auth', 'otp_verified'])->group(function () {
    Route::get('verify-tnc', [TncVerificationController::class, 'Index'])->name('verification.tnc');
    Route::post('accept-tnc', [TncVerificationController::class, 'saveTncCheckStatus'])->name('tnc.accept');
});
Route::middleware(['auth', 'otp_verified', 'tnc_verified'])->group(function () {

    Route::prefix('account')->name('account.')->group(function () {
        Route::get('profile', [AccountController::class, 'index'])->name('profile');
        Route::get('change-password', [AccountController::class, 'showChangePassword'])->name('change.password');
        Route::post('update-password', [AccountController::class, 'updatePassword'])->name('update.password');
        Route::post('update-profile', [AccountController::class, 'updateProfile'])->name('update.profile');
        Route::post('update-profile-active', [AccountController::class, 'updateActiveProfile'])->name('update.profile.active');
    });


    Route::get('dashboard', [DashboardController::class, 'create'])->name('dashboard');
//    Route::prefix('deposit')->name('deposit.')->group(function () {
//        Route::get('/create', [DepositController::class, 'create'])->name('create');
//        Route::post('/store', [DepositController::class, 'store'])->name('store');
//        Route::get('tron-deposit', [DepositController::class, 'showTronForm'])->name('tron.deposit.form');
//        Route::get('btc-deposit', [DepositController::class, 'showBitcoinForm'])->name('btc.deposit.form');
//        Route::post('deposit-summary', [DepositController::class, 'getDepositSummary'])->name('summary');
//    });

    Route::post('/invoice', [InvoiceController::class, 'store'])->name('invoice.create');
    Route::get('/add-fund', [PurchaseController::class, 'addFundInWallet'])->name('add.fund');
    Route::get('/add-fund-form/{invoice}', [PurchaseController::class, 'showAddFundForm'])->name('add.fund.form');

    Route::post('trx-paid', [DepositController::class, 'trxPaid'])->name('trx.paid');


    Route::prefix('package')->name('package.')->group(function () {
        Route::get('pricing', [PurchaseController::class, 'showPricing'])->name('pricing');
        Route::get('stake-pricing', [StakePurchaseController::class, 'showStakePricing'])->name('pricing.stake');
        Route::get('buy/{coin_plan}', [PurchaseController::class, 'buy'])->name('buy');

        Route::get('staking/{stake_coin_plan}', [StakePurchaseController::class, 'staking'])->name('staking.show');
        Route::post('buy', [PurchaseController::class, 'buyPackage'])->name('buy.attempt');
        Route::post('renew', [PurchaseController::class, 'renewPackage'])->name('buy.attempt.renew');
        Route::post('buy-delegator-plan', [PurchaseController::class, 'buyDelegatorPlan'])->name('delegator.buy');

        Route::post('buy-staking', [StakePurchaseController::class, 'buyStaking'])->name('buy.staking.attempt');


    });
    Route::prefix('partners')->name('partners.')->group(function () {
        Route::get('direct', [PartnerController::class, 'showDirectPartner'])->name('direct');
        Route::get('get-direct-list', [PartnerController::class, 'getDirectPartners'])->name('direct.list');
        Route::get('genealogy/{user_id?}', [PartnerController::class, 'showGenealogy'])->name('geneaology');
        Route::post('search-downline', [PartnerController::class, 'seachDownline'])->name('search.downline');
    });
    Route::prefix('history')->name('history.')->group(function () {
        Route::get('deposit', [HistoryController::class, 'showDepositHistory'])->name('deposit');
        Route::get('withdrawal', [HistoryController::class, 'showWithdrawalHistory'])->name('withdrawal');
        Route::get('level-staking-bonus', [HistoryController::class, 'showLevelStakingBonus'])->name('level.staking.bonus');

        Route::get('smart-meta-transaction', [UserSmartMetaController::class, 'showSmartMetaTxnHistory'])->name('smart_meta_transaction');
        Route::get('get-smart-meta-transaction', [UserSmartMetaController::class, 'getUserSmartMetaTxnHistory'])->name('get.smart_meta_transaction');

        Route::get('get-level-staking-bonus', [HistoryController::class, 'getLevelStakingBonus'])->name('get.level.staking.bonus');


    });

    Route::prefix('bonus')->name('bonus.')->group(function () {
        Route::get('binary', [BonusController::class, 'showBinaryBonus'])->name('binary');
        Route::get('binary-payout-list', [BonusController::class, 'getBinaryPayoutList'])->name('binaryList');
        Route::get('matching', [BonusController::class, 'showMatchingBonus'])->name('matching');
        Route::get('matching-payout-list', [BonusController::class, 'getMatchingPayoutList'])->name('matchingList');
        Route::get('direct', [BonusController::class, 'showDirectBonus'])->name('direct');
        Route::get('get-direct-list', [BonusController::class, 'getLevelIncomeList'])->name('directList');
        Route::get('level/{level_income_id}', [BonusController::class, 'showLevelIncomeStat'])->name('level.stats.show');
        Route::get('staking', [BonusController::class, 'showStaking'])->name('staking');
        Route::get('get-stake-incomes', [BonusController::class, 'getStakeEarnings'])->name('stake.get');

        Route::get('delegator-bonus', [BonusController::class, 'showDelegatorBonus'])->name('delegator');
        Route::get('get-delegator-bonus', [BonusController::class, 'getDelegatorEarnings'])->name('delegator.get');

        Route::get('delegator-level-bonus', [BonusController::class, 'showDelegatorLevelBonus'])->name('delegator.level');
        Route::get('get-delegator-level-bonus', [BonusController::class, 'getDelegatorLevelEarnings'])->name('delegator.level.get');

        Route::get('international-pool-bonus', [BonusController::class, 'showIPoolBonus'])->name('international.pool');
        Route::get('get-international-pool-bonus', [BonusController::class, 'getIPoolEarnings'])->name('international.pool.get');
    });

    Route::prefix('withdraw')->name('withdraw.')->group(function () {
        Route::get('accounts', [WithdrawController::class, 'showAccounts'])->name('accounts');
        Route::post('update-trx-wallet', [WithdrawController::class, 'updateTrxWallet'])->name('update.trx.wallet');
        Route::post('update-usdt-wallet', [WithdrawController::class, 'updateUsdtWallet'])->name('update.usdt.wallet');
        Route::post('send-otp', [WithdrawController::class, 'sendOtp'])->name('sendOtp');
        Route::get('select-crypto', [WithdrawController::class, 'selectCoin'])->name('select.crypto');
//        Route::get('sure', [WithdrawController::class, 'withdrawToken'])->name('token');
        Route::get('trx', [WithdrawController::class, 'withdrawTrx'])->name('trx');
            Route::get('usdt', [WithdrawController::class, 'withdrawUsdt'])->name('usdt');
        Route::post('get-withdraw-conversion', [WithdrawController::class, 'getConversionPrice'])->name('conversion');
        Route::post('submit-trx', [WithdrawController::class, 'withdrawTrxAttempt'])->name('trx.attempt');
        Route::get('withdraw/verify/{withdraw_temp_history}', [WithdrawController::class, 'showOtpForm'])->name('verify');
        Route::post('verify-otp', [WithdrawController::class, 'verifyOtp'])->name('verify.otp.attempt');
    });
    Route::prefix('staking')->name('staking.')->group(function () {
        Route::get('apply', [StakingController::class, 'index'])->name('index');
        Route::post('save', [StakingController::class, 'createStaking'])->name('save');
        Route::get('bonus/{staking_id}', [StakingController::class, 'bonusByStakingId'])->name('bonus');

    });

//    Route::get('apply', [MetaTokenController::class, 'Index'])->name('index');

    Route::prefix('convert')->name('convert.')->group(function () {
        Route::get('/', [MetaTokenController::class, 'Index'])->name('index');
        Route::post('/meta-token', [MetaTokenController::class, 'convertMetaToken'])->name('meta.token');
        Route::get('history', [UserMetaverseWalletController::class, 'Index'])->name('history');
    });

    Route::prefix('smart')->name('smart.')->group(function () {
        Route::get('meta', [UserSmartMetaController::class, 'showSmartMetaSwap'])->name('meta.index');
        Route::post('meta-token', [UserSmartMetaController::class, 'swapSmartMeta'])->name('meta.swap');
        Route::get('history', [UserSmartMetaController::class, 'history'])->name('meta.history');
        Route::get('get-history', [UserSmartMetaController::class, 'getHistory'])->name('meta.history.get');
    });

    Route::get('check-meta', [MetaTokenController::class, 'checkMetaUserEligibility']);
    Route::get('metaverse-income', [UserMetaverseWalletController::class, 'MetaverseIncome'])->name('metaverse.income');
    Route::get('metaverse-income-get', [UserMetaverseWalletController::class, 'GetMetaverseIncome'])->name('get.metaverse.income');

//    Route::prefix('wallet')->name('wallet.')->group(function () {
//        Route::get('transfer', [WalletTransferController::class, 'showTransferPage'])->name('transfer.create');
//        Route::post('transfer', [WalletTransferController::class, 'transfer'])->name('transfer.store');
//        Route::post('transfer/verify', [WalletTransferController::class, 'transferVerify'])->name('transfer.verify');
//        Route::get('transfer/history', [WalletTransferController::class, 'showTransferHistoryPage'])->name('transfer.history.create');
//    });

});
