<?php
return [
    'app_id' => env('ALCHEMY_APP_ID'),
    'token' => env('ALCHEMY_TOKEN'),
    'api_url' => env('ALCHEMY_API_URL'),
    'webhook_id' => env('ALCHEMY_WEBHOOK_ID')
];
