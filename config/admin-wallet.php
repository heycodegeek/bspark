<?php
return [
    'trx' => [
        'address' => env('ADMIN_WITHDRAW_PROCESS_WALLET_ADDRESS_TRX'),
        'priv_key' => env('ADMIN_WITHDRAW_PROCESS_WALLET_KEY_TRX')
    ]

];
