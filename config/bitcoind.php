<?php
return [
    // Bitcoin Core
    'default' => [
        'scheme' => 'http',
        'host' => 'localhost',
        'port' => env('BITCOIN_RPC_PORT'),
        'user' => env('BITCOIN_RPC_USER'),     // required
        'password' => env('BITCOIN_RPC_PASSWORD'), // required
        'ca' => null,
        'timeout' => false,
        'zeromq' => null,
    ],

    // Litecoin Core
    'litecoin' => [
        'scheme' => 'http',
        'host' => 'localhost',
        'port' => 9332,
        'user' => '(rpcuser from litecoin.conf)',     // required
        'password' => '(rpcpassword from litecoin.conf)', // required
        'ca' => null,
        'timeout' => false,
        'zeromq' => null,
    ],
];
