<?php
return [
    'default' => env('NETWORK', 'kovan'),
    'networks' => [
        'mainnet' => [
            'network_id' => 1,
            'websocket_uri' => 'wss://mainnet.infura.io/ws/v3/' . env('INFURA_KEY'),
            'rpc_uri' => 'https://mainnet.infura.io/v3/' . env('INFURA_KEY')
        ],
        'ropsten' => [
            'network_id' => 3,
            'websocket_uri' => 'wss://ropsten.infura.io/ws/v3/' . env('INFURA_KEY'),
            'rpc_uri' => 'https://mainnet.infura.io/v3/' . env('INFURA_KEY')

        ],
        'kovan' => [
            'network_id' => 42,
            'websocket_uri' => 'wss://kovan.infura.io/ws/v3/' . env('INFURA_KEY'),
            'rpc_uri' => 'https://kovan.infura.io/v3/' . env('INFURA_KEY')
        ],
        'goerli' => [
            'network_id' => 5,
            'websocket_uri' => 'wss://goerli.infura.io/ws/v3/' . env('INFURA_KEY'),
            'rpc_uri' => 'https://goerli.infura.io/v3/' . env('INFURA_KEY')
        ],
        'development' => [
            'network_id' => '*',
            'websocket_uri' => 'ws://127.0.0.1:8545',
            'rpc_uri' => 'http://127.0.0.1:8545'
        ]
    ],
    'api_key' => env('INFURA_KEY', null)
];
