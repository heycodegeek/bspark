<?php
return [
    'address' => env('FUND_WALLET_ADDRESS'),
    'priv_key' => env('FUND_WALLET_KEY', null),
    'chain_id' => env('CHAIN_ID', null)
];
