<?php

namespace Database\Seeders;

use App\Models\User;
use App\Mohiqssh\BinaryPayoutMethod;
use Illuminate\Database\Seeder;

class GenerateSelfBinarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::whereNotNull('active_at')->orderBy('active_at')->cursor() as $user) {
            BinaryPayoutMethod::init($user)->generateBinaryBusiness();
        }
    }
}
