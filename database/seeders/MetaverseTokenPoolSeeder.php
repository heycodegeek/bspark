<?php

namespace Database\Seeders;

use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPool;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetaverseTokenPoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::table('user_metaverse_wallets')
            ->select(DB::raw('count(*) as total ,sum(token) as tokens, sum(value_in_usd) as amount'))
            ->first();
        $metaTokemPrice = divDecimalStrings(castDecimalString($data->amount, 10), castDecimalString($data->tokens, 10), 10);
        MetaverseTokenPool::create([
            'released_token' => $data->tokens,
            'available_token' => $data->tokens,
            'burned_token' => '0.00',
            'value_in_usd' => $data->amount,
            'meta_token_price' => $metaTokemPrice
        ]);
        MetaTokenPrice::create([
            'price_in_usd' => $metaTokemPrice
        ]);
    }
}
