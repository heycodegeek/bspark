<?php

namespace Database\Seeders;

use App\Models\CoinPlan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            ['name' => 'Subscription', 'start_price_usd' => '150', 'end_price_usd' => '499', 'rate_daily_percent' => '0', 'rate_daily_decimal' => '0.000'],
        ];

        $binaryPlans = [
            ['rate_percent' => '10.00', 'rate_decimal' => '0.10', 'monthly_limit_usd' => '250000'],
        ];

        $levelPlans = [
            [
                ['level' => 1, 'rate_percent' => '30.00', 'rate_decimal' => '0.30']
            ],
        ];

        foreach ($plans as $plan) {
            $coinPlan = CoinPlan::create($plan);
            $coinPlan->binaryPlan()->create(
                $binaryPlans[$coinPlan->id - 1]
            );
            foreach ($levelPlans[$coinPlan->id - 1] as $levelPlan) {
                $coinPlan->levelPlans()->create($levelPlan);
            }
        }
    }
}
