<?php

namespace Database\Seeders;

use App\Models\User;
use App\Mohiqssh\AirdropSignUpMethod;
use Illuminate\Database\Seeder;

class VerifyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::where('id', '>', 5610)->whereNull('email_verified_at')->cursor() as $user) {
            $user->markEmailAsVerified();
            AirdropSignUpMethod::init($user)->create();
        }
    }
}
