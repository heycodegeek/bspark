<?php

namespace Database\Seeders;

use App\Models\StakePlan;
use Illuminate\Database\Seeder;

class StakePlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stakePlans = [
            ['name' => 'Subscription Package', 'period_in_months' => 12, 'withdraw_limit_monthly_percent' => '100', 'withdraw_limit_monthly_deciaml' => '1500'],
        ];

        foreach ($stakePlans as $stakePlan) {
            StakePlan::create($stakePlan);
        }

    }
}
