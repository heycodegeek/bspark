<?php

namespace Database\Seeders;

use App\Models\DepositTransaction;
use App\Models\StopRoiUser;
use Illuminate\Database\Seeder;

class StopRoiUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (DepositTransaction::where('crypto', 'eth')->cursor() as $dp) {
            $user = $dp->user;
            if (StopRoiUser::where('user_id', $user->id)->doesntExist()) {
                $user->stopRoiUser()->create();
            }
        }
    }
}
