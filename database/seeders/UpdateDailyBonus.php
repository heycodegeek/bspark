<?php

namespace Database\Seeders;

use App\Models\UserIncomeStat;
use Illuminate\Database\Seeder;

class UpdateDailyBonus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (UserIncomeStat::cursor() as $userIncomeStat) {
            $user = $userIncomeStat->user;
            $userCoinWallet = userCoinWalletInstance($user);
            $userCoinWallet->increment('balance', $userIncomeStat->daily_bonus);
        }
    }
}
