<?php

namespace Database\Seeders;

use App\Jobs\GenerateStakeIncomeJob;
use Illuminate\Database\Seeder;

class GenerateStakeIncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GenerateStakeIncomeJob::dispatch()->delay(now()->addSecond());
    }
}
