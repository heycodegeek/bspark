<?php

namespace Database\Seeders;

use App\Models\Coin;
use Illuminate\Database\Seeder;

class CoinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coin = Coin::create([
            'name' => 'eth',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin->coinTypes()->create([
            'name' => 'eth-legacy',
            'type' => 'legacy',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin = Coin::create([
            'name' => 'btc',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin->coinTypes()->create([
            'name' => 'btc-legacy',
            'type' => 'legacy',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);
        $coin->coinTypes()->create([
            'name' => 'btc-segwit',
            'type' => 'p2sh-segwit',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);
        $coin->coinTypes()->create([
            'name' => 'btc-bech32',
            'type' => 'bech32',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin = Coin::create([
            'name' => 'trx',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin->coinTypes()->create([
            'name' => 'trx-legacy',
            'type' => 'legacy',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

    }
}
