<?php

namespace Database\Seeders;

use App\Models\SubscriptionHistory;
use App\Models\User;
use App\Mohiqssh\UserLevelMethods;
use Illuminate\Database\Seeder;

class UpdateLevelIncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (SubscriptionHistory::orderBy('id')->cursor() as $subscriptionHistory) {
            if ($subscriptionHistory->amount_in_usd > '4999') {
                continue;
            }
            $user = $subscriptionHistory->user;
            UserLevelMethods::init($user)->eachParentV1(function ($sponsorUser, $level) use (&$user, &$subscriptionHistory) {
                if (!$this->isUserActive($sponsorUser)) {
                    return;
                }
                $levelPlan = $sponsorUser->subscription->coinPlan->levelPlans()->where('level', $level)->first();
                if (is_null($levelPlan)) {
                    return;
                }
                $userLevelIncome = $sponsorUser->userLevelIncomes()->firstOrCreate(
                    ['income_date' => $subscriptionHistory->created_at->format('Y-m-d')],
                    ['tokens' => castDecimalString('0', 8), 'usd' => castDecimalString('0', 2)]
                );

                $incomeTokens = multipleDecimalStrings($levelPlan->rate_decimal, $subscriptionHistory->stake_tokens, 8);

                $userLevelIncomeStat = $userLevelIncome->userLevelIncomeStats()->where('subscription_history_id', $subscriptionHistory->id)
                    ->where('level', $level)->first();
                if (is_null($userLevelIncomeStat)) {
                    $userLevelIncome->userLevelIncomeStats()->create([
                        'subscription_history_id' => $subscriptionHistory->id,
                        'level' => $level,
                        'income_tokens' => $incomeTokens,
                        'usd' => multipleDecimalStrings($incomeTokens, $subscriptionHistory->token_price, 2)
                    ]);

                    $userLevelIncome->increment('tokens', $incomeTokens);
                    $userLevelIncome->increment('usd', multipleDecimalStrings($incomeTokens, $subscriptionHistory->token_price, 2));

                    $userCoinWallet = userCoinWalletInstance($sponsorUser);
                    $userCoinWallet->increment('balance', $incomeTokens);

                    $userIncomeStat = userIncomeStatInstance($sponsorUser);
                    $userIncomeStat->increment('direct_bonus', $incomeTokens);
                    $userIncomeStat->increment('total_bonus', $incomeTokens);
                }


            }, 4);
        }
    }

    private function isUserActive(User $user): bool
    {
        return !is_null($user->active_at);
    }
}
