<?php

namespace Database\Seeders;

use App\Models\FundWallet;
use Illuminate\Database\Seeder;

class FundWalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FundWallet::create([
            'name' => 'main',
            'address' => config('fund-wallet.address'),
            'priv_key' => config('fund-wallet.priv_key'),
            'nonce' => 0,
            'status' => 'active'
        ]);
    }
}
