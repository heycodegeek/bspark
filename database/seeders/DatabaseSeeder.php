<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountryTableSeeder::class,
            TokenPriceTableSeeder::class,
            PlanSeeder::class,
            StakePlanTableSeeder::class,
            CoinSeeder::class,
            CoinMetricTableSeeder::class,
            UserTableSeeder::class,
            SubscriptionPoolSeeder::class,
            AdminFundWalletSeeder::class,
            WithdrawalSeeder::class,
            AdminTableSeeder::class,
        ]);
    }
}
