<?php

namespace Database\Seeders;

use App\Models\PromotionalRewardsPlan;
use Illuminate\Database\Seeder;

class PromotionalRewardsPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promotionalRewards = [
            ['team_business' => '5000.00', 'reward_amount' => '250.00'],
            ['team_business' => '20000.00', 'reward_amount' => '1000.00'],
            ['team_business' => '50000.00', 'reward_amount' => '2500.00'],
            ['team_business' => '75000.00', 'reward_amount' => '3750.00'],
            ['team_business' => '100000.00', 'reward_amount' => '5000.00'],
            ['team_business' => '150000.00', 'reward_amount' => '7500.00'],
            ['team_business' => '250000.00', 'reward_amount' => '15000.00'],
            ['team_business' => '500000.00', 'reward_amount' => '25000.00'],
            ['team_business' => '1000000.00', 'reward_amount' => '50000.00'],
            ['team_business' => '2500000.00', 'reward_amount' => '100000.00'],
        ];

        PromotionalRewardsPlan::insert($promotionalRewards);
    }
}
