<?php

namespace Database\Seeders;

use App\Models\CityOnePlan;
use Illuminate\Database\Seeder;

class CityOnePlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cityCoinPlans = [
            ['name' => 'EXECUTIVE', 'price_in_usd' => '50.00', 'rate_daily_percent' => '0.00', 'rate_daily_decimal' => '0.00', 'booster_daily_percent' => '0.00', 'booster_daily_decimal' => '0.00', 'max_amount_limit_percent' => '200.00', 'booster_amount_limit_percent' => '300.00'],
            ['name' => 'SILVER', 'price_in_usd' => '150.00', 'rate_daily_percent' => '0.500', 'rate_daily_decimal' => '0.00500', 'booster_daily_percent' => '1.00', 'booster_daily_decimal' => '0.010', 'max_amount_limit_percent' => '200.00', 'booster_amount_limit_percent' => '300.00'],
            ['name' => 'GOLD', 'price_in_usd' => '550.00', 'rate_daily_percent' => '0.500', 'rate_daily_decimal' => '0.00500', 'booster_daily_percent' => '1.00', 'booster_daily_decimal' => '0.010', 'max_amount_limit_percent' => '200.00', 'booster_amount_limit_percent' => '300.00'],
            ['name' => 'PLATINUM', 'price_in_usd' => '1150.00', 'rate_daily_percent' => '0.500', 'rate_daily_decimal' => '0.00500', 'booster_daily_percent' => '1.00', 'booster_daily_decimal' => '0.010', 'max_amount_limit_percent' => '200.00', 'booster_amount_limit_percent' => '300.00'],
        ];

        CityOnePlan::insert($cityCoinPlans);
    }
}
