<?php

namespace Database\Seeders;

use App\Models\StakingTeamDistributionPlan;
use Illuminate\Database\Seeder;

class StakingTeamDistributionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            ['level' => 1, 'team_business' => '0.00', 'rate_percent' => '20.00', 'rate_decimal' => '0.20'],
            ['level' => 2, 'team_business' => '2000.00', 'rate_percent' => '10.00', 'rate_decimal' => '0.10'],
            ['level' => 3, 'team_business' => '2000.00', 'rate_percent' => '8.00', 'rate_decimal' => '0.08'],
            ['level' => 4, 'team_business' => '5000.00', 'rate_percent' => '3.00', 'rate_decimal' => '0.03'],
            ['level' => 5, 'team_business' => '5000.00', 'rate_percent' => '3.00', 'rate_decimal' => '0.03'],
            ['level' => 6, 'team_business' => '7000.00', 'rate_percent' => '3.00', 'rate_decimal' => '0.03'],
            ['level' => 7, 'team_business' => '7000.00', 'rate_percent' => '3.00', 'rate_decimal' => '0.03'],
        ];

        foreach ($plans as $plan) {
            StakingTeamDistributionPlan::create($plan);
        }
    }
}
