<?php

namespace Database\Seeders;

use App\Models\Tree;
use App\Models\User;
use Illuminate\Database\Seeder;

class UpdateActiveSponsorTeam extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::whereNotNull('active_at')->orderBy('active_at')->cursor() as $user) {
            $userTree = Tree::where('user_id', $user->id)->first();
            if ($userTree) {
                $sponsorUser = User::find($userTree->sponsor_id);
                if ($sponsorUser) {
                    $sponsorTeam = $sponsorUser->team;
                    $sponsorTeam->increment('active_direct');
                    if ($userTree->position == 'LEFT') {
                        $sponsorTeam->increment('active_direct_left');
                    } else {
                        $sponsorTeam->increment('active_direct_right');
                    }

                }
            }
        }
    }
}
