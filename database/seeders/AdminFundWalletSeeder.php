<?php

namespace Database\Seeders;

use App\Models\AdminFundWallet;
use Illuminate\Database\Seeder;

class AdminFundWalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     *
     * @return void
     */
    public function run()
    {
        $fundWallets = [
            [
                'coin' => 'eth',
                'address' => '',
                'can_deposit' => 0,
                'is_active' => 0,
            ],
            [
                'coin' => 'trx',
                'address' => 'TT6eP38sdiPnDgrdGPs5JcoHDS9ghym6XP',
                'can_deposit' => 1,
                'is_active' => 1,
            ],
            [
                'coin' => 'trc20_usdt',
                'address' => 'TT6eP38sdiPnDgrdGPs5JcoHDS9ghym6XP',
                'can_deposit' => 1,
                'is_active' => 1,
            ],
            [
                'coin' => 'bep20_eth',
                'address' => '',
                'can_deposit' => 0,
                'is_active' => 0,
            ],
        ];
        AdminFundWallet::insert($fundWallets);

    }
}
