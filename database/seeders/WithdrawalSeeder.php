<?php

namespace Database\Seeders;

use App\Models\WithdrawCoin;
use Illuminate\Database\Seeder;

class WithdrawalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $withdrawCoins = [
            ['name' => 'usdt', 'network' => 'tron', 'contract_address' => null, 'is_active' => true],
        ];

        foreach ($withdrawCoins as $coin) {
            WithdrawCoin::create($coin);
        }
    }
}
