<?php

namespace Database\Seeders;

use App\Models\MetaTokenPrice;
use Illuminate\Database\Seeder;

class MetaTokenPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MetaTokenPrice::create([
            'price_in_usd' => '0.00005834'
        ]);
    }
}
