<?php

namespace Database\Seeders;

use App\Models\SmartMetaSupply;
use Illuminate\Database\Seeder;

class SmartMetaSupplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SmartMetaSupply::create([
            'total_supply' => '10000000000.00',
            'burned' => '0.00',
            'value_in_usd' => '7000000.00',
            'smart_meta_price' => '0.0007',
            'in_circulation' => '0.00'
        ]);
    }
}
