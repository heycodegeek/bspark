<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Tree;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Root User',
            'email' => 'root@gmail.com',
            'email_verified_at' => '2021-12-22 01:10:43',
            'password' => '12345678',
            'mobile' => '6666666666',
            'country_id' => Country::first()->id,
            'active_at' => now(),
            'ref_code' => generateRefCode(),
//            'username' => 'mohit',
        ]);

        $user->team()->create();

        $user->tree()->create([
            'sponsor_id' => 0,
            'position' => Tree::POSITIONS['LEFT'],
            'parent_id' => 0
        ]);

        $user->userCoinwallet()->create();
        $user->userUsdWallet()->create();
    }
}
