<?php

namespace Database\Seeders;

use App\Models\SubscriptionPool;
use Illuminate\Database\Seeder;

class SubscriptionPoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPool::create([
            'amount_in_usd' => '0.00'
        ]);
    }
}
