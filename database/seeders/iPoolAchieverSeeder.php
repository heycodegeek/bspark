<?php

namespace Database\Seeders;

use App\Models\InternationalPoolAchiever;
use Illuminate\Database\Seeder;

class iPoolAchieverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iPoolAchievers = [
            ['user_id' => 1167, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 844, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 1979, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 792, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 2085, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 1055, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 1808, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 150, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
            ['user_id' => 836, 'active_left_monthly' => 5, 'active_right_monthly' => 5, 'active_date' => '2022-10-31', 'is_eligible_month' => 1, 'is_active' => 1],
        ];

        foreach ($iPoolAchievers as $iPoolAchiever) {
            InternationalPoolAchiever::insert($iPoolAchiever);
        }
    }
}
