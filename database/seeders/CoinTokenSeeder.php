<?php

namespace Database\Seeders;

use App\Models\Coin;
use Illuminate\Database\Seeder;

class CoinTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coin = Coin::create([
            'name' => 'usdt',
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin->coinTypes()->create([
            'name' => 'usdt-erc20',
            'type' => 'erc20',
            'contract_address' => '0xdac17f958d2ee523a2206206994597c13d831ec7',
            'decimals' => 6,
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

        $coin->coinTypes()->create([
            'name' => 'usdt-trc20',
            'type' => 'trc20',
            'contract_address' => '0xdac17f958d2ee523a2206206994597c13d831ec7',
            'decimals' => 6,
            'can_deposit' => true,
            'can_withdraw' => false,
            'is_active' => true
        ]);

    }
}
