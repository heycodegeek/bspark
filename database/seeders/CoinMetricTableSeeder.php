<?php

namespace Database\Seeders;

use App\Models\CoinMetric;
use Illuminate\Database\Seeder;

class CoinMetricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metric = [
            'total_supply' => castDecimalString('900000000', 8),
            'circulating_supply' => castDecimalString('0', 8),
            'airdrop_supply' => castDecimalString('20000000', 8),
            'private_supply' => castDecimalString('0', 8),
            'public_supply' => castDecimalString('0', 8),
            'airdrop_sold' => castDecimalString('0', 8),
            'private_sold' => castDecimalString('0', 8),
            'public_sold' => castDecimalString('0', 8),
            'burned' => castDecimalString('0', 8),
        ];

        CoinMetric::create($metric);
    }
}

