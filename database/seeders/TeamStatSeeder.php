<?php

namespace Database\Seeders;

use App\Jobs\UpdateTeamStatJob;
use App\Models\User;
use Illuminate\Database\Seeder;

class TeamStatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::whereNotNull('active_at')->orderBy('active_at')->cursor() as $user) {
            UpdateTeamStatJob::dispatch($user, true);
        }
    }
}
