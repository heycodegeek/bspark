<?php

namespace Database\Seeders;

use App\Models\Apikey;
use Illuminate\Database\Seeder;

class ApikeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Apikey::create([
            'name' => 'Support Ticket Api',
            'api_key' => 'openTheDoor@4sure',
            'is_active' => 1,
        ]);
    }
}
