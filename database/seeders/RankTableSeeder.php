<?php

namespace Database\Seeders;

use App\Models\Rank;
use Illuminate\Database\Seeder;

class RankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            ['name' => 'KODIAK', 'left_rank_count' => 0, 'right_rank_count' => 0],
            ['name' => 'PUMA', 'left_rank_count' => 5, 'right_rank_count' => 5],
            ['name' => 'LEOPARD', 'left_rank_count' => 15, 'right_rank_count' => 15],
            ['name' => 'JAGUAR', 'left_rank_count' => 30, 'right_rank_count' => 30],
            ['name' => 'CAPITAN', 'left_rank_count' => 60, 'right_rank_count' => 60],
            ['name' => 'PANTHER', 'left_rank_count' => 150, 'right_rank_count' => 150],
            ['name' => 'MAVERICK', 'left_rank_count' => 300, 'right_rank_count' => 300],
            ['name' => 'YOSEMITE', 'left_rank_count' => 600, 'right_rank_count' => 600],
            ['name' => 'SIERRA', 'left_rank_count' => 1500, 'right_rank_count' => 1500],
            ['name' => 'BIGSUR', 'left_rank_count' => 3000, 'right_rank_count' => 3000],
            ['name' => 'MOJAVE', 'left_rank_count' => 6000, 'right_rank_count' => 6000],
            ['name' => 'CATALINA', 'left_rank_count' => 15000, 'right_rank_count' => 15000],
        ];

        foreach ($ranks as $rank) {
            Rank::create($rank);
        }
    }
}
