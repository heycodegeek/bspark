<?php

namespace Database\Seeders;

use App\Models\DelegatorPlan;
use Illuminate\Database\Seeder;

class DelegatorPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delegatorPlans = [
            ['name' => 'Delegator Plan', 'start_price_usd' => '2000', 'end_price_usd' => '2000','max_limit_percent'=>'400', 'rate_tenth_day_percent' => '2', 'rate_tenth_day_decimal' => '0.02'],
        ];
        DelegatorPlan::Insert($delegatorPlans);
    }
}
