<?php

namespace Database\Seeders;

use App\Models\StakeCoinPlan;
use App\Models\StakeDuration;
use Illuminate\Database\Seeder;

class NewStakingCoinPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            ['name' => 'Staking', 'start_price_usd' => '50', 'end_price_usd' => '50000']
        ];

        $binaryPlans = [
            ['rate_percent' => '10.00', 'rate_decimal' => '0.10', 'monthly_limit_usd' => '250000']
        ];
        $levelPlans = [
            ['level' => 1, 'rate_percent' => '7.00', 'rate_decimal' => '0.07'],
            ['level' => 2, 'rate_percent' => '3.00', 'rate_decimal' => '0.03'],
            ['level' => 3, 'rate_percent' => '1.00', 'rate_decimal' => '0.01']
        ];

        $stakeDurations = [
            ['no_of_months' => 12, 'rate_percent' => '4.00', 'rate_daily_decimal' => '0.0013'],
            ['no_of_months' => 24, 'rate_percent' => '4.50', 'rate_daily_decimal' => '0.0015'],
            ['no_of_months' => 36, 'rate_percent' => '5.00', 'rate_daily_decimal' => '0.0016'],
            ['no_of_months' => 48, 'rate_percent' => '5.50', 'rate_daily_decimal' => '0.0018'],
            ['no_of_months' => 60, 'rate_percent' => '6.00', 'rate_daily_decimal' => '0.0020'],
        ];

        foreach ($plans as $plan) {
            $stakeCoinPlan = StakeCoinPlan::create($plan);
            $stakeCoinPlan->stakeBinaryPlan()->create(
                $binaryPlans[0]
            );
            foreach ($levelPlans as $levelPlan) {
                $stakeCoinPlan->stakeLevelPlans()->create($levelPlan);
            }
        }

        foreach ($stakeDurations as $stakeDuration) {
            StakeDuration::create(
                $stakeDuration
            );
        }

    }
}
