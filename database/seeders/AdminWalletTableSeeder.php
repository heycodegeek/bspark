<?php

namespace Database\Seeders;

use App\Models\AdminWithdrawProcessWallet;
use Illuminate\Database\Seeder;

class AdminWalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminWithdrawProcessWallet::create([
            'name' => 'trx',
            'address' => config('admin-wallet.trx.address'),
            'priv_key' => config('admin-wallet.trx.priv_key'),
            'status' => 'active'
        ]);
    }
}
