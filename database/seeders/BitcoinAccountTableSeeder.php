<?php

namespace Database\Seeders;

use App\Models\BitcoinAccount;
use Illuminate\Database\Seeder;

class BitcoinAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BitcoinAccount::create([
            'fee_balance' => '0.00403644',
            'withdrawable_balance' => '0.03184776',
            'fund_receiver' => config('bitcoin_fund.fund_receiver')
        ]);
    }
}
