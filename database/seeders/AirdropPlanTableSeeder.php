<?php

namespace Database\Seeders;

use App\Enums\AirdropPlanEventEnum;
use App\Models\AirdropPlan;
use Illuminate\Database\Seeder;

class AirdropPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            ['name' => 'signup', 'event' => AirdropPlanEventEnum::On_SignUp(), 'coins' => castDecimalString('200', 8), 'will_be_locked' => true, 'cycle' => 1, 'cycle_duration_in_minutes' => null, 'is_active' => true]
        ];

        foreach ($plans as $plan) {
            AirdropPlan::create($plan);
        }

    }
}
