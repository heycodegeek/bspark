<?php

namespace Database\Seeders;

use App\Models\TokenPrice;
use Illuminate\Database\Seeder;

class TokenPriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TokenPrice::create([
            'price_in_usd' => '1.0000'
        ]);
    }
}
