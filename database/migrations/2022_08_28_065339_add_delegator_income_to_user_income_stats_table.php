<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDelegatorIncomeToUserIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_income_stats', function (Blueprint $table) {
            $table->decimal('delegator_bonus', 40, 8)->default('0')->after('matching_bonus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_income_stats', function (Blueprint $table) {
            //
        });
    }
}
