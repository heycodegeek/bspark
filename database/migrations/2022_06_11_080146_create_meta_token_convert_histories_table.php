<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaTokenConvertHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_token_convert_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('txn_id')->nullable()->default(null);
            $table->string('particular')->nullable()->default(null);
            $table->decimal('tokens', 40, 12);
            $table->decimal('meta_token_price', 40, 12);
            $table->decimal('fees', 40, 12)->nullable()->default(0);
            $table->decimal('amount', 40, 12);
            $table->decimal('receivable_amount', 40, 12);
            $table->enum('status', ['pending', 'processing', 'failed', 'success']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_token_convert_histories');
    }
}
