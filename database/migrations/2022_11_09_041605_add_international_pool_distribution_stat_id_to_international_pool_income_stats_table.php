<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInternationalPoolDistributionStatIdToInternationalPoolIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('international_pool_income_stats', function (Blueprint $table) {
            $table->foreignId('international_pool_distribution_stats_id')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('international_pool_income_stats', function (Blueprint $table) {
            //
        });
    }
}
