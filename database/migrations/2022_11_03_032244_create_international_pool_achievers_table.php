<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternationalPoolAchieversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('international_pool_achievers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('active_left_monthly')->nullable()->default(0);
            $table->integer('active_right_monthly')->nullable()->default(0);
            $table->date('active_date')->nullable()->default(null);
            $table->tinyInteger('is_eligible_month')->default(1);
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('international_pool_achievers');
    }
}
