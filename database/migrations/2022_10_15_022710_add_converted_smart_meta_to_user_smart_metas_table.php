<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConvertedSmartMetaToUserSmartMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_smart_metas', function (Blueprint $table) {
            $table->decimal('converted_smart_meta', 40, 16)->after('burned')->default('0.0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_smart_metas', function (Blueprint $table) {
            //
        });
    }
}
