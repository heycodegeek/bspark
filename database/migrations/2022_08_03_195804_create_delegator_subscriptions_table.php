<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDelegatorSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delegator_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->foreignId('delegator_plan_id');
            $table->date('end_date');
            $table->integer('stake_duration');
            $table->decimal('amount_in_usd', 40, 2);
            $table->decimal('max_amount_limit', 40, 8);
            $table->decimal('earned_so_far', 40, 8);
            $table->tinyInteger('is_active')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delegator_subscriptions');
    }
}
