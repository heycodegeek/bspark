<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakingTeamDistributionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staking_team_distribution_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('level');
            $table->decimal('team_business', 40, 4);
            $table->decimal('rate_percent', 20, 4);
            $table->decimal('rate_decimal', 20, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staking_team_distribution_plans');
    }
}
