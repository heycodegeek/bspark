<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserStakeIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_stake_incomes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('closing_date');
            $table->decimal('staked_tokens',40,8);
            $table->decimal('token_price',20,4);
            $table->integer('coin_plan_id');
            $table->decimal('token_income', 40, 8);
            $table->timestamps();

            $table->unique(['user_id', 'closing_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_stake_incomes');
    }
}
