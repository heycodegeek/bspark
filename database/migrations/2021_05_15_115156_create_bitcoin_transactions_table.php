<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitcoinTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitcoin_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('txn_id')->unique();
            $table->foreignId('user_id');
            $table->string('address');
            $table->decimal('amount', 20, 8);
            $table->integer('confirmations')->default(0);
            $table->integer('vout')->default(0);
            $table->dateTime('txn_time');
            $table->dateTime('confirmed_at')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitcoin_transactions');
    }
}
