<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBurnedTokenToUserMetaverseWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_metaverse_wallets', function (Blueprint $table) {
            $table->decimal('burned_token',40,12)->nullable()->default(null)->after('token_on_hold');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_metaverse_wallets', function (Blueprint $table) {
            //
        });
    }
}
