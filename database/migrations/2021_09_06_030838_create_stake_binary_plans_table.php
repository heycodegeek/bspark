<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakeBinaryPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stake_binary_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('stake_coin_plan_id')->unique();
            $table->decimal('rate_percent');
            $table->decimal('rate_decimal');
            $table->decimal('monthly_limit_usd', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stake_binary_plans');
    }
}
