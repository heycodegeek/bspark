<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gas_prices', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_block');
            $table->string('safe_gas_price');
            $table->string('normal_gas_price');
            $table->string('fast_gas_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gas_prices');
    }
}
