<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetaverseWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_metaverse_wallet_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('particular')->nullable()->default(null);
            $table->decimal('tokens', 40, 12);
            $table->decimal('meta_token_price', 40, 12);
            $table->decimal('amount_in_usd', 40, 4);
            $table->enum('status', ['pending', 'processing', 'failed', 'success']);
            $table->enum('type', ['debit', 'credit']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_metaverse_wallet_transactions');
    }
}
