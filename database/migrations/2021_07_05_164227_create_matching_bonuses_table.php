<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchingBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matching_bonuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('binary_payout_id');
            $table->decimal('income_usd', 40);
            $table->decimal('income_tokens', 40, 8);
            $table->decimal('token_price', 20, 4);
            $table->integer('rank');
            $table->integer('level');
            $table->timestamps();

            $table->unique(['user_id', 'binary_payout_id'], 'matching_bonus_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matching_bonuses');
    }
}
