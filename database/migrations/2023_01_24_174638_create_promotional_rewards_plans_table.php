<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionalRewardsPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotional_rewards_plans', function (Blueprint $table) {
            $table->id();
            $table->float('team_business', 40, 2);
            $table->float('cumulative_team_business', 40, 2)->nullable();
            $table->float('reward_amount', 40, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotional_rewards_plans');
    }
}
