<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakeCoinPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stake_coin_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('start_price_usd', 20)->default('0');
            $table->decimal('end_price_usd', 20)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stake_coin_plans');
    }
}
