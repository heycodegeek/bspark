<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCoinTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coin_types', function (Blueprint $table) {
            $table->string('contract_address')->nullable()->default(null)->after('type');
            $table->integer('decimals')->default(8)->after('contract_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coin_types', function (Blueprint $table) {
            //
        });
    }
}
