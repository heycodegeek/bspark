<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakingDistributionStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staking_distribution_stats', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount_in_usd', 40, 8);
            $table->integer('no_of_staking');
            $table->date('bonus_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staking_distribution_stats');
    }
}
