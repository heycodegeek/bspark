<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_income_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->decimal('daily_bonus', 40, 8)->default('0');
            $table->decimal('direct_bonus', 40, 8)->default('0');
            $table->decimal('binary_bonus', 40, 8)->default('0');
            $table->decimal('matching_bonus', 40, 8)->default('0');
            $table->decimal('total_bonus', 40, 8)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_income_stats');
    }
}
