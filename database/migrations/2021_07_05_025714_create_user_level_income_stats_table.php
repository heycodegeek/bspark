<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLevelIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_level_income_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_level_income_id');
            $table->foreignId('subscription_history_id');
            $table->integer('level');
            $table->decimal('income_tokens', 40, 8);
            $table->decimal('usd',40);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_level_income_stats');
    }
}
