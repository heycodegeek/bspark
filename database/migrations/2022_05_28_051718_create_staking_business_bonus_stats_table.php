<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakingBusinessBonusStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staking_business_bonus_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->bigInteger('from_user_id');
            $table->decimal('bonus_amount', 40, 4)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staking_business_bonus_stats');
    }
}
