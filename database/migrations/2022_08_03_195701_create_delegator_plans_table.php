<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDelegatorPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delegator_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('start_price_usd', 20)->default('0');
            $table->decimal('end_price_usd', 20)->default('0');
            $table->decimal('max_limit_percent', 20, 4)->default('0');
            $table->decimal('rate_tenth_day_percent', 20, 4)->default('0');
            $table->decimal('rate_tenth_day_decimal', 20, 4)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delegator_plans');
    }
}
