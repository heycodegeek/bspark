<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCityOneDirectIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_city_one_direct_income_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->bigInteger('from_user');
            $table->bigInteger('city_one_plan_id');
            $table->decimal('amount_in_usd', 40, 8);
            $table->decimal('amount_in_meta', 40, 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_city_one_direct_income_stats');
    }
}
