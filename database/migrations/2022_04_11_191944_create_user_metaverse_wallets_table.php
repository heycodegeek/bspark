<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetaverseWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_metaverse_wallets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->decimal('token', 40, 8);
            $table->decimal('value_in_usd', 40, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_metaverse_wallets');
    }
}
