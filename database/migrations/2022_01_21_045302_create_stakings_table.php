<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stakings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('user_usd_wallet_transaction_id')->default(0);
            $table->decimal('amount_in_usd', 40, 2);
            $table->decimal('max_amount_limit_usd', 40, 2);
            $table->decimal('earned_so_far', 40, 2);
            $table->tinyInteger('is_active')->default(1);
            $table->date('end_date')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stakings');
    }
}
