<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoyaltyPoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('royalty_pools', function (Blueprint $table) {
            $table->id();
            $table->decimal('balance', 40, 4)->default('0.00');
            $table->decimal('from_subscriptions', 40, 4)->default('0.00');
            $table->decimal('from_withdrawals', 40, 4)->default('0.00');
            $table->date('next_distribution_date')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('royalty_pools');
    }
}
