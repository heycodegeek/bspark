<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakeLevelPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stake_level_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('stake_coin_plan_id');
            $table->integer('level');
            $table->decimal('rate_percent');
            $table->decimal('rate_decimal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stake_level_plans');
    }
}
