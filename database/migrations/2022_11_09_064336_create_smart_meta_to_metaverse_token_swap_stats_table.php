<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartMetaToMetaverseTokenSwapStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smart_meta_to_metaverse_token_swap_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->decimal('smart_meta', 40, 16);
            $table->decimal('smart_meta_price', 40, 16);
            $table->decimal('metaverse_token', 40, 16);
            $table->decimal('meta_price', 40, 16);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smart_meta_to_metaverse_token_swap_stats');
    }
}
