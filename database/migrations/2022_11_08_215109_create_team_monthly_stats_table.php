<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMonthlyStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_monthly_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->unsignedInteger('left')->default(0);
            $table->unsignedInteger('right')->default(0);
            $table->unsignedInteger('direct')->default(0);
            $table->unsignedInteger('direct_left')->default(0);
            $table->unsignedInteger('direct_right')->default(0);
            $table->unsignedInteger('active_left')->default(0);
            $table->unsignedInteger('active_right')->default(0);
            $table->unsignedInteger('active_direct')->default(0);
            $table->unsignedInteger('active_direct_left')->default(0);
            $table->unsignedInteger('active_direct_right')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_monthly_stats');
    }
}
