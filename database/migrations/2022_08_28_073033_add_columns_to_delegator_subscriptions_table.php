<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToDelegatorSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delegator_subscriptions', function (Blueprint $table) {
            $table->date('bonus_start_date')->nullable()->default(null);
            $table->date('next_bonus_date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delegator_subscriptions', function (Blueprint $table) {
            //
        });
    }
}
