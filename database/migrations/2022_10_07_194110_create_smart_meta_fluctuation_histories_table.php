<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartMetaFluctuationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smart_meta_fluctuation_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable()->default(null);
            $table->decimal('amount_in_usd', 40, 8);
            $table->decimal('previous_value_usd', 40, 8);
            $table->decimal('previous_smart_meta_price', 40, 8);
            $table->string('particular');
            $table->string('added_by')->default('system');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smart_meta_fluctation_histories');
    }
}
