<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalStakingBusinessToStakingBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staking_businesses', function (Blueprint $table) {
            $table->decimal('total_staking_business', 40, 4)->default('0.00')->after('right_staking_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staking_businesses', function (Blueprint $table) {
            //
        });
    }
}
