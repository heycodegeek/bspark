<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToInternationalPoolDistributionStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('international_pool_distribution_stats', function (Blueprint $table) {
            $table->integer('no_of_beneficiaries')->after('meta_token')->default(0);
            $table->integer('first_time_achievers')->after('no_of_beneficiaries')->default(0);
            $table->integer('monthly_achievers')->after('first_time_achievers')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('international_pool_distribution_stats', function (Blueprint $table) {
            //
        });
    }
}
