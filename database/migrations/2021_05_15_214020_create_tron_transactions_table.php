<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTronTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tron_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('txn_id');
            $table->foreignId('user_id');
            $table->string('address');
            $table->decimal('amount', 30, 6);
            $table->integer('confirmations')->default(0);
            $table->dateTime('txn_time');
            $table->unsignedBigInteger('txn_timestamp');
            $table->dateTime('confirmed_at');
            $table->timestamps();

            $table->unique(['txn_id', 'address']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tron_transactions');
    }
}
