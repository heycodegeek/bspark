<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaverseTokenPoolStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metaverse_token_pool_stats', function (Blueprint $table) {
            $table->id();
            $table->decimal('released_token', 40, 10);
            $table->decimal('available_token', 40, 10);
            $table->decimal('burned_token', 40, 10);
            $table->decimal('value_in_usd', 40, 10);
            $table->decimal('meta_token_price', 40, 10);
            $table->enum('type', ['meta_released', 'usd_converted', 'convert_added', 'binary_added', 'direct_added', 'bot_added']);
            $table->enum('status', ['pending', 'failed', 'success']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metaverse_token_pool_stats');
    }
}
