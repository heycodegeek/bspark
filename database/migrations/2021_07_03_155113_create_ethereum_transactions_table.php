<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEthereumTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ethereum_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('txn_id');
            $table->foreignId('user_id');
            $table->string('address');
            $table->decimal('amount', 40, 8);
            $table->integer('confirmations');
            $table->dateTime('txn_time');
            $table->timestamps();

            $table->unique(['txn_id', 'address']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ethereum_transactions');
    }
}
