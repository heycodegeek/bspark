<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTokenOnHoldToUserMetaverseWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_metaverse_wallets', function (Blueprint $table) {
            $table->decimal('token_on_hold', 40, 12)->nullable()->default('0.00')->after('value_in_usd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_metaverse_wallets', function (Blueprint $table) {
            //
        });
    }
}
