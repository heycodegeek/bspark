<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLevelToStakingBusinessBonusStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staking_business_bonus_stats', function (Blueprint $table) {
            $table->integer('level')->nullable()->default(null)->after('bonus_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staking_business_bonus_stats', function (Blueprint $table) {
            //
        });
    }
}
