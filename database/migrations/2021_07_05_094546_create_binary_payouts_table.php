<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBinaryPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binary_payouts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->decimal('matched_usd');
            $table->decimal('capping_left');
            $table->decimal('income_amount_usd', 40, 2);
            $table->decimal('income_tokens', 40, 8);
            $table->decimal('token_price', 20, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binary_payouts');
    }
}
