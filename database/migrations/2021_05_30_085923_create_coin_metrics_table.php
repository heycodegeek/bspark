<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_metrics', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_supply', 40, 8);
            $table->decimal('circulating_supply', 40, 8);
            $table->decimal('airdrop_supply', 40, 8);
            $table->decimal('private_supply', 40, 8);
            $table->decimal('public_supply', 40, 8);
            $table->decimal('airdrop_sold', 40, 8);
            $table->decimal('private_sold', 40, 8);
            $table->decimal('public_sold', 40, 8);
            $table->decimal('burned', 40, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_metrics');
    }
}
