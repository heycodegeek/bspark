<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stake_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->integer('period_in_months');
            $table->decimal('withdraw_limit_monthly_percent');
            $table->decimal('withdraw_limit_monthly_deciaml');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stake_plans');
    }
}
