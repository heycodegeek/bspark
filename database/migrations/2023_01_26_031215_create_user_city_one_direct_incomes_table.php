<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCityOneDirectIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_city_one_direct_incomes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('direct_count')->default(0);
            $table->integer('same_and_above_package')->default(0);
            $table->decimal('amount_in_usd', 40, 8);
            $table->decimal('amount_in_meta', 40, 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_city_one_direct_incomes');
    }
}
