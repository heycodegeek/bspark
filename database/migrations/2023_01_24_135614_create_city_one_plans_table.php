<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityOnePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_one_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('price_in_usd', 20)->default('0.00');
            $table->decimal('rate_daily_percent', 8, 6);
            $table->decimal('rate_daily_decimal', 8, 6);
            $table->decimal('booster_daily_percent', 8, 6);
            $table->decimal('booster_daily_decimal', 8, 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_one_plans');
    }
}
