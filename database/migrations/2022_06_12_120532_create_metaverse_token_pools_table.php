<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaverseTokenPoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metaverse_token_pools', function (Blueprint $table) {
            $table->id();
            $table->decimal('released_token', 40, 10);
            $table->decimal('available_token', 40, 10);
            $table->decimal('burned_token', 40, 10);
            $table->decimal('value_in_usd', 40, 10);
            $table->decimal('meta_token_price', 40, 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metaverse_token_pools');
    }
}
