<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->decimal('start_price_usd', 20)->default('0');
            $table->decimal('end_price_usd', 20)->default('0');
            $table->decimal('rate_daily_percent', 8, 2);
            $table->decimal('rate_daily_decimal', 8, 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_plans');
    }
}
