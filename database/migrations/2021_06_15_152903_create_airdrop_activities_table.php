<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirdropActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airdrop_activities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->tinyInteger('twitter')->default(0);
            $table->tinyInteger('telegram')->default(0);
            $table->tinyInteger('email')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airdrop_activities');
    }
}
