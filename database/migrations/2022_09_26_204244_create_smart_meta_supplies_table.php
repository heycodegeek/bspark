<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartMetaSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smart_meta_supplies', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_supply',40,10);
            $table->decimal('available_supply', 40, 10)->default('0.00');
            $table->decimal('in_circulation', 40, 10)->default('0.00');
            $table->decimal('burned', 40, 10)->default('0.00');
            $table->decimal('total_converted', 40, 10)->default('0.00');
            $table->decimal('value_in_usd', 40, 10)->default('0.00');
            $table->decimal('smart_meta_price', 40, 10)->default('0.00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smart_meta_supplies');
    }
}
