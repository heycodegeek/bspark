<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakingBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staking_businesses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->bigInteger('sponsor_id');
            $table->decimal('left_staking_amount', 40, 4)->nullable()->default(null);
            $table->decimal('right_staking_amount', 40, 4)->nullable()->default(null);
            $table->decimal('earned_so_far', 40, 4)->nullable()->default(null);
            $table->decimal('max_amount', 40, 4)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staking_businesses');
    }
}
