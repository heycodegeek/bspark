<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDelegatorLevelBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_delegator_level_bonuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('downline_user_id');
            $table->integer('user_delegator_subscription_id');
            $table->date('closing_date');
            $table->decimal('income_usd', 40, 8);
            $table->timestamps();

//            $table->unique(['downline_user_id', 'closing_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_delegator_level_bonuses');
    }
}
