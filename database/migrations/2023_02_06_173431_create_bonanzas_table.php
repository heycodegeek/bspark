<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonanzasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonanzas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->string('name')->nullable()->default('BULLET BONANZA');
            $table->tinyInteger('is_eligible')->default(0);
            $table->enum('status', ['awaiting', 'achieved']);
            $table->integer('active_user_count')->nullable()->default(0);
            $table->tinyInteger('is_rewarded')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonanzas');
    }
}
