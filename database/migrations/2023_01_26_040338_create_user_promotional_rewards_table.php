<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPromotionalRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_promotional_rewards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('promotional_rewards_plan_id');
            $table->decimal('amount_in_usd', 40, 4);
            $table->decimal('amount_in_meta', 40, 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_promotional_rewards');
    }
}
