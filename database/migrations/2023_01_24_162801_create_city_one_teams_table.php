<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityOneTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_one_teams', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->unsignedBigInteger('direct')->default(0);
            $table->unsignedBigInteger('active_direct')->default(0);
            $table->unsignedBigInteger('total')->default(0);
            $table->unsignedBigInteger('active_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_one_teams');
    }
}
