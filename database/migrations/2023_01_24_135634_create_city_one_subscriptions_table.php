<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityOneSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_one_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->foreignId('city_one_plan_id');
            $table->date('end_date');
            $table->decimal('amount_in_usd', 40, 2);
            $table->decimal('earned_so_far', 40, 8)->nullable()->default('0.00');
            $table->timestamp('booster_at')->nullable()->default(null);
            $table->integer('booster_count')->nullable()->default(0);
            $table->tinyInteger('is_active')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_one_subscriptions');
    }
}
