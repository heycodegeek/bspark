<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBoosterToCityOnePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_one_plans', function (Blueprint $table) {
            $table->decimal('max_amount_limit_percent', 40, 4)->after('booster_daily_decimal');
            $table->decimal('booster_amount_limit_percent', 40, 4)->after('max_amount_limit_percent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_one_plans', function (Blueprint $table) {
            //
        });
    }
}
