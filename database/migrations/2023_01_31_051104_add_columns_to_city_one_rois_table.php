<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCityOneRoisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_one_rois', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->foreignId('city_one_subscription_id');
            $table->decimal('amount_in_usd', 40, 8);
            $table->decimal('amount_in_meta', 40, 12);
            $table->date('bonus_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_one_rois', function (Blueprint $table) {
            //
        });
    }
}
