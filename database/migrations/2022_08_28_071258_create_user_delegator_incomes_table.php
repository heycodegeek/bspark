<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDelegatorIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_delegator_incomes', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->integer('delegator_plan_id');
            $table->integer('delegator_subscription_id');
            $table->date('closing_date');
            $table->decimal('subscription_amount', 40, 8);
            $table->decimal('income_usd', 40, 8);
            $table->timestamps();

            $table->unique(['user_id', 'closing_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_delegator_incomes');
    }
}
