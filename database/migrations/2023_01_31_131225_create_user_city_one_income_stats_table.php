<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCityOneIncomeStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_city_one_income_stats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique();
            $table->decimal('roi', 40, 8)->nullable();
            $table->decimal('level_roi', 40, 8)->nullable();
            $table->decimal('direct', 40, 8)->nullable();
            $table->decimal('fast_track', 40, 8)->nullable();
            $table->decimal('promotional_reward', 40, 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_city_one_income_stats');
    }
}
