<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirdropPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airdrop_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('event');
            $table->decimal('coins', 40, 8);
            $table->tinyInteger('will_be_locked');
            $table->integer('cycle')->default(1);
            $table->integer('cycle_duration_in_minutes')->nullable()->default(null);
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airdrop_plans');
    }
}
