<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStakeSubscriptionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stake_subscription_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('stake_duration');
            $table->decimal('amount_in_usd', 40, 2);
            $table->decimal('stake_tokens', 40, 8);
            $table->decimal('token_price', 20, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stake_subscription_histories');
    }
}
