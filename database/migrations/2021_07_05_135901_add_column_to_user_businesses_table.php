<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUserBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_businesses', function (Blueprint $table) {
            $table->decimal('direct_left_usd', 40)->default('0')->after('direct_usd');
            $table->decimal('direct_right_usd', 40)->default('0')->after('direct_left_usd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_businesses', function (Blueprint $table) {
            //
        });
    }
}
