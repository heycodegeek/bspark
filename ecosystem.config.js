module.exports = {
    apps: [{
        name: 'Horizon',
        script: './artisan',
        interpreter: 'php',
        instances: 1,
        args: 'horizon',
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'development',
        },
        env_prod: {
            NODE_ENV: 'production',
        }
    },
        {
            name: 'Scheduler',
            script: './artisan',
            interpreter: 'php',
            instances: 1,
            args: 'short-schedule:run',
            autorestart: true,
            watch: false,
            max_memory_restart: '1G',
            env: {
                NODE_ENV: 'development',
            },
            env_prod: {
                NODE_ENV: 'production',
            }
        },

    ],


}




