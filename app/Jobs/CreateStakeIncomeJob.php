<?php

namespace App\Jobs;

use App\Models\CoinSponsorLevelPlan;
use App\Models\StakeIncomeClosing;
use App\Models\StopRoiUser;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateStakeIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Subscription $subscription;
    public StakeIncomeClosing $stakeIncomeClosing;

    /**
     * Create a new job instance.
     *
     * @param Subscription $subscription
     * @param StakeIncomeClosing $stakeIncomeClosing
     */
    public function __construct(Subscription $subscription, StakeIncomeClosing $stakeIncomeClosing)
    {
        $this->subscription = $subscription->withoutRelations();
        $this->stakeIncomeClosing = $stakeIncomeClosing->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $coinPlan = $this->subscription->coinPlan;
        $subscribedTokens = $this->subscription->stake_tokens;
        $subscribedUsd = $this->subscription->amount_in_usd;
        $user = $this->subscription->user;

        if (StopRoiUser::where('user_id', $user->id)->exists()) {
            return;
        }

        $userStakeIncome = $user->userStakeIncomes()->where('closing_date', $this->stakeIncomeClosing->closing_date)->first();
        if (is_null($userStakeIncome)) {
            $tokenIncome = divDecimalStrings(multipleDecimalStrings($subscribedUsd, $coinPlan->rate_daily_decimal, 2), tokenPrice(), 8);

//            $tokenIncome = multipleDecimalStrings($subscribedTokens, $coinPlan->rate_daily_decimal, 8);
            $userStakeIncome = $user->userStakeIncomes()->create([
                'closing_date' => $this->stakeIncomeClosing->closing_date,
                'staked_tokens' => $subscribedTokens,
                'token_price' => tokenPrice(),
                'coin_plan_id' => $coinPlan->id,
                'token_income' => $tokenIncome
            ]);
            $userCoinWallet = $user->userCoinWallet()->firstOrCreate();
            $userCoinWallet->increment('balance', $tokenIncome);

            $userStakeIncomeWallet = $user->userStakeIncomeWallet()->firstOrCreate();
            $userStakeIncomeWallet->increment('balance', $tokenIncome);

            $userIncomeStat = $user->userIncomeStat()->firstOrCreate();
            $userIncomeStat->increment('daily_bonus', $tokenIncome);
            $userIncomeStat->increment('total_bonus', $tokenIncome);
        }

//        $user->userStakeIncomes()
    }


}
