<?php

namespace App\Jobs;

use App\Blockchains\Tron\Handler;
use App\Models\DepositWallet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MoveTronFund implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public DepositWallet $depositWallet;
    public float $amount;

    /**
     * Create a new job instance.
     *
     * @param DepositWallet $depositWallet
     * @param float $amount
     */
    public function __construct(DepositWallet $depositWallet, float $amount)
    {
        $this->depositWallet = $depositWallet;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fundWallet = config('network_tron.fund_wallet');
        Handler::init()->sendTrx($this->depositWallet->address, $this->depositWallet->priv_key, $fundWallet, $this->amount);

    }
}
