<?php

namespace App\Jobs;

use App\Models\BinaryBusiness;
use App\Models\Subscription;
use App\Models\SubscriptionHistory;
use App\Models\User;
use App\Models\UserBusiness;
use App\Mohiqssh\BinaryPayoutMethod;
use Illuminate\Bus\Batch;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;

class CreateFirstBinaryBusinessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public SubscriptionHistory $subscriptionHistory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SubscriptionHistory $subscriptionHistory)
    {
        $this->onQueue('binary');
        $this->subscriptionHistory = $subscriptionHistory->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->subscriptionHistory->user;
        $userTree = $user->tree;

        $sponsorUser = User::find($userTree->sponsor_id);
        if (!$sponsorUser) {
            return;
        }
        $parentSubscription = Subscription::where('user_id', $sponsorUser->id)->where('is_active', 1)->get();
        if (is_null($parentSubscription)) {
            return;
        }
        $userSubscriptionCount = Subscription::where('user_id', $user->id)->count();
        if ($userSubscriptionCount == 1) {
            $binaryBusiness = BinaryBusiness::firstOrCreate(
                ['user_id' => $sponsorUser->id],
                ['left_business' => castDecimalString('0', 2), 'left_cf' => castDecimalString('0', 2), 'right_business' => castDecimalString('0', 2), 'right_cf' => castDecimalString('0', 2), 'direct_business' => castDecimalString('0', 2)]
            );
            $binaryBusiness->increment('direct_business', $this->subscriptionHistory->amount_in_usd);

            $userBusiness = UserBusiness::firstOrCreate(
                ['user_id' => $sponsorUser->id],
                ['left_usd' => castDecimalString('0', 2), 'right_usd' => castDecimalString('0', 2), 'direct_usd' => castDecimalString('0', 2), 'direct_left_usd' => castDecimalString('0', 2), 'direct_right_usd' => castDecimalString('0', 2)]
            );

            $userBusiness->increment('direct_usd', $this->subscriptionHistory->amount_in_usd);
            if ($userTree->position == 'LEFT') {
                $userBusiness->increment('direct_left_usd', $this->subscriptionHistory->amount_in_usd);
            } else {
                $userBusiness->increment('direct_right_usd', $this->subscriptionHistory->amount_in_usd);
            }
        }

        $batch = Bus::batch([
            new CreateTeamBinaryBusinessJob($this->subscriptionHistory, $user),
            new CreateLevelIncomeJob($this->subscriptionHistory)
        ])->then(function (Batch $batch) {

        })->finally(function (Batch $batch) {

        })->onQueue('binary')->dispatch();

        $this->generateSelfBinaryBusiness($user);
    }

    private function generateSelfBinaryBusiness(User $user)
    {
        BinaryPayoutMethod::init($user)->generateBinaryBusiness();
    }


}
