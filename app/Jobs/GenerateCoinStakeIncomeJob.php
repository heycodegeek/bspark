<?php

namespace App\Jobs;

use App\Models\NewStakeIncomeClosing;
use App\Models\StakeSubscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateCoinStakeIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('income');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $today = now()->format('Y-m-d');
        $closing = NewStakeIncomeClosing::firstOrCreate(
            ['closing_date' => $today],
            ['status' => 'pending']
        );
        if ($closing->status == 'pending') {
            foreach (StakeSubscription::whereDate('created_at', '<=', $today)->where('end_date', '>', $today)->cursor() as $subscription) {
                CreateCoinStakeIncomeJob::dispatch($subscription, $closing)->delay(now()->addSeconds(2))->onQueue('income');
            }
            $closing->update([
                'status' => 'success'
            ]);
        }
    }
}
