<?php

namespace App\Jobs;

use App\Models\NewStakeIncomeClosing;
use App\Models\StakeDuration;
use App\Models\StakeSubscription;
use App\Models\StopRoiUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateCoinStakeIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public StakeSubscription $subscription;
    public NewStakeIncomeClosing $stakeIncomeClosing;

    /**
     * Create a new job instance.
     *
     * @param StakeSubscription $subscription
     * @param NewStakeIncomeClosing $stakeIncomeClosing
     */
    public function __construct(StakeSubscription $subscription, NewStakeIncomeClosing $stakeIncomeClosing)
    {
        $this->subscription = $subscription->withoutRelations();
        $this->stakeIncomeClosing = $stakeIncomeClosing->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stakeDuration = $this->subscription->stake_duration;
        $stakeDurationPlan = StakeDuration::select('rate_percent')->where('no_of_months', $stakeDuration)->first();
        $subscribedTokens = $this->subscription->stake_tokens;
        $user = $this->subscription->user;

        if (StopRoiUser::where('user_id', $user->id)->exists()) {
            return;
        }

        $userStakeIncome = $user->userStakeIncomes()->where('closing_date', $this->stakeIncomeClosing->closing_date)->first();
        if (is_null($userStakeIncome)) {
            $tokenIncome = multipleDecimalStrings($subscribedTokens, divDecimalStrings($stakeDurationPlan->rate_percent, '100', 2), 8);

//            $tokenIncome = multipleDecimalStrings($subscribedTokens, $coinPlan->rate_daily_decimal, 8);
            $userStakeIncome = $user->userCoinStakeIncomes()->create([
                'closing_date' => $this->stakeIncomeClosing->closing_date,
                'staked_tokens' => $subscribedTokens,
                'stake_duration' => $stakeDuration,
                'token_income' => $tokenIncome
            ]);
            $userCoinWallet = $user->userCoinWallet()->firstOrCreate();
            $userCoinWallet->increment('balance', $tokenIncome);

            $userStakeIncomeWallet = $user->userStakeIncomeWallet()->firstOrCreate();
            $userStakeIncomeWallet->increment('balance', $tokenIncome);

            $userIncomeStat = $user->userIncomeStat()->firstOrCreate();
            $userIncomeStat->increment('daily_bonus', $tokenIncome);
            $userIncomeStat->increment('total_bonus', $tokenIncome);
        }
    }
}
