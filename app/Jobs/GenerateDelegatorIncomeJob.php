<?php

namespace App\Jobs;

use App\Models\DelegatorIncomeClosing;
use App\Models\DelegatorSubscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateDelegatorIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $incomeDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $incomeDate)
    {
        $this->onQueue('income');
        $this->incomeDate = $incomeDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $today = $this->incomeDate;
        $closing = DelegatorIncomeClosing::firstOrCreate(
            ['closing_date' => $today],
            ['status' => 'pending']
        );
        if ($closing->status == 'pending') {
            foreach (DelegatorSubscription::where('is_active', true)->whereDate('created_at', '<', $today)->where('end_date', '>', $today)->where('next_bonus_date', $today)->cursor() as $delegatorSubscription) {
                CreateDelegatorIncomeJob::dispatch($delegatorSubscription, $closing)->delay(now()->addSeconds(1))->onQueue('income');
            }
            $closing->update([
                'status' => 'success'
            ]);
        }
    }
}
