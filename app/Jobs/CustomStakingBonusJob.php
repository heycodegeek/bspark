<?php

namespace App\Jobs;

use App\Models\Staking;
use App\Models\StakingBonus;
use App\Models\StakingDistributionStat;
use App\Models\StakingPool;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomStakingBonusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $bonus;
    public $bonusDate;

    public function __construct($bonus,$bonusDate)
    {
        $this->bonus = castDecimalString($bonus, 8);
        $this->bonusDate = date('Y-m-d',$bonusDate);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stakings = Staking::where('max_amount_limit_usd', '>', 'earned_so_far')->orderBy('user_id', 'ASC')->get();
        $totalBonus = (float)0;
        $no_of_staking = 0;

        foreach ($stakings as $staking) {
            $max_amount_limit_usd = castDecimalString($staking->max_amount_limit_usd, 8);
            $earned_so_far = castDecimalString($staking->earned_so_far, 8);
            $bonusAmount = castDecimalString(multipleDecimalStrings($staking->amount_in_usd, $this->bonus), 8);
            if (((float)$earned_so_far + (float)$bonusAmount) > (float)$max_amount_limit_usd) {
                $bonusAmount = $max_amount_limit_usd - $earned_so_far;
            }
            $stakingBonus = StakingBonus::where(
                [
                    'staking_id' => $staking->id,
                    'bonus_date' =>  $this->bonusDate
                ])->count();
            if ($stakingBonus == 0) {
                $totalBonus = (float)$totalBonus + (float)$bonusAmount;
                $no_of_staking = $no_of_staking + 1;
                $staking->stakingBonus()->create([
                    'amount_in_usd' => $bonusAmount,
                    'bonus_date' =>  $this->bonusDate
                ]);
                $staking->increment('earned_so_far', $bonusAmount);
                $this->addCoinToWallet($staking->user, $bonusAmount);
            }
        }
        if ($no_of_staking > 0) {
            StakingPool::where('id', 1)->increment('distributed_so_for', $totalBonus);

        }

    }

    private function addCoinToWallet(User $user, $amount)
    {
        $userCoinWallet = userCoinWalletInstance($user);
        $userCoinWallet->increment('balance', $amount);
    }
}
