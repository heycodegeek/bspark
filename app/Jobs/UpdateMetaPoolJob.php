<?php

namespace App\Jobs;

use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPoolStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UpdateMetaPoolJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private float $amount_in_usd;
    private float $token;
    private string $type;
    /**
     * @var bool|int
     */
    private $is_buddy_bot;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(float $amount_in_usd, float $token, $type, bool $is_buddy_bot = false)
    {
        $this->amount_in_usd = $amount_in_usd;
        $this->token = $token;
        $this->type = $type;
        $this->is_buddy_bot = $is_buddy_bot;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if ($this->is_buddy_bot){
            $this->type = MetaverseTokenPoolStat::TYPE['BOT_ADDED'];
        }

        $amount_in_usd = castDecimalString($this->amount_in_usd, 10);
        $new_token = castDecimalString($this->token, 16);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('released_token', $new_token);
            $metaPool->increment('available_token', $new_token);
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaPool->save();

            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 16);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => $this->type,
                'status' => 'success'
            ]);
        }
    }
}
