<?php

namespace App\Jobs;

use App\Models\UserSmartMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateSmartMetaToMetaverseTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (UserSmartMeta::where('balance', '>', castDecimalString('0', 2))->where('meta_converted', 0)->cursor() as $userSmartMeta) {
            CreateSmartMetaToMetaverseTokenJob::dispatch($userSmartMeta)->delay(now()->addSeconds(2))->onQueue('income');
        }
    }
}
