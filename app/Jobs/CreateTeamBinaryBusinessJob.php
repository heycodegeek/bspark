<?php

namespace App\Jobs;

use App\Events\BinaryPayoutCreated;
use App\Models\BinaryBusiness;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPoolStat;
use App\Models\Subscription;
use App\Models\SubscriptionHistory;
use App\Models\User;
use App\Models\UserBinaryInfo;
use App\Models\UserBusiness;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CreateTeamBinaryBusinessJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public SubscriptionHistory $subscriptionHistory;

    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SubscriptionHistory $subscriptionHistory, User $user)
    {
        $this->onQueue('binary');
        $this->subscriptionHistory = $subscriptionHistory->withoutRelations();
        $this->user = $user->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userTree = $this->user->tree;

        $parentUser = User::find($userTree->parent_id);
        if (!$parentUser) {
            return;
        }
        $parentSubscription = Subscription::where('user_id', $parentUser->id)->get();
        if (is_null($parentSubscription)) {
            return;
        }

        $binaryBusiness = BinaryBusiness::firstOrCreate(
            ['user_id' => $parentUser->id],
            ['left_business' => castDecimalString('0', 2), 'left_cf' => castDecimalString('0', 2), 'right_business' => castDecimalString('0', 2), 'right_cf' => castDecimalString('0', 2), 'direct_business' => castDecimalString('0', 2)]
        );

        $userBusiness = UserBusiness::firstOrCreate(
            ['user_id' => $parentUser->id],
            ['left_usd' => castDecimalString('0', 2), 'right_usd' => castDecimalString('0', 2), 'direct_usd' => castDecimalString('0', 2), 'direct_left_usd' => castDecimalString('0', 2), 'direct_right_usd' => castDecimalString('0', 2)]
        );

        if ($userTree->position == 'LEFT') {
            $binaryBusiness->increment('left_business', $this->subscriptionHistory->amount_in_usd);
            $userBusiness->increment('left_usd', $this->subscriptionHistory->amount_in_usd);

        } else {
            $binaryBusiness->increment('right_business', $this->subscriptionHistory->amount_in_usd);
            $userBusiness->increment('right_usd', $this->subscriptionHistory->amount_in_usd);
        }

        if (!is_null($parentUser->active_at) && $this->hasUserStandardBinary($parentUser)) {
            $binaryBusiness->refresh();
            $parentUserSubscription = $parentUser->subscription;

            if ($binaryBusiness->left_business > $binaryBusiness->right_business) {
                $matchedBusiness = $binaryBusiness->right_business;
            } elseif ($binaryBusiness->right_business > $binaryBusiness->left_business) {
                $matchedBusiness = $binaryBusiness->left_business;
            } else {
                $matchedBusiness = $binaryBusiness->right_business;
            }


            $incomeUsd = multipleDecimalStrings($matchedBusiness, '0.10', 2);

            $parentUserBinaryInfo = UserBinaryInfo::firstOrCreate(
                ['user_id' => $parentUser->id],
                ['binary_capping_amount' => castDecimalString('150000', 2), 'binary_used_weekly' => castDecimalString('0', 2)]
            );
            $parentUserBinaryInfo = $parentUser->userBinaryInfo;

//            $cappingAvailable = subDecimalStrings($parentUserBinaryInfo->binary_capping_amount, $parentUserBinaryInfo->binary_used_weekly, 2);
//
//            if ($incomeUsd <= $cappingAvailable) {
//
//                $this->createBinaryPayout($parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);
//
//            } else {
//                $incomeUsd = $cappingAvailable;
            $cappingAvailable = $incomeUsd;
            $this->createBinaryPayout($parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);
//            }
        }

        CreateTeamBinaryBusinessJob::dispatch($this->subscriptionHistory, $parentUser);
    }

    private function hasUserStandardBinary(User $user)
    {
        $userTeam = $user->team;
        if ($userTeam->active_direct_left && $userTeam->active_direct_right) {
            return true;
        }
        return false;
    }

    private function createBinaryPayout(User $parentUser, string $matchedBusiness, string $incomeUsd, string $cappingAvailable, UserBinaryInfo $parentUserBinaryInfo, BinaryBusiness $binaryBusiness)
    {
        DB::transaction(function () use (&$parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, &$parentUserBinaryInfo, &$binaryBusiness) {
            if ($incomeUsd > '0') {

                $UsdForMetaPool = multipleDecimalStrings(castDecimalString($incomeUsd, 10), '0.10', 10);
                $incomeUsd = multipleDecimalStrings(castDecimalString($incomeUsd, 10), '0.90', 10);

                $incomeTokens = numberOfTokens($incomeUsd, tokenPrice());


                $binaryPayout = $parentUser->binaryPayouts()->create([
                    'matched_usd' => $matchedBusiness,
                    'income_amount_usd' => $incomeUsd,
                    'capping_left' => $cappingAvailable,
                    'income_tokens' => $incomeTokens,
                    'token_price' => tokenPrice()
                ]);
                $parentUserBinaryInfo->increment('binary_used_weekly', $incomeUsd);



//                $userCoinWallet = userCoinWalletInstance($parentUser);
//                $withdrawableWallet = withdrawableWalletInstance($parentUser);
//                $halfIncome = multipleDecimalStrings($incomeTokens, '1', 8);
//                $userCoinWallet->increment('balance', $halfIncome);
//                $withdrawableWallet->increment('balance', $halfIncome);

                UpdateUserMetaverseWalletJob::dispatch($parentUser, $incomeUsd, MetaverseTokenPoolStat::TYPE['BINARY_ADDED'])->delay(now()->addSecond());

                $userIncomeStat = userIncomeStatInstance($parentUser);
                $userIncomeStat->increment('binary_bonus', $incomeTokens);
                $userIncomeStat->increment('total_bonus', $incomeTokens);
                //add bal in meta pool

                event(new BinaryPayoutCreated($binaryPayout));
//                $UsdForMetaPool = multipleDecimalStrings(castDecimalString($incomeUsd, 10), '0.10', 10);
                $this->updateMetaPool($UsdForMetaPool);
            }

            $binaryBusiness->decrement('right_business', $matchedBusiness);
            $binaryBusiness->decrement('left_business', $matchedBusiness);
        });
    }

    public function updateMetaPool($amount_in_usd)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 10);
//            $metaTokenPrice = divDecimalStrings(castDecimalString($metaPool->value_in_usd, 10), castDecimalString($metaPool->tokens, 10), 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['BINARY_ADDED'],
                'status' => 'success'
            ]);
        }
    }

}
