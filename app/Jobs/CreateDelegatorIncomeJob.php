<?php

namespace App\Jobs;

use App\Models\DelegatorIncomeClosing;
use App\Models\DelegatorSubscription;
use App\Models\User;
use App\Models\UserDelegatorIncome;
use App\Mohiqssh\IncomeMethods;
use App\Mohiqssh\UserLevelMethods;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CreateDelegatorIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public DelegatorSubscription $delegatorSubscription;
    public DelegatorIncomeClosing $delegatorIncomeClosing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DelegatorSubscription $delegatorSubscription, DelegatorIncomeClosing $delegatorIncomeClosing)
    {
        $this->delegatorSubscription = $delegatorSubscription->withoutRelations();
        $this->delegatorIncomeClosing = $delegatorIncomeClosing->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->delegatorSubscription->refresh();
        $delegatorPlan = $this->delegatorSubscription->delegatorPlan;
        $subscribedAmount = $this->delegatorSubscription->amount_in_usd;
        $user = $this->delegatorSubscription->user;

        if (!$this->delegatorSubscription->is_active) {
            return;
        }

//        $userStop = userStop($user);
//        if ($userStop->stake) {
//            return;
//        }

        $userStakeIncome = $user->userDelegatorIncomes()->where('closing_date', $this->delegatorIncomeClosing->closing_date)->where('delegator_subscription_id', $this->delegatorSubscription->id)->exists();
        if (!$userStakeIncome) {
            $usdIncome = multipleDecimalStrings($subscribedAmount, $delegatorPlan->rate_tenth_day_decimal, 2);
            $levelBonus = multipleDecimalStrings($usdIncome, '0.20', 2);
            $receivedIncome = IncomeMethods::init($user, $usdIncome)->updateIncomeByDelegatorSubscription($this->delegatorSubscription);
            if ($receivedIncome <= castDecimalString('0', 2)) {
                return;
            }
//            $user->userDelegatorIncomes()->create([
//                'closing_date' => $this->delegatorIncomeClosing->closing_date,
//                'delegator_subscription_id' => $this->delegatorSubscription->id,
//                'subscription_amount' => $subscribedAmount,
//                'delegator_plan_id' => $delegatorPlan->id,
//                'income_usd' => $receivedIncome,
//            ]);

            $userDelegatorIncome = UserDelegatorIncome::create([
                'user_id' => $user->id,
                'closing_date' => $this->delegatorIncomeClosing->closing_date,
                'delegator_subscription_id' => $this->delegatorSubscription->id,
                'subscription_amount' => $subscribedAmount,
                'delegator_plan_id' => $delegatorPlan->id,
                'income_usd' => $receivedIncome,
            ]);

            Log::info($userDelegatorIncome);

            $userCoinWallet = userCoinWalletInstance($user);
            $userCoinWallet->increment('balance', $receivedIncome);

            $userIncomeStat = userIncomeStatInstance($user);
            $userIncomeStat->increment('delegator_bonus', $receivedIncome);
            $userIncomeStat->increment('total_bonus', $receivedIncome);
            $closingDate = Carbon::createFromFormat('Y-m-d', $this->delegatorIncomeClosing->closing_date);
            DelegatorSubscription::where('id', $this->delegatorSubscription->id)->update([
                'next_bonus_date' => $closingDate->addDays(10),
            ]);


            $this->updateDelegatorLevelIncome($user, $levelBonus, $this->delegatorIncomeClosing->closing_date, $userDelegatorIncome->delegator_subscription_id);
        }
    }

    public function updateDelegatorLevelIncome($user, $levelBonus, $closing_date, $userDelegatorSubscriptionId)
    {

        UserLevelMethods::init($user)->eachParentV1(function ($sponsorUser, $level) use (&$user, $levelBonus, $closing_date, $userDelegatorSubscriptionId) {
            if (!$this->isUserActive($sponsorUser)) {
                return;
            }

            $subscription = $sponsorUser->subscription;
            if ($sponsorUser->id > 564) {
                if (is_null($subscription)) {
                    return;
                }
            }

            $sponsorUser->userDelegatorLevelBonuses()->create([
                'closing_date' => $closing_date,
                'downline_user_id' => $user->id,
                'user_delegator_subscription_id' => $userDelegatorSubscriptionId,
                'income_usd' => $levelBonus,
            ]);

            $userCoinWallet = userCoinWalletInstance($sponsorUser);
            $userCoinWallet->increment('balance', $levelBonus);


            $userIncomeStat = userIncomeStatInstance($sponsorUser);
            $userIncomeStat->increment('delegator_level_bonus', $levelBonus);
            $userIncomeStat->increment('total_bonus', $levelBonus);

        }, 1);

    }

    private function isUserActive(User $user): bool
    {
        return !is_null($user->active_at);
    }
}
