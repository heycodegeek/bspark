<?php

namespace App\Jobs;

use App\Models\SmartMetaConvertHistory;
use App\Models\SmartMetaSupply;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SmartMetaSwapJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;
    private float $swapAmount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $swapAmount)
    {
        $this->user = $user;
        $this->swapAmount = $swapAmount;
        Log::info($this->swapAmount);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (userSmartMetaInstance($this->user)->balance < $this->swapAmount) {
            return null;
        }

        $amount = $this->swapAmount;
        $smartMetaPrice = smartMetaPrice();
        $tokenInUsd = multipleDecimalStrings($this->swapAmount, $smartMetaPrice, 10);
        $feeAmount = multipleDecimalStrings($tokenInUsd, '0.10', 10);
        $burnSmartMeta = multipleDecimalStrings($this->swapAmount, '0.10', 10);
        $netReceivableAmount = $tokenInUsd - $feeAmount;

        $userSmartMeta = userSmartMetaInstance($this->user);
        $userSmartMeta->decrement('balance', $amount);
        $userSmartMeta->increment('on_hold', $amount);

        SmartMetaConvertHistory::create([
            'user_id' => $this->user->id,
            'txn_id' => Str::uuid(),
            'particular' => "user swapped the smart meta!",
            'smart_meta' => $amount,
            'smart_meta_price' => $smartMetaPrice,
            'fees' => '0.00',
            'amount' => $tokenInUsd,
            'receivable_amount' => $netReceivableAmount,
            'status' => 'success',
        ]);

        $userSmartMeta->decrement('on_hold', $amount);
        $userSmartMeta->increment('burned', $burnSmartMeta);
        $userSmartMeta->increment('converted_smart_meta', $amount);

        $smartMetaSupply = smartMetaSupplyInstance();
        $smartMetaSupply->increment('total_converted', $amount);
        $smartMetaSupply->increment('burned', $burnSmartMeta);

        $smartMetaSupply->decrement('total_supply', $burnSmartMeta);

        $newSmartMetaPrice = divDecimalStrings($smartMetaSupply->value_in_usd, $smartMetaSupply->total_supply);

        SmartMetaSupply::where('id', $smartMetaSupply->id)->update([
            'smart_meta_price' => $newSmartMetaPrice
        ]);

        $userCoinWallet = userCoinWalletInstance($this->user);
        $userCoinWallet->increment('balance', $netReceivableAmount);

        Log::info('swap finished!');
    }
}
