<?php

namespace App\Jobs;

use App\Blockchains\Tron\Handler;
use App\Models\AdminWithdrawProcessWallet;
use App\Models\EvmTransactionCount;
use App\Models\SubscriptionPool;
use App\Models\User;
use App\Models\WithdrawalHistory;
use App\Mohiqssh\EthereumRpc;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessWithdrawalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public WithdrawalHistory $withdrawalHistory;


    /**
     * Create a new job instance.
     *
     * @param WithdrawalHistory $withdrawalHistory
     */
    public function __construct(WithdrawalHistory $withdrawalHistory)
    {
        $this->onQueue('default');
        $this->withdrawalHistory = $withdrawalHistory->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if ($this->withdrawalHistory->status != 'pending') {
            return;
        }

        $this->withdrawalHistory->update([
            'status' => 'processing'
        ]);

        $withdrawCoin = $this->withdrawalHistory->withdrawCoin;
        if ($withdrawCoin->name === 'trc20') {
            $adminProcessWallet = AdminWithdrawProcessWallet::where('status', 'active')->where('name', $withdrawCoin->name)->whereNotNull('address')->whereNotNull('priv_key')->first();
        } else {
            $adminProcessWallet = null;
        }

        if (is_null($adminProcessWallet)) {
            $this->fail();
            return;
        }

        $userAddress = $this->withdrawalHistory->address;

        $withdrawalableAmount = $this->withdrawalHistory->receivable_amount;

//        $transaction = Handler::init()->sendTrx($adminProcessWallet->address, $adminProcessWallet->priv_key, $userAddress, floatval($withdrawalableAmount));
        $transaction = Handler::init()->sendUsdt($adminProcessWallet->address, $adminProcessWallet->priv_key, $userAddress, floatval($withdrawalableAmount));

        if ($transaction && is_array($transaction)) {
            if (isset($transaction['result']) && $transaction['result'] == true) {
                $this->withdrawalHistory->update([
                    'txn_id' => $transaction['txid'],
                    'status' => 'success'
                ]);
                $this->withdrawalHistory->user->userCoinWallet()->decrement('balance_on_hold', $this->withdrawalHistory->tokens);
//                $this->createSubscriptionPoolStat($this->withdrawalHistory->user_id, $this->withdrawalHistory->fees);
//                $this->addSubscriptionPool($this->withdrawalHistory->fees);

                UpdateSmartMetaFluctuationJob::dispatch($this->withdrawalHistory->fees, $this->withdrawalHistory->user, 'user withdrawal');

            } else {
                $this->withdrawalHistory->update([
                    'status' => 'success'
                ]);

                $this->withdrawalHistory->user->userCoinWallet()->decrement('balance_on_hold', $this->withdrawalHistory->tokens);

                $this->createSubscriptionPoolStat($this->withdrawalHistory->user_id, $this->withdrawalHistory->fees);
                $this->addSubscriptionPool($this->withdrawalHistory->fees);
            }

        }

//        if ($transaction && is_array($transaction)) {
//            $this->withdrawalHistory->update([
//                'txn_id' => $transaction['txid'],
//                'status' => 'success'
//            ]);
//            $this->withdrawalHistory->user->userCoinWallet()->decrement('balance_on_hold', $this->withdrawalHistory->tokens);
//        }


    }

    private function createSubscriptionPoolStat($user, string $amountInUsd)
    {
        $user = User::find($user);
        return $user->subscritionPoolStats()->create([
            'user_id' => $user,
            'amount_in_usd' => castDecimalString($amountInUsd, '2'),
            'type' => 'Withdrawal',
            'status' => 'Success'
        ]);
    }

    private function addSubscriptionPool(string $amountInUsd)
    {
        $amount = castDecimalString($amountInUsd, '2');
        $subscriptionPool = SubscriptionPool::take(1)->first();
        $subscriptionPool->increment('amount_in_usd', $amount);
    }

}
