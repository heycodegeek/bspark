<?php

namespace App\Jobs;

use App\Models\MetaverseTokenPoolStat;
use App\Models\SmartMetaToMetaverseTokenSwapStat;
use App\Models\UserSmartMeta;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateSmartMetaToMetaverseTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private UserSmartMeta $userSmartMeta;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserSmartMeta $userSmartMeta)
    {
        $this->userSmartMeta = $userSmartMeta;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $smartMeta = castDecimalString($this->userSmartMeta->balance, 16);
        $smartMetaPrice = smartMetaPrice();
        $smartMetaUsd = multipleDecimalStrings($smartMeta, $smartMetaPrice);
        $metaTokenPrice = metaTokenPrice();
        $metaverseToken = divDecimalStrings($smartMetaUsd, $metaTokenPrice);

        SmartMetaToMetaverseTokenSwapStat::create([
            'user_id' => $this->userSmartMeta->user_id,
            'smart_meta' => $smartMeta,
            'smart_meta_price' => $smartMetaUsd,
            'metaverse_token' => $metaverseToken,
            'meta_price' => $metaTokenPrice
        ]);

        UserSmartMeta::where('id', $this->userSmartMeta->id)->update([
            'balance' => castDecimalString('0', 10),
            'value_in_usd' => castDecimalString('0', 4),
            'meta_converted' => 1
        ]);
        UpdateUserMetaverseWalletJob::dispatch($this->userSmartMeta->user, $smartMetaUsd, MetaverseTokenPoolStat::TYPE['META_RELEASED'])->delay(now()->addSecond());
    }
}
