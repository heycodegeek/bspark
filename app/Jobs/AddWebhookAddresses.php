<?php

namespace App\Jobs;

use App\Models\DepositWallet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class AddWebhookAddresses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public DepositWallet $userWallet;

    /**
     * Create a new job instance.
     *
     * @param DepositWallet $userWallet
     */
    public function __construct(DepositWallet $userWallet)
    {
        $this->userWallet = $userWallet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::withHeaders([
            'X-Alchemy-Token' => config('alchemy.token'),
        ])->patch(config('alchemy.api_url') . '/update-webhook-addresses', [
            'webhook_id' => config('alchemy.webhook_id'),
            'addresses_to_add' => [$this->userWallet->address],
            'addresses_to_remove' => []
        ]);
    }
}
