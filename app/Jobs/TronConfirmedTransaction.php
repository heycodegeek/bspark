<?php

namespace App\Jobs;

use App\Blockchains\Tron\TransferContractTransaction;
use App\Events\DepositTransactionCreated;
use App\Models\Coin;
use App\Models\CoinType;
use App\Models\DepositWallet;
use App\Models\TronTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TronConfirmedTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $txn;

    /**
     * Create a new job instance.
     *
     * @param array $txn
     */
    public function __construct(array $txn)
    {
        $this->txn = $txn;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transferContract = TransferContractTransaction::init($this->txn);
        $txnId = $transferContract->getTxnId();
        if ($this->isTransactionExists($txnId)) {
            return;
        }
        $depositWallet = $this->findDepositWallet($transferContract->getToAddress());
        if (is_null($depositWallet)) {
            return;
        }
        $user = $depositWallet->user;

        if ((string)$transferContract->getAmount() < castDecimalString('1', 6)) {
            return;
        }

        $tronTransaction = $user->tronTransactions()->create([
            'txn_id' => $txnId,
            'address' => $transferContract->getToAddress(),
            'amount' => $transferContract->getAmount(),
            'confirmations' => 1,
            'txn_time' => $transferContract->getTxnDateTime(),
            'txn_timestamp' => $transferContract->getBlockTimestamp(),
            'confirmed_at' => $transferContract->getTxnDateTime(),
        ]);

        if ($tronTransaction) {
            $depositTransaction = $user->depositTransactions()->create([
                'txn_id' => $tronTransaction->txn_id,
                'address' => $tronTransaction->address,
                'crypto' => 'trx',
                'crypto_price' => tronPrice(),
                'token_price' => tokenPrice(),
                'amount' => castDecimalString((string)$tronTransaction->amount, 8),
                'amount_in_usd' => multipleDecimalStrings(castDecimalString((string)$tronTransaction->amount, 8), tronPrice(), 2),
                'txn_time' => $tronTransaction->txn_time
            ]);

            if ($depositTransaction) {
                PurchaseJob::dispatch($depositTransaction);
            }
        }

        MoveTronFund::dispatch($depositWallet, floatval($transferContract->getAmount()))->delay(now()->addSecond())->onQueue('newFilter');

    }

    private function isTransactionExists(string $txnId): bool
    {
        return TronTransaction::where('txn_id', $txnId)->exists();
    }

    private function findDepositWallet(string $address)
    {
        $coin = Coin::where('name', 'trx')->first();
        $coinType = $coin->coinTypes()->where('type', CoinType::TYPES['legacy'])->first();
        return DepositWallet::where('address', $address)->where('coin_type_id', $coinType->id)->first();
    }
}
