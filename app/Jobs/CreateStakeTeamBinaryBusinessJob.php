<?php

namespace App\Jobs;

use App\Models\BinaryBusiness;
use App\Models\StakeCoinPlan;
use App\Models\StakeSubscriptionHistory;
use App\Models\User;
use App\Models\UserBinaryInfo;
use App\Models\UserBusiness;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CreateStakeTeamBinaryBusinessJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public StakeSubscriptionHistory $subscriptionHistory;

    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StakeSubscriptionHistory $subscriptionHistory, User $user)
    {
        $this->onQueue('binary');
        $this->subscriptionHistory = $subscriptionHistory->withoutRelations();
        $this->user = $user->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userTree = $this->user->tree;

        $parentUser = User::find($userTree->parent_id);
        if (!$parentUser) {
            return;
        }

        $binaryBusiness = BinaryBusiness::firstOrCreate(
            ['user_id' => $parentUser->id],
            ['left_business' => castDecimalString('0', 2), 'left_cf' => castDecimalString('0', 2), 'right_business' => castDecimalString('0', 2), 'right_cf' => castDecimalString('0', 2), 'direct_business' => castDecimalString('0', 2)]
        );

        $userBusiness = UserBusiness::firstOrCreate(
            ['user_id' => $parentUser->id],
            ['left_usd' => castDecimalString('0', 2), 'right_usd' => castDecimalString('0', 2), 'direct_usd' => castDecimalString('0', 2), 'direct_left_usd' => castDecimalString('0', 2), 'direct_right_usd' => castDecimalString('0', 2)]
        );

        if ($userTree->position == 'LEFT') {
            $binaryBusiness->increment('left_business', $this->subscriptionHistory->amount_in_usd);
            $userBusiness->increment('left_usd', $this->subscriptionHistory->amount_in_usd);

        } else {
            $binaryBusiness->increment('right_business', $this->subscriptionHistory->amount_in_usd);
            $userBusiness->increment('right_usd', $this->subscriptionHistory->amount_in_usd);
        }

        if (!is_null($parentUser->active_at) && $this->hasUserStandardBinary($parentUser)) {
            $binaryBusiness->refresh();
            $parentUserSubscription = $parentUser->stakeSubscription;


            if ($binaryBusiness->left_business > $binaryBusiness->right_business) {
                $matchedBusiness = $binaryBusiness->right_business;
            } elseif ($binaryBusiness->right_business > $binaryBusiness->left_business) {
                $matchedBusiness = $binaryBusiness->left_business;
            } else {
                $matchedBusiness = $binaryBusiness->right_business;
            }


            $incomeUsd = multipleDecimalStrings($matchedBusiness, StakeCoinPlan::first()->StakeBinaryPlan->rate_decimal, 2);

            $parentUserBinaryInfo = $parentUser->userBinaryInfo;

            $cappingAvailable = subDecimalStrings($parentUserBinaryInfo->binary_capping_amount, $parentUserBinaryInfo->binary_used_weekly, 2);

            if ($incomeUsd <= $cappingAvailable) {

                $this->createBinaryPayout($parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);


            } else {
                $incomeUsd = $cappingAvailable;
                $this->createBinaryPayout($parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);
            }
        }

        CreateStakeTeamBinaryBusinessJob::dispatch($this->subscriptionHistory, $parentUser);
    }

    private function hasUserStandardBinary(User $user)
    {
        $userTeam = $user->team;
        if ($userTeam->active_direct_left && $userTeam->active_direct_right) {
            return true;
        }
        return false;
    }

    private function createBinaryPayout(User $parentUser, string $matchedBusiness, string $incomeUsd, string $cappingAvailable, UserBinaryInfo $parentUserBinaryInfo, BinaryBusiness $binaryBusiness)
    {
        DB::transaction(function () use (&$parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, &$parentUserBinaryInfo, &$binaryBusiness) {
            if ($incomeUsd > '0') {
                $incomeTokens = numberOfTokens($incomeUsd, tokenPrice());
                $binaryPayout = $parentUser->binaryPayouts()->create([
                    'matched_usd' => $matchedBusiness,
                    'income_amount_usd' => $incomeUsd,
                    'capping_left' => $cappingAvailable,
                    'income_tokens' => $incomeTokens,
                    'token_price' => tokenPrice()
                ]);
                $parentUserBinaryInfo->increment('binary_used_weekly', $incomeUsd);
                $userCoinWallet = userCoinWalletInstance($parentUser);
                $withdrawableWallet = withdrawableWalletInstance($parentUser);
                $halfIncome = multipleDecimalStrings($incomeTokens, '0.5', 8);
                $userCoinWallet->increment('balance', $halfIncome);
                $withdrawableWallet->increment('balance', $halfIncome);
                $userIncomeStat = userIncomeStatInstance($parentUser);
                $userIncomeStat->increment('binary_bonus', $incomeTokens);
                $userIncomeStat->increment('total_bonus', $incomeTokens);

//                event(new BinaryPayoutCreated($binaryPayout));
            }

            $binaryBusiness->decrement('right_business', $matchedBusiness);
            $binaryBusiness->decrement('left_business', $matchedBusiness);
        });
    }

}
