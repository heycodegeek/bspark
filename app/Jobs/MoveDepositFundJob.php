<?php

namespace App\Jobs;

use App\Models\DepositWallet;
use App\Models\FundWallet;
use App\Models\GasPrice;
use App\Mohiqssh\EthereumRpc;
use App\Mohiqssh\FundWalletMethod;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MoveDepositFundJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public DepositWallet $userWallet;

    /**
     * Create a new job instance.
     *
     * @param DepositWallet $userWallet
     */
    public function __construct(DepositWallet $userWallet)
    {
        $this->userWallet = $userWallet;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $fromAddress = $this->userWallet->address;
        $privKey = $this->userWallet->priv_key;

        $networkConfig = config('network.default');
        $requestUrl = config('network.networks.' . $networkConfig . '.rpc_uri');
        $getBalance = $this->getEthBalance($requestUrl, $fromAddress);
        if (isset($getBalance['error'])) {
            return;
        }
//        $this->updateUserWalletBalance($this->userWallet, $getBalance['result']);
        $this->userWallet->refresh();
        $balance = $this->userWallet->balance;
        $fundWallet = FundWallet::where('status', 'active')->whereNotNull('priv_key')->first();

        if ($balance > '0.01' && !is_null($fundWallet)) {
            $fundWalletMethod = FundWalletMethod::fromFundWallet($fundWallet);

            $gasPrice = GasPrice::first()->normal_gas_price + 5;
            $feesOnSend = $this->feesOnTransaction($gasPrice);
            $amountToSend = subDecimalStrings($balance, $feesOnSend, 8);

            if ($amountToSend > '0') {

                $getTransactionCount = json_decode($this->getTransactionCount($requestUrl, $fromAddress), true);

                if (!isset($getTransactionCount['error'])) {
                    $nonce = hexdec($getTransactionCount['result']);
                    $rawTransaction = $this->getRawTransaction($nonce, $fromAddress, $fundWalletMethod->getAddress(), $gasPrice, $amountToSend, $privKey, (int)config('network.networks.' . $networkConfig . '.network_id'));
                    $sendRawTransaction = $this->sendRawTransaction(config('network.networks.' . $networkConfig . '.rpc_uri'), $rawTransaction);

                }


            }

        }

    }

    private function getEthBalance($url, $address)
    {
        return json_decode(EthereumRpc::getBalance($url, $address), true);
    }

    private function feesOnTransaction(string $gasPriceInGwei, $transactionCost = '21000')
    {
        $feesInWei = bcmul($gasPriceInGwei, $transactionCost);
        return bcdiv($feesInWei, pow(10, 9), 9);
    }

    private function getTransactionCount(string $url, string $address)
    {
        return EthereumRpc::getTrasactionCount($url, $address);
    }

    private function getRawTransaction($nonce, $fromAddress, $toAddress, $gasPriceInGwei, $valueInEth, $privKey, int $chainId = 1)
    {
        $transaction = new \Web3p\EthereumTx\Transaction([
            'nonce' => '0x' . dechex((string)$nonce),
            'from' => strtolower($fromAddress),
            'to' => strtolower($toAddress),
            'gas' => '0x' . bcDecHex('21000'),
            'gasPrice' => '0x' . dechex(bcmul((string)$gasPriceInGwei, pow(10, 9))),
            'value' => '0x' . bcDecHex(toWei($valueInEth)),
            'chainId' => $chainId,
            'data' => '0x'
        ]);
        return '0x' . $transaction->sign('0x' . $privKey);
    }

    private function sendRawTransaction($url, $rawTransaction)
    {
        return EthereumRpc::sendRawTransaction($url, $rawTransaction);
    }

}
