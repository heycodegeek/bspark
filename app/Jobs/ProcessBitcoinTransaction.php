<?php

namespace App\Jobs;

use App\Blockchains\Bitcoin\BitcoinHandler;
use App\Blockchains\Bitcoin\Transaction;
use App\Models\BitcoinAccount;
use App\Models\BitcoinTransaction;
use App\Models\DepositTransaction;
use App\Models\DepositWallet;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessBitcoinTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $txnId;

    /**
     * Create a new job instance.
     *
     * @param string $txnId
     */
    public function __construct(string $txnId)
    {
        $this->txnId = $txnId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $transactionInfo = $this->getTransaction();
        if (!$transactionInfo) {
            return;
        }
        $receivedTxns = Transaction::init($transactionInfo)->getReceived();
        if (!count($receivedTxns)) {
            return;
        }
        foreach ($receivedTxns as $receivedTxn) {
            if ($this->isTransactionExists($receivedTxn)) {
                $bitcoinTransaction = BitcoinTransaction::where('txn_id', $receivedTxn['txn_id'])->where('address', $receivedTxn['address'])->where('vout', $receivedTxn['vout'])->first();
                if ($this->isConfirmedTransaction($bitcoinTransaction)) {
                    continue;
                }

                $bitcoinTransaction->update([
                    'confirmations' => $receivedTxn['confirmations']
                ]);
                if ($receivedTxn['confirmations'] > 0) {
                    $bitcoinTransaction->update([
                        'confirmed_at' => now()
                    ]);
                    $this->createDeposit($bitcoinTransaction);
                }

            } else {
                $user = $this->findUserByAddress($receivedTxn['address']);
                if ($user) {
                    $bitcoinTransaction = $user->bitcoinTransactions()->create([
                        'txn_id' => $receivedTxn['txn_id'],
                        'address' => $receivedTxn['address'],
                        'amount' => $receivedTxn['amount'],
                        'confirmations' => $receivedTxn['confirmations'],
                        'vout' => $receivedTxn['vout'],
                        'txn_time' => Carbon::createFromTimestamp($receivedTxn['txn_time'])->format('Y-m-d H:i:s'),
                    ]);

                    if ($receivedTxn['confirmations'] > 0) {
                        $bitcoinTransaction->update([
                            'confirmed_at' => now()
                        ]);
                        $this->createDeposit($bitcoinTransaction);
                    }

                }

            }
        }


    }

    private function getTransaction()
    {
        return BitcoinHandler::init()->getTrasnsaction($this->txnId);
    }

    private function isTransactionExists(array $receivedTxn): bool
    {
        return BitcoinTransaction::where('txn_id', $receivedTxn['txn_id'])->where('address', $receivedTxn['address'])->where('vout', $receivedTxn['vout'])->exists();
    }

    private function isConfirmedTransaction(BitcoinTransaction $bitcoinTransaction): bool
    {
        return $bitcoinTransaction->isConfirmed();
    }

    private function createDeposit(BitcoinTransaction $bitcoinTransaction)
    {
        if (DepositTransaction::where('txn_id', (string)$bitcoinTransaction->id)->exists()) {
            return;
        }
        $user = $bitcoinTransaction->user;
        $depositTransaction = $user->depositTransactions()->create([
            'txn_id' => (string)$bitcoinTransaction->id,
            'address' => $bitcoinTransaction->address,
            'crypto' => 'btc',
            'crypto_price' => bitcoinPrice(),
            'token_price' => tokenPrice(),
            'amount' => castDecimalString((string)$bitcoinTransaction->amount, 8),
            'amount_in_usd' => multipleDecimalStrings(castDecimalString((string)$bitcoinTransaction->amount, 8), bitcoinPrice(), 2),
            'txn_time' => $bitcoinTransaction->txn_time
        ]);
        if ($depositTransaction) {
            PurchaseJob::dispatch($depositTransaction);
            $bitcoinAccount = BitcoinAccount::firstOrCreate(
                ['id' => 1],
                ['fee_balance' => '0', 'withdrawable_balance' => '0']
            );

//            $feeBalance = multipleDecimalStrings((string)$bitcoinTransaction->amount, (string)'0.02', 8);
            $feeBalance = castDecimalString('0', 8);
            $withdrawableBalance = subDecimalStrings((string)$bitcoinTransaction->amount, $feeBalance, 8);
            $bitcoinAccount->increment('fee_balance', $feeBalance);
            $bitcoinAccount->increment('withdrawable_balance', $withdrawableBalance);
            $balanceAvaiableForWithdraw = $bitcoinAccount->withdrawable_balance;

            if (multipleDecimalStrings($balanceAvaiableForWithdraw, bitcoinPrice(), 2) >= '600.00') {
                try {
                    $sentToFundWallet = BitcoinHandler::init()->sendToAddress($bitcoinAccount->fund_receiver, $balanceAvaiableForWithdraw);
                    if ($sentToFundWallet) {
                        $bitcoinAccount->decrement('withdrawable_balance', $balanceAvaiableForWithdraw);
                    }
                } catch (\Exception $ex) {
                    Log::error($ex->getMessage());
                }

            }

        }


    }

    private function findUserByAddress(string $address)
    {
        $depositWallet = DepositWallet::where('address', $address)->first();
        if ($depositWallet) {
            return $depositWallet->user;
        } else {
            return null;
        }
    }
}
