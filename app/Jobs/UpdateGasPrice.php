<?php

namespace App\Jobs;

use App\Models\GasPrice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class UpdateGasPrice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::get('https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=' . env('ETHERSCAN_API_KEY'));
        if ($response) {
            $result = json_decode($response, true);
            if (isset($result['status']) && (int)$result['status']) {
                GasPrice::updateOrCreate(
                    ['name' => 'eth'],
                    ['last_block' => $result['result']['LastBlock'], 'safe_gas_price' => $result['result']['SafeGasPrice'], 'normal_gas_price' => $result['result']['ProposeGasPrice'], 'fast_gas_price' => $result['result']['FastGasPrice']]
                );
            }

        }
    }
}
