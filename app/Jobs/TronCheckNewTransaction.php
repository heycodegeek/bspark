<?php

namespace App\Jobs;

use App\Blockchains\Tron\Handler;
use App\Blockchains\Tron\TronRpc;
use App\Models\DepositWallet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TronCheckNewTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public DepositWallet $depositWallet;

    /**
     * Create a new job instance.
     *
     * @param DepositWallet $depositWallet
     */
    public function __construct(DepositWallet $depositWallet)
    {
        $this->depositWallet = $depositWallet->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->isTronWallet()) {
            return;
        }
        //        $getBalance = Handler::init()->getBalance($this->depositWallet->address);
        //        if ((string)$getBalance > castDecimalString('0', 6)) {
        $address = $this->depositWallet->address;
        $user = $this->depositWallet->user;
        $tronTransactions = $user->tronTransactions()->orderByDesc('txn_timestamp')->first();
        if (is_null($tronTransactions)) {
            $newTransactions = json_decode(TronRpc::getAccountTransactions(Handler::init()->getRpcUrl(), $address), true);
        } else {
            $newTransactions = json_decode(TronRpc::getAccountTransactions(Handler::init()->getRpcUrl(), $address, true, false, $tronTransactions->txn_timestamp + 1), true);
        }
        if (!isset($newTransactions['data'])) {
//            TronCheckNewTransaction::dispatch($this->depositWallet)->delay(now()->addMinutes(30))->onQueue('newFilter');
            return;
        }

        foreach ($newTransactions['data'] as $txn) {
            if (!$this->isTransferContractTxn($txn)) {
                continue;
            }
            if ($txn['ret'][0]['contractRet'] == "FAILED") {
                continue;
            }
            if ($txn['ret'][0]['contractRet'] == 'PENDING') {
                TronCheckNewTransaction::dispatch($this->depositWallet)->delay(now()->addSeconds(5))->onQueue('newFilter');
                continue;
            }
            if ($txn['ret'][0]['contractRet'] == 'SUCCESS') {
                TronConfirmedTransaction::dispatch($txn)->delay(now()->addSeconds(1))->onQueue('newFilter');
                continue;
            }

        }

        //        TronCheckNewTransaction::dispatch($this->depositWallet)->delay(now()->addMinutes(5))->onQueue('newFilter');
        //        TronCheckNewTransaction::dispatch($this->depositWallet)->delay(now()->addMinutes(10))->onQueue('newFilter');

    }

    private function isTronWallet(): bool
    {
        $coin = $this->depositWallet->coinType->coin;
        return $coin->name == 'trx';
    }

    private function isTransferContractTxn(array $txn)
    {
        if (!isset($txn['ret'])) {
            return false;
        }
        if (!isset($txn['raw_data'])) {
            return false;
        }
        if (!isset($txn['raw_data']['contract'])) {
            return false;
        }
        if ($txn['raw_data']['contract'][0]['type'] !== 'TransferContract') {
            return false;
        }
        return true;

    }


}
