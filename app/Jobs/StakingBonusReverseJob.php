<?php

namespace App\Jobs;

use App\Models\Staking;
use App\Models\StakingBonus;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StakingBonusReverseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $bonusDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bonusDate)
    {
        $this->bonusDate = $bonusDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $stakingBonus = StakingBonus::where('bonus_date', $this->bonusDate)->get();
        foreach ($stakingBonus as $bonus) {
            $staking = Staking::where('id', $bonus->staking_id)->first();
            $staking->decrement('earned_so_far', $bonus->amount_in_usd);
            $this->deductCoinFromWallet($staking->user, $bonus->amount_in_usd);
            StakingBonus::where('id', $bonus->id)->delete();
        }
    }

    private function deductCoinFromWallet(User $user, $amount)
    {
        $userCoinWallet = userCoinWalletInstance($user);
        $userCoinWallet->decrement('balance', $amount);
    }
}
