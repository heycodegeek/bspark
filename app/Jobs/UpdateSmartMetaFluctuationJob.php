<?php

namespace App\Jobs;

use App\Models\SmartMetaFluctuationHistory;
use App\Models\SmartMetaSupply;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateSmartMetaFluctuationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public float $liquidityAmount;
    public User $user;
    public string $particular;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($liquidityAmount, User $user, $particular)
    {
        $this->liquidityAmount = $liquidityAmount;
        $this->user = $user;
        $this->particular = $particular;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smartMetaSupply = smartMetaSupplyInstance();

        $SmartMetaFluctuationHistory = SmartMetaFluctuationHistory::create([
            'user_id' => $this->user->id,
            'amount_in_usd' => $this->liquidityAmount,
            'previous_value_usd' => $smartMetaSupply->value_in_usd,
            'previous_smart_meta_price' => $smartMetaSupply->smart_meta_price,
            'particular' => $this->particular
        ]);

        if (!is_null($SmartMetaFluctuationHistory)) {
            $smartMetaSupply->increment('value_in_usd', $this->liquidityAmount);
            $newSmartMetaSupply = smartMetaSupplyInstance();

            $newSmartMetaPrice = divDecimalStrings(castDecimalString($newSmartMetaSupply->value_in_usd, 16), castDecimalString($newSmartMetaSupply->total_supply, 16));

            SmartMetaSupply::where('id', $newSmartMetaSupply->id)->update([
                'smart_meta_price' => $newSmartMetaPrice
            ]);

        }
    }
}
