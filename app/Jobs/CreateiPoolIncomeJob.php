<?php

namespace App\Jobs;

use App\Models\InternationalPoolAchiever;
use App\Models\InternationalPoolDistributionStats;
use App\Models\InternationalPoolIncomeStats;
use App\Models\MetaverseTokenPoolStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateiPoolIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private InternationalPoolAchiever $iPoolAchiever;
    private InternationalPoolDistributionStats $iPoolDistributionStats;
    private float $incomeUsd;
    private float $incomeMetaToken;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(InternationalPoolAchiever $iPoolAchiever, $incomeUsd, $incomeMetaToken, InternationalPoolDistributionStats $iPoolDistributionStats)
    {
        $this->iPoolAchiever = $iPoolAchiever;
        $this->incomeUsd = $incomeUsd;
        $this->incomeMetaToken = $incomeMetaToken;
        $this->iPoolDistributionStats = $iPoolDistributionStats;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        InternationalPoolIncomeStats::create([
            'user_id' => $this->iPoolAchiever->user_id,
            'international_pool_distribution_stats_id' => $this->iPoolDistributionStats->id,
            'amount' => $this->incomeUsd,
            'meta_token' => $this->incomeMetaToken,
            'income_date' => $this->iPoolDistributionStats->income_date
        ]);

        UpdateUserMetaverseWalletJob::dispatch($this->iPoolAchiever->user, $this->incomeUsd, MetaverseTokenPoolStat::TYPE['META_RELEASED'])->delay(now()->addSecond());

//        $userMetaWallet = userMetaverseWalletInstance($this->iPoolAchiever->user);
//        $userMetaWallet->increment('token', $this->incomeMetaToken);
//        $userMetaWallet->increment('value_in_usd', $this->incomeUsd);

    }
}
