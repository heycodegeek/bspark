<?php

namespace App\Jobs;

use App\Models\StakeIncomeClosing;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateStakeIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('income');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $today = now()->format('Y-m-d');
        $closing = StakeIncomeClosing::firstOrCreate(
            ['closing_date' => $today],
            ['status' => 'pending']
        );
        if ($closing->status == 'pending') {
            foreach (Subscription::whereDate('created_at', '<=', $today)->where('end_date', '>', $today)->cursor() as $subscription) {
                CreateStakeIncomeJob::dispatch($subscription, $closing)->delay(now()->addSeconds(2))->onQueue('income');
            }
            $closing->update([
                'status' => 'success'
            ]);
        }
    }
}
