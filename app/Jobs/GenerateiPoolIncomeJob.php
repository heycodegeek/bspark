<?php

namespace App\Jobs;

use App\Models\InternationalPool;
use App\Models\InternationalPoolAchiever;
use App\Models\InternationalPoolDistributionStats;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateiPoolIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $incomeDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $incomeDate)
    {
        $this->onQueue('income');
        $this->incomeDate = $incomeDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $distributionDate = $this->incomeDate;
        $iPool = InternationalPool::where('balance', '>', castDecimalString('0', 4))->where('next_distribution_date', $distributionDate)->first();
        if (is_null($iPool)) {
            return;
        }
        $totalAchiever = InternationalPoolAchiever::where('is_eligible_month', 1)->where('is_active', 1)->count();
        if ($totalAchiever === 0) {
            return;
        }

        $availableFund = castDecimalString($iPool->balance, 4);
        $incomeUsd = divDecimalStrings($availableFund, $totalAchiever);
        $incomeMetaToken = divDecimalStrings($incomeUsd, metaTokenPrice());

        $iPoolDistributionStats = InternationalPoolDistributionStats::create([
            'income_date' => $distributionDate,
            'amount' => $availableFund,
            'meta_token' => divDecimalStrings($availableFund, metaTokenPrice()),
            'no_of_beneficiaries' => $totalAchiever,
            'first_time_achievers' => $totalAchiever,
            'monthly_achievers' => 0
        ]);

        foreach (InternationalPoolAchiever::where('is_eligible_month', 1)->where('is_active', 1)->cursor() as $iPoolAchiever) {
            CreateiPoolIncomeJob::dispatch($iPoolAchiever, $incomeUsd, $incomeMetaToken, $iPoolDistributionStats)->delay(now()->addSeconds())->onQueue('income');
        }
        InternationalPool::where('id', $iPool->id)->update([
            'balance' => castDecimalString('0', 4),
            'next_distribution_date' => '2023-01-01'
        ]);

    }
}
