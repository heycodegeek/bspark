<?php

namespace App\Jobs;

use App\Models\MetaverseTokenPoolStat;
use App\Models\User;
use App\Models\UserMetaverseWalletTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateUserMetaverseWalletJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;
    private float $amount_in_usd;
    private string $type;
    private bool $is_buddy_bot;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, float $amount_in_usd, $type, $is_buddy_bot = false)
    {
        $this->user = $user;
        $this->amount_in_usd = $amount_in_usd;
        $this->type = $type ?? MetaverseTokenPoolStat::TYPE['META_RELEASED'];
        $this->is_buddy_bot = $is_buddy_bot;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $metaTokenPrice = metaTokenPrice();
        $token = divDecimalStrings($this->amount_in_usd, $metaTokenPrice);
        $userMeta = userMetaverseWalletInstance($this->user);
        $userMeta->increment('token', $token);
        $userMeta->increment('value_in_usd', $this->amount_in_usd);
        dispatch(new UpdateMetaPoolJob($this->amount_in_usd, $token, $this->type, $this->is_buddy_bot));

        UserMetaverseWalletTransaction::create([
            'user_id' => $this->user->id,
            'particular' => $this->type,
            'tokens' => $token,
            'meta_token_price' => $metaTokenPrice,
            'amount_in_usd' => $this->amount_in_usd,
            'status' => 'success',
            'type' => 'credit'
        ]);
    }
}
