<?php

namespace App\Jobs;

use App\Models\SubscriptionPool;
use App\Models\SubscriptionPoolBonus;
use App\Models\SubscriptionPoolHistory;
use App\Models\User;
use App\Models\UserCoinWallet;
use App\Models\UserIncomeStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DistributePoolIncomeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('income');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $poolBalance = SubscriptionPool::first()->amount_in_usd;
        if ($poolBalance < 1) {
            return;
        }
        $paidUser = User::has('subscription')->get();
        if ($paidUser->count() < 1) {
            return null;
        }
        $decPoolBalance = castDecimalString($poolBalance, 2);
        $SubscriptionHistory = SubscriptionPoolHistory::create([
            'amount_in_usd' => $decPoolBalance,
            'receiver' => $paidUser->count(),
            'status' => 'success'
        ]);
        if (!is_null($SubscriptionHistory)) {
            $decPoolBalancePerUser = divDecimalStrings($SubscriptionHistory->amount_in_usd, $paidUser->count());

            foreach ($paidUser as $user) {
                $this->updateStats($user->id, $decPoolBalancePerUser);
                SubscriptionPoolBonus::create([
                    'user_id' => $user->id,
                    'subscription_pool_history_id' => $SubscriptionHistory->id,
                    'amount_in_usd' => $decPoolBalancePerUser,
                    'status' => 'success'
                ]);
            }
            $pool = SubscriptionPool::first();
            $pool->amount_in_usd = 0;
            $pool->save();
        } else {
            return null;
        }
    }

    public function updateStats($user_id, $amount)
    {
        $amount = castDecimalString($amount, 2);
        $incomeStats = UserIncomeStat::where('user_id', $user_id)->first();
        if (is_null($incomeStats)) {
            $incomeStats = UserIncomeStat::Create(['user_id' => $user_id, 'daily_bonus' => castDecimalString('0', 2), 'direct_bonus' => castDecimalString('0', 2), 'matching_bonus' => castDecimalString('0', 2), 'total_bonus' => castDecimalString('0', 2), 'binary_bonus' => castDecimalString('0', 2)]);
        }

        $incomeStats->increment('total_bonus', $amount);
        $incomeStats->increment('matching_bonus', $amount);

        $userCoinWallet = UserCoinWallet::where('user_id', $user_id)->first();
        $userCoinWallet->increment('balance', $amount);
    }
}
