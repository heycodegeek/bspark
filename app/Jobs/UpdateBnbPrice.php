<?php

namespace App\Jobs;

use App\Models\CryptoPrice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class UpdateBnbPrice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::get('https://api.binance.com/api/v3/ticker/price?symbol=BNBUSDT');

        if ($response) {
            $result = json_decode($response, true);
            if (!isset($result['code'])) {
                CryptoPrice::updateOrCreate(
                    ['name' => 'bnb'],
                    ['price_in_usd' => $result['price'], 'price_in_btc' => '0.0000000']
                );
            }
        }

        $response = Http::get('https://api.binance.com/api/v3/ticker/price?symbol=LTCUSDT');

        if ($response) {
            $result = json_decode($response, true);
            if (!isset($result['code'])) {
                CryptoPrice::updateOrCreate(
                    ['name' => 'ltc'],
                    ['price_in_usd' => $result['price'], 'price_in_btc' => '0.0000000']
                );
            }
        }

        $response = Http::get('https://api.binance.com/api/v3/ticker/price?symbol=XTZUSDT');

        if ($response) {
            $result = json_decode($response, true);
            if (!isset($result['code'])) {
                CryptoPrice::updateOrCreate(
                    ['name' => 'xtz'],
                    ['price_in_usd' => $result['price'], 'price_in_btc' => '0.0000000']
                );
            }
        }
    }
}
