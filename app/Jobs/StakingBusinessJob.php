<?php

namespace App\Jobs;

use App\Models\StakingBusiness;
use App\Models\Subscription;
use App\Models\Tree;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StakingBusinessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $userTree;
    public $amount;
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $amount)
    {
        $this->onQueue('income');
        $this->user = $user;
        $this->amount = $amount;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userTree = $this->user->tree;

        if (is_null($userTree)) {
            return;
        }

        if (!$userTree->sponsor_id) {
            return;
        }

        $sponsor = User::where('id', $userTree->sponsor_id)->first();
        if (!$this->isUserActive($sponsor)) {
            return;
        }
        $sponsorUserTree = Tree::where('user_id', $userTree->sponsor_id)->first();
        $stakingBusiness = StakingBusiness::firstOrCreate(
            ['user_id' => $userTree->sponsor_id],
            ['sponsor_id' => $sponsorUserTree->sponsor_id ?? 0,
                'left_staking_amount' => castDecimalString('0', 2),
                'right_staking_amount' => castDecimalString('0', 2),
                'total_staking_business' => castDecimalString('0', 2)
            ]);

        $stakingBusiness->increment('total_staking_business', $this->amount);

        if ($userTree->position == 'LEFT') {
            $stakingBusiness->increment('left_staking_amount', $this->amount);

        } else {
            $stakingBusiness->increment('right_staking_amount', $this->amount);
        }

////        $user = $this->user;
//        UserLevelMethods::init($this->user)->eachParentV1(function ($sponsorUser, $level) use (&$user, $userTree) {
//            if (!$this->isUserActive($sponsorUser)) {
//                return;
//            }
//
//            $stakingBusiness = StakingBusiness::firstOrCreate(
//                ['user_id' => $sponsorUser->id],
//                ['sponsor_id' => $sponsorUser->sponsor->id ?? 0, 'left_staking_amount' => castDecimalString('0', 2), 'right_staking_amount' => castDecimalString('0', 2),]
//            );
//
//            if ($userTree->position == 'LEFT') {
//                $stakingBusiness->increment('left_staking_amount', $this->amount);
//
//            } else {
//                $stakingBusiness->increment('right_staking_amount', $this->amount);
//            }
//
//        }, 7);

    }

    private function isUserActive(User $user): bool
    {
        $subscriptions = Subscription::where('user_id', $user->id)->get();
        return !is_null($subscriptions) ? true : false;
    }
}
