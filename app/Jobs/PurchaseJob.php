<?php

namespace App\Jobs;

use App\Models\DepositTransaction;
use App\Mohiqssh\PurchaseMethod;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PurchaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public DepositTransaction $txn;

    /**
     * Create a new job instance.
     *
     * @param DepositTransaction $txn
     */
    public function __construct(DepositTransaction $txn)
    {
        $this->txn = $txn->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PurchaseMethod::fromTransaction($this->txn)->init();
    }
}
