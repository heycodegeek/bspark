<?php

namespace App\Jobs;

use App\Models\MetaverseTokenPoolStat;
use App\Models\StakingBusiness;
use App\Models\StakingBusinessBonusStats;
use App\Models\StakingTeamDistributionPlan;
use App\Models\Subscription;
use App\Models\Tree;
use App\Models\User;
use App\Mohiqssh\UserLevelMethods;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StakingBonusTeamDistributionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $amount;
    public User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $amount)
    {
        $this->onQueue('income');
        $this->user = $user;
        $this->amount = $amount;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $amount = $this->amount;

        if (!$this->user->tree->sponsor_id) {
            return;
        }

        UserLevelMethods::init($this->user)->eachParentV7(function ($sponsorUser, $level) use (&$user, $amount) {
            if (!$this->isUserActive($sponsorUser)) {
                return;
            }

            $sponsorUserTree = Tree::where('user_id', $sponsorUser->id)->first();
            $stakingBusiness = StakingBusiness::firstOrCreate(
                ['user_id' => $sponsorUser->id],
                ['sponsor_id' => $sponsorUserTree->sponsor_id ?? 0,
                    'left_staking_amount' => castDecimalString('0', 2),
                    'right_staking_amount' => castDecimalString('0', 2),
                    'total_staking_business' => castDecimalString('0', 2)
                ]);

            $stakingBonusPlan = StakingTeamDistributionPlan::where('level', $level)->first();
            if (is_null($stakingBonusPlan)) {
                return;
            }

            if (castDecimalString($stakingBonusPlan->team_business, 4) > castDecimalString($stakingBusiness->total_staking_business, 4)) {
                return;
            }

            $bonusAmount = multipleDecimalStrings(castDecimalString($amount, 4), castDecimalString($stakingBonusPlan->rate_decimal, 4));
            StakingBusinessBonusStats::create([
                'user_id' => $sponsorUser->id,
                'from_user_id' => $this->user->id ?? 0,
                'bonus_amount' => $bonusAmount,
                'level' => $level
            ]);
            $stakingBusiness->increment('earned_so_far', castDecimalString($bonusAmount, 4));
            $this->addCoinToWallet($sponsorUser, $bonusAmount);
//            UpdateUserMetaverseWalletJob::dispatch($sponsorUser, $bonusAmount, MetaverseTokenPoolStat::TYPE['STAKING_LEVEL_BONUS'],true)->delay(now()->addSecond());

        }, 7);

    }

    private function isUserActive(User $user): bool
    {
        $subscriptions = Subscription::where('user_id', $user->id)->get();
        return !is_null($subscriptions) ? true : false;
    }

    private function addCoinToWallet(User $user, $amount)
    {
        $userCoinWallet = userCoinWalletInstance($user);
        $userCoinWallet->increment('balance', $amount);
    }

}
