<?php

namespace App\Jobs;

use App\Models\Staking;
use App\Models\StakingBusiness;
use App\Models\Subscription;
use App\Models\Tree;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GenerateBuddyBotBusiness implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Staking $staking;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Staking $staking)
    {
        $this->staking = $staking;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userTree = $this->staking->user->tree;

        if (is_null($userTree)) {
            return;
        }

        if (!$userTree->sponsor_id) {
            return;
        }

        $sponsor = User::where('id', $userTree->sponsor_id)->first();
        if (!$this->isUserActive($sponsor)) {
            return;
        }
        $sponsorUserTree = Tree::where('user_id', $userTree->sponsor_id)->first();
        $stakingBusiness = StakingBusiness::firstOrCreate(
            ['user_id' => $userTree->sponsor_id],
            ['sponsor_id' => $sponsorUserTree->sponsor_id ?? 0,
                'left_staking_amount' => castDecimalString('0', 2),
                'right_staking_amount' => castDecimalString('0', 2),
                'total_staking_business' => castDecimalString('0', 2)
            ]);

        $stakingBusiness->increment('total_staking_business', $this->staking->amount_in_usd);

        if ($userTree->position == 'LEFT') {
            $stakingBusiness->increment('left_staking_amount', $this->staking->amount_in_usd);

        } else {
            $stakingBusiness->increment('right_staking_amount', $this->staking->amount_in_usd);
        }

    }

    private function isUserActive(User $user): bool
    {
        $subscriptions = Subscription::where('user_id', $user->id)->get();
        return !is_null($subscriptions);
    }
}
