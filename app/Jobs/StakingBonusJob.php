<?php

namespace App\Jobs;

use App\Models\MetaverseTokenPoolStat;
use App\Models\SmartMetaConvertHistory;
use App\Models\Staking;
use App\Models\StakingBonus;
use App\Models\StakingDistributionStat;
use App\Models\StakingPool;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class StakingBonusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $bonus;

    public function __construct($bonus)
    {
        $this->bonus = castDecimalString($bonus, 8);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stakings = Staking::where('max_amount_limit_usd', '>', 'earned_so_far')->orderBy('user_id', 'ASC')->where('is_active', 1)->get();
//        $stakings = Staking::where('max_amount_limit_usd', '>', 'earned_so_far')->where('amount_in_usd', '<=', castDecimalString('2000', 2))->orderBy('user_id', 'ASC')->get();
        $totalBonus = (float)0;
        $no_of_staking = 0;

        foreach ($stakings as $staking) {
            $max_amount_limit_usd = castDecimalString($staking->max_amount_limit_usd, 8);
            $earned_so_far = castDecimalString($staking->earned_so_far, 8);
            $bonusAmount = castDecimalString(multipleDecimalStrings($staking->amount_in_usd, $this->bonus), 8);
            if (((float)$earned_so_far + (float)$bonusAmount) > (float)$max_amount_limit_usd) {
                $bonusAmount = $max_amount_limit_usd - $earned_so_far;
            }
            $stakingBonus = StakingBonus::where(
                [
                    'staking_id' => $staking->id,
                    'bonus_date' => date("Y-m-d")
                ])->count();
            if ($stakingBonus == 0) {
                $totalBonus = (float)$totalBonus + (float)$bonusAmount;
                $no_of_staking = $no_of_staking + 1;
                $staking->stakingBonus()->create([
                    'amount_in_usd' => $bonusAmount,
                    'bonus_date' => date("Y-m-d")
                ]);
                $staking->increment('earned_so_far', $bonusAmount);
//                $this->addUserSmartMeta($staking->user, $bonusAmount);
//                UpdateUserMetaverseWalletJob::dispatch($staking->user, $bonusAmount, MetaverseTokenPoolStat::TYPE['STAKING_BONUS'], true)->delay(now()->addSecond());
                $this->addCoinToWallet($staking->user, $bonusAmount);
                if ($staking->id > 157) {
                    StakingBonusTeamDistributionJob::dispatch($staking->user, $bonusAmount);
                }
            }
        }
        if ($no_of_staking > 0) {
            StakingPool::where('id', 1)->increment('distributed_so_for', $totalBonus);

            /**
             * distribute to upline;
             */

            StakingDistributionStat::firstOrCreate([
                'amount_in_usd' => $totalBonus,
                'no_of_staking' => $no_of_staking,
                'bonus_date' => date("Y-m-d")
            ]);
        }
    }

    private function addCoinToWallet(User $user, $amount)
    {
        $userCoinWallet = userCoinWalletInstance($user);
        $userCoinWallet->increment('balance', $amount);
    }

    public function addUserSmartMeta(User $user, $amount)
    {

        $bonusAmount = castDecimalString($amount, 4);
        $smartMetaSupply = smartMetaSupplyInstance();
        $releaseCoin = divDecimalStrings($bonusAmount, smartMetaPrice());

        $userSmartMeta = userSmartMetaInstance($user);
        $userSmartMeta->increment('balance', $releaseCoin);
        $userSmartMeta->increment('value_in_usd', $bonusAmount);

        $smartMetaSupply->increment('in_circulation', $releaseCoin);
        $smartMetaSupply->decrement('available_supply', $releaseCoin);

        SmartMetaConvertHistory::create([
            'user_id' => $user->id,
            'txn_id' => Str::uuid(),
            'particular' => "user smart meta released!",
            'smart_meta' => $releaseCoin,
            'smart_meta_price' => smartMetaPrice(),
            'fees' => '0.00',
            'amount' => $bonusAmount,
            'receivable_amount' => $bonusAmount,
            'status' => 'success',
        ]);

    }


}
