<?php

namespace App\Jobs;

use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GenerateIPoolAchieverJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $startDate = '2022-11-02';
        $endDate = '2022-12-03';

        foreach (Subscription::whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<', $endDate)->orderBy('id', 'ASC')->cursor() as $subscription) {
            CreateIPoolAchieverJob::dispatch($subscription)->delay(now()->addSeconds());
        }
    }
}
