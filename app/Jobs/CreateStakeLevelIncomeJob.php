<?php

namespace App\Jobs;

use App\Models\StakeCoinPlan;
use App\Models\StakeSubscriptionHistory;
use App\Models\User;
use App\Mohiqssh\UserLevelMethods;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateStakeLevelIncomeJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public StakeSubscriptionHistory $subscriptionHistory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StakeSubscriptionHistory $subscriptionHistory)
    {
        $this->onQueue('income');
        $this->subscriptionHistory = $subscriptionHistory;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->subscriptionHistory->user;
        UserLevelMethods::init($user)->eachParentV1(function ($sponsorUser, $level) use (&$user) {
            if (!$this->isUserActive($sponsorUser)) {
                return;
            }
//            if (!$this->hasSponsorCompletedStandardBinary($sponsorUser)) {
//                return;
//            }

//            $stakeSubscription = $sponsorUser->stakeSubscription;
//            if (is_null($stakeSubscription)) {
//                return;
//            }
            $levelPlan = StakeCoinPlan::first()->stakelevelPlans()->where('level', $level)->first();
            if (is_null($levelPlan)) {
                return;
            }
            $userLevelIncome = $sponsorUser->userLevelIncomes()->firstOrCreate(
                ['income_date' => $this->subscriptionHistory->created_at->format('Y-m-d')],
                ['tokens' => castDecimalString('0', 8), 'usd' => castDecimalString('0', 2)]
            );


            $incomeTokens = multipleDecimalStrings($levelPlan->rate_decimal, $this->subscriptionHistory->stake_tokens, 8);

            $userLevelIncome->userLevelIncomeStats()->create([
                'subscription_history_id' => $this->subscriptionHistory->id,
                'level' => $level,
                'income_tokens' => $incomeTokens,
                'usd' => multipleDecimalStrings($incomeTokens, $this->subscriptionHistory->token_price, 2)
            ]);

            $userLevelIncome->increment('tokens', $incomeTokens);
            $userLevelIncome->increment('usd', multipleDecimalStrings($incomeTokens, $this->subscriptionHistory->token_price, 2));

            $userCoinWallet = userCoinWalletInstance($sponsorUser);
            $withdrawableWallet = withdrawableWalletInstance($sponsorUser);
            $halfIncome = multipleDecimalStrings($incomeTokens, '0.5', 8);
            $userCoinWallet->increment('balance', $halfIncome);
            $withdrawableWallet->increment('balance', $halfIncome);
            $userIncomeStat = userIncomeStatInstance($sponsorUser);
            $userIncomeStat->increment('direct_bonus', $incomeTokens);
            $userIncomeStat->increment('total_bonus', $incomeTokens);

        }, 4);
    }

    private function isUserActive(User $user): bool
    {
        return !is_null($user->active_at);
    }

    private function hasSponsorCompletedStandardBinary(User $user): bool
    {
        $userTeam = $user->team;
        if ($userTeam && $userTeam->active_direct_left && $userTeam->active_direct_right) {
            return true;
        }
        return false;
    }
}
