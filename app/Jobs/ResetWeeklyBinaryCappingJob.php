<?php

namespace App\Jobs;

use App\Models\UserBinaryInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ResetWeeklyBinaryCappingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $timeout = 1200;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('binary');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (now()->isMonday()) {
            foreach (UserBinaryInfo::cursor() as $userBinaryInfo) {
                $userBinaryInfo->update([
                    'binary_used_weekly' => castDecimalString('0', 2)
                ]);

            }
        }

    }
}
