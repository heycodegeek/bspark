<?php

namespace App\Jobs;

use App\Models\Subscription;
use App\Models\Tree;
use App\Models\User;
use App\Mohiqssh\MonthlyTeamStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CreateIPoolAchieverJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Subscription $subscription;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->subscription->refresh();
        $userTree = Tree::where('user_id', $this->subscription->user_id)->first();
        if ($userTree) {
            $sponsorUser = User::find($userTree->sponsor_id);
            if ($sponsorUser) {
                $sponsorTeam = $sponsorUser->teamMonthlyStat()->firstOrCreate();
                $sponsorTeam->increment('active_direct');
                if ($userTree->position == 'LEFT') {
                    $sponsorTeam->increment('active_direct_left');
                } else {
                    $sponsorTeam->increment('active_direct_right');
                }
            }
        }

        TeamMonthlyStatsJob::dispatch($this->subscription->user)->delay(now()->addSeconds());

    }
}
