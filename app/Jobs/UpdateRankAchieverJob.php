<?php

namespace App\Jobs;

use App\Models\User;
use App\Mohiqssh\RankMethod;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateRankAchieverJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public User $user;

    public $timeout = 1200;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->onQueue('newFilter');
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        if (is_null($this->user->active_at)) {
//            return;
//        }
//
//        if ($this->user->userRank) {
//            return;
//        }
//        $userBusiness = $this->user->userBusiness;
//        if (!$userBusiness) {
//            return;
//        }
//        if ($userBusiness->left_usd < castDecimalString('100', 2) || $userBusiness->right < castDecimalString('100', 2)) {
//            return;
//        }
//
//        if (!$this->user->rankTeam) {
//            $userRankTeam = RankTeam::firstOrCreate(
//                ['user_id' => $this->user->id],
//                ['left_rank_count' => 0, 'right_rank_count' => 0]
//            );
//
//            $this->user->userRank()->create([
//                'rank_id' => 1
//            ]);
//        } else {
//            $userRankTeam = $this->user->rankTeam;
//            $rank = Rank::where('left_rank_count', '<=', $userRankTeam->left_rank_count)->where('right_rank_count', '<=', $userRankTeam->right_rank_count)->orderByDesc('id')->first();
//            $this->user->userRank->update([
//                'rank_id' => $rank->id
//            ]);
//        }
//
//
//        $user = $this->user;
//
//        while (true) {
//            $parentUser = $user->parent;
//            if (is_null($parentUser)) {
//                return;
//            }
//            $parentRankTeam = RankTeam::firstOrCreate(
//                ['user_id' => $parentUser->id],
//                ['left_rank_count' => 0, 'right_rank_count' => 0]
//            );
//            if ($user->tree->position == 'LEFT') {
//                $parentRankTeam->increment('left_rank_count');
//            } else {
//                $parentRankTeam->increment('right_rank_count');
//            }
//            if (is_null($parentUser->active_at)) {
//                continue;
//            }
//
//            $parentUserBusiness = $parentUser->userBusiness;
//            if (!$parentUserBusiness) {
//                continue;
//            }
//            if ($parentUserBusiness->left_usd < castDecimalString('100', 2) || $parentUserBusiness->right < castDecimalString('100', 2)) {
//                continue;
//            }
//            $parentRankTeam = $parentRankTeam->refresh();
//            $rank = Rank::where('left_rank_count', '<=', $parentRankTeam->left_rank_count)->where('right_rank_count', '<=', $parentRankTeam->right_rank_count)->orderByDesc('id')->first();
//            if ($parentUser->userRank) {
//                $parentUser->userRank->update([
//                    'rank_id' => $rank->id
//                ]);
//            } else {
//                $parentUser->userRank()->create([
//                    'rank_id' => $rank->id
//                ]);
//            }
//
//            $user = $parentUser;
//        }
        RankMethod::init($this->user->refresh())->updateRank();
    }
}
