<?php

namespace App\Jobs;

use App\Models\LevelPlan;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPoolStat;
use App\Models\SubscriptionHistory;
use App\Models\User;
use App\Mohiqssh\UserLevelMethods;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateLevelIncomeJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public SubscriptionHistory $subscriptionHistory;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SubscriptionHistory $subscriptionHistory)
    {
        $this->onQueue('income');
        $this->subscriptionHistory = $subscriptionHistory;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->subscriptionHistory->user;

        return null;
//        UserLevelMethods::init($user)->eachParentV1(function ($sponsorUser, $level) use (&$user) {
//            if (!$this->isUserActive($sponsorUser)) {
//                return;
//            }
//
////            $directAmount = castDecimalString(27, 2);
////            $UsdForMetaPool = castDecimalString(3, 10);
//
//            $directAmount = castDecimalString(6.75, 2);
//            $UsdForMetaPool = castDecimalString(0.75, 10);
//
//            $subscription = $sponsorUser->subscription;
//            $levelPlan = LevelPlan::first();
//            if ($sponsorUser->id > 564) {
//                if (is_null($subscription)) {
//                    return;
//                }
//            }
//
//            $userLevelIncome = $sponsorUser->userLevelIncomes()->firstOrCreate(
//                ['income_date' => date('Y-m-d')],
//                ['tokens' => castDecimalString('0', 8),
//                    'usd' => castDecimalString('0', 2)]
//            );
//
//            $userLevelIncome->userLevelIncomeStats()->create([
//                'subscription_history_id' => $this->subscriptionHistory->id,
//                'level' => $level,
//                'income_tokens' => 0,
//                'usd' => multipleDecimalStrings($directAmount, 1, 2)
//            ]);
//
//            $userLevelIncome->increment('usd', multipleDecimalStrings($directAmount, 1, 2));
//
//            $userCoinWallet = userCoinWalletInstance($sponsorUser);
//            $Income = multipleDecimalStrings($directAmount, '1', 8);
//            $userCoinWallet->increment('balance', $Income);
//            $userIncomeStat = userIncomeStatInstance($sponsorUser);
//            $userIncomeStat->increment('direct_bonus', $directAmount);
//            $userIncomeStat->increment('total_bonus', $directAmount);
//
//            $this->updateMetaPool($UsdForMetaPool);
//
//        }, 1);
    }

    private function isUserActive(User $user): bool
    {
        return !is_null($user->active_at);
    }

    public function updateMetaPool($amount_in_usd)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['DIRECT_ADDED'],
                'status' => 'success'
            ]);
        }
    }

}
