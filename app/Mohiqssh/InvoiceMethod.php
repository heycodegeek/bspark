<?php


namespace App\Mohiqssh;


use App\Models\Invoice;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class InvoiceMethod
{
    private string $invoiceNo;
    private int $expiryInMinutes;

    public static function init(): self
    {
        return new static();
    }

    /**
     * @param int $expiryInMinutes
     */
    public function setExpiryInMinutes(int $expiryInMinutes = 30): void
    {
        $this->expiryInMinutes = (int)$expiryInMinutes;
    }

    /**
     * @param mixed $invoiceNo
     */
    public function setInvoiceNo(string $invoiceNo): void
    {
        $this->invoiceNo = $invoiceNo;
    }

    /**
     * @throws \Exception
     */
    public function create(User $user, string $crypto, string $cryptoAmount, string $cryptoPrice, string $amountInUsd, string $depositAddress, int $expiryInMinutes = 30): Invoice
    {
        $validator = Validator::make(['crypto' => strtolower($crypto), 'cryptoAmount' => $cryptoAmount, 'cryptoPrice' => $cryptoPrice, 'amountInUsd' => $amountInUsd, 'depositAddress' => $depositAddress, 'expiryInMinutes' => $expiryInMinutes], [
            'crypto' => ['bail', 'required', 'string', 'exists:coins,name'],
            'cryptoAmount' => ['bail', 'required', 'numeric'],
            'cryptoPrice' => ['bail', 'required', 'numeric'],
            'amountInUsd' => ['bail', 'required', 'numeric'],
            'depositAddress' => ['bail', 'required', 'string'],
            'expiryInMinutes' => ['bail', 'required', 'numeric', 'min:30']
        ]);


        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first());
        }

        $this->expiryInMinutes = $expiryInMinutes;

        return $user->invoices()->create([
            'invoice_no' => Str::uuid(),
            'crypto' => strtolower($crypto),
            'crypto_amount' => castDecimalString($cryptoAmount, 8),
            'crypto_price' => castDecimalString($cryptoPrice, 8),
            'amount_in_usd' => castDecimalString($amountInUsd, 2),
            'deposit_address' => $depositAddress,
            'expired_at' => now()->addMinutes($this->expiryInMinutes)
        ]);
    }

}
