<?php

namespace App\Mohiqssh;

use App\Models\DelegatorSubscription;
use App\Models\User;

class IncomeMethods
{
    private User $user;
    private string $totalIncomeAmount;


    public function __construct(User $user, string $totalIncomeAmount)
    {
        $this->user = $user;
        $this->totalIncomeAmount = $totalIncomeAmount;
    }

    public static function init(User $user, string $totalIncomeAmount): self
    {
        return new static($user, $totalIncomeAmount);
    }

    public function updateIncome(string $incomeType): string
    {
        $incomeUsd = $this->totalIncomeAmount;
        foreach (DelegatorSubscription::where('user_id', $this->user->id)->where('is_active', true)->cursor() as $subscription) {
            $availableUsd = subDecimalStrings($subscription->max_amount_limit_usd, $subscription->earned_so_far, 2);
            if ($availableUsd == '0.00') {
                $subscription->update([
                    'is_active' => false
                ]);
                continue;
            }
            if ($availableUsd < $incomeUsd) {
                $subscription->update([
                    'earned_so_far' => $subscription->max_amount_limit_usd,
                    'is_active' => false
                ]);
                $incomeUsd = subDecimalStrings($incomeUsd, $availableUsd, 2);
            } else {
                $subscription->increment('earned_so_far', $incomeUsd);
                $incomeUsd = subDecimalStrings($incomeUsd, $incomeUsd, 2);
            }
            if ($incomeUsd == '0.00') {
                break;
            }
        }
        return subDecimalStrings($this->totalIncomeAmount, $incomeUsd, 2);
    }


    public function updateIncomeByDelegatorSubscription(DelegatorSubscription $delegatorSubscription): string
    {
        $incomeUsd = $this->totalIncomeAmount;
        $availableUsd = subDecimalStrings($delegatorSubscription->max_amount_limit, $delegatorSubscription->earned_so_far ?? '0', 2);
        if ($availableUsd == '0.00') {
            $delegatorSubscription->update([
                'is_active' => false
            ]);
            return castDecimalString('0', 2);
        }
        if ($availableUsd <= $incomeUsd) {
            $delegatorSubscription->update([
                'earned_so_far' => $delegatorSubscription->max_amount_limit_usd,
                'is_active' => false
            ]);
            $incomeUsd = subDecimalStrings($incomeUsd, $availableUsd, 2);
        } else {
            $delegatorSubscription->increment('earned_so_far', $incomeUsd);
            $incomeUsd = subDecimalStrings($incomeUsd, $incomeUsd, 2);
        }


        return subDecimalStrings($this->totalIncomeAmount, $incomeUsd, 2);
    }


}
