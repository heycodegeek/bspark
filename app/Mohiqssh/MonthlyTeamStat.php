<?php


namespace App\Mohiqssh;


use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class MonthlyTeamStat implements ShouldQueue
{
    use InteractsWithQueue;

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function updateStatToUpline($activeTeamData = false): bool
    {
        $user = $this->user;
        $sponsor = $user->tree->sponsor;
        if (!is_null($sponsor) && $activeTeamData == false) {
            $this->incrementDirectCount($sponsor, 'direct');
        }

        Log::info('-----');
        Log::info('start team updation.');
        Log::info($this->user);
        $i = 0;
        while (true) {
            $parentUser = $user->parent;
            $i = ++$i;
            Log::info('parent user ' . $i);
            Log::info($parentUser);
            if (is_null($parentUser)) {
                Log::info('team updates complete.');
                Log::info('-----');
                return true;
            }
            if ($user->tree->position == 'LEFT') {
                $this->incrementLeftCount($parentUser, $activeTeamData ? 'active_left' : 'left');
                if ($parentUser->id == $sponsor->id) {
                    $this->incrementLeftCount($parentUser, 'direct_left');
                }
            } else {
                $this->incrementRightCount($parentUser, $activeTeamData ? 'active_right' : 'right');
                if ($parentUser->id == $sponsor->id) {
                    $this->incrementRightCount($parentUser, 'direct_right');
                }
            }

            $user = $parentUser;
        }


    }

    private function incrementDirectCount(User $user, $field)
    {

        $userTeamMonthlyStat = $user->teamMonthlyStat()->firstOrCreate();
        $userTeamMonthlyStat->increment($field);

//        $user->teamMonthlyStat()->increment($field);
//        if ($user->team->direct == 2) {
//            $fdate = $this->user->created_at;
//            $tdate = $user->team->updated_at;
//            $datetime1 = new DateTime($fdate);
//            $datetime2 = new DateTime($tdate);
//            $interval = $datetime1->diff($datetime2);
//            $days = $interval->format('%a');
//            if ($days <= 10) {
//                UserMetaverseWallet::create([
//                    'user_id' => $user->id,
//                    'token' => '1200000',
//                    'value_in_usd' => '70'
//                ]);
//            }
//        }
    }

    private function incrementLeftCount(User $user, $field)
    {
        $userTeamMonthlyStat = $user->teamMonthlyStat()->firstOrCreate();
        $userTeamMonthlyStat->increment($field);
//        $user->teamMonthlyStat()->increment($field);
    }

    private function incrementRightCount(User $user, $field)
    {
        $userTeamMonthlyStat = $user->teamMonthlyStat()->firstOrCreate();
        $userTeamMonthlyStat->increment($field);
//        $user->teamMonthlyStat()->increment($field);
    }
}
