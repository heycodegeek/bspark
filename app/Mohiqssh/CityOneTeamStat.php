<?php


namespace App\Mohiqssh;


use App\Events\ActiveTeamCreated;
use App\Models\CityOneTeam;
use App\Models\CityOneTree;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CityOneTeamStat
{

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function eachParent(callable $callback)
    {
        $this->user->refresh();
        $user = $this->user;

        $level = 1;
        while (true) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
            $level++;
            continue;
        }
    }

    public function updateStatToUpline($activeTeamData = false): bool
    {
        $this->user->refresh();
        $user = $this->user;
        $sponsorId = $user->sponsor_id;
        if ($sponsorId) {
            $this->updateTeamCount($sponsorId, $activeTeamData ? 'active_direct' : 'direct');
            $this->updateTeamCount($sponsorId, $activeTeamData ? 'active_total' : 'total');
        }

        while (true) {
            $parentUser = CityOneTree::select('sponsor_id')->where('user_id', $sponsorId)->first();
            if (!$parentUser) {
                break;
            }
            $parentUserId = $parentUser->sponsor_id;
            $this->updateTeamCount($parentUserId, $activeTeamData ? 'active_total' : 'total');
            $sponsorId = $parentUserId;
        }
        return true;
    }

    private function updateTeamCount($userId, $field)
    {
        CityOneTeam::where('user_id', $userId)->update([
            $field => DB::raw($field . ' + 1')
        ]);
        if ($field == 'active_direct') {
            event(new ActiveTeamCreated($userId));
        }
    }

}
