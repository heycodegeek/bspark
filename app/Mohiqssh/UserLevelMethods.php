<?php


namespace App\Mohiqssh;


use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserLevelStat;

class UserLevelMethods
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function eachParent(callable $callback)
    {
        $user = $this->user;

            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;

            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
        }
    }

    public function eachParentV1(callable $callback, int $maxLevel = 1, int $startLevel = 1)
    {
        $user = $this->user;
        for ($level = $startLevel; $level <= $maxLevel; $level++) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
        }
    }

    public function eachParentV7(callable $callback, int $maxLevel = 7, int $startLevel = 1)
    {
        $user = $this->user;
        for ($level = $startLevel; $level <= $maxLevel; $level++) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
        }
    }

    public function eachParentV2(callable $callback)
    {
        $user = $this->user;
        $level = 1;
        while (true) {
            $parentUser = $user->parent;
            if (is_null($parentUser)) {
                return;
            }
            $response = $callback($parentUser->withoutRelations(), $level);
            if ($response) {
                return;
            }
            $user = $parentUser;
            $level++;
        }
    }

    public function eachSponsor(callable $callback)
    {
        $this->user->refresh();
        $user = $this->user;

        $level = 1;
        while (true) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
            $level++;
        }
    }

    public function updateUplineStats($activeTeamData = false): bool
    {
        $user = $this->user;

        for ($i = 1; $i <= 10; $i++) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return true;
            }
//            $this->incrementTeamCount($parentUser, $i, $activeTeamData ? 'active_team' : 'team');
            $this->createUserLevelStat($parentUser, $i);
            $user = $parentUser;
        }
        return true;
    }

    private function createUserLevelStat(User $parentUser, $level)
    {
        UserLevelStat::create([
            'user_id' => $parentUser->id,
            'downline_user_id' => $this->user->id,
            'level' => $level
        ]);
    }

    public function convertUsersToGroups()
    {
        $this->user->userLevels()->chunk(1, function ($userLevel) {

            $users = $userLevel->first()->users;
            foreach ($users as $userId) {
                $userLevelGroup = $userLevel->first()->userLevelGroups()->where('user_count', '<', 25)->orderByDesc('id')->first();
                if (is_null($userLevelGroup)) {
                    $userLevel->first()->userLevelGroups()->create([
                        'users' => collect([$userId]),
                        'user_count' => 1
                    ]);
                } else {
                    $usersCollection = $userLevelGroup->users;
                    $usersCollection->push($userId);
                    $userLevelGroup->update([
                        'users' => $usersCollection,
                        'user_count' => $userLevelGroup->user_count + 1
                    ]);

                }
            }
        });

    }

    private function incrementTeamCount(User $user, $level, $field)
    {
        $userLevel = UserLevel::firstOrCreate(
            ['user_id' => $user->id, 'level' => $level],
            ['team' => 0, 'active_team' => 0, 'users' => collect([])]
        );

        $this->addUserInParentLevelCollection($userLevel);
        $userLevel->increment($field);

    }

    public function addUserInParentLevelCollection(UserLevel $userLevel)
    {
        $usersCollection = $userLevel->users;
        $usersCollection->push($this->user->id);
        $userLevel->update([
            'users' => $usersCollection
        ]);
    }


}
