<?php


namespace App\Mohiqssh;


use App\Models\Tree;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;


class BinaryTree
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->user->refresh();
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function eachSponsor(callable $callback)
    {

        $user = $this->user;

        $level = 1;
        while (true) {
            $parentUser = $user->sponsor;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
            $level++;
        }
    }

    public function eachParent(callable $callback)
    {
        $user = $this->user;

        $level = 1;
        while (true) {
            $parentUser = $user->parent;
            if (is_null($parentUser)) {
                return;
            }
            $callback($parentUser->withoutRelations(), $level);
            $user = $parentUser;
            $level++;
        }
    }

    /**
     * @param string $position
     * @return User
     * @throws Exception
     */
    public function child(string $position): ?User
    {
        if ($position == 'left' || $position == 'right') {
            $childTree = Tree::where('parent_id', $this->user->id)->where('position', strtoupper($position))->first();
            if ($childTree) {
                return $childTree->user;
            }
            return null;
        }
        throw new Exception("Position should be left or right");
    }

    public function children(): ?Collection
    {
        $children = Tree::where('parent_id', $this->user->id)->get();
        if (!$children->count()) {
            return null;
        }
        $userEloquentCollection = new Collection();
        foreach ($children as $child) {
            $userEloquentCollection->add($child->user);
        }
        return $userEloquentCollection;

    }

    public function isBelongToTeam(User $sponsor): bool
    {
        $user = $this->user;
        while (true) {
            $parentUser = $user->parent;
            if (is_null($parentUser)) {
                return false;
            }

            if ($parentUser->id == $sponsor->id) {
                return true;
            }
            $user = $parentUser;
        }
    }
}
