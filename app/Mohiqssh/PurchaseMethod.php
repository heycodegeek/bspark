<?php


namespace App\Mohiqssh;

use App\Models\DepositTransaction;
use App\Models\Purchase;
use App\Models\User;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;

class PurchaseMethod
{
    private DepositTransaction $transaction;
    private User $user;

    public function __construct($transaction)
    {
        $this->transaction = $transaction;
        $this->user = $this->transaction->user;

    }

    public static function fromTransaction(DepositTransaction $transaction): self
    {
        return new static($transaction);
    }

    public function init()
    {
        $purchase = $this->createPurchase();
        $this->addCoinToWallet($purchase);

        $batch = Bus::batch([
            new CreateLevelIncomeJob($purchase),
            new CreateUserBusinessJob($purchase)
        ])->then(function (Batch $batch) {

        })->finally(function (Batch $batch) {

        })->onQueue('default')->dispatch();
    }

    private function createPurchase(): Purchase
    {
        return $this->transaction->purchase()->create([
            'user_id' => $this->user->id,
            'crypto' => $this->transaction->crypto,
            'crypto_price' => $this->transaction->crypto_price,
            'deposit_amount' => $this->transaction->amount,
            'deposit_amount_in_usd' => $this->transaction->amount_in_usd,
            'token_price' => $this->transaction->token_price,
            'token_qty' => numberOfTokens($this->transaction->amount_in_usd, $this->transaction->token_price)
        ]);

    }

    private function addCoinToWallet(Purchase $purchase)
    {
        $userCoinWallet = userCoinWalletInstance($purchase->user);
        $userCoinWallet->increment('balance', $purchase->token_qty);
    }


}
