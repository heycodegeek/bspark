<?php


namespace App\Mohiqssh;


use App\Events\BinaryPayoutCreated;
use App\Jobs\UpdateUserMetaverseWalletJob;
use App\Models\BinaryBusiness;
use App\Models\MetaverseTokenPoolStat;
use App\Models\User;
use App\Models\UserBinaryInfo;
use Illuminate\Support\Facades\DB;

class BinaryPayoutMethod
{
    private User $user;

    public function __construct(User $user)
    {

        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function generateBinaryBusiness()
    {
        $user = $this->user;
        $binaryBusiness = BinaryBusiness::firstOrCreate(
            ['user_id' => $user->id],
            ['left_business' => castDecimalString('0', 2), 'left_cf' => castDecimalString('0', 2), 'right_business' => castDecimalString('0', 2), 'right_cf' => castDecimalString('0', 2), 'direct_business' => castDecimalString('0', 2)]
        );
        if (!is_null($user->active_at) && $this->hasUserStandardBinary($user)) {

            $userSubscription = $user->subscription;

            if ($binaryBusiness->left_business > $binaryBusiness->right_business) {
                $matchedBusiness = $binaryBusiness->right_business;
            } elseif ($binaryBusiness->right_business > $binaryBusiness->left_business) {
                $matchedBusiness = $binaryBusiness->left_business;
            } else {
                $matchedBusiness = $binaryBusiness->right_business;
            }


            $incomeUsd = multipleDecimalStrings($matchedBusiness, 0.10, 2);

            $parentUserBinaryInfo = $user->userBinaryInfo;

            $cappingAvailable = subDecimalStrings($parentUserBinaryInfo->binary_capping_amount, $parentUserBinaryInfo->binary_used_weekly, 2);

            if ($incomeUsd <= $cappingAvailable) {

                $this->createBinaryPayout($user, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);


            } else {
                $incomeUsd = $cappingAvailable;
                $this->createBinaryPayout($user, $matchedBusiness, $incomeUsd, $cappingAvailable, $parentUserBinaryInfo, $binaryBusiness);
            }
        }
    }

    private function hasUserStandardBinary(User $user)
    {
        $userTeam = $user->team;
        if ($userTeam->active_direct_left && $userTeam->active_direct_right) {
            return true;
        }
        return false;
    }

    private function createBinaryPayout(User $parentUser, string $matchedBusiness, string $incomeUsd, string $cappingAvailable, UserBinaryInfo $parentUserBinaryInfo, BinaryBusiness $binaryBusiness)
    {
        DB::transaction(function () use (&$parentUser, $matchedBusiness, $incomeUsd, $cappingAvailable, &$parentUserBinaryInfo, &$binaryBusiness) {
            if ($incomeUsd > '0') {
                $incomeTokens = numberOfTokens($incomeUsd, tokenPrice());
                $binaryPayout = $parentUser->binaryPayouts()->create([
                    'matched_usd' => $matchedBusiness,
                    'income_amount_usd' => $incomeUsd,
                    'capping_left' => $cappingAvailable,
                    'income_tokens' => $incomeTokens,
                    'token_price' => tokenPrice()
                ]);
                $parentUserBinaryInfo->increment('binary_used_weekly', $incomeUsd);

//                $userCoinWallet = userCoinWalletInstance($parentUser);
//                $withdrawableWallet = withdrawableWalletInstance($parentUser);
//                $halfIncome = multipleDecimalStrings($incomeTokens, '1.0', 8);
//                $userCoinWallet->increment('balance', $halfIncome);
//                $withdrawableWallet->increment('balance', $halfIncome);

                UpdateUserMetaverseWalletJob::dispatch($parentUser, $incomeUsd, MetaverseTokenPoolStat::TYPE['BINARY_ADDED'])->delay(now()->addSecond());


                $userIncomeStat = userIncomeStatInstance($parentUser);
                $userIncomeStat->increment('binary_bonus', $incomeTokens);
                $userIncomeStat->increment('total_bonus', $incomeTokens);

                event(new BinaryPayoutCreated($binaryPayout));
            }

            $binaryBusiness->decrement('right_business', $matchedBusiness);
            $binaryBusiness->decrement('left_business', $matchedBusiness);
        });
    }
}
