<?php

namespace App\Mohiqssh;

use App\Models\DepositTransaction;
use App\Models\Purchase;
use App\Models\User;
use App\Models\UserCoinWallet;

class UserDepositFundMethod
{
    private DepositTransaction $transaction;
    private User $user;

    public function __construct($transaction)
    {
        $this->transaction = $transaction;
        $this->user = $this->transaction->user;
    }

    public static function fromTransaction(DepositTransaction $transaction): self
    {
        return new static($transaction);
    }

    public function init()
    {
        $purchase = $this->createPurchase();

        $this->addCoinToWallet($purchase);
    }

    private function createPurchase(): Purchase
    {
        $tokenPrice = tokenPrice();
        $tokenQty = divDecimalStrings($this->transaction->amount_in_usd, $tokenPrice, 8);
        return $this->transaction->purchase()->create([
            'user_id' => $this->user->id,
            'crypto' => $this->transaction->crypto,
            'crypto_price' => $this->transaction->crypto_price,
            'deposit_amount' => $this->transaction->amount,
            'deposit_amount_in_usd' => $this->transaction->amount_in_usd,
            'token_price' => $tokenPrice,
            'token_qty' => $tokenQty,
            'created_at' => $this->transaction->created_at
        ]);
    }

    private function addCoinToWallet(Purchase $purchase)
    {
        $userCoinWallet = UserCoinWallet::where('user_id', $purchase->user->id)->first();
        $userCoinWallet->increment('balance', $purchase->deposit_amount_in_usd);
    }
}
