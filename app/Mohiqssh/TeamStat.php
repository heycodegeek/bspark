<?php


namespace App\Mohiqssh;


use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TeamStat implements ShouldQueue
{
    use InteractsWithQueue;

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function updateStatToUpline($activeTeamData = false): bool
    {
        $user = $this->user;
        $sponsor = $user->tree->sponsor;
        if (!is_null($sponsor) && $activeTeamData == false) {
            $this->incrementDirectCount($sponsor, 'direct');
        }

        while (true) {
            $parentUser = $user->parent;
            if (is_null($parentUser)) {
                return true;
            }
            if ($user->tree->position == 'LEFT') {
                $this->incrementLeftCount($parentUser, $activeTeamData ? 'active_left' : 'left');
                if ($parentUser->id == $sponsor->id) {
                    $this->incrementLeftCount($parentUser, 'direct_left');
                }
            } else {
                $this->incrementRightCount($parentUser, $activeTeamData ? 'active_right' : 'right');
                if ($parentUser->id == $sponsor->id) {
                    $this->incrementRightCount($parentUser, 'direct_right');
                }
            }

            $user = $parentUser;
        }

    }

    private function incrementDirectCount(User $user, $field)
    {
        $user->team()->increment($field);
//        if ($user->team->direct == 2) {
//            $fdate = $this->user->created_at;
//            $tdate = $user->team->updated_at;
//            $datetime1 = new DateTime($fdate);
//            $datetime2 = new DateTime($tdate);
//            $interval = $datetime1->diff($datetime2);
//            $days = $interval->format('%a');
//            if ($days <= 10) {
//                UserMetaverseWallet::create([
//                    'user_id' => $user->id,
//                    'token' => '1200000',
//                    'value_in_usd' => '70'
//                ]);
//            }
//        }
    }

    private function incrementLeftCount(User $user, $field)
    {
        $user->team()->increment($field);
    }

    private function incrementRightCount(User $user, $field)
    {
        $user->team()->increment($field);
    }
}
