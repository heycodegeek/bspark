<?php


namespace App\Mohiqssh;


use App\Models\Rank;
use App\Models\RankTeam;
use App\Models\User;
use App\Models\UserRank;

class RankMethod
{

    private User $user;
    private $sponsor;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->sponsor = $this->user->sponsor;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function updateRank()
    {
        if (is_null($this->sponsor)) {
            return;
        }

        if (!$this->isActive($this->sponsor)) {
            return;
        }

        if (!hasUserStandardBinary($this->sponsor)) {
            return;
        }
        if (!$this->hasMinimumBusinessOnBothLegs($this->sponsor)) {
            return;
        }
        if (UserRank::where('user_id', $this->sponsor->id)->exists()) {
            return;
        }

        $this->createKodiakUserRank($this->sponsor);
        $this->updateRankToTeam($this->sponsor);
    }

    private function isActive(User $user): bool
    {
        if (is_null($user->active_at)) {
            return false;
        }
        return true;
    }

    private function hasMinimumBusinessOnBothLegs(User $user): bool
    {
        $userBusiness = $user->userBusiness;
        if (is_null($userBusiness)) {
            return false;
        }
        if ($userBusiness->direct_left_usd >= '100' && $userBusiness->direct_right_usd >= '100') {
            return true;
        }
        return false;
    }

    private function createKodiakUserRank(User $user)
    {
        $user->userRank()->create(
            ['rank_id' => 1]
        );

    }

    private function updateRankToTeam(User $user)
    {
        $userTreePosition = $user->tree->position;
        UserLevelMethods::init($user)->eachParentV2(function ($parentUser, $level) use (&$userTreePosition) {


            $rankTeam = RankTeam::firstOrCreate(
                ['user_id' => $parentUser->id],
                ['left_rank_count' => 0, 'right_rank_count' => 0]
            );
            if ($userTreePosition == 'LEFT') {
                $rankTeam->increment('left_rank_count');
            } else {
                $rankTeam->increment('right_rank_count');
            }
            $userRank = UserRank::where('user_id', $parentUser->id)->first();
            if ($userRank) {
                $rank = Rank::where('left_rank_count', '<=', $rankTeam->left_rank_count)->where('right_rank_count', '<=', $rankTeam->right_rank_count)->orderByDesc('id')->first();
                $userRank->update([
                    'rank_id' => $rank->id
                ]);
            }
            $userTreePosition = $parentUser->tree->position;

        });
    }
}
