<?php


namespace App\Mohiqssh;


use App\Models\CoinMetric;
use App\Models\SignUpAirdrop;
use App\Models\User;

class AirdropSignUpMethod
{

    private User $user;
    private string $tokens = '0';
    private string $sponsorTokens = '0';

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function init(User $user): self
    {
        return new static($user);
    }

    public function create(): bool
    {
        $coinMetric = CoinMetric::first();
        if ($coinMetric->airdrop_sold > $coinMetric->airdrop_supply) {
            return false;
        }

        if ($this->user->signUpAirdrop) {
            return false;
        }

        $this->user->signUpAirdrop()->create([
            'tokens' => castDecimalString($this->tokens, 8)
        ]);


        $userCoinWallet = $this->user->userCoinwallet()->firstOrCreate();
        $userCoinWallet->increment('balance', castDecimalString($this->tokens, 8));
        $userCoinWallet->increment('balance_locked', castDecimalString($this->tokens, 8));
        $coinMetric->increment('airdrop_sold', castDecimalString($this->tokens, 8));
        $sponsorUser = $this->user->sponsor;
        if ($sponsorUser) {

            $signUpAirdrop = SignUpAirdrop::firstOrCreate(
                ['user_id' => $sponsorUser->id],
                ['tokens' => castDecimalString('0', 8)]
            );

            $signUpAirdrop->increment('tokens', castDecimalString($this->sponsorTokens, 8));
            $sponsorUserWallet = $sponsorUser->userCoinwallet()->firstOrCreate();
            $sponsorUserWallet->increment('balance', castDecimalString($this->sponsorTokens, 8));
            $sponsorUserWallet->increment('balance_locked', castDecimalString($this->sponsorTokens, 8));
            $coinMetric->increment('airdrop_sold', castDecimalString($this->sponsorTokens, 8));

        }

        return true;
    }

}
