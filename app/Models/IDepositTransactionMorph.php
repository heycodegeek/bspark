<?php


namespace App\Models;


interface IDepositTransactionMorph
{
    public function depositTransaction();

}
