<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DelegatorSubscription extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delegatorPlan()
    {
        return $this->belongsTo(DelegatorPlan::class);
    }

    public function userDelegatorIncomes()
    {
        return $this->hasMany(UserDelegatorIncome::class);
    }
}
