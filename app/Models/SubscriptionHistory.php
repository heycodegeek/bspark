<?php

namespace App\Models;

use App\Events\RankSubscribed;
use App\Events\SubscriptionHistoryCreated;
use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionHistory extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'created' => SubscriptionHistoryCreated::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
