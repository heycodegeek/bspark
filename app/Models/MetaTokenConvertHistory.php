<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaTokenConvertHistory extends Model
{
    use HasFactory;

    const TXN_TYPE = [
        'DEBIT' => 'debit',
        'CREDIT' => 'credit'
    ];
    const TXN_SUMMARY = [
        'ACTIVE_PLAN' => 'Plan Activation',
        'REWARD_TRANSFERRED' => 'Converted into USD',
    ];
    const TXN_STATUS = [
        'SUCCESS' => 'success',
        'PENDING' => 'pending',
        'FAIL' => 'fail'
    ];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userMetaverseWallet()
    {
        return $this->belongsTo(UserMetaverseWallet::class);
    }

}
