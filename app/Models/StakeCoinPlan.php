<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StakeCoinPlan extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function stakeBinaryPlan()
    {
        return $this->hasOne(StakeBinaryPlan::class);
    }

    public function stakeLevelPlans()
    {
        return $this->hasMany(StakeLevelPlan::class);
    }

    public function stakeSubscriptions()
    {
        return $this->hasMany(StakeSubscription::class);
    }
}
