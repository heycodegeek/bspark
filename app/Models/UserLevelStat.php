<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLevelStat extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function downlineUser()
    {
        return $this->belongsTo(User::class, 'downline_user_id');
    }

    public function downlineUserBusiness()
    {
        return $this->belongsTo(UserBusiness::class, 'downline_user_id', 'user_id');
    }

    public function userBusiness()
    {
        return $this->belongsTo(UserBusiness::class, 'user_id', 'user_id');
    }
}
