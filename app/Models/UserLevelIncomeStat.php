<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLevelIncomeStat extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    protected $casts = [
        'income_date' => 'date'
    ];

    public function userLevelIncome()
    {
        return $this->belongsTo(UserLevelIncome::class);
    }


}
