<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    protected $casts = [
        'is_active' => 'boolean',
        'can_deposit' => 'boolean',
        'can_withdraw' => 'boolean',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function coinTypes()
    {
        return $this->hasMany(CoinType::class);
    }
}
