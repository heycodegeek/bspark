<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StakingBonus extends Model
{
    use HasFactory;
    protected $guarded =[];

    public function staking()
    {
        return $this->belongsTo(Staking::class);
    }


}
