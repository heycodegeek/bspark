<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternationalPoolDistributionStats extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function internationalPoolIncomeStats()
    {
        return $this->hasMany(InternationalPoolIncomeStats::class);
    }
}
