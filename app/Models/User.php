<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmail;
use App\SerializeDateTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, SerializeDateTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile',
        'country_id',
        'ref_code',
        'placed_into_tree',
        'active_at',
        'tnc_verified_at',
        'last_login_at',
        'last_login_ip_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'tnc_verified_at' => 'datetime',
        'active_at' => 'datetime',
        'created_at' => 'datetime',
        'placed_into_tree' => 'boolean'
    ];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

//    public function getJoinedOnAttribute()
//    {
//        return $this->created_at->format('Y-m-d');
//    }

    public function team()
    {
        return $this->hasOne(Team::class);
    }

    public function teamMonthlyStat()
    {
        return $this->hasOne(TeamMonthlyStat::class);
    }
    public function tree()
    {
        return $this->hasOne(Tree::class);
    }

    public function scopeWhereRefCode($query, $refCode)
    {
        return $query->where('ref_code', $refCode);
    }

    public function isPlacedIntoTree(): bool
    {
        return $this->placed_into_tree;
    }

    public function markUserPlacedIntoTree(): bool
    {
        return $this->update([
            'placed_into_tree' => true
        ]);
    }

    public function sponsor()
    {
        return $this->tree->sponsor();
    }

    public function parent()
    {
        return $this->tree->parent();
    }

    public function depositWallets()
    {
        return $this->hasMany(DepositWallet::class);
    }

    public function depositTransactions()
    {
        return $this->hasMany(DepositTransaction::class);
    }

    public function bitcoinTransactions()
    {
        return $this->hasMany(BitcoinTransaction::class);
    }

    public function ethereumTransactions()
    {
        return $this->hasMany(EthereumTransaction::class);
    }

    public function userCoinwallet()
    {
        return $this->hasOne(UserCoinWallet::class);
    }

    public function userMetaverseWallet()
    {
        return $this->hasOne(UserMetaverseWallet::class);
    }

    public function tronTransactions()
    {
        return $this->hasMany(TronTransaction::class);
    }

    public function cryptApiWallets()
    {
        return $this->hasMany(CryptApiWallet::class);
    }


    public function cryptApiTransactions()
    {
        return $this->hasMany(CryptApiTransaction::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function userUsdWallet()
    {
        return $this->hasOne(UserUsdWallet::class);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function signUpAirdrop()
    {
        return $this->hasOne(SignUpAirdrop::class);
    }

    public function airdropActivity()
    {
        return $this->hasOne(AirdropActivity::class);
    }

    public function developmentWallet()
    {
        return $this->hasOne(DevelopmentWallet::class);
    }

    public function userBusiness()
    {
        return $this->hasOne(UserBusiness::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function stakings()
    {
        return $this->hasMany(Staking::class);
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    public function userBinaryInfo()
    {
        return $this->hasOne(UserBinaryInfo::class);
    }

    public function subscriptionHistories()
    {
        return $this->hasMany(SubscriptionHistory::class);
    }

    public function subscriptionPoolBonus()
    {
        return $this->hasMany(SubscriptionPoolBonus::class);
    }

    public function userLevelIncomes()
    {
        return $this->hasMany(UserLevelIncome::class);
    }

    public function subscritionPoolStats()
    {
        return $this->hasMany(SubscriptionPoolStats::class);
    }

    public function userIncomeStat()
    {
        return $this->hasOne(UserIncomeStat::class);
    }

    public function userRank()
    {
        return $this->hasOne(UserRank::class);
    }

    public function rankTeam()
    {
        return $this->hasOne(RankTeam::class);
    }

    public function binaryPayouts()
    {
        return $this->hasMany(BinaryPayout::class);
    }

    public function matchingBonuses()
    {
        return $this->hasMany(MatchingBonus::class);
    }

    public function userStakeIncomes()
    {
        return $this->hasMany(UserStakeIncome::class);
    }

    public function userStakeIncomeWallet()
    {
        return $this->hasOne(UserStakeIncomeWallet::class);
    }

    public function withdrawWallets()
    {
        return $this->hasMany(WithdrawWallet::class);
    }

    public function otps()
    {
        return $this->hasMany(Otp::class);
    }

    public function withdrawalTemps()
    {
        return $this->hasMany(WithdrawalTemp::class);
    }

    public function withdrawalHistories()
    {
        return $this->hasMany(WithdrawalHistory::class);
    }

    public function stopRoiUser()
    {
        return $this->hasOne(StopRoiUser::class);
    }

    public function withdrawableWallet()
    {
        return $this->hasOne(WithdrawableWallet::class);
    }

    public function walletTransfers()
    {
        return $this->hasMany(WalletTransfer::class);
    }

    public function transferStat()
    {
        return $this->hasOne(TransferStat::class);
    }


    public function stakeSubscription()
    {
        return $this->hasOne(StakeSubscription::class);
    }

    public function stakeSubscriptionHistories()
    {
        return $this->hasMany(StakeSubscriptionHistory::class);
    }

    public function userCoinStakeIncomes()
    {
        return $this->hasMany(UserCoinStakeIncome::class);
    }

    public function stakingBusinessBonusStats()
    {
        return $this->hasMany(StakingBusinessBonusStats::class);
    }

    public function stakingBusiness()
    {
        return $this->hasOne(StakingBusiness::class);
    }

    public function metaTokenConvertHistories()
    {
        return $this->hasMany(MetaTokenConvertHistory::class);
    }

    public function delegatorSubscriptions()
    {
        return $this->hasMany(DelegatorSubscription::class);
    }

    public function userDelegatorIncomes()
    {
        return $this->hasMany(UserDelegatorIncome::class);
    }

    public function userDelegatorLevelBonuses()
    {
        return $this->hasMany(UserDelegatorLevelBonus::class);
    }

    public function userSponsor()
    {
        return $this->hasOne(UserSponsor::class);
    }

    public function smartMetaConvertHistory()
    {
        return $this->hasMany(SmartMetaConvertHistory::class);
    }

    public function userSmartMeta()
    {
        return $this->hasOne(UserSmartMeta::class);
    }

    public function voucherTransactions()
    {
        return $this->hasMany(VoucherTransaction::class);
    }

    public function internationalPoolAchiever()
    {
        return $this->hasMany(InternationalPoolAchiever::class);
    }

    public function smartMetaToMetaverseTokenSwapStat()
    {
        return $this->hasOne(SmartMetaToMetaverseTokenSwapStat::class);
    }

}
