<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StakeLevelPlan extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function stakeCoinPlan()
    {
        return $this->belongsTo(StakeCoinPlan::class);
    }
}
