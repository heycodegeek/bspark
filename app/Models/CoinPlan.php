<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinPlan extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function binaryPlan()
    {
        return $this->hasOne(BinayPlan::class);
    }

    public function levelPlans()
    {
        return $this->hasMany(LevelPlan::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
