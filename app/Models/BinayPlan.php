<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BinayPlan extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function coinPlan()
    {
        return $this->belongsTo(CoinPlan::class);
    }
}
