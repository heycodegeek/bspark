<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DelegatorPlan extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function userDelegatorIncomes()
    {
        return $this->hasMany(UserDelegatorIncome::class);
    }

    public function delegatorSubscriptions()
    {
        return $this->hasMany(DelegatorSubscription::class);
    }

}
