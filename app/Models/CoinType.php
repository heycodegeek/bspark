<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinType extends Model
{
    use HasFactory, SerializeDateTrait;

    const TYPES = [
        'legacy' => 'legacy',
        'p2sh-segwit' => 'p2sh-segwit',
        'bech32' => 'bech32'
    ];
    protected $guarded = [];
    protected $casts = [
        'is_active' => 'boolean',
        'can_deposit' => 'boolean',
        'can_withdraw' => 'boolean',
    ];

    public function coin()
    {
        return $this->belongsTo(Coin::class);
    }

    public function depositWallets()
    {
        return $this->hasMany(DepositWallet::class);
    }
}
