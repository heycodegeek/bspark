<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    protected $casts = [
        'in_compounding' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coinPlan()
    {
        return $this->belongsTo(CoinPlan::class);
    }
}
