<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BitcoinTransaction extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isConfirmed()
    {
        return $this->confirmations >= 1;
    }

    public function depositTransaction()
    {
        return $this->morphOne(DepositTransaction::class, 'deposit_transactionable');
    }
}
