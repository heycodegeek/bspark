<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Inertia\Inertia;

class StructureController extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function create(): \Inertia\Response
    {
        return Inertia::render('Structure');
    }
}
