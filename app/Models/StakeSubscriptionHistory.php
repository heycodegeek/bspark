<?php

namespace App\Models;

use App\Events\StakeSubscriptionHistoryCreated;
use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StakeSubscriptionHistory extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    protected $dispatchesEvents = [
        'created' => StakeSubscriptionHistoryCreated::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
