<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaverseTokenPool extends Model
{
    use HasFactory;
    protected $guarded = [];
}
