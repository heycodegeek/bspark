<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaverseTokenPoolStat extends Model
{
    use HasFactory;

    const TYPE = [
        'META_RELEASED' => 'meta_released', 'USD_CONVERTED' => 'usd_converted', 'CONVERT_ADDED' => 'convert_added', 'BINARY_ADDED' => 'binary_added', 'DIRECT_ADDED' => 'direct_added', 'BOT_ADDED' => 'bot_added', 'STAKING_BONUS' => 'staking_bonus', 'STAKING_LEVEL_BONUS' => 'staking_level_bonus'
    ];
    protected $guarded = [];
}
