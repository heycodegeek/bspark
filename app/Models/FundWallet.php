<?php

namespace App\Models;

use App\SerializeDateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FundWallet extends Model
{
    use HasFactory, SerializeDateTrait;

    protected $guarded = [];

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = strtolower($value);
    }
}
