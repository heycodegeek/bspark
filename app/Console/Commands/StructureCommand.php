<?php

namespace App\Console\Commands;

use App\Jobs\PostRegisterationJob;
use App\Models\User;
use App\Mohiqssh\Downline;
use App\Mohiqssh\TeamStat;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class StructureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'structure:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Structure in Binary Tree';


    private User $topUser;
    private string $position;
    private int $noOfUsers;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->askTopUserEmail();
    }

    private function askTopUserEmail()
    {
        $topUserEmail = $this->ask("Enter top user email address?");

        $validator = Validator::make(['email' => $topUserEmail], [
            'email' => ['required', 'email', 'exists:users,email']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('email'));
            exit();
        }

        $topUser = User::where('email', $topUserEmail)->first();
        $this->table(
            ['User Id', 'User Name', 'Email', 'Joined On', 'isActive'],
            [[$topUser->id, $topUser->name, $topUser->email, $topUser->created_at, $topUser->active_at]]
        );
        if (!$this->confirm("Please confirm to proceed?", true)) {
            $this->error("You have cancelled.");
            exit();
        }
        $this->topUser = $topUser;
        $this->choicePosition();
    }

    private function choicePosition()
    {
        $this->position = $this->choice(
            'What is the position of structure?',
            ['LEFT', 'RIGHT'], 0
        );
        $this->askNoOfUsers();

    }

    private function askNoOfUsers()
    {
        $noOfUsers = $this->ask('Enter no of users?');
        $validator = Validator::make(['no_of_users' => $noOfUsers], [
            'no_of_users' => ['required', 'numeric', 'digits_between:1,100']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('no_of_users'));
            exit();
        }
        $this->noOfUsers = $noOfUsers;
        $this->createNewUsers();
    }

    private function createNewUsers()
    {
        $topUser = $this->topUser;
        $this->table(
            ['User Id', 'User Name', 'Email', 'Position', 'No Of Users'],
            [[$topUser->id, $topUser->name, $topUser->email, $this->position, $this->noOfUsers]]
        );
        if (!$this->confirm("Please confirm to proceed?", true)) {
            $this->error("You have cancelled.");
            exit();
        }

        for ($i = 0; $i < $this->noOfUsers; $i++) {
            try {
                $user = User::factory()->create();
                $user->team()->create();
                $user->airdropActivity()->create();
//            $sponsorUser = User::whereRefCode($this->topUser->ref_code)->first();

                $lastTreeLegUser = Downline::init($user)->findLastChild($this->topUser, $this->position);
                $this->createTree($user, $lastTreeLegUser);

                $user->update([
                    'placed_into_tree' => true
                ]);

                TeamStat::init($user)->updateStatToUpline();
            } catch (\Exception $ex) {
                $this->error("Error in :" . $i);
            }


        }
//        $users = User::factory()->count($this->noOfUsers)->create();
//        foreach ($users as $user) {
//
//            $user->team()->create();
//            $user->airdropActivity()->create();
////            $sponsorUser = User::whereRefCode($this->topUser->ref_code)->first();
//
//            $lastTreeLegUser = Downline::init($user)->findLastChild($this->topUser, $this->position);
//            $this->createTree($user, $lastTreeLegUser);
//
//            $user->update([
//                'placed_into_tree' => true
//            ]);
//
//            TeamStat::init($user)->updateStatToUpline();
////            PostRegisterationJob::dispatch($user, $this->topUser, $this->position)->delay(now()->addSecond());
//        }
    }

    private function createTree(User $user, User $lastTreeLegUser)
    {
        $user->tree()->create([
            'sponsor_id' => $this->topUser->id,
            'position' => $this->position,
            'parent_id' => $lastTreeLegUser->id
        ]);
    }
}
