<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Mohiqssh\AirdropSignUpMethod;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class VerifyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify User Email';

    private User $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->choiceUser();
    }

    private function choiceUser()
    {
        $idType = $this->choice('User search type?', ['email', 'id'], 1);
        $this->idType = $idType;

        if ($this->idType == 'email') {
            $this->askEmailUser();
        } else {
            $this->askUserId();
        }
    }

    private function askEmailUser()
    {
        $userEmailAddress = $this->ask('Enter Email');
        $validator = Validator::make(['email' => $userEmailAddress], [
            'email' => ['required', 'email', 'exists:users,email']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('email'));
            exit();
        }

        $this->user = User::where('email', $userEmailAddress)->first();

        $this->process();
    }

    private function process()
    {
        $this->table(
            ['User Id', 'User Name', 'Email', 'Joined On', 'Verified'],
            [[$this->user->id, $this->user->name, $this->user->email, $this->user->created_at, $this->user->email_verified_at]]
        );

        if (!$this->confirm("Please confirm to proceed?", true)) {
            exit();
        }

        if ($this->user->hasVerifiedEmail()) {
            $this->error('User Email Already verified');
            exit();
        }

        $this->user->markEmailAsVerified();
        $this->info("Email Verified Successfully");

        AirdropSignUpMethod::init($this->user)->create();
        $this->info("Airdrop Created");
        exit();
    }

    private function askUserId()
    {
        $userId = $this->ask('Enter User Id');
        $validator = Validator::make(['id' => $userId], [
            'id' => ['required', 'numeric', 'exists:users,id']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('id'));
            exit();
        }

        $this->user = User::find($userId);
        $this->process();

    }
}
