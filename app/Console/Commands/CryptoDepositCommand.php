<?php

namespace App\Console\Commands;

use App\Models\Coin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CryptoDepositCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manual Deposit Crypto Currency';

    private Coin $coinModel;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->choiceDepositCoin();
    }

    private function choiceDepositCoin()
    {
        $coins = Coin::select('name')->where('can_deposit', true)->where('is_active', true)->orderBy('id')->get();
        $selectCoin = $this->choice('In which coin you want to deposit?',
            $coins->pluck('name')->toArray()
        );

        $this->coinModel = Coin::where('name', 'eth')->first();


    }
}
