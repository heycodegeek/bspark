<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CheckMetaverseEligibility extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:meta-eligibility {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->argument('email');

        $validator = Validator::make(['email' => $email], [
            'email' => ['required', 'email', 'exists:users,email']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('email'));
            exit();
        }

        $user = User::with('subscription')->whereEmail($email)->first();

        if (is_null($user)) {
            $this->error('user with subscription not exist!');
            exit();
        }
        $from_date = $user->subscription->created_at->format('Y-m-d');
        $to_date = $user->subscription->created_at->addDay(10)->format('Y-m-d');

        Log::info($from_date);
        $this->info($from_date);
        Log::info($to_date);
        $this->info($to_date);

    }
}
