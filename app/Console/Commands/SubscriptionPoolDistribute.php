<?php

namespace App\Console\Commands;

use App\Jobs\DistributePoolIncomeJob;
use Illuminate\Console\Command;

class SubscriptionPoolDistribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pool:distribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription Pool Distribution';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new DistributePoolIncomeJob())->delay(now()->addSecond());
    }
}
