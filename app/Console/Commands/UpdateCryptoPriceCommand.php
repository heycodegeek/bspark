<?php

namespace App\Console\Commands;

use App\Jobs\UpdateBnbPrice;
use App\Jobs\UpdateBtcPrice;
use App\Jobs\UpdateEthereumPrice;
use App\Jobs\UpdateLtcPrice;
use App\Jobs\UpdateTrxPrice;
use App\Jobs\UpdateXrpPrice;
use Illuminate\Console\Command;

class UpdateCryptoPriceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:price-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Crypto Prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
//        dispatch(new UpdateEthereumPrice())->delay(now()->addSecond());
//        dispatch(new UpdateBnbPrice())->delay(now()->addSecond());
//        dispatch(new UpdateBtcPrice())->delay(now()->addSecond());
        dispatch(new UpdateTrxPrice())->delay(now()->addSecond());
    }
}
