<?php

namespace App\Console\Commands;

use App\Jobs\TronCheckNewTransaction;
use App\Models\Coin;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class TronTransactionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tron:txn';

    protected User $user;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Transaction By User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->askUserEmail();
        $this->checkNewTronTransaction();
    }

    private function askUserEmail()
    {
        $userEmail = $this->ask('Enter user email address');

        $validator = Validator::make(['user_email' => strtolower($userEmail)], [
            'user_email' => ['required', 'email', 'exists:users,email']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('user_email'));
            exit();
        }

        $this->user = User::where('email', strtolower($userEmail))->first();

    }

    private function checkNewTronTransaction()
    {
        $coin = Coin::where('name', 'trx')->first();
        $coinType = $coin->coinTypes()->where('type', 'legacy')->first();
        $depositWallet = $this->user->depositWallets()->where('coin_type_id', $coinType->id)->first();
        if (is_null($depositWallet)) {
            $this->error('Tron wallet is not available for user');
            exit();
        }

        TronCheckNewTransaction::dispatch($depositWallet)->delay(now()->addSeconds(2))->onQueue('newFilter');
        exit();
    }
}
