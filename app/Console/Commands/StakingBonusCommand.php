<?php

namespace App\Console\Commands;

use App\Jobs\StakingBonusJob;
use Illuminate\Console\Command;

class StakingBonusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staking:bonus {bonus}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Staking bonus distribution command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bonus = $this->argument('bonus');
        if (is_null($bonus) || $bonus < 0.25 || $bonus > 0.50) {
            $this->error("Please pass the  Bonus Percentage !");
            exit();
        }
        $bonus = divDecimalStrings($bonus, 100);
        dispatch(new StakingBonusJob($bonus));
        $this->info($bonus);
    }
}
