<?php

namespace App\Console\Commands;
use App\Jobs\GenerateIPoolAchieverJob;
use Illuminate\Console\Command;

class IPoolAchieverCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipool:achiever';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'International pool achiever command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GenerateIPoolAchieverJob::dispatch()->delay(now()->addSeconds());
    }

}
