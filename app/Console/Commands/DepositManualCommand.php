<?php

namespace App\Console\Commands;

use App\Jobs\EyfiPurchaseJob;
use App\Jobs\MoveDepositFundJob;
use App\Jobs\PurchaseJob;
use App\Models\UserWallet;
use App\Mohiqssh\Ethereum;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DepositManualCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposit:manual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Deposit which are missed';


    private string $txnId;

    private string $fromAddress;

    private string $toAddress;

    private string $depositAmountInEth;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->askTransactionHash();
    }

    private function askTransactionHash()
    {
        $txnHash = $this->ask('Enter the transaction hash?');

        $validator = Validator::make(['txn_hash' => $txnHash], [
            'txn_hash' => ['required', 'string', 'unique:transactions,txn_id']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('txn_hash'));
            exit();
        }

        $this->txnId = strtolower($txnHash);
        $this->askFromAddress();
    }

    private function askFromAddress()
    {
        $fromAddress = $this->ask('Enter from Address?');
        $validator = Validator::make(['from_address' => $fromAddress], [
            'from_address' => ['required', 'string',
                function ($attribute, $value, $fail) {
                    if (!Ethereum::wallet()->isValidAddress(strtolower($value))) {
                        $fail('Invalid address');
                    }
                }
            ]
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('from_address'));
            exit();
        }
        $this->fromAddress = strtolower($fromAddress);
        $this->askToAddress();
    }

    private function askToAddress()
    {
        $toAddress = $this->ask('Enter deposit Address?');
        $validator = Validator::make(['to_address' => $toAddress], [
            'to_address' => ['required', 'string', 'exists:user_wallets,address']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('to_address'));
            exit();
        }
        $this->toAddress = strtolower($toAddress);
        $this->askDepsitAmount();
    }

    private function askDepsitAmount()
    {
        $depositAmount = $this->ask('Deposit Amount In ETH?');
        $validator = Validator::make(['deposit_amount' => $depositAmount], [
            'deposit_amount' => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('deposit_amount'));
            exit();
        }
        $this->depositAmountInEth = castDecimalString($depositAmount, 8);
        $this->initDeposit();
    }

    private function initDeposit()
    {
        $userWallet = UserWallet::where('address', $this->toAddress)->first();
        $user = $userWallet->user;
        $this->table(
            ['Txn Hash', 'From Address', 'To Address', 'Amount', 'User Id', 'User Email'],
            [
                [$this->shortString($this->txnId), $this->shortString($this->fromAddress), $this->shortString($this->toAddress), $this->depositAmountInEth, $user->id, $user->email]
            ]
        );

        if (!$this->confirm("Please confirm to proceed?", true)) {
            exit();
        }

        $isEyfiWallet = $userWallet->is_eyfi_wallet;
        $transaction = $user->depositTransactions()->create([
            'txn_id' => $this->txnId,
            'address' => $this->toAddress,
            'crypto' => 'eth',
            'crypto_price' => ethereumPrice(),
            'token_price' => $isEyfiWallet ? eyfiTokenPrice() : tokenPrice(),
            'amount' => castDecimalString((string)$this->depositAmountInEth, 8),
            'amount_in_usd' => multipleDecimalStrings(castDecimalString((string)$this->depositAmountInEth, 8), ethereumPrice(), 2),
            'txn_time' => now(),
            'is_eyfi_wallet' => $isEyfiWallet
        ]);

        if ($transaction) {
            if ($isEyfiWallet) {
                dispatch(new EyfiPurchaseJob($transaction))->delay(now()->addSeconds(5));
            } else {
                dispatch(new PurchaseJob($transaction))->delay(now()->addSeconds(5));
            }

        }

        $userWallet->increment('balance', castDecimalString((string)$this->depositAmountInEth, 8));
        dispatch(new MoveDepositFundJob($userWallet))->delay(now()->addSeconds(5));
    }

    private function shortString(string $string, int $length = 5, string $deliminator = '...')
    {
        $startString = Str::substr($string, 0, $length);
        $endString = Str::substr($string, $length * -1);
        return $startString . $deliminator . $endString;
    }
}
