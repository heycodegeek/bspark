<?php

namespace App\Console\Commands;

use App\Jobs\CustomStakingBonusJob;
use Illuminate\Console\Command;

class CustomStakingBonusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom_staking:bonus {bonus} {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Staking bonus distribution command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bonus = $this->argument('bonus');
        $bonusDate = $this->argument('date');
        if (is_null($bonus) or $bonus < 0.25) {
            $this->error("Please pass the  Bonus Percentage!");
            exit();
        }
        $bonus = divDecimalStrings($bonus, 100);
        dispatch(new CustomStakingBonusJob($bonus,$bonusDate));
        $this->info($bonus);
    }
}
