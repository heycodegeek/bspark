<?php

namespace App\Console\Commands;

use App\Jobs\StakingBonusReverseJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class StakingBonusReverseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staking:bonus-reverse {bonus_date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reverse wrong staking bonus ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bonusDate = $this->argument('bonus_date');

        $validator = Validator::make(['bonus_date' => $bonusDate], [
            'bonus_date' => ['required', 'date', 'exists:staking_distribution_stats,bonus_date']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('bonus_date'));
            exit();
        }
        dispatch(new StakingBonusReverseJob($bonusDate));
        $this->info($bonusDate);
    }
}
