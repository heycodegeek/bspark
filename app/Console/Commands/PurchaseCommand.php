<?php

namespace App\Console\Commands;

use App\Jobs\PurchaseJob;
use App\Models\User;
use App\Mohiqssh\UserMethods;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class PurchaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Offline Purchase';


    protected string $idType;

    protected User $user;

    protected string $usdDepositAmount;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->choiceIdSearchType();
    }

    protected function askUserId()
    {
        $userId = $this->ask('Enter User Id');
        $validator = Validator::make(['id' => $userId], [
            'id' => ['required', 'numeric', 'exists:users,id']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('id'));
            exit();
        }

        $this->user = User::find($userId);

        $this->table(
            ['User Id', 'User Name', 'Email', 'Joined On', 'isActive'],
            [[$this->user->id, $this->user->name, $this->user->email, $this->user->created_at, $this->user->active_at]]
        );

        if (!$this->confirm("Please confirm to proceed?", true)) {
            exit();
        }

        $this->askUsdDepositAmount();

    }

    protected function askEmailUser()
    {
        $userEmailAddress = $this->ask('Enter Email');
        $validator = Validator::make(['email' => $userEmailAddress], [
            'email' => ['required', 'email', 'exists:users,email']
        ]);
        if ($validator->fails()) {
            $this->error($validator->errors()->first('email'));
            exit();
        }

        $this->user = User::where('email', $userEmailAddress)->first();

        $this->table(
            ['User Id', 'User Name', 'Email', 'Joined On', 'isActive'],
            [[$this->user->id, $this->user->name, $this->user->email, $this->user->created_at, $this->user->active_at]]
        );

        if (!$this->confirm("Please confirm to proceed?", true)) {
            exit();
        }

        $this->askUsdDepositAmount();

    }


    protected function askUsdDepositAmount()
    {
        $usdDepositAmount = $this->ask('USD Deposit Amount?');

        $validator = Validator::make(['amount_usd' => $usdDepositAmount], [
            'amount_usd' => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('amount_usd'));
            exit();
        }

        $this->usdDepositAmount = $usdDepositAmount;

        $payableEther = divDecimalStrings($this->usdDepositAmount, ethereumPrice(), 8);

        $createEthereumTxn = $this->user->ethereumTransactions()->create([
            'txn_id' => Uuid::uuid4(),
            'address' => '',
            'amount' => $payableEther,
            'confirmations' => 1,
            'txn_time' => now(),
        ]);
        if (is_null($createEthereumTxn)) {
            $this->error("Unable to create Ethereum Transaction. Try again later");
        }
        $transaction = $this->user->depositTransactions()->create([
            'txn_id' => $createEthereumTxn->txn_id,
            'address' => $createEthereumTxn->address,
            'crypto' => 'eth',
            'crypto_price' => ethereumPrice(),
            'token_price' => tokenPrice(),
            'amount' => $createEthereumTxn->amount,
            'amount_in_usd' => multipleDecimalStrings(castDecimalString((string)$createEthereumTxn->amount, 8), ethereumPrice(), 2),
            'txn_time' => $createEthereumTxn->txn_time

        ]);
//
        if ($transaction) {
            dispatch(new PurchaseJob($transaction));
        }

        //        $this->user->userWallet()->increment('balance', castDecimalString((string)$event->activity->getValue(), 8));

    }


    protected function choiceIdSearchType()
    {
        $idType = $this->choice('User search type?', ['email', 'id'], 0);
        $this->idType = $idType;

        if ($this->idType == 'email') {
            $this->askEmailUser();
        } else {
            $this->askUserId();
        }


    }
}
