<?php

namespace App\Console\Commands;

use App\Jobs\ProcessBitcoinTransaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BitcoinTransactionCommand extends Command
{

    protected $txnId;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'btc:txn {txn_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bitcoin Process New Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->txnId = $this->argument('txn_id');
        dispatch(new ProcessBitcoinTransaction($this->txnId))->onQueue('newFilter')->delay(now()->addSecond());
    }
}
