<?php

namespace App\Console\Commands;

use App\Jobs\GenerateSmartMetaToMetaverseTokenJob;
use Illuminate\Console\Command;

class SwapSmartMetaToMetaverseTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smart-meta:metaverse-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Swap smart-meta to metaverse-token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GenerateSmartMetaToMetaverseTokenJob::dispatch()->delay(now()->addSecond());
    }
}
