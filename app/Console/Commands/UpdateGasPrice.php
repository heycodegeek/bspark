<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateGasPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ethereum:gas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Ethereum Gas Price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        dispatch(new \App\Jobs\UpdateGasPrice())->delay(now()->addSeconds(1));
    }
}
