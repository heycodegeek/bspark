<?php

namespace App\Console\Commands;

use App\Models\InternationalPool;
use App\Models\Subscription;
use Illuminate\Console\Command;

class GenerateInternationalPoolBalanceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipool:balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscriptions = Subscription::where('created_at', '>=', '2022-11-01')->where('created_at', '<', '2022-12-02')->sum('amount_in_usd');
        if (castDecimalString($subscriptions, 2) > '0.00') {
            $poolBalance = multipleDecimalStrings($subscriptions, '0.10', 2);
            $iPool = InternationalPool::UpdateOrCreate([
                'id' => 1
            ], [
                'balance' => $poolBalance,
                'from_subscriptions' => $poolBalance,
                'from_withdrawals' => '0.00',
                'next_distribution_date' => '2022-12-01'
            ]);
            $this->info($poolBalance . ' $ inserted into i Pool.');
        } else {
            $this->error('total pool balance for the given month is 0.00');
        }
    }
}
