<?php

namespace App\Console\Commands;

use App\Models\Team;
use App\Models\User;
use App\Mohiqssh\Downline;
use App\Mohiqssh\TeamStat;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class StructurePendingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'structure:pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Place Structure into tree';

    private int $startId;
    private int $endId;
    private User $topUser;
    private string $position;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->askTopUserEmail();
    }

    private function askTopUserEmail()
    {
        $topUserEmail = $this->ask("Enter top user email address?");

        $validator = Validator::make(['email' => $topUserEmail], [
            'email' => ['required', 'email', 'exists:users,email']
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors()->first('email'));
            exit();
        }

        $topUser = User::where('email', $topUserEmail)->first();
        $this->table(
            ['User Id', 'User Name', 'Email', 'Joined On', 'isActive'],
            [[$topUser->id, $topUser->name, $topUser->email, $topUser->created_at, $topUser->active_at]]
        );
        if (!$this->confirm("Please confirm to proceed?", true)) {
            $this->error("You have cancelled.");
            exit();
        }
        $this->topUser = $topUser;
        $this->choicePosition();
    }

    private function choicePosition()
    {
        $this->position = $this->choice(
            'What is the position of structure?',
            ['LEFT', 'RIGHT'], 0
        );
        $this->askIdRange();

    }

    private function askIdRange()
    {
        $startId = $this->ask('Enter Start User ID?');
        if (!$startId) {
            $this->error("Invalid User Start Id");
            exit();
        }
        $this->startId = $startId;

        $endId = $this->ask('Enter End User Id?');

        if (!$endId) {
            $this->error("Invalid User End Id");
            exit();
        }
        if ($endId < $startId) {
            $this->error("End Id should be greater than start Id");
            exit();
        }
        $this->endId = $endId;


        foreach (User::where('placed_into_tree', false)->where('id', '>=', $this->startId)->where('id', '<=', $endId)->cursor() as $user) {

            $user->team()->firstOrCreate();
            $user->airdropActivity()->firstOrCreate();
//
            $lastTreeLegUser = Downline::init($user)->findLastChild($this->topUser, $this->position);
            $this->createTree($user, $lastTreeLegUser);
//
            $user->update([
                'placed_into_tree' => true
            ]);


            TeamStat::init($user)->updateStatToUpline();
        }

    }

    private function createTree(User $user, User $lastTreeLegUser)
    {
        $user->tree()->create([
            'sponsor_id' => $this->topUser->id,
            'position' => $this->position,
            'parent_id' => $lastTreeLegUser->id
        ]);
    }
}
