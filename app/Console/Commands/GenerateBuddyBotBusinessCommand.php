<?php

namespace App\Console\Commands;

use App\Jobs\GenerateBuddyBotBusiness;
use App\Models\Staking;
use Illuminate\Console\Command;

class GenerateBuddyBotBusinessCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buddy-bot:business';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Buddy Bot Business';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Staking::orderBy('user_id', 'ASC')->cursor() as $staking) {
            GenerateBuddyBotBusiness::dispatch($staking)->delay(now()->addSeconds(2))->onQueue('income');
        }
    }
}
