<?php

namespace App\Console;

use App\Jobs\DistributePoolIncomeJob;
use App\Jobs\GenerateCoinStakeIncomeJob;
use App\Jobs\GenerateStakeIncomeJob;
use App\Jobs\ResetWeeklyBinaryCappingJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\ShortSchedule\ShortSchedule;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->job(new ResetWeeklyBinaryCappingJob())->dailyAt('00:01')->runInBackground();
//        $schedule->job(new GenerateStakeIncomeJob())->dailyAt('00:05')->runInBackground();
        $schedule->job(new GenerateCoinStakeIncomeJob())->monthlyOn(19, '00:20')->runInBackground();
//        $schedule->job(new DistributePoolIncomeJob())->dailyAt('02:05')->runInBackground();
//        $schedule->job(new DistributePoolIncomeJob())->dailyAt('00:30')->runInBackground();


//        $schedule->command('pool:distribute')->dailyAt('02:00');


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    protected function shortSchedule(ShortSchedule $shortSchedule)
    {
        // this artisan command will run every second
//        $shortSchedule->command('ethereum:price')->everySeconds(30)->withoutOverlapping();
        //       $shortSchedule->command('ethereum:gas')->everySeconds(3600)->withoutOverlapping();
//        $shortSchedule->command('bnb:price')->everySeconds(30)->withoutOverlapping();
//        $shortSchedule->command('btc:price')->everySeconds(30)->withoutOverlapping();
        $shortSchedule->command('crypto:price-update')->everySeconds(60)->withoutOverlapping();
//        $shortSchedule->command('bsc:run')->everySeconds(10)->withoutOverlapping();
    }
}
