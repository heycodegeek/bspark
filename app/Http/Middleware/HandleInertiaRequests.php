<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function share(Request $request)
    {
        if (Auth::guard('admin')->check()) {
            return array_merge(parent::share($request), [
                'appName' => config('app.name'),
                'auth' => [
                    'user' => $request->user(),
                ],
                'token_price' => [
                    'usd' => \tokenPrice()
                ],
                'meta_token_price' => [
                    'usd' => \metaTokenPrice()
                ],
                'crypto_prices' => [
                    'eth' => [
                        'usd' => ethereumPrice()
                    ]
                ],
                'flash' => [
                    'message' => fn() => $request->session()->get('message'),
                    'notification' => fn() => $request->session()->get('notification')
                ],
            ]);
        } else {
            return array_merge(parent::share($request), [
                'appName' => config('app.name'),
                'auth' => [
                    'user' => $request->user(),
                ],
                'token_price' => [
                    'usd' => \tokenPrice()
                ],
                'meta_token_price' => [
                    'usd' => \metaTokenPrice()
                ],
                'crypto_prices' => [
                    'eth' => [
                        'usd' => ethereumPrice()
                    ]
                ],
                'flash' => [
                    'message' => fn() => $request->session()->get('message'),
                    'notification' => fn() => $request->session()->get('notification')
                ],
            ]);
        }

    }
}
