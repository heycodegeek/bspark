<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubscriptionPoolStatsRequest;
use App\Http\Requests\UpdateSubscriptionPoolStatsRequest;
use App\Models\SubscriptionPoolStats;

class SubscriptionPoolStatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubscriptionPoolStatsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscriptionPoolStatsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubscriptionPoolStats  $subscriptionPoolStats
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPoolStats $subscriptionPoolStats)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubscriptionPoolStats  $subscriptionPoolStats
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionPoolStats $subscriptionPoolStats)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSubscriptionPoolStatsRequest  $request
     * @param  \App\Models\SubscriptionPoolStats  $subscriptionPoolStats
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubscriptionPoolStatsRequest $request, SubscriptionPoolStats $subscriptionPoolStats)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubscriptionPoolStats  $subscriptionPoolStats
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriptionPoolStats $subscriptionPoolStats)
    {
        //
    }
}
