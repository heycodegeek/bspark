<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\TncAccepted;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class TncVerificationController extends Controller
{
    public function Index()
    {
        return Inertia::render('TncVerify');
    }

    public function saveTncCheckStatus(Request $request)
    {
        $request->validate([
            'tnc' => ['required', Rule::in([true])]
        ]);

        if (!is_null(auth()->user()->tnc_verified_at)) {
            return redirect()->route('dashboard');
        }
        $update = User::where('id', auth()->user()->id)->update([
            'tnc_verified_at' => Carbon::now()
        ]);

        auth()->user()->notify(new TncAccepted());

        return redirect()->route('dashboard')->with('notification', ['T & C Accepted!', 'success']);
    }
}
