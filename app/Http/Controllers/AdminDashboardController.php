<?php

namespace App\Http\Controllers;

use App\Jobs\DistributePoolIncomeJob;
use App\Models\DepositTransaction;
use App\Models\EyfiCoinWallet;
use App\Models\Team;
use App\Models\User;
use App\Models\WithdrawalHistory;
use Carbon\Carbon;
use Inertia\Inertia;

class AdminDashboardController extends Controller
{
    public function showDashboard()
    {
        $topTeam = Team::where('user_id', 1)->first();
        $todayBusiness = DepositTransaction::whereDate('created_at', now()->format('Y-m-d'))->sum('amount_in_usd');
        $totalTokenWithdrawal = WithdrawalHistory::where('status', 'success')->sum('receivable_amount');
        $totalWithdrawal = WithdrawalHistory::where('status', 'success')->sum('amount');
        $last24HourWithdrawal = WithdrawalHistory::where('created_at', '>=', Carbon::yesterday()->format('Y-m-d'))->where('status', 'success')->sum('amount');
        $todayWithdrawal = WithdrawalHistory::whereDate('created_at', now()->format('Y-m-d'))->where('status', 'success')->sum('amount');
        return Inertia::render('Admin/Dashboard', [
            'participants' => $topTeam->left + $topTeam->right,
            'active_participants' => $topTeam->active_left + $topTeam->active_right,
            'deposits' => [
                'from_yesterday' => divDecimalStrings($todayBusiness, '1000', 2) . 'K'
            ],
            'withdrawals' => [
                'tokens' => divDecimalStrings($totalTokenWithdrawal, '1000', 2) . 'K',
                'trx' => castDecimalString($totalWithdrawal, 2) . ' USDT',
                'from_yesterday' => castDecimalString($last24HourWithdrawal, 2) . 'USDT',
                'today_withdrawal' => castDecimalString($todayWithdrawal, 2) . ' USDT'
            ],
            'users' => User::all()->count(),
        ]);
    }

    public function distributeSubscriptionPool()
    {
        dispatch(new DistributePoolIncomeJob())->delay(now()->addSecond());
        return redirect()->route('admin.dashboard')->with('notification', ['Subscription bonus distribution processed successfully. ', 'success']);

    }
}
