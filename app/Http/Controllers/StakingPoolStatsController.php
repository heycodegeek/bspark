<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStakingPoolStatsRequest;
use App\Http\Requests\UpdateStakingPoolStatsRequest;
use App\Models\StakingPoolStats;

class StakingPoolStatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStakingPoolStatsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStakingPoolStatsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StakingPoolStats  $stakingPoolStats
     * @return \Illuminate\Http\Response
     */
    public function show(StakingPoolStats $stakingPoolStats)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StakingPoolStats  $stakingPoolStats
     * @return \Illuminate\Http\Response
     */
    public function edit(StakingPoolStats $stakingPoolStats)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStakingPoolStatsRequest  $request
     * @param  \App\Models\StakingPoolStats  $stakingPoolStats
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStakingPoolStatsRequest $request, StakingPoolStats $stakingPoolStats)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StakingPoolStats  $stakingPoolStats
     * @return \Illuminate\Http\Response
     */
    public function destroy(StakingPoolStats $stakingPoolStats)
    {
        //
    }
}
