<?php

namespace App\Http\Controllers;

use App\Models\InternationalPoolAchiever;
use App\Models\MetaTokenConvertHistory;
use App\Models\SmartMetaSupply;
use App\Models\Staking;
use App\Models\UserMetaverseWallet;
use App\Models\UserSmartMeta;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function create()
    {
        $user = auth()->user();

        $team = $user->team;
        $userCoinWallet = $user->userCoinWallet;
        $userStakeIncomeWallet = $user->userStakeIncomeWallet;
        $tokens = $userCoinWallet ? castDecimalString($userCoinWallet->balance, 2) : castDecimalString('0', 2);
        $UserCoinWalletBalance = $userCoinWallet ? castDecimalString($userCoinWallet->balance, 2) : castDecimalString('0', 2);
        $lockedTokens = $userCoinWallet ? castDecimalString($userCoinWallet->balance_locked, 2) : castDecimalString('0', 2);
        $developmentWallet = $user->developmentWallet ? castDecimalString($user->developmentWallet->balance, 2) : null;
        $subscription = $user->subscription()->with(['coinPlan.levelPlans', 'coinPlan.binaryPlan', 'user.userBinaryInfo'])->first();
        $stakeSubscription = $user->stakeSubscription()->with(['stakeCoinPlan.stakeLevelPlans', 'stakeCoinPlan.stakeBinaryPlan', 'user.userBinaryInfo'])->first();
        $totalStakingBonus = Staking::where('user_id', auth()->user()->id)->sum('earned_so_far');
        $level_staking_bonus = auth()->user()->stakingBusiness;
        $smartMetaSupply = SmartMetaSupply::first();
//        $mySmartMeta = UserSmartMeta::where('user_id', auth()->user()->id)->first();
        $mySmartMeta = userSmartMetaInstance(auth()->user());

        return Inertia::render('Dashboard', [
            'tokens' => $tokens,
            'locked_tokens' => $lockedTokens,
            'portfolio_value' => $tokens,
            'development_wallet' => $developmentWallet,
            'team' => auth()->user()->team,
            'my_subscription' => $subscription,
            'my_stake_subscription' => $stakeSubscription,
            'coin_stake_income' => auth()->user()->userCoinStakeIncomes->sum('token_income'),
            'level_staking_bonus' => castDecimalString($level_staking_bonus->earned_so_far ?? 0, 2) ?? 0,
            'user_income_stat' => $user->userIncomeStat,
            // 'user_rank' => $user->userRank ? $user->userRank->rank->name : 'No Active',
            'rank_team' => $user->rankTeam,
            'userCoinWalletBalance' => $UserCoinWalletBalance,
            'total_staking_bonus' => $totalStakingBonus ?? 0,
            'metaverse_token' => UserMetaverseWallet::where('user_id', auth()->user()->id)->first()->token ?? 0,
            'metaverse_income' => MetaTokenConvertHistory::where('user_id', auth()->user()->id)->sum('receivable_amount'),
            'smart_meta_supply' => $smartMetaSupply,
            'my_smart_meta' => $mySmartMeta,
            'smart_meta_price' => smartMetaPrice(),
            'i_pool_achiever' => InternationalPoolAchiever::where('user_id', auth()->user()->id)->exists()
        ]);
    }
}
