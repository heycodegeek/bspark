<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubscriptionPoolRequest;
use App\Http\Requests\UpdateSubscriptionPoolRequest;
use App\Models\SubscriptionPool;

class SubscriptionPoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubscriptionPoolRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubscriptionPoolRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubscriptionPool  $subscriptionPool
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPool $subscriptionPool)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubscriptionPool  $subscriptionPool
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionPool $subscriptionPool)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSubscriptionPoolRequest  $request
     * @param  \App\Models\SubscriptionPool  $subscriptionPool
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubscriptionPoolRequest $request, SubscriptionPool $subscriptionPool)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubscriptionPool  $subscriptionPool
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriptionPool $subscriptionPool)
    {
        //
    }
}
