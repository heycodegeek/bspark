<?php

namespace App\Http\Controllers;


use App\Models\MetaverseTokenPoolStat;
use App\Models\Staking;
use App\Models\StakingBusiness;
use App\Models\Tree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function showHome()
    {
        $metaverseTokenPrice = metaTokenPrice();

        $tokenStats = MetaverseTokenPoolStat::select("created_at as date", "meta_token_price as value")->withCasts(["date" => "datetime:Y-m-d"])->orderByDesc('id')->groupBy('created_at')->limit(100)->get();

       // return response()->json($tokenStats);
        return view('home', [
            'metaverse_token_price' => $metaverseTokenPrice,
            'token_stats' => $tokenStats
        ]);
    }

    public function showAbout()
    {
        return view('about');
    }

    public function showServices()
    {
        return view('services');
    }

    public function showTnC()
    {
        return view('terms');
    }

    public function showPrivacyPolicy()
    {
        return view('privacy');
    }

    public function showReferPage(Request $request, $ref_code = null)
    {
//        return redirect()->route('register', [$ref_code, 'pos' => $request->get('pos')]);
        return view('landing', [
            'ref_code' => $ref_code
        ]);
    }

    public function systemUpdation()
    {

    }

    public function contact()
    {
        return view('contact');
    }

    public function contactPost(Request $request)
    {
        $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'exists:users,email'],
                'comment' => ['required', 'string', 'max:350'],
            ]
        );

        Mail::send('emails.contact_us', [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'comment' => $request->get('comment')],
            function ($message) {
                $message->from('suretraders3@gmail.com');
                $message->to('info@suretraders.io', 'Contact Us Form')
                    ->subject('New message from contact us page.');
            });
        return back()->with('success', 'Thanks for contacting Us, We will get back to you soon!');

    }

    public function buddyBotBusiness($user_id)
    {
        $buddyBotBusiness = StakingBusiness::select('user_id', 'left_staking_amount', 'right_staking_amount', 'total_staking_business')->where('user_id', $user_id)->first();
        echo "<pre>";
        echo json_encode($buddyBotBusiness);

        $referrals = Tree::where('sponsor_id', $user_id)->pluck('user_id');

        $stakings = Staking::select('user_id', 'amount_in_usd')->whereIn('user_id', $referrals)->get();

        echo json_encode($stakings);


    }
}
