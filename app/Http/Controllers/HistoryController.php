<?php

namespace App\Http\Controllers;

use App\Models\StakingBusinessBonusStats;
use Inertia\Inertia;

class HistoryController extends Controller
{
    public function showDepositHistory()
    {
        $depositTransactions = auth()->user()->depositTransactions()->with('depositTransactionable')->orderByDesc('id')->get();
        return Inertia::render('History/DepositHistory', [
            'history' => $depositTransactions
        ]);
    }

    public function showWithdrawalHistory()
    {
        $withdrawalTransactions = auth()->user()->withdrawalHistories()->with('withdrawCoin')->orderByDesc('id')->get();
        return Inertia::render('History/WithdrawalHistory', [
            'history' => $withdrawalTransactions
        ]);
    }

    public function showLevelStakingBonus()
    {
        $levelStakingBonus = auth()->user()->stakingBusinessBonusStats()->with(['fromUser'])->orderByDesc('id')->get();
//        return $levelStakingBonus;
        $stakingBusiness = auth()->user()->stakingBusiness;
        return Inertia::render('History/LevelStakingBonus', [
            'level_staking_bonus' => $levelStakingBonus,
            'staking_business' => $stakingBusiness,
        ]);
    }

    public function getLevelStakingBonus()
    {
        $levelStakingBonus = StakingBusinessBonusStats::with(['fromUser'])->where('user_id', auth()->user()->id)->withCasts(['created_at'=>'datetime:Y-m-d'])->orderByDesc('id')->simplePaginate(10);
        return response()->json($levelStakingBonus);
    }
}
