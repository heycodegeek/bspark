<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStakingPoolRequest;
use App\Http\Requests\UpdateStakingPoolRequest;
use App\Models\StakingPool;

class StakingPoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStakingPoolRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStakingPoolRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StakingPool  $stakingPool
     * @return \Illuminate\Http\Response
     */
    public function show(StakingPool $stakingPool)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StakingPool  $stakingPool
     * @return \Illuminate\Http\Response
     */
    public function edit(StakingPool $stakingPool)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStakingPoolRequest  $request
     * @param  \App\Models\StakingPool  $stakingPool
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStakingPoolRequest $request, StakingPool $stakingPool)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StakingPool  $stakingPool
     * @return \Illuminate\Http\Response
     */
    public function destroy(StakingPool $stakingPool)
    {
        //
    }
}
