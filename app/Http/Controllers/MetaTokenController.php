<?php

namespace App\Http\Controllers;

use App\Models\MetaTokenConvertHistory;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPoolStat;
use App\Models\Team;
use App\Models\Tree;
use App\Rules\MinimumTokenWithdrawRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Inertia;

class MetaTokenController extends Controller
{
    public function Index()
    {

        $metaUsdValue = multipleDecimalStrings(userMetaverseWalletInstance(\auth()->user())->token, metaTokenPrice(), 8);
        if ($metaUsdValue > castDecimalString('2000', 8)){
            return Inertia::render('UnderMaintenance');
        }
        return Inertia::render('UnderMaintenance');
        return Inertia::render('MetaToken/Index', [
            'user_meta_token_balance' => userMetaverseWalletInstance(\auth()->user())->token,
            'min_withdrawable_token' => getMinimumTransferCoin(),
            'fees_percent' => castDecimalString('10', 2),
            'fees_percent_decimal' => castDecimalString('0.10', 4),
            'meta_token_price' => metaTokenPrice()
        ]);
    }

    public function convertMetaToken(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric', 'gt:0', new MinimumTokenWithdrawRule(getMinimumTransferCoin()),
                function ($attribute, $value, $fail) {
                    if (userMetaverseWalletInstance(\auth()->user())->token < $value) {
                        $fail('You dont have sufficient balance.');
                    }
                }
            ]
        ]);

        $meta_token_price = metaTokenPrice();
        $tokenInUsd = multipleDecimalStrings($request->amount, $meta_token_price, 10);
        $feeAmount = multipleDecimalStrings($tokenInUsd, '0.10', 10);
        $netReceivableAmount = $tokenInUsd - $feeAmount;

        DB::transaction(function () use (&$meta_token_price, &$feeAmount, &$netReceivableAmount, &$request, &$tokenInUsd) {
            $userMetaverseWallet = auth()->user()->userMetaverseWallet;
            $userMetaverseWallet->decrement('token', $request->amount);
            $userMetaverseWallet->increment('token_on_hold', $request->amount);
            MetaTokenConvertHistory::create([
                'user_id' => $request->user()->id,
                'status' => MetaTokenConvertHistory::TXN_STATUS['SUCCESS'],
                'txn_id' => Str::uuid(),
                'tokens' => $request->amount,
                'meta_token_price' => $meta_token_price,
                'fees' => $feeAmount,
                'amount' => $tokenInUsd,
                'receivable_amount' => $netReceivableAmount,
                'particular' => 'Meta Converted into USD.'
            ]);

            $userCoinWallet = userCoinWalletInstance(auth()->user());
            $userCoinWallet->increment('balance', $netReceivableAmount);

            $userMetaverseWallet->decrement('token_on_hold', $request->amount);
            $userMetaverseWallet->increment('burned_token', $request->amount);
            $this->updateMetaPool($feeAmount, $request->amount);
        });

        return redirect()->back()->with('notification', ['Metaverse Token Converted successfully', 'success']);

    }

    public function updateMetaPool($amount_in_usd, $token)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        $burn_token = castDecimalString($token, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('burned_token', $burn_token);
            $metaPool->decrement('available_token', $burn_token);
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaPool->save();

            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['USD_CONVERTED'],
                'status' => 'success'
            ]);
        }
    }

    public function checkMetaUserEligibility()
    {
        echo "<pre>";
        $teams = Team::with('user.subscription')->where('active_direct', '>', 1)->orderBy('id', 'ASC')->get();

        print_r('tc-' . count($teams));

        $teamArray = [];
        foreach ($teams as $team) {
            $tree = Tree::with('user.subscription')->where('sponsor_id', $team->user_id)->orderBy('id', 'ASC')->limit(2)->get();
            $teamsDet = [];
            $teamsSubs = [];
            foreach ($tree as $t) {
                $teamsDet[] = $t->user_id;
                $teamsSubs[] = $t->user->subscription;
            }

            $newData = array(
                'parent' => $team->user->name,
                'parent_id' => $team->user_id,
                'parent_sub_dt' => $team->user->subscription,
                'teamsDet' => $teamsDet
            );

            $teamArray[] = $newData;
        }

        print_r($teamArray);
    }

}
