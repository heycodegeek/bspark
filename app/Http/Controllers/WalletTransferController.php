<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WalletTransfer;
use App\Mohiqssh\OtpMethod;
use App\Mohiqssh\UserLevelMethods;
use App\Notifications\OtpNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class WalletTransferController extends Controller
{
    public function showTransferPage()
    {
        $withdrawableWallet = withdrawableWalletInstance(auth()->user());
        $coinWallet=userCoinWalletInstance(\auth()->user());
        return Inertia::render('Wallets/WalletTransfer', [
            'withdrawable_balance' => $withdrawableWallet->balance,
            'coin_balance'=>$coinWallet->balance
        ]);
    }

    public function showTransferHistoryPage()
    {
        $history = \auth()->user()->walletTransfers()->with('toUser:id,email,name')->orderByDesc('id')->get();
        return Inertia::render('Wallets/History', [
            'history' => $history
        ]);
    }

    public function transfer(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email',
                function ($attribute, $value, $fail) {
                    if (strtolower($value) === strtolower(auth()->user()->email)) {
                        $fail('You can not transfer to yourself');
                    }
                    if (!$this->isUserBelongToTeam(User::where('email', $value)->first())) {
                        $fail('You can only transfer to your downline');
                    }
                }
            ],
            'amount' => ['required', 'numeric', 'gt:0',
                function ($attribute, $value, $fail) {
                    if (auth()->user()->withdrawableWallet->balance < $value) {
                        $fail('You dont have sufficient balance');
                    }
                }
            ]
        ]);


        $this->sendOtp($request);

        return redirect()->back()->with('message', 'Otp sent on your email');


    }

    private function isUserBelongToTeam(User $user)
    {
        $belongToTeam = false;
        UserLevelMethods::init($user)->eachParentV2(function ($parentUser, $level) use (&$belongToTeam) {
            if ($parentUser->id == \auth()->user()->id) {
                $belongToTeam = true;
                return true;

            }
        });

        return $belongToTeam;
    }

    public function sendOtp(Request $request)
    {
        $this->sendOtpOnEmail($request);

    }

    private function sendOtpOnEmail(Request $request)
    {
        $otpModel = $request->user()->otps()->where('is_used', false)->orderByDesc('id')->first();

        if (is_null($otpModel) || now()->unix() > $otpModel->expire_at->unix()) {
            $otpModel = OtpMethod::init()->create()->save($request->user(), 30);
        }
        $request->user()->notify(new OtpNotification($otpModel->code));

    }

    public function transferVerify(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email',
                function ($attribute, $value, $fail) {
                    if (strtolower($value) === strtolower(auth()->user()->email)) {
                        $fail('You can not transfer to yourself');
                    }
                    if (!$this->isUserBelongToTeam(User::where('email', $value)->first())) {
                        $fail('You can only transfer to your downline');
                    }
                }
            ],
            'amount' => ['required', 'numeric', 'gt:0',
                function ($attribute, $value, $fail) {
                    if (auth()->user()->withdrawableWallet->balance < $value) {
                        $fail('You dont have sufficient balance');
                    }
                }
            ],

        ]);
        $this->validateEmailOtp($request);

        DB::transaction(function () use ($request) {
            $otpModel = Auth::user()->otps()->where('code', $request->otp)->where('is_used', false)->orderByDesc('id')->first();
            $toUser = User::where('email', $request->email)->first();
            $toUserWithdrawableWallet = withdrawableWalletInstance($toUser);
            $userWithdrawableWallet = withdrawableWalletInstance($request->user());

            $userWithdrawableWallet->decrement('balance', $request->amount);
            $toUserWithdrawableWallet->increment('balance', $request->amount);

            WalletTransfer::create([
                'user_id' => $request->user()->id,
                'to_user_id' => $toUser->id,
                'amount' => $request->amount
            ]);

            $transferStat = $request->user()->transferStat()->firstOrCreate();

            $transferStat->increment('amount', $request->amount);
            $otpModel->update([
                'is_used' => true
            ]);
        });

        return redirect()->route('wallet.transfer.create')->with('notification', ['Transfer successfully', 'success']);
    }

    private function validateEmailOtp(Request $request)
    {

        $request->validate([
            'otp' => ['required', 'digits:6',
                function ($attribute, $value, $fail) {
                    $otpModel = Auth::user()->otps()->where('code', $value)->where('is_used', false)->orderByDesc('id')->first();
                    if (is_null($otpModel)) {
                        $fail("OTP does not exists.");
                    } elseif (now()->unix() > $otpModel->expire_at->unix()) {
                        $fail("OTP has expired. Please to resend again.");
                    }
                }
            ]
        ]);

    }
}
