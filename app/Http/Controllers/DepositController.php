<?php

namespace App\Http\Controllers;

use App\Blockchains\Bitcoin\BitcoinHandler;
use App\Blockchains\Tron\Handler;
use App\Blockchains\Wallets\EthereumWallet;
use App\Jobs\AddWebhookAddresses;
use App\Jobs\TronCheckNewTransaction;
use App\Models\Coin;
use App\Models\CoinType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DepositController extends Controller
{
    public function create()
    {
        return Inertia::render('Deposit/Create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'currency' => ['required', Rule::in(['trx','btc','usdt'])],
        ]);
        /*        $request->validate([
                    'currency' => ['required', Rule::in(['eth', 'trx', 'btc'])],
                ]);*/
//        $validator = $this->validatorDepositCurrency($request->all());
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->all()], 422);
//        }
        if ($request->currency == 'usdt') {
            return redirect()->route('deposit.tron.deposit.form');
        }elseif ($request->currency == 'trx') {
            return redirect()->route('deposit.tron.deposit.form');
        }elseif ($request->currency == 'btc'){
            return redirect()->route('deposit.btc.deposit.form');
        }
//        $getDepositData = $this->getDepositData($request->amount, $request->currency);
//        if (is_null($getDepositData)) {
//            return response()->json(['error', 'Payment unavailable. Please try again later.'], 422);
//        }
//        try {
//            $createInvoice = InvoiceMethod::init()->create($request->user(), $getDepositData['crypto'], $getDepositData['amount'], $getDepositData['crypto_price'], $request->amount, $getDepositData['address']);
//            return response()->json(['invoice' => $createInvoice->invoice_no]);
//        } catch (\Exception $ex) {
//            Log::error($e);
//            return response()->json(['error', $ex->getTraceAsString()]);
//        }
    }

    public function trxPaid()
    {
        return $this->tronCheckNewTransaction();
    }

    private function tronCheckNewTransaction()
    {
        $coin = Coin::where('name', 'trx')->first();
        $coinType = $coin->coinTypes()->where('type', 'legacy')->first();
        $depositWallet = auth()->user()->depositWallets()->where('coin_type_id', $coinType->id)->first();

        if (is_null($depositWallet)) {
            return response()->json(['error' => 'unable find deposit wallet'], 422);
        }

        TronCheckNewTransaction::dispatch($depositWallet)->delay(now()->addSeconds(10))->onQueue('newFilter');
        TronCheckNewTransaction::dispatch($depositWallet)->delay(now()->addMinutes(2))->onQueue('newFilter');
        return response()->json(['success', 'We will update once transactions is confirmed']);
    }

    public function getDepositSummary(Request $request)
    {
        $request->validate([
            'amount_usd' => ['required', 'numeric', 'gt:0'],
            'currency' => ['required', Rule::in(['eth', 'trx', 'btc'])],
        ]);

        $getDepositData = $this->getDepositData($request->amount_usd, $request->currency);
        if (is_null($getDepositData)) {
            return response()->json(['error' => 'Unable to generate address. Please true again later'], 422);
        }
        return response()->json($getDepositData);
    }

    private function getDepositData($amountInUsd, $depositCurrency)
    {
        if ($depositCurrency == 'trx') {
            $cryptoPrice = tronPrice();
            $paybleCrypto = divDecimalStrings($amountInUsd, $cryptoPrice, 5);
            $userWallet = $this->generateWallet('trx');
            if (is_null($userWallet)) {
                return null;
            }

            return ['address' => $userWallet->address, 'qr' => base64_encode(QrCode::size(250)->generate($userWallet->address . '?value=' . $paybleCrypto)), 'amount' => $paybleCrypto, 'crypto' => 'trx', 'crypto_price' => $cryptoPrice];
        } elseif ($depositCurrency == 'eth') {
            $cryptoPrice = ethereumPrice();
            $paybleCrypto = divDecimalStrings($amountInUsd, $cryptoPrice, 8);
            $userWallet = $this->generateWallet('eth');
            if (is_null($userWallet)) {
                return null;
            }
            return ['address' => strtolower($userWallet->address), 'qr' => base64_encode(QrCode::size(250)->generate(strtolower($userWallet->address) . '?value=' . $paybleCrypto)), 'amount' => $paybleCrypto, 'crypto' => 'eth', 'crypto_price' => $cryptoPrice];

        } else {
            $cryptoPrice = bitcoinPrice();
            $paybleCrypto = divDecimalStrings($amountInUsd, $cryptoPrice, 8);
            $userWallet = $this->generateWallet();
            if (is_null($userWallet)) {
                return null;
            }

            return ['address' => $userWallet->address, 'qr' => base64_encode(QrCode::size(250)->generate('bitcoin:' . $userWallet->address)), 'amount' => $paybleCrypto, 'crypto' => 'btc', 'crypto_price' => $cryptoPrice];
        }

    }

    private function generateWallet(string $coinName = 'btc')
    {
        $coin = Coin::where('name', strtolower($coinName))->first();
        $coinType = $coin->coinTypes()->where('type', CoinType::TYPES['legacy'])->first();
        $userWallet = auth()->user()->depositWallets()->where('coin_type_id', $coinType->id)->first();
        if ($userWallet && !empty($userWallet->address)) {
            return $userWallet;
        }
        if ($coinName == 'btc') {
            $newWallet = $this->generateBitcoinAddress(auth()->user()->id);

        } elseif ($coinName == 'trx') {
            $newWallet = $this->generateTronAddress();

        } elseif ($coinName == 'eth') {
            $newWallet = $this->generateEthereumWallet();

        } else {
            return null;
        }
        if (!is_array($newWallet)) {
            return null;
        }
        $userWallet = auth()->user()->depositWallets()->create([
            'address' => $newWallet['address'], 'priv_key' => $newWallet['priv_key'], 'coin_type_id' => $coinType->id, 'balance' => '0'
        ]);
        if ($coinName == 'eth') {
            dispatch(new AddWebhookAddresses($userWallet))->afterCommit()->delay(now()->addSeconds(1))->onQueue('alchemy');
        }
        return $userWallet;

    }

    private function generateBitcoinAddress(string $label = '')
    {
        return BitcoinHandler::init()->getNewAddress($label);
    }

    private function generateTronAddress()
    {
        return Handler::init()->getNewAddress();
    }

    private function generateEthereumWallet()
    {
        $ethereumWallet = EthereumWallet::wallet()->generateNewWallet();
        if (!is_array($ethereumWallet)) {
            return null;
        }
        if (strtolower($ethereumWallet['address']) !== '0x' . strtolower(EthereumWallet::wallet()->fromPrivateKey($ethereumWallet['priv_key'])->getAddress())) {
            return null;
        }
        return $ethereumWallet;
    }

    public function showTronForm(Request $request)
    {
        return Inertia::render('Deposit/TronDepositForm');
    }

    public function showBitcoinForm(Request $request)
    {
        return Inertia::render('Deposit/BitcoinDepositForm');
    }

    public function showPaymentForm(Request $request)
    {

    }

    private function validatorDepositCurrency(array $data)
    {
        return Validator::make($data, [
            'currency' => ['required', Rule::in(['eth', 'trx', 'btc'])],
//            'amount' => ['required', 'numeric', 'min:100']
        ]);
//        return Validator::make($data, [
//            'currency' => ['required', Rule::in(['eth', 'trx', 'bnb', 'btc'])],
//            'amount' => ['required', 'numeric', 'min:100']
//        ]);
    }
}
