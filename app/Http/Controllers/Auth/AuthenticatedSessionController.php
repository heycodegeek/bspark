<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use function PHPUnit\Framework\isNull;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return Inertia::render('Auth/Login', [
            'canResetPassword' => Route::has('password.request'),
            'status' => session('status'),
        ]);
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param \App\Http\Requests\Auth\LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {

        $request->validate([
            'email' => ['required', 'email', 'exists:users,email',
                function ($attribute, $value, $fail) {
                    $isBlocked = User::where('email', $value)->where('is_blocked', 0)->first();
                    if (is_null($isBlocked)) {
                        $fail('Login Failed!. Please contact to admin for more details..');
                    }
//                    $withinMonthlyLogin = User::where('email', $value)->where('last_login_at', '>', now()->subDays(15)->endOfDay())->first();
//                    if (is_null($withinMonthlyLogin)) {
//                        $fail('Your Account is inactive. Please contact to admin for more details..');
//                    }
                }

            ]
        ]);

//        function ($attribute, $value, $fail) {
//            $withinMonthlyLogin = User::where('email', $value)->where('last_login_at', '>', now()->subDays(30)->endOfDay())->first();
//            if (is_null($withinMonthlyLogin)) {
//                $fail('Your Account is blocked due to none activity within the month. Please contact to admin for more details..');
//            }
//        }

        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
