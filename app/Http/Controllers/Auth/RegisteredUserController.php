<?php

namespace App\Http\Controllers\Auth;

use App\Events\Registered;
use App\Http\Controllers\Controller;
use App\Jobs\PostRegisterationJob;
use App\Models\User;
use App\Mohiqssh\OtpMethod;
use App\Notifications\OtpNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use function PHPUnit\Framework\isNull;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     */
    public function create($ref_code = null)
    {
        $validator = Validator::make(['ref_code' => $ref_code], [
            'ref_code' => ['required', 'exists:users,ref_code']
        ]);
        $position = \request()->get('pos');
        return Inertia::render('Auth/Register', [
            'referred_by' => $validator->fails() ? null : $ref_code,
            'position' => $position && (strtolower($position == 'left')) || strtolower($position == 'right') ? strtolower(\request()->get('pos')) : null
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = User::create([
            'name' => $request->full_name,
            'email' => $request->email,
            'password' => $request->password,
            'country_id' => $request->country,
            'ref_code' => generateRefCode()
        ]);
        $user->team()->create();
        $user->userCoinwallet()->create();

        $sponsorUser = User::whereRefCode($request->referral)->first();

        if (!is_null($sponsorUser)) {
            $user->userSponsor()->create([
                'sponsor_id' => $sponsorUser->id,
                'sponsor_code' => $sponsorUser->ref_code,
                'position' => $request->position
            ]);
        }

//        PostRegisterationJob::dispatch($user, $sponsorUser, $request->position)->delay(now()->addSecond());


        $otpModel = OtpMethod::init()->create()->save($user, 30);
        $user->notify(new OtpNotification($otpModel->code));

        event(new Registered($user));
        Auth::login($user);
        return redirect(RouteServiceProvider::HOME);
    }

    protected function validator(array $data): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($data, [
            'full_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'position' => ['required', Rule::in(['left', 'right'])],
            'country' => ['required', 'exists:countries,id'],
            'referral' => ['required', 'string', 'exists:users,ref_code'],
        ]);
    }
}
