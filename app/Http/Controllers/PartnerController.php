<?php

namespace App\Http\Controllers;

use App\Models\Tree;
use App\Models\User;
use App\Mohiqssh\BinaryTree;
use App\Mohiqssh\Downline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class PartnerController extends Controller
{
    public function seachDownline(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email']
        ]);
        $downlineUser = User::where('email', strtolower($request->email))->first();
        if (BinaryTree::init($downlineUser)->isBelongToTeam(auth()->user())) {
            $rootUser = $downlineUser;
            return redirect()->route('partners.geneaology', [$rootUser->id]);
        }
        return redirect()->back()->with('notification', ['User does not belod to your team', 'danger']);
    }

    public function showGenealogy(Request $request, $user_id = null)
    {
        $rootUser = auth()->user();
        if (!is_null($user_id)) {
            $validator = Validator::make(['user' => $user_id], [
                'user' => ['nullable', 'exists:users,id']
            ]);
            if ($validator->fails()) {
                $rootUser = auth()->user();
            } else {
                $downlineUser = User::find($user_id);
                if (BinaryTree::init($downlineUser)->isBelongToTeam(auth()->user())) {
                    $rootUser = $downlineUser;
                }

            }
        }
        $rootUserTree = $rootUser->tree()->with(['sponsor:id,email,name', 'user.team:id,user_id,left,right,active_left,active_right,direct,active_direct', 'user.userBusiness'])->first();

        $firstLevel1st = $this->getDirectMember($rootUser, Tree::POSITIONS['LEFT']);
        $firstLevel2nd = $this->getDirectMember($rootUser, Tree::POSITIONS['RIGHT']);
        $secondLevel1st = null;
        $secondLevel2nd = null;
        $secondLevel3rd = null;
        $secondLevel4th = null;
        $thirdLevel1st = null;
        $thirdLevel2nd = null;
        $thirdLevel3rd = null;
        $thirdLevel4th = null;
        $thirdLevel5th = null;
        $thirdLevel6th = null;
        $thirdLevel7th = null;
        $thirdLevel8th = null;

        if (!is_null($firstLevel1st)) {
            $secondLevel1st = $this->getDirectMember($firstLevel1st->user, Tree::POSITIONS['LEFT']);
            $secondLevel2nd = $this->getDirectMember($firstLevel1st->user, Tree::POSITIONS['RIGHT']);

        }
        if (!is_null($firstLevel2nd)) {
            $secondLevel3rd = $this->getDirectMember($firstLevel2nd->user, Tree::POSITIONS['LEFT']);
            $secondLevel4th = $this->getDirectMember($firstLevel2nd->user, Tree::POSITIONS['RIGHT']);
        }

        if (!is_null($secondLevel1st)) {
            $thirdLevel1st = $this->getDirectMember($secondLevel1st->user, Tree::POSITIONS['LEFT']);
            $thirdLevel2nd = $this->getDirectMember($secondLevel1st->user, Tree::POSITIONS['RIGHT']);
        }
        if (!is_null($secondLevel2nd)) {
            $thirdLevel3rd = $this->getDirectMember($secondLevel2nd->user, Tree::POSITIONS['LEFT']);
            $thirdLevel4th = $this->getDirectMember($secondLevel2nd->user, Tree::POSITIONS['RIGHT']);
        }
        if (!is_null($secondLevel3rd)) {
            $thirdLevel5th = $this->getDirectMember($secondLevel3rd->user, Tree::POSITIONS['LEFT']);
            $thirdLevel6th = $this->getDirectMember($secondLevel3rd->user, Tree::POSITIONS['RIGHT']);
        }
        if (!is_null($secondLevel4th)) {
            $thirdLevel7th = $this->getDirectMember($secondLevel4th->user, Tree::POSITIONS['LEFT']);
            $thirdLevel8th = $this->getDirectMember($secondLevel4th->user, Tree::POSITIONS['RIGHT']);
        }

        $geneology = [
            'root' => $rootUserTree,
            'firstLevel' => [
                $firstLevel1st,
                $firstLevel2nd
            ],
            'secondLevel' => [
                $secondLevel1st,
                $secondLevel2nd,
                $secondLevel3rd,
                $secondLevel4th,
            ],
            'thirdLevel' => [
                $thirdLevel1st,
                $thirdLevel2nd,
                $thirdLevel3rd,
                $thirdLevel4th,
                $thirdLevel5th,
                $thirdLevel6th,
                $thirdLevel7th,
                $thirdLevel8th,
            ],
            'links' => [
                'down_left' => !is_null($thirdLevel1st) ? route('partners.geneaology', [strtolower($thirdLevel1st->user->username)]) : null,
                'down_right' => !is_null($thirdLevel8th) ? route('partners.geneaology', [strtolower($thirdLevel8th->user->username)]) : null,
                'up_parent' => ($rootUserTree->user->id == auth()->user()->id) ? null : route('partners.geneaology', [strtolower($rootUserTree->user->parent->username)]),
                'top_root' => route('partners.geneaology', [strtolower(auth()->user()->username)])
            ]
        ];
        return Inertia::render('Partners/Geneaology', [
            'tree' => $geneology,
            'team' => $rootUser->team,
        ]);
    }

    protected function getDirectMember(User $user, $position): ?Tree
    {
        return Downline::init($user)->getDirectMember($position);
    }

    public function showDirectPartner()
    {
        return Inertia::render('Partners/Direct', [
            'team' => auth()->user()->team
        ]);
    }

    public function getDirectPartners()
    {
        $user = auth()->user();
        $directDownlind = Tree::with(['user.subscription.coinPlan'])->where('sponsor_id', $user->id)->simplePaginate(15);
        return response()->json($directDownlind);
    }
}
