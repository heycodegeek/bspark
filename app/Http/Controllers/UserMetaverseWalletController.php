<?php

namespace App\Http\Controllers;

use App\Models\MetaTokenConvertHistory;
use App\Models\UserMetaverseWalletTransaction;
use Inertia\Inertia;

class UserMetaverseWalletController extends Controller
{
    public function Index()
    {
        $meta_wallet = userMetaverseWalletInstance(auth()->user());
        $converted = MetaTokenConvertHistory::where('user_id', auth()->user()->id)->sum('tokens');
        $history = MetaTokenConvertHistory::where('user_id', auth()->user()->id)->withCasts(['created_at' => 'datetime:Y-m-d'])->orderByDesc('id')->get();
        return Inertia::render('History/MetaConvertHistory', [
            'history' => $history,
            'meta_wallet' => $meta_wallet,
            'converted' => castDecimalString($converted, 12) ?? 0
        ]);
    }

    public function MetaverseIncome()
    {
        return Inertia::render('MetaToken/CreditHistory');
    }

    public function GetMetaverseIncome()
    {
        $creditHistory= UserMetaverseWalletTransaction::where('user_id',auth()->user()->id)->where('status','success')->where('type','credit')->withCasts(['created_at'=>'datetime:Y-m-d'])->orderByDesc('id')->simplePaginate(10);
        return response()->json($creditHistory);
    }
}
