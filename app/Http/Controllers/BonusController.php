<?php

namespace App\Http\Controllers;

use App\Models\InternationalPoolIncomeStats;
use App\Models\UserDelegatorLevelBonus;
use App\Models\UserLevelIncome;
use Cassandra\Inet;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BonusController extends Controller
{
    public function showLevelIncomeStat(Request $request, UserLevelIncome $level_income_id)
    {
//        $levelIncomeStats = $level_income_id->userLevelIncomeStats()->with(['subscriptionHistory', 'subscriptionHistory.user'])->orderBy('level')->get();
        $levelIncomeStats = $level_income_id->userLevelIncomeStats()->orderBy('level')->get();
        return Inertia::render('Bonus/LevelIncomeStat', [
            'earning_stats' => $levelIncomeStats,
            'income_date' => $level_income_id->income_date
        ]);
    }

    public function showBinaryBonus()
    {
        return Inertia::render('Bonus/BinaryBonus', [
            'user_income_stat' => auth()->user()->userIncomeStat
        ]);
    }

    public function showMatchingBonus()
    {
        return Inertia::render('Bonus/MatchingBonus', [
            'user_income_stat' => auth()->user()->userIncomeStat
        ]);
    }

    public function showDirectBonus()
    {
        return Inertia::render('Bonus/DirectBonus', [
            'user_income_stat' => auth()->user()->userIncomeStat
        ]);
    }

    public function showStaking()
    {
        return Inertia::render('Bonus/DailyBonus', [
            'user_income_stat' => auth()->user()->userIncomeStat
        ]);
    }

    public function showDelegatorBonus()
    {
        return Inertia::render('Bonus/DelegatorBonus', [

        ]);
    }

    public function showDelegatorLevelBonus()
    {
        return Inertia::render('Bonus/DelegatorLevelBonus', [

        ]);
    }


    public function getBinaryPayoutList()
    {
        $binaryBonus = auth()->user()->binaryPayouts()->orderByDesc('id')->simplePaginate(10);
        return response()->json($binaryBonus);
    }

    public function getMatchingPayoutList()
    {
//        $matchingBonus = auth()->user()->matchingBonuses()->with('binaryPayout')->orderByDesc('id')->simplePaginate(10);
        $matchingBonus = auth()->user()->subscriptionPoolBonus()->withCasts(['created_at' => 'datetime:Y-m-d'])->orderByDesc('id')->simplePaginate(10);
        return response()->json($matchingBonus);
    }

    public function getLevelIncomeList()
    {
        $levelIncome = auth()->user()->userLevelIncomes()->with('userLevelIncomeStats:id,user_level_income_id')->orderByDesc('id')->simplePaginate(10);
        return response()->json($levelIncome);
    }


    public function getStakeEarnings()
    {
        $stakeEarnings = auth()->user()->userStakeIncomes()->select('id', 'closing_date', 'staked_tokens', 'token_price', 'token_income')->orderByDesc('closing_date')->simplePaginate(10);
        return response()->json($stakeEarnings);
    }

    public function getDelegatorEarnings()
    {
        $delegatorEarnings = auth()->user()->userDelegatorIncomes()->orderByDesc('closing_date')->simplePaginate(10);
        return response()->json($delegatorEarnings);
    }

    public function getDelegatorLevelEarnings()
    {
        $delegatorEarnings = UserDelegatorLevelBonus::with(['downlineUser'])->where('user_id', auth()->user()->id)->orderByDesc('closing_date')->simplePaginate(10);
        return response()->json($delegatorEarnings);
    }

    public function showIPoolBonus()
    {
        return Inertia::render('Bonus/iPoolBonus');
    }

    public function getIPoolEarnings()
    {
        $iPoolEarnings = InternationalPoolIncomeStats::where('user_id', auth()->user()->id)->orderByDesc('income_date')->simplePaginate(10);
        return response()->json($iPoolEarnings);
    }


}
