<?php

namespace App\Http\Controllers;

use App\Blockchains\Tron\Handler;
use App\Blockchains\Wallets\EthereumWallet;
use App\Jobs\ProcessWithdrawalJob;
use App\Models\InternationalPool;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPoolStat;
use App\Models\Staking;
use App\Models\User;
use App\Models\WithdrawalTemp;
use App\Models\WithdrawCoin;
use App\Mohiqssh\OtpMethod;
use App\Notifications\OtpNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class WithdrawController extends Controller
{

    public function verifyOtp(Request $request)
    {
        $request->validate([
            'withdraw_id' => ['required', 'numeric', 'exists:withdrawal_temps,id',
                function ($attribute, $value, $fail) {
                    $withdrawHistory = WithdrawalTemp::find($value);
                    if ($withdrawHistory->status != 'pending') {
                        $fail("Invalid request.");
                    } elseif ($withdrawHistory->user_id != Auth::user()->id) {
                        $fail("You are not authorized to request this withdrawal");
                    }
                }
            ],
            'otp' => ['required', 'digits:6',
                function ($attribute, $value, $fail) {
                    $otpModel = Auth::user()->otps()->where('code', $value)->where('is_used', false)->orderByDesc('id')->first();
                    if (is_null($otpModel)) {
                        $fail("Invalid OTP");
                    } elseif (now()->unix() > $otpModel->expire_at->unix()) {
                        $fail("OTP has expired. Please to resend again.");
                    }
                }
            ]
        ]);


        $otpModel = Auth::user()->otps()->where('code', $request->otp)->where('is_used', false)->orderByDesc('id')->first();
        $otpModel->update([
            'is_used' => true
        ]);

        $withdrawalTemp = WithdrawalTemp::find($request->withdraw_id);
//        if (\auth()->user()->withdrawalHistories()->whereDate('created_at', now()->format('Y-m-d'))->exists()) {
//            $withdrawalTemp->update([
//                'status' => 'failed'
//            ]);
//            return redirect()->route('withdraw.select.crypto')->with('notification', ['Only 1 transaction is allowed in 24 Hours.']);
//        }

        if ($withdrawalTemp->tokens > getAvailableCoinBalance(\auth()->user())) {
            $withdrawalTemp->update([
                'status' => 'failed'
            ]);

            return redirect()->route('withdraw.select.crypto')->with('notification', ['You dont have sufficient balance', 'danger']);
        }
        $withdrawalHistory = null;

        DB::transaction(function () use (&$withdrawalTemp, &$request, &$withdrawalHistory) {
            $userCoinWallet = auth()->user()->userCoinWallet()->select('id', 'balance', 'balance_on_hold', 'balance_locked')->first();
            $userCoinWallet->decrement('balance', $withdrawalTemp->tokens);
            $userCoinWallet->increment('balance_on_hold', $withdrawalTemp->tokens);

            $withdrawalHistory = $request->user()->withdrawalHistories()->create([
                'txn_id' => null,
                'withdraw_coin_id' => $withdrawalTemp->withdraw_coin_id,
                'address' => $withdrawalTemp->address,
                'tokens' => $withdrawalTemp->tokens,
                'token_price' => $withdrawalTemp->token_price,
                'withdrawal_crypto_price' => $withdrawalTemp->withdrawal_crypto_price,
                'fees' => $withdrawalTemp->fees,
                'amount' => $withdrawalTemp->amount,
                'receivable_amount' => $withdrawalTemp->receivable_amount,
                'status' => 'pending'
            ]);
            $withdrawalTemp->update([
                'status' => 'success'
            ]);
        });

        $usdForMeta = multipleDecimalStrings($withdrawalTemp->fees, '0.70');
        $usdForiPool = multipleDecimalStrings($withdrawalTemp->fees, '0.30');

        $this->updateMetaPool($usdForMeta);
        $this->updateIPool($usdForiPool);

        if (multipleDecimalStrings($withdrawalHistory->tokens, $withdrawalHistory->token_price, 2) < castDecimalString('0', 2)) {
            return redirect()->route('history.withdrawal')->with('notification', ['Withdrawal submitted successfully & will process in 24 hours.', 'success']);
        } else {
            ProcessWithdrawalJob::dispatch($withdrawalHistory)->afterCommit()->delay(now()->addSecond());


            return redirect()->route('history.withdrawal')->with('notification', ['Withdrawal submitted successfully', 'success']);
        }


    }

    public function withdrawTrxAttempt(Request $request)
    {
        $request->validate([
            'coin_amount' => ['required', 'numeric', 'gt:0',
                function ($attribute, $value, $fail) {
                    if (is_null(auth()->user()->userCoinWallet) || getAvailableCoinBalance(\auth()->user()) < $value) {
                        $fail('You dont have sufficient balance');
                    }
                    if (multipleDecimalStrings($value, tokenPrice(), 2) < '10') {
                        $fail('Minimum amount should be 10 USD');
                    }
//                    if (\auth()->user()->withdrawalHistories()->whereDate('created_at', now()->format('Y-m-d'))->exists()) {
//                        $fail('Only 1 transaction is allowed in 24 hours');
//                    }
                    $maxWithdrawalLimit = '50';
                    if (multipleDecimalStrings($value, tokenPrice(), 2) > $maxWithdrawalLimit) {
                        $fail('Maximum amount should be ' . $maxWithdrawalLimit . ' USD');
                    }

                    $dailyWithdrawal = \auth()->user()->withdrawalHistories()->whereDate('created_at', now()->format('Y-m-d'))->sum('amount');
                    if ($dailyWithdrawal >= castDecimalString($maxWithdrawalLimit, 2) || addDecimalStrings($dailyWithdrawal, $value, 2) > castDecimalString($maxWithdrawalLimit, 2)) {
                        $fail('Maximum amount should be ' . $maxWithdrawalLimit . ' USD in 24 hours.');
                    }
                }
            ]
        ]);


        $investments = Staking::where('user_id', auth()->user()->id)->sum('amount_in_usd');
//        if (castDecimalString($investments, 2) > castDecimalString('2000', 2)) {
//            return redirect()->route('withdraw.accounts')->with('notification', ['There is some technical issue, Please try again later!', 'danger']);
//        }
//
//        if (getAvailableCoinBalance(\auth()->user()) > castDecimalString('200', 2)) {
//            return redirect()->route('withdraw.accounts')->with('notification', ['There is some technical issue, Please try again later!', 'danger']);
//        }

        $withdrawCoin = WithdrawCoin::where('name', 'trc20')->first();
        if (is_null(auth()->user()->withdrawWallets()->where('withdraw_coin_id', $withdrawCoin->id)->first())) {
            return redirect()->route('withdraw.accounts')->with('notification', ['Please update your wallet', 'danger']);
        }

        $trxAmount = $this->convertTokenToTrx($request->coin_amount);
        $fees = multipleDecimalStrings($trxAmount, '0.10', 6);
        $withdrawWallet = auth()->user()->withdrawWallets()->where('withdraw_coin_id', $withdrawCoin->id)->first();
        $withdrawAddress = $withdrawWallet->address;

        $withdrawalTemp = auth()->user()->withdrawalTemps()->create([
            'withdraw_coin_id' => $withdrawCoin->id,
            'address' => $withdrawAddress,
            'tokens' => $request->coin_amount,
            'token_price' => tokenPrice(),
            'withdrawal_crypto_price' => tronPrice(),
            'fees' => $fees,
            'amount' => $trxAmount,
            'receivable_amount' => subDecimalStrings($trxAmount, $fees, 6),
            'status' => 'pending'
        ]);
        $otpModel = OtpMethod::init()->create()->save($request->user(), 30);
        $request->user()->notify(new OtpNotification($otpModel->code));
        return redirect()->route('withdraw.verify', [$withdrawalTemp->id])->with('notification', ['OTP sent to your email address', 'success']);
    }

    private function convertTokenToTrx(string $tokenAmount)
    {

        $tokenPrice = tokenPrice();
//        $tronPrice = tronPrice();
        $tronPrice = 1;
        $tokensAmountInUsd = multipleDecimalStrings($tokenAmount, $tokenPrice, 2);
        return divDecimalStrings($tokensAmountInUsd, $tronPrice, 6);

    }

    public function showOtpForm(Request $request, WithdrawalTemp $withdraw_temp_history)
    {
        $userId = $withdraw_temp_history->user_id;
        if ($userId != auth()->user()->id) {
            abort(404);
        }
        if ($withdraw_temp_history->status != 'pending') {
            abort(404);
        }
        return Inertia::render('Withdraw/WithdrawVerify', [
            'withdraw_details' => $withdraw_temp_history
        ]);
    }

    public function getConversionPrice(Request $request)
    {
        $request->validate([
            'coin_amount' => ['required', 'numeric', 'gt:0',
                function ($attribute, $value, $fail) {
                    if (is_null(auth()->user()->userCoinWallet) || getAvailableCoinBalance(\auth()->user()) < $value) {
                        $fail('You dont have sufficient balance');
                    }
                    if (multipleDecimalStrings($value, tokenPrice(), 2) < '20') {
                        $fail('Minimum amount should be 20 USD');
                    }
                    $maxWithdrawalLimit = '50';
                    if (multipleDecimalStrings($value, tokenPrice(), 2) > $maxWithdrawalLimit) {
                        $fail('Maximum amount should be ' . $maxWithdrawalLimit . ' USD');
                    }

                    $dailyWithdrawal = \auth()->user()->withdrawalHistories()->whereDate('created_at', now()->format('Y-m-d'))->sum('amount');
                    if ($dailyWithdrawal >= castDecimalString($maxWithdrawalLimit, 2) || addDecimalStrings($dailyWithdrawal, $value, 2) > castDecimalString($maxWithdrawalLimit, 2)) {
                        $fail('Maximum amount should be ' . $maxWithdrawalLimit . ' USD in 24 hours.');
                    }
                }
            ]
        ]);
        $trxAmount = $this->convertTokenToTrx($request->coin_amount);
        $fees = multipleDecimalStrings($trxAmount, '0.10', 6);

        return response()->json(['amount' => $trxAmount, 'fees' => $fees, 'receivable_amount' => subDecimalStrings($trxAmount, $fees, 6)]);

    }

    public function withdrawToken()
    {

        return Inertia::render('Withdraw/WithdrawToken', [
            'available_balance' => getAvailableCoinBalance(\auth()->user())
        ]);
    }

    public function withdrawTrx()
    {
        return Inertia::render('Withdraw/WithdrawTrx', [
            'available_balance' => getAvailableCoinBalance(\auth()->user())
        ]);
    }

    public function withdrawUsdt()
    {
       // return Inertia::render('UnderMaintenance');
        $withdrawal_eligible = 1;
        if (auth()->user()->id > 564 && is_null(auth()->user()->subscription)) {
            $withdrawal_eligible = 0;
        }
        return Inertia::render('Withdraw/WithdrawUsdt', [
            'available_balance' => getAvailableCoinBalance(\auth()->user()),
            'withdrawal_eligible' => $withdrawal_eligible,
        ]);
    }

    public function selectCoin()
    {
        return Inertia::render('Withdraw/WithdrawCoin');
    }

    public function showAccounts()
    {

        $usdtWithdrawWallet = WithdrawCoin::where('name', 'trc20')->first()->withdrawWallets()->where('user_id', auth()->user()->id)->first();
        return Inertia::render('Withdraw/WithdrawAccount', [
            'usdt_withdraw_address' => $usdtWithdrawWallet ? $usdtWithdrawWallet->address : null
        ]);
    }

    public function updateTrxWallet(Request $request)
    {
        $request->validate([
            'address' => ['required', 'string',
                function ($attribute, $value, $fail) {
                    if (!Handler::init()->isAddress($value)) {
                        $fail('TRX address is not valid');
                    }
                }
            ]
        ]);

        $this->validateEmailOtp($request);
        DB::transaction(function () use (&$request) {
            $otpModel = Auth::user()->otps()->where('code', $request->otp)->where('is_used', false)->orderByDesc('id')->first();
            $withdrawCoin = WithdrawCoin::where('name', 'trc20')->first();
            $withdrawWallet = auth()->user()->withdrawWallets()->where('withdraw_coin_id', $withdrawCoin->id)->first();
            if ($withdrawWallet) {
                $withdrawWallet->update([
                    'address' => $request->address
                ]);
            } else {
                auth()->user()->withdrawWallets()->create([
                    'withdraw_coin_id' => $withdrawCoin->id,
                    'address' => $request->address
                ]);
            }

            $otpModel->update([
                'is_used' => true
            ]);
        });


        return back()->with('notification', ['Address updated successfully']);
    }

    private function validateEmailOtp(Request $request)
    {

        $request->validate([
            'otp' => ['required', 'digits:6',
                function ($attribute, $value, $fail) {
                    $otpModel = Auth::user()->otps()->where('code', $value)->where('is_used', false)->orderByDesc('id')->first();
                    if (is_null($otpModel)) {
                        $fail("OTP does not exists.");
                    } elseif (now()->unix() > $otpModel->expire_at->unix()) {
                        $fail("OTP has expired. Please to resend again.");
                    }
                }
            ]
        ]);

    }

    public function updateDukuWallet(Request $request)
    {
        $request->validate([
            'address' => ['required', 'string',
                function ($attribute, $value, $fail) {
                    if (!EthereumWallet::wallet()->isValidAddress($value)) {
                        $fail('Address is not valid');
                    }
                }
            ]
        ]);

        $this->validateEmailOtp($request);
        DB::transaction(function () use (&$request) {
            $otpModel = Auth::user()->otps()->where('code', $request->otp)->where('is_used', false)->orderByDesc('id')->first();
            $withdrawCoin = WithdrawCoin::where('name', 'duku')->first();
            $withdrawWallet = auth()->user()->withdrawWallets()->where('withdraw_coin_id', $withdrawCoin->id)->first();
            if ($withdrawWallet) {
                $withdrawWallet->update([
                    'address' => $request->address
                ]);
            } else {
                auth()->user()->withdrawWallets()->create([
                    'withdraw_coin_id' => $withdrawCoin->id,
                    'address' => $request->address
                ]);
            }

            $otpModel->update([
                'is_used' => true
            ]);
        });

        return back()->with('notification', ['Address updated successfully']);
    }

    public function sendOtp(Request $request)
    {
        $this->sendOtpOnEmail($request);
        return back();

    }

    private function sendOtpOnEmail(Request $request)
    {
        $otpModel = $request->user()->otps()->where('is_used', false)->orderByDesc('id')->first();

        if (is_null($otpModel) || now()->unix() > $otpModel->expire_at->unix()) {
            $otpModel = OtpMethod::init()->create()->save($request->user(), 30);
        }
        $request->user()->notify(new OtpNotification($otpModel->code));

    }

    public function updateMetaPool($amount_in_usd)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['META_RELEASED'],
                'status' => 'success'
            ]);
        }
    }

    public function updateIPool($amountUsd)
    {
        $amountUsd = castDecimalString($amountUsd, 4);
        $iPool = InternationalPool::first();
        $iPool->increment('balance', $amountUsd);
        $iPool->increment('from_withdrawals', $amountUsd);

    }
}
