<?php

namespace App\Http\Controllers;

use Inertia\Inertia;

class AirdropController extends Controller
{
    public function showAirdropForm()
    {
        return Inertia::render('Claim', [
            'activity' => auth()->user()->airdropActivity
        ]);
    }
}
