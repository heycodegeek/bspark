<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StakingBusiness;
use App\Models\StakingTeamDistributionPlan;
use App\Models\Subscription;
use App\Models\User;
use App\Models\WithdrawalHistory;
use App\Mohiqssh\UserLevelMethods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use PDF;

class AdminUserController extends Controller
{
    public function showAllUsersPage()
    {
        return Inertia::render('Admin/Users/Index');
    }

    public function getUsers(Request $request)
    {
        $users = User::with(['team', 'tree.sponsor', 'userCoinWallet'])->orderByDesc('id')->paginate(10);
        return response()->json($users);
    }

    public function searchUser(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email']
        ]);
        return response()->json(User::where('email', strtolower($request->email))->with('country')->first());
    }

    public function filterUsers(Request $request)
    {
        $request->validate([
            'filter' => ['required', 'string']
        ]);
        $users = User::with(['team', 'tree.sponsor', 'userCoinWallet'])
            ->where('name', 'like', '%' . $request->filter . '%')
            ->orWhere('email', 'like', '%' . $request->filter . '%')
            ->orWhere('mobile', 'like', '%' . $request->filter . '%')
            ->orderByDesc('id')->paginate(10);
        return response()->json($users);
    }

    public function showUser(User $user)
    {
        return Inertia::render('Admin/User/Index', [
            'user' => $user,
            'sponsor' => $user->tree->sponsor,
            'wallets' => [
                'cash' => userCoinWalletInstance($user),
            ]
        ]);
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore(\request()->post('user_id'))],
            'name' => ['required', 'string', Rule::unique('users', 'name')->ignore(\request()->post('user_id'))],
            'mobile' => ['required', 'numeric', Rule::unique('users', 'mobile')->ignore(\request()->post('user_id'))],
        ]);
        $user = User::find($request->user_id);
        $user->update([
            'email' => strtolower($request->email),
            'name' => ucwords($request->name),
            'mobile' => $request->mobile
        ]);
        return redirect()->route('admin.user.create', [$request->user_id])->with('notification', ['Profile Update Successfully', 'success']);
    }

    public function showWithdrawalHistory()
    {
        return Inertia::render('Admin/Withdrawal/Index');
    }

    public function withdrawalHistory(Request $request)
    {
        $where = [];
        if ($request->status) {
            $where[] = ['status', $request->status];
        }
        if ($request->filter) {
            $where[] = ['address', $request->filter];
        }
        $users = WithdrawalHistory::with(['user'])->where($where)->orderByDesc('id')->paginate(10);
        return response()->json($users);
    }

    public function filterWithdrawalHistory(Request $request)
    {
        $request->validate([
            'filter' => ['required', 'string']
        ]);
        $users = WithdrawalHistory::with(['user'])
            ->where('txn_id', 'like', '%' . $request->filter . '%')
            ->orWhere('address', 'like', '%' . $request->filter . '%')
            ->orWhere('id', 'like', '%' . $request->filter . '%')
            ->orderByDesc('id')->paginate(10);
        return response()->json($users);
    }

    public function statusFilterWithdrawalHistory(Request $request)
    {
        $request->validate([
            'status' => ['required', 'string', Rule::in(['pending', 'processing', 'success', 'failed'])]
        ]);
        $users = WithdrawalHistory::with(['user'])
            ->where('status', strtolower($request->status))
            ->orderByDesc('id')->paginate(10);
        return response()->json($users);
    }

    public function exportPdf(Request $request)
    {
        $where = [];
        $where[] = ['id', '>', '0'];
        $status = $request->get('status');
        $filter = $request->get('filter');
        if ($filter != 'null' && $filter != '') {
            $users = WithdrawalHistory::with(['user'])
                ->where('txn_id', 'like', '%' . $request->filter . '%')
                ->orWhere('address', 'like', '%' . $request->filter . '%')
                ->orWhere('id', 'like', '%' . $request->filter . '%')
                ->get();
        } else if ($status != 'null' && $status != '') {
            $users = WithdrawalHistory::with(['user'])
                ->where('status', $request->status)
                ->orderByDesc('id')->get();
        } else {
            $users = WithdrawalHistory::with(['user'])->where('status', 'pending')->orderByDesc('id')->limit(20)->get();
        }
        $pdf = PDF::loadView('withdrawal_history', ['users' => $users]);
        return $pdf->download('WithdrawalHistory-' . date('Y-m-d h:i:s') . '.pdf');
    }

//    public function processWithdrawal(Request $request)
//    {
//        $request->validate([
//            'id' => ['required', 'numeric', 'exists:withdrawal_histories,id']
//        ]);
//        $process = Artisan::queue('withdrawal:process', ['withdraw_history_id' => $request->id]);
//        if ($process) {
//            return response()->json(['message' => 'Withdrawal Successfully processed'], 200);
//        } else {
//            return response()->json(['message' => 'Withdrawal process failed'], 422);
//        }
//    }

    public function loginByUserId(User $user)
    {
        if (is_null($user)) {
            return back()->with('notification', ['User Not Exist ', 'danger']);
        }
        Auth::guard('web')->login($user);
        return \redirect()->route('dashboard');
    }

    public function check($userid)
    {
        $user = User::find($userid);
        $sponsorUserDet = User::where('id', $user->tree->sponsor_id)->first();
        UserLevelMethods::init($user)->eachParentV7(function ($sponsorUser, $level) use (&$user) {
            Log::info('--------------->');
            Log::info('level: ' . $level . '  user_id: ' . $sponsorUser->id);
            if (!$this->isUserActive($sponsorUser)) {
                Log::info(1);
                return;
            }

            $stakingBusiness = StakingBusiness::where('user_id', $sponsorUser->id)->first();

            if (is_null($stakingBusiness) && $level > 1) {
                Log::info(2);
                return;
            }
            $stakingBonusPlan = StakingTeamDistributionPlan::where('level', $level)->first();
            if (is_null($stakingBonusPlan)) {
                Log::info(3);
                return;
            }

            if ($stakingBonusPlan->team_business > $stakingBusiness->total_staking_business) {
                Log::info(4);

                Log::info($stakingBusiness->total_staking_business);
                Log::info($stakingBonusPlan);
                return;
            }
            Log::info(5);

        }, 7);

    }

    private function isUserActive(User $user): bool
    {
        $subscriptions = Subscription::where('user_id', $user->id)->get();
        return !is_null($subscriptions) ? true : false;
    }

}
