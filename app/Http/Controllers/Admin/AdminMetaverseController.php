<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPool;
use App\Models\MetaverseTokenPoolStat;
use App\Models\User;
use App\Models\UserMetaverseWallet;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AdminMetaverseController extends Controller
{
    public function Index()
    {
        $meta_pool = MetaverseTokenPool::first();
        return Inertia::render('Admin/Metaverse/User', [
            'meta_wallet' => $meta_pool
        ]);
    }

    public function getMetaUser()
    {
        $meta_user = UserMetaverseWallet::with('user')->orderByDesc('id')->simplePaginate(20);
        return response()->json($meta_user);
    }

    public function metaPool()
    {
        $meta_pool_stats = MetaverseTokenPoolStat::orderByDesc('id')->paginate(10);
//        return $meta_pool_stats;
        return Inertia::render('Admin/Metaverse/UpdateMetaPrice', [
            'meta_pool_stats' => $meta_pool_stats
        ]);
    }

    public function addUsdInMetaPool(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'gt:0', 'numeric']
        ]);

        $amount_in_usd = castDecimalString($request->amount, 10);
        $metaPool = metaverseTokenPoolInstance();
        $metaPool->increment('value_in_usd', $amount_in_usd);
        $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->available_token, 10);
        $metaPool->meta_token_price = $metaTokenPrice;
        $metaPool->save();

        $metaToken = MetaTokenPrice::first();
        $metaToken->price_in_usd = $metaTokenPrice;
        $metaToken->save();

        MetaverseTokenPoolStat::create([
            'released_token' => 0,
            'available_token' => 0,
            'burned_token' => 0,
            'value_in_usd' => $amount_in_usd,
            'meta_token_price' => $metaTokenPrice,
            'type' => MetaverseTokenPoolStat::TYPE['BOT_ADDED'],
            'status' => 'success'
        ]);

        return redirect()->route('admin.metaverse.buddy.bot')->with('notification', ['Metaverse price updated successfully', 'success']);

    }

    public function viewMetaEligibility(): \Inertia\Response
    {
        return Inertia::render('Admin/Metaverse/CheckMetaEligibility');
    }

    public function releaseMetaTokenToUser(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email']
        ]);
        $user = User::with('team', 'subscription')->whereEmail($request->email)->first();
        if (!$user) {
            return back()->with('notification', ['invalid user', 'danger']);
        }
        if ($user->team && $user->team->active_direct_left > 0 && $user->team->active_direct_right > 0) {
            $token = divDecimalStrings(castDecimalString(70, 10), metaTokenPrice());
            $amount_in_usd = castDecimalString(70, 10);

            $userMeta = userMetaverseWalletInstance($user);
            $userMeta->token = $token;
            $userMeta->value_in_usd = $amount_in_usd;
            $userMeta->save();
            $this->updateMetaPool($amount_in_usd, $token);
            return back()->with('notification', ['metaverse token released', 'success']);
        } else {
            return back()->with('notification', ['not eligible for metaverse', 'danger']);
        }
    }

    public function updateMetaPool($amount_in_usd, $token)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        $new_token = castDecimalString($token, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('released_token', $new_token);
            $metaPool->increment('available_token', $new_token);
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaPool->save();

            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->available_token, 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['META_RELEASED'],
                'status' => 'success'
            ]);
        }
    }
}
