<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\UserDepositFundJob;
use App\Models\User;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class AdminUserWalletController extends Controller
{
    public function addFund(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric', 'gte:1'],
            'user_id' => ['required', 'numeric', 'exists:users,id'],
        ]);
        $user = User::find($request->user_id);
        $usdDepositAmount = castDecimalString($request->amount, 8);
        $voucherTransaction = $user->voucherTransactions()->create([
            'txn_id' => Uuid::uuid4(),
            'amount' => $usdDepositAmount,
            'txn_time' => now(),
        ]);

        $depositTransaction = $voucherTransaction->depositTransaction()->create([
            'user_id' => $user->id,
            'address' => '',
            'crypto' => 'voucher_usd',
            'crypto_price' => castDecimalString('1', 8),
            'amount' => castDecimalString($usdDepositAmount, 8),
            'amount_in_usd' => castDecimalString($usdDepositAmount, 2),
            'txn_time' => $voucherTransaction->txn_time
        ]);

        if ($depositTransaction) {
            dispatch_sync(new UserDepositFundJob($depositTransaction));
        }
        return redirect()->route('admin.user.create', [$request->user_id])->with('notification', ['Fund added successfully!', 'success']);
    }
}
