<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function isUserExist(Request $request)
    {
        $request->validate([
            'sponsor_id' => ['required', 'numeric', 'exists:users,id']
        ]);

        return response()->json(['sponsor_name' => User::find($request->sponsor_id)->name]);
    }

    public function isUserExistByRefCode(Request $request)
    {
        $request->validate([
            'referral' => ['required', 'string', 'exists:users,ref_code']
        ]);

        return response()->json(['sponsor_name' => User::whereRefCode($request->referral)->first()->name]);
    }

    public function getUserApi(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'api_key' => 'required|string|exists:apikeys,api_key',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {

            return response()->json(['error' => 'Invalid Input', 'result' => $validator->messages()], 433);
        }

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth()->user();
            return response()->json([
                'status' => 'success',
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'username' => $user->ref_code,
            ]);
        } else {
            return response()->json(['error' => 'Invalid login details']);
        }
    }
}
