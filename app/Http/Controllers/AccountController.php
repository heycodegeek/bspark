<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class AccountController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return Inertia::render('Account/Profile', [
            'profile' => [
                'id' => $user->id,
                'full_name' => $user->name,
                'email' => $user->email,
                'sponsor_id' => auth()->user()->tree->sponsor ? auth()->user()->tree->sponsor->ref_code : null,
                'joined_on' => $user->created_at->format('Y-m-d'),
                'mobile' => $user->mobile

            ]

        ]);
    }

    public function showChangePassword()
    {
        return Inertia::render('Account/ChangePassword');
    }

    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'current_password' => [
                'required',
                function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) {
                        $fail('Your password was not updated, since the provided current password does not match.');
                    }
                }
            ],
            'new_password' => [
                'required', 'min:6', 'confirmed', 'different:current_password'
            ]
        ]);
        $user->fill([
            'password' => $request->new_password
        ])->save();
        return redirect()->route('dashboard')->with('notification', ['Password Changed Successfully', 'success']);

    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'full_name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'digits_between:10,15', 'unique:users,mobile'],
        ]);

        auth()->user()->update([
            'mobile' => $request->mobile,
            'name' => $request->full_name
        ]);
        return redirect()->back()->with('notification', ['Profile updated successfully', 'success']);
    }

    public function updateActiveProfile(Request $request)
    {
        $request->validate([
            'full_name' => ['required', 'string'],
            'mobile' => ['required', 'digits_between:10,15', 'unique:users,mobile'],
        ]);

        auth()->user()->update([
            'name' => $request->full_name,
            'name' => $request->full_name
        ]);
        return redirect()->back()->with('notification', ['Profile updated successfully', 'success']);
    }
}
