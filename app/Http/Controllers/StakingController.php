<?php

namespace App\Http\Controllers;

use App\Jobs\StakingBonusJob;
use App\Jobs\StakingBusinessJob;
use App\Models\Staking;
use App\Models\StakingBonus;
use App\Models\StakingDistributionStat;
use App\Models\StakingPool;
use App\Models\SubscriptionPool;
use App\Models\SubscriptionPoolStats;
use Illuminate\Http\Request;
use Inertia\Inertia;

class StakingController extends Controller
{
    public function index()
    {
        $eligible_for_staking = 1;
        $subs = auth()->user()->subscription ? 1 : 0;
        if ($subs > 0 && auth()->user()->id > 564) {
            $eligible_for_staking = 1;
        }
        return Inertia::render('Staking/Index', [
            'stakings' => auth()->user()->stakings()->withCasts(['created_at' => 'datetime:Y-m-d'])->get(),
            'wallet_balance' => auth()->user()->userCoinWallet->balance ?? 0,
            'eligible_for_staking' => $eligible_for_staking,
        ]);
    }

    public function bonusByStakingId($staking_id)
    {
        return Inertia::render('Staking/Bonus', [
            'staking_bonuses' => StakingBonus::where('staking_id', $staking_id)->orderBy('id', 'DESC')->get()
        ]);
    }

    public function createStaking(Request $request)
    {
        $request->validate([
            'amount_usd' => ['required', 'numeric', 'gte:10',
                function ($attribute, $value, $fail) {
                    if (($value % 10) != 0) {
                        $fail('Amount should be multiple of 10 ');
                    }
                }
            ]
        ]);


        $userCoinWallet = userCoinWalletInstance(auth()->user());

        if ($request->amount_usd > $userCoinWallet->balance) {
            return back()->with('notification', ['You dont have sufficient balance', 'danger']);
        }

        $amountUsd = castDecimalString($request->amount_usd, 2);
        $max_amount_limit = multipleDecimalStrings($amountUsd, '2.5');
        $staking = auth()->user()->stakings()->create([
            'user_usd_wallet_transaction_id' => 0,
            'amount_in_usd' => $amountUsd,
            'max_amount_limit_usd' => $max_amount_limit,
            'earned_so_far' => 0,
            'is_active' => 1,
            'end_date' => date('Y-m-d', strtotime('+2 years'))
        ]);

        $staking_pool = StakingPool::firstOrCreate(
            ['id' => 1],
            ['amount_in_usd' => 0, 'distributed_so_for' => 0]
        );
        $staking_pool->increment('amount_in_usd', $amountUsd);

        $userCoinWallet->decrement('balance', $amountUsd);

        StakingBusinessJob::dispatch(auth()->user(), $amountUsd)->delay(now());

        return redirect()->route('staking.index')->with('notification', ['Amount added in staking successfully']);

    }

    public function adminStakingList()
    {
        $todayStaking = Staking::whereDate('created_at', now()->format('Y-m-d'))->sum('amount_in_usd');
        $todayDistribution = StakingDistributionStat::whereDate('bonus_date', now()->format('Y-m-d'))->sum('amount_in_usd');
        return Inertia::render("Admin/Staking/Index", [
            'total_staking' => StakingPool::first()->amount_in_usd,
            'today_staking' => $todayStaking ?? 0,
            'today_distribution' => $todayDistribution ?? 0,
        ]);
    }

    public function adminGetStakingList()
    {
        $staking = Staking::with(['user'])->withCasts(['created_at' => 'datetime:Y-m-d'])->orderByDesc('id')->paginate(10);
        return response()->json($staking);
    }

    public function adminStakingBonus()
    {
    }

    public function adminGetStakingBonus()
    {
    }

    public function adminBonusDistribute(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate(
            ['bonus_percent' => 'required|numeric|gte:0.25']
        );

        $distributionStats = StakingDistributionStat::where('bonus_date', date("Y-m-d"))->get()->count();
        if ($distributionStats > 0) {
            return redirect()->route('admin.staking.list')->with('notification', ['Staking bonus already distributed for today. ', 'success']);
        }
        $bonusPercentDecimal = divDecimalStrings($request->bonus_percent, 100);
        dispatch(new StakingBonusJob($bonusPercentDecimal));
        return redirect()->route('admin.staking.list')->with('notification', ['Staking bonus distribution processed successfully. ', 'success']);

    }

    public function adminBonusPoolAdd(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate(
            ['amount' => 'required|numeric|gt:0']
        );

        $subscriptionPoolStats = SubscriptionPoolStats::whereDate('created_at', now()->format('Y-m-d'))->get()->count();
        if ($subscriptionPoolStats > 0) {
            return redirect()->route('admin.staking.list')->with('notification', ['Staking bonus already added in subscription pool. ', 'success']);
        }
        $amount = castDecimalString($request->amount, 4);

        $stats = SubscriptionPoolStats::create(
            [
                'user_id' => 0,
                'amount_in_usd' => $amount,
                'type' => 'Staking Bonus',
                'status' => 'Success'
            ]
        );
        $subscriptionPool = SubscriptionPool::first();
        $subscriptionPool->increment('amount_in_usd', $amount);
        $subscriptionPool->save();

        return redirect()->route('admin.staking.list')->with('notification', ['Staking bonus distribution processed successfully. ', 'success']);

    }

}
