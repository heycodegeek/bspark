<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InvoiceController extends Controller
{
    public function showInvoice(Request $request, $invoice_no)
    {
        $invoice = Invoice::where('invoice_no', $invoice_no)->first();
        return Inertia::render('Invoice/Create', [
            'invoice' => $invoice,
            'qr' => $this->generateQr($invoice->crypto, $invoice->deposit_address, $invoice->crypto_amount),
            'payment_link' => $this->getPaymentLink($invoice->crypto, $invoice->deposit_address, $invoice->crypto_amount)
        ]);
    }

    private function generateQr(string $crypto, string $address, string $cryptoAmount)
    {
        if ($crypto == 'btc') {
            return base64_encode(QrCode::size(250)->generate('bitcoin:' . $address));
        } else if ($crypto == 'trx') {
            return base64_encode(QrCode::size(250)->generate($address));
        } else {
            return base64_encode(QrCode::size(250)->generate(strtolower($address) . '?value=' . $cryptoAmount));
        }
    }

    private function getPaymentLink(string $crypto, string $address, string $cryptoAmount)
    {
        if ($crypto == 'btc') {
            return 'bitcoin:' . $address;
        } else {
            return $address . '?value=' . $cryptoAmount;
        }
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'amount' => ['required', 'numeric', 'gt:0'],
            'currency' => ['required', 'string', Rule::in(
                array_keys(getDepositCoins())
            )]
        ]);

        $invoice = auth()->user()->invoices()->create([
            'amount_in_usd' => castDecimalString($request->amount, 2),
            'coin' => strtolower($request->currency),
            'status' => 'pending',
        ]);

        if (!$invoice) {
            throw ValidationException::withMessages([
                'invoice' => 'Error while creating invoice',
            ]);
        }
        return redirect()->route('add.fund.form', $invoice->id);

    }
}
