<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateSmartMetaFluctuationJob;
use App\Jobs\UpdateTeamStatJob;
use App\Jobs\UpdateUserMetaverseWalletJob;
use App\Models\AdminFundWallet;
use App\Models\CoinPlan;
use App\Models\CryptApiWallet;
use App\Models\DelegatorPlan;
use App\Models\DelegatorSubscription;
use App\Models\Invoice;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPool;
use App\Models\MetaverseTokenPoolStat;
use App\Models\Subscription;
use App\Models\SubscriptionPool;
use App\Models\Tree;
use App\Models\User;
use App\Models\UserBinaryInfo;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PurchaseController extends Controller
{

    public function addFundInWallet()
    {
        return Inertia::render('Deposit/AddFundInWallet', [
            'currencies' => getDepositCoins()
        ]);
    }

    public function showAddFundForm(Request $request, Invoice $invoice)
    {
        $validator = Validator::make(['status' => $invoice->status], [
            'status' => ['required', 'string', Rule::in(['pending'])]
        ]);
        if ($validator->fails()) {
            abort(404);
        }
        $cryptApiWallet = auth()->user()->cryptApiWallets()->where('invoice_id', $invoice->id)->first();
        if (is_null($cryptApiWallet)) {
            $cryptApiWallet = $this->generateNewAddress($invoice->coin, $invoice->id);
            if (is_null($cryptApiWallet)) {
                return redirect()->back();
            }
        }

        if (in_array($invoice->coin, ['trc20_usdt', 'erc20_usdt', 'bep20_usdt'])) {

            $extraPercentDecimal = '0.02';
            $depositAmountInCrypto = addDecimalStrings($invoice->amount_in_usd, multipleDecimalStrings($invoice->amount_in_usd, $extraPercentDecimal, 2), 2);
//            $depositAmountInCrypto = $invoice->amount_in_usd;
        } else {
            $extraPercentDecimal = '0.02';
            $usdAmount = addDecimalStrings($invoice->amount_in_usd, multipleDecimalStrings($invoice->amount_in_usd, $extraPercentDecimal, 2), 2);
            $depositAmountInCrypto = divDecimalStrings($usdAmount, getDepositCoinPrice($invoice->coin), 6);
        }

        return Inertia::render('PaymentForm', [
            'address' => $cryptApiWallet->address_in,
            'qr' => base64_encode(QrCode::size(250)->generate($cryptApiWallet->address_in)),
            'currency' => $invoice->coin == "trc20_usdt" ? 'USDT' : $invoice->coin,
            'amount' => $depositAmountInCrypto,
            'explorer' => config('explorers.' . $invoice->coin . ".address") . $cryptApiWallet->address_in
        ]);
    }

    private function generateNewAddress($coinName, $invoiceId): ?CryptApiWallet
    {
        $pwtFundWallet = AdminFundWallet::where('coin', $coinName)->first();
        if (is_null($pwtFundWallet)) {
            return null;
        }
        if ($pwtFundWallet->address) {
            $fundWallet = $pwtFundWallet->address;
        } else {
            return null;
        }
        $callbackUrl = route('gateway.callback') . "/";
        $parameters = ['coin' => strtolower($coinName), 'invoice_id' => $invoiceId];
        $reqParameters = http_build_query($parameters);
        $callbackUrlComplete = "{$callbackUrl}?{$reqParameters}";
        $pgresponse = Http::post(config('app.pg_url') . $coinName, [
            'address_out' => $fundWallet,
            'callback_url' => $callbackUrlComplete,
        ]);
        if ($pgresponse["status"] == "success") {
            return auth()->user()->cryptApiWallets()->create([
                'invoice_id' => $invoiceId,
                'crypto' => strtolower($coinName),
                'callback_url' => $callbackUrlComplete,
                'address_out' => $fundWallet,
                'address_in' => $pgresponse["address_in"]
            ]);
        }
        return null;
    }


    public function showPricing()
    {
        $coinPlans = CoinPlan::with(['levelPlans', 'binaryPlan'])->get();
        $delegatorSubscription = DelegatorSubscription::where('user_id', auth()->user()->id)->orderByDesc('id')->first();
        return Inertia::render('Package/Pricing', [
            'plans' => $coinPlans,
            'delegator_plan' => DelegatorPlan::first(),
            'subscription' => auth()->user()->subscription ? 1 : 0,
            'delegator_subscription' => $delegatorSubscription
        ]);
    }


    public function buy(CoinPlan $coinPlan)
    {
        return Inertia::render('Package/Buy', [
            'plan' => $coinPlan,
            'available_coin_balance' => userCoinWalletInstance(auth()->user())->balance,
            'withdrawable_coin_balance' => withdrawableWalletInstance(auth()->user())->balance,
            'subscription' => auth()->user()->subscription ? 1 : 0,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function buyPackage(Request $request)
    {

        $request->validate([
            'package_id' => ['required', 'numeric', 'exists:coin_plans,id'],
//            'amount_usd' => ['required', 'numeric', 'gt:0',
//                function ($attribute, $value, $fail) {
//                    $coinPlan = CoinPlan::find(\request()->post('package_id'));
//                    if ($value < addDecimalStrings($coinPlan->start_price_usd, '17')) {
//                        $fail('Amount can not be less than ' . addDecimalStrings($coinPlan->start_price_usd, '17'));
//                    }
//                }
//            ]
        ]);
        $coinPlan = CoinPlan::where('id', $request->package_id)->first();

        $userCoinWallet = userCoinWalletInstance(auth()->user());
        $amountUsd = castDecimalString($coinPlan->start_price_usd, 2);
        if (addDecimalStrings($amountUsd, '17', 2) > castDecimalString($userCoinWallet->balance, 2)) {
            return back()->with('notification', ['You dont have sufficient balance', 'danger']);
        }

        $currentSubscription = auth()->user()->subscription;
        if (is_null($currentSubscription)) {
            $coinPlan = CoinPlan::first();
            $subscription = auth()->user()->subscription()->create([
                'coin_plan_id' => $coinPlan->id,
                'end_date' => now()->addMonths($request->stake_duration)->format('Y-m-d'),
                'stake_duration' => $request->stake_duration,
                'amount_in_usd' => $amountUsd,
                'stake_tokens' => 0,
                'in_compounding' => true
            ]);
            $userBinaryInfo = UserBinaryInfo::firstOrCreate(
                ['user_id' => auth()->user()->id],
                ['binary_capping_amount' => multipleDecimalStrings($subscription->amount_in_usd, '1000', 2), 'binary_used_weekly' => castDecimalString('0', 2)]
            );

            $userCoinWallet->decrement('balance', castDecimalString('167', 2));

            auth()->user()->update([
                'active_at' => now()
            ]);

            $this->createSubscriptionHistory(auth()->user(), $subscription->stake_duration, $amountUsd, 0, 1);
//            $this->createSubscriptionPoolStat(auth()->user(), $amountUsd);
//            $this->addSubscriptionPool($amountUsd);


            $userTree = Tree::where('user_id', auth()->user()->id)->first();
            if ($userTree) {
                $sponsorUser = User::find($userTree->sponsor_id);
                if ($sponsorUser) {
                    $sponsorTeam = $sponsorUser->team;
                    $sponsorTeam->increment('active_direct');
                    if ($userTree->position == 'LEFT') {
                        $sponsorTeam->increment('active_direct_left');
                    } else {
                        $sponsorTeam->increment('active_direct_right');
                    }

                    $sponsorUserSubs = Subscription::where('user_id', $sponsorUser->id)->count();
                    if ($sponsorUserSubs > 0) {
                        UpdateUserMetaverseWalletJob::dispatch($sponsorUser, 20, MetaverseTokenPoolStat::TYPE['DIRECT_ADDED'])->delay(now()->addSecond());
                    }

                    if ($sponsorUserSubs > 0 && $sponsorTeam->active_direct == 2) {
                        $fdate = $sponsorUser->active_at;
                        $tdate = $sponsorTeam->updated_at;
                        $datetime1 = new DateTime($fdate);
                        $datetime2 = new DateTime($tdate);
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');
                        if ($days <= 21) {
//                            $token = divDecimalStrings(castDecimalString(70, 10), metaTokenPrice());
//                            $amount_in_usd = castDecimalString(70, 10);

                            UpdateUserMetaverseWalletJob::dispatch($sponsorUser, 70, MetaverseTokenPoolStat::TYPE['META_RELEASED'])->delay(now()->addSecond());

//                            $userMeta = userMetaverseWalletInstance($sponsorUser);
//                            $userMeta->token = $token;
//                            $userMeta->value_in_usd = $amount_in_usd;
//                            $userMeta->save();

//                            UserMetaverseWallet::create([
//                                'user_id' => $sponsorUser->id,
//                                'token' => $token,
//                                'value_in_usd' => $amount_in_usd
//                            ]);

//                            $this->updateMetaPool($amount_in_usd, $token);
                        }
                    }

                }
            }


            UpdateTeamStatJob::dispatch(auth()->user(), true);

//            $liquidityAmount = multipleDecimalStrings($amountUsd, '0.10');
//            $fromUser = User::where('id', auth()->user()->id)->first();
//            UpdateSmartMetaFluctuationJob::dispatch($liquidityAmount, $fromUser, 'user activated');

            return redirect()->route('dashboard')->with('notification', ['Package purchased successfully']);

        }

    }

    private function createSubscriptionHistory(User $user, int $stakeDuration, string $amountInUsd, string $stakeTokens, string $tokenPrice)
    {
        return $user->subscriptionHistories()->create([
            'stake_duration' => $stakeDuration,
            'amount_in_usd' => $amountInUsd,
            'stake_tokens' => $stakeTokens,
            'token_price' => $tokenPrice
        ]);
    }

    private function createSubscriptionPoolStat(User $user, string $amountInUsd)
    {
        return $user->subscritionPoolStats()->create([
            'user_id' => $user,
            'amount_in_usd' => multipleDecimalStrings($amountInUsd, '0.10'),
            'status' => 'Success'
        ]);
    }

    private function addSubscriptionPool(string $amountInUsd)
    {
        $amount = multipleDecimalStrings($amountInUsd, '0.10');
        $subscriptionPool = SubscriptionPool::take(1)->first();
        $subscriptionPool->increment('amount_in_usd', $amount);
    }

    public function updateMetaPool($amount_in_usd, $token)
    {
        $amount_in_usd = castDecimalString($amount_in_usd, 10);
        $new_token = castDecimalString($token, 10);
        if ($amount_in_usd > castDecimalString(0, 10)) {
            $metaPool = metaverseTokenPoolInstance();
            $metaPool->increment('released_token', $new_token);
            $metaPool->increment('available_token', $new_token);
            $metaPool->increment('value_in_usd', $amount_in_usd);
            $metaPool->save();

            $metaTokenPrice = divDecimalStrings($metaPool->value_in_usd, $metaPool->released_token, 10);
            $metaPool->meta_token_price = $metaTokenPrice;
            $metaPool->save();

            $metaToken = MetaTokenPrice::first();
            $metaToken->price_in_usd = $metaTokenPrice;
            $metaToken->save();

            MetaverseTokenPoolStat::create([
                'released_token' => 0,
                'available_token' => 0,
                'burned_token' => 0,
                'value_in_usd' => $amount_in_usd,
                'meta_token_price' => $metaTokenPrice,
                'type' => MetaverseTokenPoolStat::TYPE['META_RELEASED'],
                'status' => 'success'
            ]);
        }
    }

    public function renewPackage(Request $request)
    {

        $request->validate([
            'package_id' => ['required', 'numeric', 'exists:coin_plans,id'],
//            'amount_usd' => ['required', 'numeric', 'gt:0',
//                function ($attribute, $value, $fail) {
//                    $coinPlan = CoinPlan::find(\request()->post('package_id'));
//                    if ($value < $coinPlan->start_price_usd) {
//                        $fail('Amount can not be less than ' . $coinPlan->start_price_usd);
//                    }
//                }
//            ]
        ]);
        $coinPlan = CoinPlan::first();
        $userCoinWallet = userCoinWalletInstance(auth()->user());
        $amountUsd = castDecimalString($coinPlan->start_price_usd, 2);
        if (addDecimalStrings($amountUsd, '17', 2) > castDecimalString($userCoinWallet->balance, 2)) {
            return back()->with('notification', ['You dont have sufficient balance', 'danger']);
        }

//        $amountUsd = castDecimalString($request->amount_usd, 2);
        $currentSubscription = auth()->user()->subscription;
        if (!is_null($currentSubscription)) {
            $coinPlan = CoinPlan::first();
            $subscription = auth()->user()->subscription()->create([
                'coin_plan_id' => $coinPlan->id,
                'end_date' => now()->addMonths($request->stake_duration)->format('Y-m-d'),
                'stake_duration' => $request->stake_duration,
                'amount_in_usd' => $amountUsd,
                'stake_tokens' => 0,
                'in_compounding' => true
            ]);
            $userBinaryInfo = UserBinaryInfo::firstOrCreate(
                ['user_id' => auth()->user()->id],
                ['binary_capping_amount' => castDecimalString('1000.00', 2), 'binary_used_weekly' => castDecimalString('0', 2)]
            );

            $userCoinWallet->decrement('balance', $amountUsd);
            $this->createSubscriptionHistory(auth()->user(), $subscription->stake_duration, $amountUsd, 0, 1);
            return redirect()->route('dashboard')->with('notification', ['Package Renewed successfully']);

        }
    }

    public function buyDelegatorPlan(Request $request)
    {

        $request->validate([
            'delegator_plan' => ['required', 'numeric', 'exists:delegator_plans,id'],
        ]);

        $userCoinWallet = userCoinWalletInstance(auth()->user());
        $delegatorPlan = DelegatorPlan::first();
        if ($delegatorPlan->start_price_usd > $userCoinWallet->balance) {
            return back()->with('notification', ['You dont have sufficient balance', 'danger']);
        }

        $currentSubscription = auth()->user()->subscription;
        if (is_null($currentSubscription)) {
            return back()->with('notification', ['Please activate $150 Package first.', 'danger']);
        }

        $subscription = auth()->user()->delegatorSubscriptions()->create([
            'delegator_plan_id' => $delegatorPlan->id,
            'end_date' => now()->addMonths(24)->format('Y-m-d'),
            'bonus_start_date' => now()->addDays(10)->format('Y-m-d'),
            'next_bonus_date' => now()->addDays(10)->format('Y-m-d'),
            'stake_duration' => '24',
            'amount_in_usd' => $delegatorPlan->start_price_usd,
            'max_amount_limit' => multipleDecimalStrings(castDecimalString($delegatorPlan->start_price_usd, 2), 5),
            'earned_so_far' => '0.00',
            'is_active' => '1'
        ]);

        $userCoinWallet->decrement('balance', castDecimalString($delegatorPlan->start_price_usd, 2));
        return redirect()->route('dashboard')->with('notification', ['Delegator Package activated successfully!']);

    }


}
