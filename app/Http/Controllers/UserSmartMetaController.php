<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Jobs\SmartMetaSwapJob;
use App\Models\SmartMetaConvertHistory;
use App\Models\SmartMetaSupply;
use App\Models\User;
use App\Rules\MinimumTokenWithdrawRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Inertia\Inertia;

class UserSmartMetaController extends Controller
{

    public function showSmartMetaTxnHistory()
    {
        return Inertia::render('History/SmartMetaTxnHistory', [
            'user_smart_meta' => userSmartMetaInstance(auth()->user()),
            'smart_meta_price' => smartMetaPrice()
        ]);
    }

    public function getUserSmartMetaTxnHistory()
    {
        $smartMetaHistory = SmartMetaConvertHistory::where('user_id', auth()->user()->id)->withCasts(['created_at' => 'datetime:Y-m-d'])->orderByDesc('id')->simplePaginate(10);
        return response()->json($smartMetaHistory);
    }

    public function showSmartMetaSwap()
    {
        return Inertia::render('SmartMeta/SwapForm', [
            'user_meta_token_balance' => userSmartMetaInstance(\auth()->user())->balance,
            'min_withdrawable_token' => getMinimumSwapCoin(),
            'fees_percent' => castDecimalString('10', 2),
            'fees_percent_decimal' => castDecimalString('0.10', 4),
            'meta_token_price' => smartMetaPrice()
        ]);
    }

    public function swapSmartMeta(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric', 'gt:0', new MinimumTokenWithdrawRule(getMinimumSwapCoin()),
                function ($attribute, $value, $fail) {
                    if (userSmartMetaInstance(\auth()->user())->balance < $value) {
                        $fail('You dont have sufficient balance.');
                    }
                }
            ]
        ]);

        $user = $request->user();
        $amount = $request->amount;
        SmartMetaSwapJob::dispatch($user, $amount)->delay(now()->addMinutes(1));

        return redirect()->route('smart.meta.index')->with('notification', ['smart meta swapping in progress', 'success']);
//        return redirect()->back()->with('notification', ['smart meta swapping in progress', 'success']);
    }

    public function history()
    {
        return Inertia::render('SmartMeta/History');
    }

    public function getHistory()
    {
        $history = SmartMetaConvertHistory::where('user_id', auth()->user()->id)->where('particular', 'user swapped the smart meta!')->simplePaginate(10);
        return response()->json($history);
    }

}
