<?php

namespace App\Http\Controllers;

use App\Models\StakeCoinPlan;
use App\Models\StakeSubscription;
use App\Models\Tree;
use App\Models\User;
use App\Models\UserBinaryInfo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class StakePurchaseController extends Controller
{
    public function showStakePricing()
    {
        $coinPlans = StakeCoinPlan::with(['stakeLevelPlans', 'stakeBinaryPlan'])->get();
        return Inertia::render('Package/StakePricing', [
            'plans' => $coinPlans
        ]);
    }

    public function staking(StakeCoinPlan $stake_coin_plan)
    {

        return Inertia::render('Package/BuyStaking', [
            'plan' => $stake_coin_plan,
            'available_coin_balance' => userCoinWalletInstance(auth()->user())->balance,
            'withdrawable_coin_balance' => withdrawableWalletInstance(auth()->user())->balance
        ]);
    }

    public function buyStaking(Request $request)
    {

        $request->validate([
            'package_id' => ['required', 'numeric', 'exists:stake_coin_plans,id'],

            'stake_duration' => ['required', Rule::in(['12', '24', '36', '48', '60']),
                function ($attribute, $value, $fail) {
                    $subscription = auth()->user()->stakeSubscription;
                    if ($subscription && $subscription->stake_duration > $value) {
                        $fail('Stake duration can not be less than ' . $subscription->stake_duration . ' Months');
                    }
                }
            ],
            'amount_usd' => ['required', 'numeric', 'gt:0',
                function ($attribute, $value, $fail) {
                    $coinPlan = StakeCoinPlan::find(\request()->post('package_id'));
                    $subscriptionHistory = auth()->user()->stakeSubscriptionHistories()->orderByDesc('id')->first();
                    if ($subscriptionHistory) {
                        $nextTopUpAmount = multipleDecimalStrings($subscriptionHistory->amount_in_usd, '2', 2);
                        if ($value < $nextTopUpAmount) {
                            $fail("Amount can not be less than $" . $nextTopUpAmount);
                        }
                    }
                    if ($value < $coinPlan->start_price_usd) {
                        $fail('Amount can not be less than ' . $coinPlan->start_price_usd);
                    }
                    if ($value > $coinPlan->end_price_usd) {
                        $fail('Amount can not be greater than ' . $coinPlan->end_price_usd);
                    }
                }
            ]
        ]);


        $tokenPrice = tokenPrice();
        $requiredCoins = numberOfTokens($request->amount_usd, $tokenPrice);
        $halfRequiredCoins = multipleDecimalStrings($requiredCoins, '0.5', 8);
        $userCoinWallet = userCoinWalletInstance(auth()->user());
        $withdrawableWallet = withdrawableWalletInstance(auth()->user());
//        if ($request->amount_usd < '100') {
//            if ($requiredCoins > getAvailableCoinBalance(auth()->user())) {
//                return back()->with('notification', ['You dont have sufficient balance. Locked Coin can not be used below 100 USD.', 'danger']);
//            }
//        } else {

        if ($requiredCoins > addDecimalStrings($userCoinWallet->balance, $withdrawableWallet->balance, 8)) {
            return back()->with('notification', ['You dont have sufficient balance', 'danger']);
        }
        if ($halfRequiredCoins > $userCoinWallet->balance) {
            return back()->with('notification', ['You can use max 50% from Utility wallet', 'danger']);
        }
//        }


        $amountUsd = castDecimalString($request->amount_usd, 2);
        $currentSubscription = auth()->user()->stakeSubscription;
        if (is_null($currentSubscription)) {
            $coinPlan = $this->getCoinPlanByAmount($amountUsd);

            if (is_null($coinPlan)) {
                return back()->with('notificaton', ['Unable to process. Please try again later.']);
            }


            $subscription = auth()->user()->stakeSubscription()->create([
                'stake_coin_plan_id' => $coinPlan->id,
                'end_date' => now()->addMonths($request->stake_duration)->format('Y-m-d'),
                'stake_duration' => $request->stake_duration,
                'amount_in_usd' => $amountUsd,
                'stake_tokens' => $requiredCoins,
            ]);

            $userBinaryInfo = UserBinaryInfo::firstOrCreate(
                ['user_id' => auth()->user()->id],
                ['binary_capping_amount' => multipleDecimalStrings($subscription->amount_in_usd, '2', 2), 'binary_used_weekly' => castDecimalString('0', 2)]
            );

            if ($withdrawableWallet->balance > $halfRequiredCoins) {
                $withdrawableWalletAmount = $halfRequiredCoins;
            } else {
                $withdrawableWalletAmount = $withdrawableWallet->balance;
            }
            $coinWalletAmount = subDecimalStrings($requiredCoins, $withdrawableWalletAmount, 8);
            $withdrawableWallet->decrement('balance', $withdrawableWalletAmount);
            $userCoinWallet->decrement('balance', $coinWalletAmount);
            if ($request->amount_usd >= '100') {
                $lockedTokens = $userCoinWallet->balance_locked;
                if ($lockedTokens > '0') {
                    if ($lockedTokens >= $requiredCoins) {
                        $userCoinWallet->decrement('balance_locked', $requiredCoins);
                    } else {
                        $userCoinWallet->decrement('balance_locked', $lockedTokens);
                    }
                }
            }


            auth()->user()->update([
                'active_at' => now()
            ]);

            $this->createStakeSubscriptionHistory(auth()->user(), $subscription->stake_duration, $amountUsd, $requiredCoins, $tokenPrice);

            $userTree = Tree::where('user_id', auth()->user()->id)->first();
            if ($userTree) {
                $sponsorUser = User::find($userTree->sponsor_id);
                if ($sponsorUser) {
                    $sponsorTeam = $sponsorUser->team;
                    $sponsorTeam->increment('active_direct');
                    if ($userTree->position == 'LEFT') {
                        $sponsorTeam->increment('active_direct_left');
                    } else {
                        $sponsorTeam->increment('active_direct_right');
                    }

                }
            }

//            UpdateTeamStatJob::dispatch(auth()->user(), true);
            return redirect()->route('dashboard')->with('notification', ['Package purchased successfully']);

        } else {
            if ($currentSubscription->stake_duration > $request->stake_duration) {
                return redirect()->back()->with('notification', ['Stake Duration can not be less than previous duration', 'danger']);
            }
            $currentSubscription->increment('amount_in_usd', $amountUsd);
            $currentSubscription->increment('stake_tokens', $requiredCoins);
            $currentSubscription->update([
                'end_date' => now()->addMonths($request->stake_duration)->format('Y-m-d'),
                'stake_duration' => $request->stake_duration,
            ]);

            if ($withdrawableWallet->balance > $halfRequiredCoins) {
                $withdrawableWalletAmount = $halfRequiredCoins;
            } else {
                $withdrawableWalletAmount = $withdrawableWallet->balance;
            }
            $coinWalletAmount = subDecimalStrings($requiredCoins, $withdrawableWalletAmount, 8);
            $withdrawableWallet->decrement('balance', $withdrawableWalletAmount);
            $userCoinWallet->decrement('balance', $coinWalletAmount);
            if ($request->amount_usd >= '100') {
                $lockedTokens = $userCoinWallet->balance_locked;
                if ($lockedTokens > '0') {
                    if ($lockedTokens >= $requiredCoins) {
                        $userCoinWallet->decrement('balance_locked', $requiredCoins);
                    } else {
                        $userCoinWallet->decrement('balance_locked', $lockedTokens);
                    }
                }
            }


            $this->updateSubscriptionCoinPlan(auth()->user()->stakeSubscription->refresh());

            $userBinaryInfo = auth()->user()->userBinaryInfo;
            $this->createStakeSubscriptionHistory(auth()->user(), $currentSubscription->stake_duration, $amountUsd, $requiredCoins, $tokenPrice);

            $userBinaryInfo->update([
                'binary_capping_amount' => multipleDecimalStrings(auth()->user()->stakeSubscription->refresh()->amount_in_usd, '2', 2)
            ]);
            return redirect()->route('dashboard')->with('notification', ['Package purchased successfully']);
        }
    }

    private function getCoinPlanByAmount(string $amountUsd)
    {
        return StakeCoinPlan::where('start_price_usd', '<=', $amountUsd)->orderByDesc('start_price_usd')->first();
    }

    private function createStakeSubscriptionHistory(User $user, int $stakeDuration, string $amountInUsd, string $stakeTokens, string $tokenPrice)
    {
        return $user->stakeSubscriptionHistories()->create([
            'stake_duration' => $stakeDuration,
            'amount_in_usd' => $amountInUsd,
            'stake_tokens' => $stakeTokens,
            'token_price' => $tokenPrice
        ]);
    }

    private function updateSubscriptionCoinPlan(StakeSubscription $userSubscription)
    {
        $newCoinPlan = $this->getCoinPlanByAmount($userSubscription->amount_in_usd);
        if ($newCoinPlan) {
            $userSubscription->update([
                'stake_coin_plan_id' => $newCoinPlan->id
            ]);
        }
    }
}
