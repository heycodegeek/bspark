<?php

namespace App\Listeners;

use App\Events\Registered;
use App\Jobs\PlaceUserIntoTree;

class PlaceUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        PlaceUserIntoTree::dispatch($event->user)->delay(now()->addSecond());
    }
}
