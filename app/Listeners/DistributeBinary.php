<?php

namespace App\Listeners;

use App\Events\SubscriptionHistoryCreated;
use App\Jobs\CreateFirstBinaryBusinessJob;

class DistributeBinary
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SubscriptionHistoryCreated $event
     * @return void
     */
    public function handle(SubscriptionHistoryCreated $event)
    {
        $subscriptionHistory = $event->subscriptionHistory;
        CreateFirstBinaryBusinessJob::dispatch($subscriptionHistory)->delay(now()->addSecond());
    }
}
