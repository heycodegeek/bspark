<?php

namespace App\Listeners;

use App\Events\BinaryPayoutCreated;
use App\Mohiqssh\UserLevelMethods;

class CreateMatchingBonus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BinaryPayoutCreated $event
     * @return void
     */
    public function handle(BinaryPayoutCreated $event)
    {
        $binaryPayout = $event->binaryPayout;
        $user = $binaryPayout->user;

        UserLevelMethods::init($user)->eachParentV1(function ($sponsorUser, $level) use (&$binaryPayout) {
            if ($sponsorUser->userRank()->exists()) {
                $userRankId = $sponsorUser->userRank->rank_id;
                $rankPercentArray = config('matching-bonus.' . (string)$userRankId);
                $i = 1;
                foreach ($rankPercentArray as $rankPercent) {
                    if ($level == $i) {
                        $rankPercentDecimal = divDecimalStrings((string)$rankPercent, '100', 2);
                        $incomeUsd = multipleDecimalStrings($binaryPayout->income_amount_usd, $rankPercentDecimal, 2);
                        $incomeTokens = numberOfTokens($incomeUsd, $binaryPayout->token_price);
                        $sponsorUser->matchingBonuses()->create([
                            'binary_payout_id' => $binaryPayout->id,
                            'income_usd' => $incomeUsd,
                            'income_tokens' => $incomeTokens,
                            'rank' => $userRankId,
                            'level' => $level,
                            'token_price' => $binaryPayout->token_price
                        ]);

                        $userCoinWallet = userCoinWalletInstance($sponsorUser);
                        $withdrawableWallet = withdrawableWalletInstance($sponsorUser);
                        $halfIncome = multipleDecimalStrings($incomeTokens, '0.5', 8);
                        $userCoinWallet->increment('balance', $halfIncome);
                        $withdrawableWallet->increment('balance', $halfIncome);

                        $userIncomeStat = userIncomeStatInstance($sponsorUser);
                        $userIncomeStat->increment('matching_bonus', $incomeTokens);
                        $userIncomeStat->increment('total_bonus', $incomeTokens);
                    }
                    $i++;

                }
            }

        }, 10);

    }
}
