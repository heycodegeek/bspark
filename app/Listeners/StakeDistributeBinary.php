<?php

namespace App\Listeners;

use App\Events\StakeSubscriptionHistoryCreated;
use App\Jobs\CreateStakeFirstBinaryBusinessJob;

class StakeDistributeBinary
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param StakeSubscriptionHistoryCreated $event
     * @return void
     */
    public function handle(StakeSubscriptionHistoryCreated $event)
    {
        $subscriptionHistory = $event->subscriptionHistory;
        CreateStakeFirstBinaryBusinessJob::dispatch($subscriptionHistory)->delay(now()->addSecond());
    }
}
