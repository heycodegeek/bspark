<?php

namespace App\Listeners;

use App\Events\Verified;
use App\Jobs\PostRegisterationJob;
use App\Models\User;
use App\Models\UserSponsor;
use App\Mohiqssh\AirdropSignUpMethod;
use Illuminate\Support\Facades\Log;

class CreateAirdrop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Verified $event
     * @return void
     */
    public function handle(Verified $event)
    {
        $user = $event->user;
        $this->createSignUpAirdrop($user);
    }

    private function createSignUpAirdrop(User $user)
    {
        AirdropSignUpMethod::init($user)->create();
    }
}
