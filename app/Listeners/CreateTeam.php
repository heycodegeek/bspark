<?php

namespace App\Listeners;

use App\Events\Registered;

class CreateTeam
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $event->user->tree()->create([
            'sponsor_id' => $event->sponsorUser->id,
            'position' => $event->position,
            'parent_id' => null
        ]);
    }
}
