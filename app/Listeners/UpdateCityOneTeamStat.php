<?php

namespace App\Listeners;

use App\Events\Registered;
use App\Mohiqssh\TeamStat;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateCityOneTeamStat implements ShouldQueue
{
    use InteractsWithQueue;

    public $timeout = 600;

    public $delay = 5;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        TeamStat::init($event->user)->updateStatToUpline();
    }
}
