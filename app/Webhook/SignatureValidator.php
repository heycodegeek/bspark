<?php


namespace App\Webhook;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\WebhookClient\Exceptions\WebhookFailed;
use Spatie\WebhookClient\WebhookConfig;

class SignatureValidator implements \Spatie\WebhookClient\SignatureValidator\SignatureValidator
{

    public function isValid(Request $request, WebhookConfig $config): bool
    {
        return true;
        $signature = $request->header($config->signatureHeaderName);
        if (!$signature) {
            return false;
        }
        $signingSecret = $config->signingSecret;

        if (empty($signingSecret)) {
            throw WebhookFailed::signingSecretNotSet();
        }

        $computedSignature = hash_hmac('sha256', $request->getContent(), $signingSecret);


        return hash_equals($signature, $computedSignature);
    }
}
