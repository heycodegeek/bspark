<?php


namespace App\Webhook;


use App\Events\WebhookReceived;
use Spatie\WebhookClient\ProcessWebhookJob;

class WebhookHandler extends ProcessWebhookJob
{
    public function handle()
    {
        event(new WebhookReceived($this->webhookCall));
    }

}
