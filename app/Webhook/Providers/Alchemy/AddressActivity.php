<?php


namespace App\Webhook\Providers\Alchemy;


use App\Models\DepositWallet;
use App\Models\User;
use App\Models\UserWallet;

class AddressActivity
{

    private array $activity;

    public function __construct(array $activity)
    {
        $this->activity = $activity;
    }

    public function getFromAddress(): string
    {
        return $this->activity['fromAddress'];
    }

    public function getBlockNumberHex(): string
    {
        return $this->activity['blockNum'];
    }

    public function getBlockNumberDec()
    {
        return hexdec($this->activity['blockNum']);
    }

    public function getHash(): string
    {
        return strtolower($this->activity['hash']);
    }

    public function getCategory(): string
    {
        return $this->activity['category'];
    }

    public function getValue(): string
    {
        return castDecimalString((string)$this->activity['value'], 8);
    }

    public function getAsset(): string
    {
        return $this->activity['asset'];
    }

    public function getRawContract(): array
    {
        return $this->activity['rawContract'];
    }

    public function getUser(string $address): ?User
    {
        $depositWallet = DepositWallet::where('address', strtolower($address))->first();
        if ($depositWallet) {
            return $depositWallet->user;
        }
        return null;
    }

    public function getToAddress(): string
    {
        return strtolower($this->activity['toAddress']);
    }


}

//{"id":7,"name":"default","payload":{"app":"Backend","network":"KOVAN","webhookType":"ADDRESS_ACTIVITY","timestamp":null,"activity":[{"fromAddress":"0x41e09d3294c376213b8720901650da938958a3b6","toAddress":"0xb8702eae83c3ea2891fdd7aa44ef889b4c84edea","blockNum":"0x1637702","hash":"0xd8b1bc79981f98d81cbfb133e3ea55a0998da833e415262c23a61dec9c3a04c4","category":"external","value":0.1000000000000000055511151231257827021181583404541015625,"asset":"ETH","rawContract":{"rawValue":"0x16345785d8a0000","address":null,"decimals":18}}]},"exception":null,"created_at":"2021-02-03T19:32:45.000000Z","updated_at":"2021-02-03T19:32:45.000000Z"}
//array (
//    'app' => 'Backend',
//    'network' => 'KOVAN',
//    'webhookType' => 'ADDRESS_ACTIVITY',
//    'timestamp' => NULL,
//    'activity' =>
//        array (
//            0 =>
//                array (
//                    'fromAddress' => '0x41e09d3294c376213b8720901650da938958a3b6',
//                    'toAddress' => '0xb8702eae83c3ea2891fdd7aa44ef889b4c84edea',
//                    'blockNum' => '0x1637702',
//                    'hash' => '0xd8b1bc79981f98d81cbfb133e3ea55a0998da833e415262c23a61dec9c3a04c4',
//                    'category' => 'external',
//                    'value' => 0.1000000000000000055511151231257827021181583404541015625,
//                    'asset' => 'ETH',
//                    'rawContract' =>
//                        array (
//                            'rawValue' => '0x16345785d8a0000',
//                            'address' => NULL,
//                            'decimals' => 18,
//                        ),
//                ),
//        ),
//)
