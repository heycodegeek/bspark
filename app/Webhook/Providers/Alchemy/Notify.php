<?php


namespace App\Webhook\Providers\Alchemy;


use Spatie\WebhookClient\Models\WebhookCall;

class Notify
{

    private WebhookCall $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public static function init(WebhookCall $webhookCall): self
    {
        return new static($webhookCall);
    }

    public function getApp(): string
    {
        return $this->getPayload()['app'];
    }

    public function getPayload(): array
    {
        return $this->webhookCall->payload;
    }

    public function getNetwork(): string
    {
        return $this->getPayload()['network'];
    }

    public function eachActivity(callable $callback)
    {
        foreach ($this->getAddressActivities() as $activity) {
            $callback(new AddressActivity($activity));
        }
    }

    public function getAddressActivities()
    {
        return $this->getPayload()['activity'];
    }

}
