<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MinimumTokenWithdrawRule implements Rule
{

    public string $minimumTokenQty;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($minimumTokenQty)
    {
        $this->minimumTokenQty = $minimumTokenQty;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (castDecimalString($value, 8) < $this->minimumTokenQty) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The minimum :attribute should be '.$this->minimumTokenQty;
    }
}
