<?php

namespace App\Providers;

use App\Events\Registered;
use App\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        Registered::class => [
//            SendEmailVerificationNotification::class,
//        ],
        'App\Events\Verified' => [
            'App\Listeners\CreateAirdrop'
        ],
        'App\Events\SubscriptionHistoryCreated' => [
            'App\Listeners\DistributeBinary'
        ],
        'App\Events\StakeSubscriptionHistoryCreated' => [
            'App\Listeners\StakeDistributeBinary'
        ],
        'App\Events\BinaryPayoutCreated' => [
            'App\Listeners\CreateMatchingBonus'
        ],
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\UserLoginAt',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
