<?php

namespace App\Events;

use App\Models\BinaryPayout;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BinaryPayoutCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public BinaryPayout $binaryPayout;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(BinaryPayout $binaryPayout)
    {
        $this->binaryPayout = $binaryPayout->withoutRelations();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
