<?php

use App\Models\CryptoPrice;
use App\Models\MetaTokenPrice;
use App\Models\MetaverseTokenPool;
use App\Models\SmartMetaSupply;
use App\Models\TokenPrice;
use App\Models\User;
use App\Models\UserCoinWallet;
use App\Models\UserIncomeStat;
use App\Models\UserMetaverseWallet;
use App\Models\UserSmartMeta;
use Illuminate\Support\Str;

if (!function_exists('multiplyDecimalStrings')) {

    function multipleDecimalStrings(string $firstNumberString, string $secondNumberString, $precision = 6): string
    {
        return sprintf('%.' . $precision . 'f', $firstNumberString * $secondNumberString);
    }
}

if (!function_exists('castDecimalString')) {

    function castDecimalString(string $numberString, $precision = 2): string
    {
        return sprintf('%.' . $precision . 'f', $numberString);
    }
}

if (!function_exists('addDecimalStrings')) {

    function addDecimalStrings(string $firstNumberString, string $secondNumberString, $precision = 6): string
    {
        return sprintf('%.' . $precision . 'f', $firstNumberString + $secondNumberString);
    }
}

if (!function_exists('subDecimalStrings')) {

    function subDecimalStrings(string $firstNumberString, string $secondNumberString, $precision = 6): string
    {
        return sprintf('%.' . $precision . 'f', $firstNumberString - $secondNumberString);
    }
}

if (!function_exists('divDecimalStrings')) {

    function divDecimalStrings(string $dividend, string $divisor, $precision = 16): string
    {
        return sprintf('%.' . $precision . 'f', $dividend / $divisor);
    }
}

if (!function_exists('tokenLatestPrice')) {
    function tokenLatestPrice()
    {
        return TokenPrice::orderBy('id', 'desc')->first()->price_in_usd;
    }
}

//if (!function_exists('ethLatestPrice')) {
//    function ethLatestPrice()
//    {
//        return CryptoPrice::where('name', 'eth')->first()->price_in_usd;
//    }
//}
//if (!function_exists('bnbLatestPrice')) {
//    function bnbLatestPrice()
//    {
//        return CryptoPrice::where('name', 'bnb')->first()->price_in_usd;
//    }
//}

if (!function_exists('calculateNoOfTokens')) {
    function calculateNoOfTokens(string $ethAmount, string $ethPriceInUsd, string $tokenPrice): string
    {
        $depositEth = multipleDecimalStrings($ethAmount, $ethPriceInUsd, 2);
        return divDecimalStrings($depositEth, $tokenPrice, 8);
    }
}

if (!function_exists('numberOfTokens')) {
    function numberOfTokens(string $amountInUsd, string $tokenPrice): string
    {
        return divDecimalStrings($amountInUsd, $tokenPrice, 8);
    }
}

if (!function_exists('ethereumPrice')) {
    function ethereumPrice()
    {
        return CryptoPrice::where('name', 'eth')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}
if (!function_exists('bitcoinPrice')) {
    function bitcoinPrice()
    {
        return CryptoPrice::where('name', 'btc')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}
if (!function_exists('tronPrice')) {
    function tronPrice()
    {
        return CryptoPrice::where('name', 'trx')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}
if (!function_exists('bnbLatestPrice')) {
    function bnbLatestPrice()
    {
        return CryptoPrice::where('name', 'bnb')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}
//
if (!function_exists('tokenPrice')) {
    function tokenPrice()
    {
        return TokenPrice::select('price_in_usd')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}

if (!function_exists('metaTokenPrice')) {
    function metaTokenPrice()
    {
        return MetaTokenPrice::select('price_in_usd')->orderBy('id', 'desc')->first()->price_in_usd;
    }
}

if (!function_exists('smartMetaPrice')) {
    function smartMetaPrice()
    {
        return SmartMetaSupply::select('smart_meta_price')->orderBy('id', 'desc')->first()->smart_meta_price;
    }
}

if (!function_exists('generateRefCode')) {

    /**
     * Generate RandomUserName
     *
     * @param int|null $digits
     *
     * @return string
     */
    function generateRefCode(int $digits = 10): string
    {
        while (true) {
            $refCode = Str::random($digits);
            if (User::where('ref_code')->first()) {
                continue;
            } else {
                return $refCode;
            }
        }
    }
}

if (!function_exists('feesOnTransaction')) {
    function feesOnTransaction(string $gasPriceInGwei, $transactionCost = '21000')
    {
        $feesInWei = bcmul($gasPriceInGwei, $transactionCost);
        return bcdiv($feesInWei, pow(10, 9), 9);
    }
}

if (!function_exists('bcHexToDec')) {
    function bcHexToDec(string $hex): string
    {

        if (strpos($hex, '0x') !== false) {
            $hex = explode('0x', $hex)[1];
        }
        //        $hex = 'c228914c756c0000';
        $dec = 0;
        $len = strlen($hex);
        for ($i = 1; $i <= $len; $i++) {
            $dec = bcadd($dec, bcmul(strval(hexdec($hex[$i - 1])), bcpow('16', strval($len - $i))));
        }
        return $dec;
    }
}
if (!function_exists('bcDecHex')) {
    function bcDecHex(string $dec): string
    {
        $hex = '';
        do {
            $last = bcmod($dec, 16);
            $hex = dechex($last) . $hex;
            $dec = bcdiv(bcsub($dec, $last), 16);
        } while ($dec > 0);
        return $hex;
    }
}

if (!function_exists('toWei')) {
    function toWei(string $val): string
    {
        return bcmul($val, (string)pow(10, 18));
    }
}

if (!function_exists('fromWei')) {
    function fromWei(string $val, int $precision = 8): string
    {
        return bcdiv($val, (string)pow(10, 18), $precision);
    }
}

if (!function_exists('userCoinWalletInstance')) {
    function userCoinWalletInstance(User $user): \App\Models\UserCoinWallet
    {
        return UserCoinWallet::firstOrCreate(
            ['user_id' => $user->id],
            ['balance' => castDecimalString('0', 8), 'balance_on_hold' => castDecimalString('0', 8), 'balance_locked' => castDecimalString('0', 8)]
        );
    }
}

if (!function_exists('userMetaverseWalletInstance')) {
    function userMetaverseWalletInstance(User $user): \App\Models\UserMetaverseWallet
    {
        return UserMetaverseWallet::firstOrCreate(
            ['user_id' => $user->id],
            ['token' => castDecimalString('0', 8), 'value_in_usd' => castDecimalString('0', 8), 'token_on_hold' => castDecimalString('0', 8)]
        );
    }
}

if (!function_exists('userSmartMetaInstance')) {
    function userSmartMetaInstance(User $user): \App\Models\UserSmartMeta
    {
        return UserSmartMeta::firstOrCreate(
            ['user_id' => $user->id],
            ['balance' => castDecimalString('0', 16), 'value_in_usd' => castDecimalString('0', 16), 'on_hold' => castDecimalString('0', 16), 'burned' => castDecimalString('0', 16),'converted_smart_meta' => castDecimalString('0', 16)]
        );
    }
}

if (!function_exists('smartMetaSupplyInstance')) {
    function smartMetaSupplyInstance(): \App\Models\SmartMetaSupply
    {
        return SmartMetaSupply::first();
    }
}

if (!function_exists('getMinimumTransferCoin')) {
    function getMinimumTransferCoin(): string
    {
        $miniMumUsd = castDecimalString('10', 2);
        $metaTokenPrice = metaTokenPrice();
        return divDecimalStrings($miniMumUsd, $metaTokenPrice, 8);

    }
}

if (!function_exists('getMinimumSwapCoin')) {
    function getMinimumSwapCoin(): string
    {
        $miniMumUsd = castDecimalString('10', 2);
        $smartMetaPrice = smartMetaPrice();
        return divDecimalStrings($miniMumUsd, $smartMetaPrice, 8);

    }
}

if (!function_exists('withdrawableWalletInstance')) {
    function withdrawableWalletInstance(User $user): \App\Models\WithdrawableWallet
    {
        return \App\Models\WithdrawableWallet::firstOrCreate(
            ['user_id' => $user->id],
            ['balance' => castDecimalString('0', 8), 'balance_on_hold' => castDecimalString('0', 8)]
        );
    }
}

if (!function_exists('userIncomeStatInstance')) {
    function userIncomeStatInstance(User $user): \App\Models\UserIncomeStat
    {
        return UserIncomeStat::firstOrCreate(
            ['user_id' => $user->id],
            ['daily_bonus' => castDecimalString('0', 8), 'direct_bonus' => castDecimalString('0', 8), 'binary_bonus' => castDecimalString('0', 8), 'matching_bonus' => castDecimalString('0', 8), 'delegator_bonus' => castDecimalString('0', 8), 'delegator_level_bonus' => castDecimalString('0', 8), 'total_bonus' => castDecimalString('0', 8)]
        );
    }
}

if (!function_exists('metaverseTokenPoolInstance')) {
    function metaverseTokenPoolInstance(): \App\Models\MetaverseTokenPool
    {
        return MetaverseTokenPool::firstOrCreate(
            ['id' => 1],
            ['released_token' => castDecimalString('0', 10), 'available_token' => castDecimalString('0', 10), 'burned_token' => castDecimalString('0', 10), 'value_in_usd' => castDecimalString('0', 10), 'meta_token_price' => castDecimalString('0', 10)]
        );
    }
}

if (!function_exists('getAvailableCoinBalance')) {
    function getAvailableCoinBalance(User $user): string
    {
        $userCoinWallet = userCoinWalletInstance($user);
        return subDecimalStrings($userCoinWallet->balance, $userCoinWallet->balance_locked, 8);
    }
}

if (!function_exists('hasUserStandardBinary')) {
    function hasUserStandardBinary(User $user): bool
    {
        $userTeam = $user->team;

        if ($userTeam && $userTeam->active_direct_left && $userTeam->active_direct_right) {
            return true;
        }
        return false;
    }
}

if (!function_exists('getDepositCoins')) {
    function getDepositCoins()
    {
        return [
            'trx' => [
                'symbol' => 'trx',
                'price' => fn() => tronPrice(),
            ],
            'trc20_usdt' => [
                'symbol' => 'trc20_usdt',
                'price' => fn() => castDecimalString('1', 2)
            ],
            'erc20_usdt' => [
                'symbol' => 'erc20_usdt',
                'price' => fn() => castDecimalString('1', 2)
            ],
            'bep20_usdt' => [
                'symbol' => 'bep20_usdt',
                'price' => fn() => castDecimalString('1', 2)
            ],
            'btc' => [
                'symbol' => 'btc',
                'price' => fn() => bitcoinPrice()
            ],
            'bep20_eth' => [
                'symbol' => 'bep20_eth',
                'price' => fn() => ethereumPrice()
            ],
            'bep20_bnb' => [
                'symbol' => 'bep20_bnb',
                'price' => fn() => bnbLatestPrice()
            ],
            'eth' => [
                'symbol' => 'eth',
                'price' => fn() => ethereumPrice()
            ],

        ];
    }

}
if (!function_exists('getDepositCoinPrice')) {
    function getDepositCoinPrice(string $coinSymbol)
    {
        return getDepositCoins()[$coinSymbol]['price']();
    }
}


