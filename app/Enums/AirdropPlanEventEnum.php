<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The AirdropPlanEventEnum enum.
 *
 * @method static self On_SignUp()
 */
class AirdropPlanEventEnum extends Enum
{
    const On_SignUp = 'on_signup';
}
